/*****************************************************************************************************************************
 * @file        Rte_Interfaces.h                                                                                             *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        08.12.2020 16:30:16                                                                                          *
 * @brief       Generated Rte Interaction Layer Header File: Rte_Interfaces.h                                                *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.4                                                                          *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

#ifndef __RTE_INTERFACES_H
#define __RTE_INTERFACES_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Cfg.h"
#include "Rte_Types.h"
#include "Rte_TestDummy.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* Connect Server Port : "Rte_Server_PpDummyCSInterfaceServer" */
#define Rte_Client_PpDummyCSInterfaceClient_dummyFncName          Rte_Server_PpDummyCSInterfaceServer_dummyFncName
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* Connect Sender Port : "Rte_Sender_PpDummySRInterface" */
#define Rte_Sender_PpDummySRInterface_dummySRElement1( x )        ( Rte_Sender_PpDummySRInterface_dummySRElement1_Var = x )
#define Rte_Sender_PpDummySRInterface_dummySRElement2( x )        ( Rte_Sender_PpDummySRInterface_dummySRElement2_Var = x )
#define Rte_Receiver_PpDummySRInterfaceReceiver_dummySRElement1() Rte_Sender_PpDummySRInterface_dummySRElement1_Var
#define Rte_Receiver_PpDummySRInterfaceReceiver_dummySRElement2() Rte_Sender_PpDummySRInterface_dummySRElement2_Var
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
extern VAR(uint8) Rte_Sender_PpDummySRInterface_dummySRElement1_Var;

extern VAR(uint64) Rte_Sender_PpDummySRInterface_dummySRElement2_Var;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __RTE_INTERFACES_H*/
