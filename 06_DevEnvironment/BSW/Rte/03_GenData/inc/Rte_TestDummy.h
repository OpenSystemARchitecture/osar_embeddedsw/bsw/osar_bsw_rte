/*****************************************************************************************************************************
 * @file        Rte_TestDummy.h                                                                                              *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        08.12.2020 16:30:16                                                                                          *
 * @brief       Generated Rte Module Interface Header File: Rte_TestDummy.h                                                  *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.4                                                                          *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

#ifndef __RTE_TESTDUMMY_H
#define __RTE_TESTDUMMY_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_Interfaces.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of available Server Port Error Information ++++++++++++++++++++++++++++++ */
#define Rte_ErrorType_PpDummyCSInterfaceServer_dummyFncName_NotImplemented              100
#define Rte_ErrorType_PpDummyCSInterfaceServer_dummyFncName_Generic_Programming_Failure 101
#define Rte_ErrorType_PpDummyCSInterfaceServer_dummyFncName_NullPointer                 102
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of available Server Port Error Information ++++++++++++++++++++++++++++++ */
#define Rte_ErrorType_PpDummyCSInterfaceClient_dummyFncName_NotImplemented              100
#define Rte_ErrorType_PpDummyCSInterfaceClient_dummyFncName_Generic_Programming_Failure 101
#define Rte_ErrorType_PpDummyCSInterfaceClient_dummyFncName_NullPointer                 102
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++++++++++ Generation of Init Runnable Prototypes ++++++++++++++++++++++++++++++++++++++++ */
VAR(void) TestDummy_Init( VAR(void) );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++++++++++++++ Generation of Cyclic Runnable Prototypes +++++++++++++++++++++++++++++++++++++++ */
VAR(void) TestDummy_Mainfunction( VAR(void) );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++++++++++++ Generation of Server Port Runnable Prototypes ++++++++++++++++++++++++++++++++++++ */
VAR(Rte_ErrorType) Rte_Server_PpDummyCSInterfaceServer_dummyFncName( VAR(uint8) fncArg1, P2VAR(uint8) pFncArg2 );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of Client Port Runnable Prototype Information ++++++++++++++++++++++++++++++ */
extern VAR(Rte_ErrorType) Rte_Client_PpDummyCSInterfaceClient_dummyFncName( VAR(uint8) fncArg1, P2VAR(uint8) pFncArg2 );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++ Generation of Sender Receiver Port Runnable Prototype Information ++++++++++++++++++++++++++ */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __RTE_TESTDUMMY_H*/
