/*****************************************************************************************************************************
 * @file        Rte_Types                                                                                                    *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        08.12.2020 16:30:16                                                                                          *
 * @brief       Generated Rte Data Types.                                                                                    *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.4                                                                          *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

#ifndef __RTE_TYPES_H
#define __RTE_TYPES_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Rte generated standard user data type >> Rte_ErrorType
 * @details         New data type based on standard data type: "uint16" with UUID: 82ae6b94f3a249aa853f79d1c3a83810
 * @uuid            bcf2b9942c4511e9b210d663bd873d93
 */
typedef uint16 Rte_ErrorType;

/**
 * @brief           Rte generated standard user data type >> TestStandardType_UINT8
 * @details         New data type based on standard data type: "uint8" with UUID: 1433f660d6ae4a5c85e2222f77a7846e
 * @uuid            4d7884bf481342dea88fd283a6eb71c9
 */
typedef uint8 TestStandardType_UINT8;

/**
 * @brief           Rte generated user struct data type >> TestStructType_Struct
 * @param[in,out]   Struct element "StructElementU8" uses "uint8" with UUID: 1433f660d6ae4a5c85e2222f77a7846e as
 *                  base data type
 * @param[in,out]   Struct element "StructElementU16" uses "uint16" with UUID: 82ae6b94f3a249aa853f79d1c3a83810 as
 *                  base data type
 * @uuid            48cea7b3a75544ed9d51b045329f649e
 */
typedef struct{
  uint8 StructElementU8;
  uint16 StructElementU16;
}TestStructType_Struct;

/**
 * @brief           Rte generated user enumeration data type >> TestStructType_Enum
 * @uuid            a9baaa933bf1414d9cd3c87cba4efb92
 */
typedef enum{
  EnumElement1 = 0,
  EnumElement2,
  EnumElement3
}TestStructType_Enum;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __RTE_TYPES_H*/
