/*****************************************************************************************************************************
 * @file        TestDummy.h                                                                                                  *
 * @author      OSAR Team                                                                                                    *
 * @date        03.03.2019 08:46:15                                                                                          *
 * @brief       Implementation of definitions / interface function prototypes / datatypes and generic module interface       *
 *              informations of the "TestDummy" module.                                                                      *
 *                                                                                                                           *
 * @details     TODO: Add module description.                                                                                *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.1.5                                                                           *
*****************************************************************************************************************************/

#ifndef __TESTDUMMY_H
#define __TESTDUMMY_H

/**
 * @addtogroup TestDummy 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
#include "TestDummy_Cfg.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Available application errors of the TestDummy
 * @details         This enumeration implements all available application return and error values within this specific 
 *                  module. The Application error could be used as DET information or as return type of internal functions. 
 */
typedef enum{
  TESTDUMMY_E_OK = 0,
  TESTDUMMY_E_NOT_OK = 1,
  TESTDUMMY_E_PENDING = 2,
  TESTDUMMY_E_NOT_IMPLEMENTED = 3,
  TESTDUMMY_E_GENERIC_PROGRAMMING_FAILURE = 4
}TestDummy_ReturnType;

/**
 * @brief           Available application return values of the TestDummy
 * @details         Redefinition of the TestDummy_ReturnType as error type.
 */
typedef TestDummy_ReturnType TestDummy_ErrorType;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void TestDummy_Init( void );

/**
 * @brief           Module global mainfunction.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called form the actual runtime environment within an fixed cycle.
 */
void TestDummy_Mainfunction( void );

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */

#endif /* __TESTDUMMY_H*/
