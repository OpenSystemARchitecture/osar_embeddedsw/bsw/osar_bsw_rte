/*****************************************************************************************************************************
 * @file        TestDummy.c                                                                                                  *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        08.12.2020 16:30:16                                                                                          *
 * @brief       Generated Rte Module Interface file: TestDummy.c                                                             *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.4                                                                          *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_SWC 
 * @{
 */
/**
 * @addtogroup TestDummy 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_TestDummy.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of USER include and definition area           << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/* <Add user code here > */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           End of USER include and definition area            << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map functions into code memory ---------------------------------------------*/
#define TestDummy_START_SEC_CODE
#include "TestDummy_MemMap.h"
/**
 * @brief           Rte generated module initialization function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: System Init
 * @uuid            c33753d613e6448298883d1c282dbe0e
 */
VAR(void) TestDummy_Init( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  /* Add user init code */
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module cyclic function.
 * @param[in]       None
 * @retval          None
 * @details         Accessible Client Interfaces:
 *                  VAR(Rte_ErrorType) Rte_Client_PpDummyCSInterfaceClient_dummyFncName( VAR(uint8) fncArg1, P2VAR(uint8)
 *                  pFncArg2)
 *                  Available Error Types:
 *                  > Rte_ErrorType_PpDummyCSInterfaceClient_dummyFncName_NotImplemented
 *                  > Rte_ErrorType_PpDummyCSInterfaceClient_dummyFncName_Generic_Programming_Failure
 *                  > Rte_ErrorType_PpDummyCSInterfaceClient_dummyFncName_NullPointer
 *                  Accessible Receiver Interfaces:
 *                  VAR(uint64) Rte_Receiver_PpDummySRInterfaceReceiver_dummySRElement2();
 *                  Accessible Sender Interfaces:
 *                  VAR(void) Rte_Sender_PpDummySRInterface_dummySRElement2( VAR(uint64) x );
 * @note            Trigger: Cyclic 100ms
 * @uuid            be610dd28b404215833c7b4b5a187c0a
 */
VAR(void) TestDummy_Mainfunction( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  /* User Mainfunction code */
  static uint64 testDataMainfunction = 0, testDataMainfunction2 = 0;
  testDataMainfunction++;
  uint8 dummy1 = 1, dummy2 = 2;

  Rte_Sender_PpDummySRInterface_dummySRElement2(testDataMainfunction);
  testDataMainfunction2 = Rte_Receiver_PpDummySRInterfaceReceiver_dummySRElement2();
  testDataMainfunction2++;

  Rte_Client_PpDummyCSInterfaceClient_dummyFncName(dummy1, &dummy2);

  testDataMainfunction = testDataMainfunction2;
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module Server Port Function.
 * @param[in]       VAR(uint8) uint8
 * @param[in,out]   P2VAR(uint8) uint8
 * @retval          VAR(Rte_ErrorType) >> Available Error Types:
 *                  > Rte_ErrorType_PpDummyCSInterfaceServer_dummyFncName_NotImplemented
 *                  > Rte_ErrorType_PpDummyCSInterfaceServer_dummyFncName_Generic_Programming_Failure
 *                  > Rte_ErrorType_PpDummyCSInterfaceServer_dummyFncName_NullPointer
 * @note            Trigger: Operation Invocation
 * @uuid            a4267b84dfdb40a4a196661338cefe0b
 */
VAR(Rte_ErrorType) Rte_Server_PpDummyCSInterfaceServer_dummyFncName( VAR(uint8) fncArg1, P2VAR(uint8) pFncArg2 )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  /* User Server Runnable Code */
  static uint8 testDataServerRunnable = 0;
  testDataServerRunnable++;

  return RTE_E_OK;
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

#define TestDummy_STOP_SEC_CODE
#include "TestDummy_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of USER function area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/* < Add user functions here > */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of USER function area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

