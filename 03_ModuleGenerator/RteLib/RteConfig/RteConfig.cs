﻿/******************************************************************************
  * @file    RteConfig.cs
  * @author  Reinemuth Sebastian
  * @date    05-01-2019
  * @brief   Definition of the RTE Configuration xml file structure
  *****************************************************************************/
using OsarResources.Generic;
using OsarResources.XML;
using RteLib.RteInterface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RteLib.RteConfig
{
  /**
  * @brief      RTE Config XML File Structure
  * @note       This class is used for the Types File Structure with additional functionalities
  */
  public class RteConfig
  {
    private XmlFileVersion xmlFileVersion;
    private UInt16 detModuleID;                                                    //!< Used Det module id
    private SystemState detModuleUsage;                                            //!< Det functionality enable flag
    private string pathToBaseProjectFolder;                                        //!< Relative path to the project base folder from RteConfig file location
    private string pathToRteTypeFile;                                              //!< Relative path to the XML RteType Database file with file name from RteConfig file location
    private string pathToRteInterfaceFile;                                         //!< Relative path to the XML RteType Database file with file name from RteConfig file location
    private string pathToHyperspaceProjectFile;                                    //!< Relative path to the XML Hyperspace project file with file name from RteConfig file location
    private List<RteConfigUsedModuleCfg> rteUsedModules;                           //!< List of used modules
    private List<RteClientServerPortConnection> rteClientServerConnections;        //!< List with connected client server Ports
    private List<RteSenderReceiverPortConnection> rteSenderReceiverConnections;    //!< List with connected client server Ports
    private List<RteSystemApplication> rteSystemApplications;                      //!< List with available system applications

    /**
      * @brief      Constructor
      */
    public RteConfig()
    {
      xmlFileVersion = new XmlFileVersion();
      xmlFileVersion.MajorVersion = Convert.ToUInt16(RteConfigResources.RteConfigXmlMajorVersion);
      xmlFileVersion.MinorVersion = Convert.ToUInt16(RteConfigResources.RteConfigXmlMinorVersion);
      xmlFileVersion.PatchVersion = Convert.ToUInt16(RteConfigResources.RteConfigXmlPatchVersion);

      detModuleID = 0;
      detModuleUsage = SystemState.STD_ON;

      pathToBaseProjectFolder = "";
      pathToRteTypeFile = "";
      pathToRteInterfaceFile = "";
      pathToHyperspaceProjectFile = "";

      rteUsedModules = new List<RteConfigUsedModuleCfg>();
      rteClientServerConnections = new List<RteClientServerPortConnection>();
      rteSenderReceiverConnections = new List<RteSenderReceiverPortConnection>();
      rteSystemApplications = new List<RteSystemApplication>();
    }

    /**
      * @brief      Destructor
      */
    ~RteConfig()
    {

    }

    #region Attributes
    public XmlFileVersion XmlFileVersion
    {
      get { return xmlFileVersion; }
      set { xmlFileVersion = value; }
    }

    public UInt16 DetModuleID
    {
      get { return detModuleID; }
      set { detModuleID = value; }
    }

    public SystemState DetModuleUsage
    {
      get { return detModuleUsage; }
      set { detModuleUsage = value; }
    }

    public string PathToBaseProjectFolder
    {
      get { return pathToBaseProjectFolder; }
      set { pathToBaseProjectFolder = value; }
    }

    public string PathToRteTypeFile
    {
      get { return pathToRteTypeFile; }
      set { pathToRteTypeFile = value; }
    }

    public string PathToRteInterfaceFile
    {
      get { return pathToRteInterfaceFile; }
      set { pathToRteInterfaceFile = value; }
    }

    public string PathToHyperspaceProjectFile
    {
      get { return pathToHyperspaceProjectFile; }
      set { pathToHyperspaceProjectFile = value; }
    }

    public List<RteConfigUsedModuleCfg> RteUsedModules
    {
      get { return rteUsedModules; }
      set { rteUsedModules = value; }
    }

    public List<RteClientServerPortConnection> RteClientServerConnections
    {
      get { return rteClientServerConnections; }
      set { rteClientServerConnections = value; }
    }

    public List<RteSenderReceiverPortConnection> RteSenderReceiverConnections
    {
      get { return rteSenderReceiverConnections; }
      set { rteSenderReceiverConnections = value; }
    }

    public List<RteSystemApplication> RteSystemApplications
    {
      get { return rteSystemApplications; }
      set { rteSystemApplications = value; }
    }
    #endregion

    #region Request Functions
    /// <summary>
    /// Interface to check if the server port is available in the connection list
    /// </summary>
    /// <param name="serverPort"></param>
    /// <param name="csConnection"> Out reference to list element</param>
    /// <returns></returns>
    public bool ContainServerPortInConnectionList(RteGeneralInterfaceInfo serverPort, out RteClientServerPortConnection csConnection)
    {
      bool retVal = false;
      csConnection = new RteClientServerPortConnection();

      foreach (RteClientServerPortConnection connection in rteClientServerConnections)
      {
        if(connection.RteServerPort == serverPort)
        {
          retVal = true;
          csConnection = connection;
        }
      }

      return retVal;
    }

    /// <summary>
    /// Interface to check if the client port is available in the provided connection
    /// </summary>
    /// <param name="clientPort"></param>
    /// <param name="csConnection"></param>
    /// <returns></returns>
    public bool ContainClientPortInCSConnection(RteGeneralInterfaceInfo clientPort, RteClientServerPortConnection csConnection)
    {
      bool retVal = false;

      foreach (RteGeneralInterfaceInfo port in csConnection.RteClientPorts)
      {
        if (port == clientPort)
        {
          retVal = true;
        }
      }

      return retVal;
    }

    /// <summary>
    /// Interface to delete a client port from an client server connection
    /// </summary>
    /// <param name="serverPort"></param>
    /// <param name="clientPort"></param>
    /// <returns></returns>
    public bool DeleteClientPortFromCSConnection(RteGeneralInterfaceInfo serverPort, RteGeneralInterfaceInfo clientPort)
    {
      bool retVal = false;
      RteClientServerPortConnection connection;

      if (true == ContainServerPortInConnectionList(serverPort, out connection))
      {
        if(true == ContainClientPortInCSConnection(clientPort, connection))
        {
          connection.RteClientPorts.Remove(clientPort);
          retVal = true;
        }
      }

      return retVal;
    }

    /// <summary>
    /// Interface to check if the sender port is available in the connection list
    /// </summary>
    /// <param name="port"></param>
    /// <param name="srConnection"></param>
    /// <returns></returns>

    public bool ContainSenderPortInConnectionList(RteGeneralInterfaceInfo port, out RteSenderReceiverPortConnection srConnection)
    {
      bool retVal = false;
      srConnection = new RteSenderReceiverPortConnection();

      foreach (RteSenderReceiverPortConnection connection in RteSenderReceiverConnections)
      {
        if (connection.RteSenderPort == port)
        {
          retVal = true;
          srConnection = connection;
        }
      }

      return retVal;
    }

    /// <summary>
    /// Interface to check if the sender port is available in the provided connection
    /// </summary>
    /// <param name="port"></param>
    /// <param name="srConnection"></param>
    /// <returns></returns>
    public bool ContainReceiverPortInSRConnection(RteGeneralInterfaceInfo port, RteSenderReceiverPortConnection srConnection)
    {
      bool retVal = false;

      foreach (RteGeneralInterfaceInfo conPort in srConnection.RteReceiverPorts)
      {
        if (conPort == port)
        {
          retVal = true;
        }
      }

      return retVal;
    }

    /// <summary>
    /// Interface to delete a client port from an client server connection
    /// </summary>
    /// <param name="senderPort"></param>
    /// <param name="receiverPort"></param>
    /// <returns></returns>
    public bool DeleteReceiverPortFromSRConnection(RteGeneralInterfaceInfo senderPort, RteGeneralInterfaceInfo receiverPort)
    {
      bool retVal = false;
      RteSenderReceiverPortConnection connection;

      if (true == ContainSenderPortInConnectionList(senderPort, out connection))
      {
        if (true == ContainReceiverPortInSRConnection(receiverPort, connection))
        {
          connection.RteReceiverPorts.Remove(receiverPort);
          retVal = true;
        }
      }

      return retVal;
    }
    #endregion

    #region Actions
    /*
     * @brief       Function to store the current rte config data base to an xml file
     * @param       string Path to RteConfig Database file
     * @retval      none
     */
    public void SaveActiveRteConfigDataBaseToXml(string pathToXmlFile)
    {
      XmlSerializer writer = new XmlSerializer(this.GetType());
      StreamWriter file = new StreamWriter(pathToXmlFile);
      writer.Serialize(file, this);
      file.Close();
    }

    /*
     * @brief       Function to read the current configuration structure from an xml file
     * @param       string Path to RteConfig Database file
     * @retval      none
     */
    public void ReadActiveRteConfigDataBaseFromXml(string pathToXmlFile)
    {
      RteConfig tempRteConfig;
      XmlSerializer reader = new XmlSerializer(this.GetType());
      StreamReader file = new StreamReader(pathToXmlFile);
      tempRteConfig = (RteConfig)reader.Deserialize(file);

      this.xmlFileVersion = tempRteConfig.xmlFileVersion;
      this.detModuleID = tempRteConfig.detModuleID;
      this.detModuleUsage = tempRteConfig.detModuleUsage;
      this.pathToBaseProjectFolder = tempRteConfig.pathToBaseProjectFolder;
      this.pathToRteInterfaceFile = tempRteConfig.pathToRteInterfaceFile;
      this.pathToRteTypeFile = tempRteConfig.pathToRteTypeFile;
      this.pathToHyperspaceProjectFile = tempRteConfig.pathToHyperspaceProjectFile;
      this.rteUsedModules = tempRteConfig.rteUsedModules;
      this.rteClientServerConnections = tempRteConfig.rteClientServerConnections;
      this.rteSenderReceiverConnections = tempRteConfig.rteSenderReceiverConnections;
      this.rteSystemApplications = tempRteConfig.rteSystemApplications;
      
      file.Close();
    }
    #endregion
  }
}
