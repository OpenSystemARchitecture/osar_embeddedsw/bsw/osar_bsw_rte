﻿/*****************************************************************************************************************************
 * @file        RteConfigTypes.cs                                                                                            *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        05.01.2019                                                                                                   *
 * @brief       Definition of the RTE Configuration data types                                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RteLib.RteConfig
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using RteLib.RteInterface;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RteLib.RteConfig
{
  /**
  * @brief      RTE Enumeration of the different OSAR Module types
  * @note       SWC     >> Standard Software Component
  *             CD      >> Complex Device
  *             CDD     >> Complex Device Driver
  *             BSW     >> Basis Software
  *             MCAL    >> Micro Controller Abstraction Layer
  */
  public enum RteConfigModuleTypes
  {
    SWC,
    CD,
    CDD,
    BSW,
    MCAL
  }

  /**
  * @brief      RTE Module Config Structure
  * @note       This struct is used to identify the different used modules
  */
  public class RteConfigUsedModuleCfg : IEquatable<RteConfigUsedModuleCfg>
  {
    private string moduleName = "";
    private RteConfigModuleTypes moduleType = new RteConfigModuleTypes();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteConfigUsedModuleCfg() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteConfigUsedModuleCfg(RteConfigUsedModuleCfg obj)
    {
      this.ModuleName = obj.ModuleName;
      this.ModuleType = obj.ModuleType;
    }

    #region Attributes
    
    public string ModuleName
    {
      get { return moduleName; }
      set { moduleName = value; }
    }

    
    public RteConfigModuleTypes ModuleType
    {
      get { return moduleType; }
      set { moduleType = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteConfigUsedModuleCfg lhs, RteConfigUsedModuleCfg rhs)
    {
      bool retVal = false;

      if (( lhs.ModuleName == rhs.ModuleName ) && ( lhs.ModuleType == rhs.ModuleType ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteConfigUsedModuleCfg lhs, RteConfigUsedModuleCfg rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteConfigUsedModuleCfg rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE Module Config Structure for an Client Server Port connection
  */
  public class RteClientServerPortConnection : IEquatable<RteClientServerPortConnection>
  {
    private RteGeneralInterfaceInfo rteServerPort = new RteGeneralInterfaceInfo();
    private List<RteGeneralInterfaceInfo> rteClientPorts = new List<RteGeneralInterfaceInfo>();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteClientServerPortConnection() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteClientServerPortConnection(RteClientServerPortConnection obj)
    {
      this.RteServerPort = new RteGeneralInterfaceInfo(obj.RteServerPort);
      this.RteClientPorts = new List<RteGeneralInterfaceInfo>(obj.RteClientPorts);
    }

    #region Attributes
    
    public RteGeneralInterfaceInfo RteServerPort
    {
      get { return rteServerPort; }
      set { rteServerPort = value; }
    }

    
    public List<RteGeneralInterfaceInfo> RteClientPorts
    {
      get { return rteClientPorts; }
      set { rteClientPorts = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteClientServerPortConnection lhs, RteClientServerPortConnection rhs)
    {
      bool retVal = false;

      if (( lhs.rteServerPort == rhs.rteServerPort ) && 
          ( true == lhs.rteClientPorts.SequenceEqual(rhs.rteClientPorts) ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteClientServerPortConnection lhs, RteClientServerPortConnection rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteClientServerPortConnection rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE Module Config Structure for an Sender Receiver Port connection
  */
  public class RteSenderReceiverPortConnection : IEquatable<RteSenderReceiverPortConnection>
  {
    private RteGeneralInterfaceInfo rteSenderPort = new RteGeneralInterfaceInfo();
    private List<RteGeneralInterfaceInfo> rteReceiverPorts = new List<RteGeneralInterfaceInfo>();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteSenderReceiverPortConnection() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteSenderReceiverPortConnection(RteSenderReceiverPortConnection obj)
    {
      this.RteSenderPort = new RteGeneralInterfaceInfo( obj.rteSenderPort );
      this.RteReceiverPorts = new List<RteGeneralInterfaceInfo>(obj.RteReceiverPorts);
    }

    #region Attributes
    
    public RteGeneralInterfaceInfo RteSenderPort
    {
      get { return rteSenderPort; }
      set { rteSenderPort = value; }
    }

    
    public List<RteGeneralInterfaceInfo> RteReceiverPorts
    {
      get { return rteReceiverPorts; }
      set { rteReceiverPorts = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteSenderReceiverPortConnection lhs, RteSenderReceiverPortConnection rhs)
    {
      bool retVal = false;

      if (( lhs.rteSenderPort == rhs.rteSenderPort ) && 
          ( true == lhs.rteReceiverPorts.SequenceEqual(rhs.rteReceiverPorts) ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteSenderReceiverPortConnection lhs, RteSenderReceiverPortConnection rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteSenderReceiverPortConnection rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief   RTE Module Config Structure for an Rte System Application
  * @note    Used for Os scheduling
  */
  public class RteSystemApplication : IEquatable<RteSystemApplication>
  {
    private string rteSystemApplicationName = "";
    private string uuid = "";
    private List<RteConfigUsedModuleCfg> rteModule = new List<RteConfigUsedModuleCfg>();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteSystemApplication() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteSystemApplication(RteSystemApplication obj)
    {
      this.RteSystemApplicationName = obj.RteSystemApplicationName;
      this.RteModule = new List<RteConfigUsedModuleCfg>(obj.RteModule);
      this.UUID = obj.UUID;
    }

    #region Attributes
    
    public string RteSystemApplicationName
    {
      get { return rteSystemApplicationName; }
      set { rteSystemApplicationName = value; }
    }

    
    public string UUID
    {
      get { return uuid; }
      set { uuid = value; }
    }

    
    public List<RteConfigUsedModuleCfg> RteModule
    {
      get { return rteModule; }
      set { rteModule = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteSystemApplication lhs, RteSystemApplication rhs)
    {
      bool retVal = false;

      if (( lhs.rteSystemApplicationName == rhs.rteSystemApplicationName )  &&
          ( lhs.UUID                 == rhs.UUID )                      &&
          ( true == lhs.rteModule.SequenceEqual(rhs.rteModule) ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteSystemApplication lhs, RteSystemApplication rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteSystemApplication rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }
}
/**
* @}
*/
