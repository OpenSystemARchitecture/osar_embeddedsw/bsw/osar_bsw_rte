﻿/*****************************************************************************************************************************
 * @file        RteConfigImportHandler.cs                                                                                    *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        12.12.2020                                                                                                   *
 * @brief       Implementation of the import handler for the rte configuration. In this class all import operation into the  *
 *              Rte Configuration are available.                                                                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RteLib.RteConfig
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using RteLib.RteResources;
using RteLib.RteModuleInternalBehavior;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RteLib.RteConfig
{
  public class RteConfigImportHandler
  {
    private RteConfig rteConfig;                                            //!< Reference to the Rte Configuration
    private RteModuleInternalBehavior.RteModuleInternalBehavior mibConfig;  //!< actualUsedMibConfig
    private string absPathToRteConfiguartionFile;                           //!< Absolute Path to Rte Configuration File

    /**
     * @brief     Constructor
     * @param[in] Reference to the rte configuration object
     * @param[in] Path the Rte config file
     */
    public RteConfigImportHandler(ref RteConfig config, string absPathToRteConfigFile)
    {
      rteConfig = config;
      absPathToRteConfiguartionFile = absPathToRteConfigFile;
    }

    #region Import functions
    /**
     * @brief     Import function to add the corresponding module internal behavior configuration into
     *            the rte configuration.
     * @param[in] moduleName: Name of the module >> Also the folder of the module must be named so!
     * @param[in] moduleType: Type of the module >> Also the base folder of the module must be named so!
     * @return    None
     * @exception - FileNotFoundException
     *            - OsarConfigMismatchException
     *            - OsarImportException
     * @details   Pre-Actions:
     *            - Check of module internal behavior file exists
     *            - Check of module internal behavior config fits with provided information
     *            Following informations are imported:
     *            - Module Name and Type
     *            - Available Server-Ports
     *            - Available Sender-Ports
     */
    public void ImportMiBFileIntoRteConfig(string moduleName, RteConfigModuleTypes moduleType)
    {
      try
      {
        LoadMiBFile(moduleName, moduleType);

        // Add module to Rte Configuration
        if (false == CheckIfModuleAlreadyConfigured(moduleName, moduleType))
        {
          RteConfigUsedModuleCfg newRteUsedModule = new RteConfigUsedModuleCfg();
          newRteUsedModule.ModuleName = moduleName;
          newRteUsedModule.ModuleType = moduleType;
          rteConfig.RteUsedModules.Add(newRteUsedModule);
        }

        // Import all server ports from MiB into rte server connection
        foreach (RteAvailableServerPort serverPort in mibConfig.AvailableServerPorts)
        {
          if(false == CheckIfServerPortIsAlreadyConfigured(serverPort))
          {
            // Server port is not available in rte config >> Import it into rte config
            RteClientServerPortConnection newClientServerConnection = new RteClientServerPortConnection();
            newClientServerConnection.RteServerPort = serverPort.ServerPort.InterfacePrototype;
            newClientServerConnection.RteClientPorts = new List<RteInterface.RteGeneralInterfaceInfo>();
            rteConfig.RteClientServerConnections.Add(newClientServerConnection);
          }
        }


        // Import all server ports from MiB into rte server connection
        foreach (RteAvailableSRPort senderReceiverPort in mibConfig.AvailableSRPorts)
        {
          if(senderReceiverPort.SenderReceiverPort.InterfaceType == RteInterface.RteSRInterfaceImplementationType.SENDER)
          {
            if (false == CheckIfSenderPortIsAlreadyConfigured(senderReceiverPort))
            {
              // Server port is not available in rte config >> Import it into rte config
              RteSenderReceiverPortConnection newSenderReceiverConnection = new RteSenderReceiverPortConnection();
              newSenderReceiverConnection.RteSenderPort = senderReceiverPort.SenderReceiverPort.InterfacePrototype;
              newSenderReceiverConnection.RteReceiverPorts = new List<RteInterface.RteGeneralInterfaceInfo>();
              rteConfig.RteSenderReceiverConnections.Add(newSenderReceiverConnection);
            }
          }
        }

      }
      catch (Exception)
      {
        throw; // Forward the exception
      }
    }

    /**
     * @brief     Additional API to Import MiB information to Rte Config
     * @param[in] Module to be imported
     * @return    None
     * @details   Forward API
     */
    public void ImportMiBFileIntoRteConfig(RteConfigUsedModuleCfg module)
    {
      ImportMiBFileIntoRteConfig(module.ModuleName, module.ModuleType);
    }
    #endregion

    #region Helper functions
    /**
     * @brief     Helper function which loads the module internal behavior file
     * @param[in] moduleName: Name of the module >> Also the folder of the module must be named so!
     * @param[in] moduleType: Type of the module >> Also the base folder of the module must be named so!
     * @return    None
     * @exception - FileNotFoundException
     *            - OsarConfigMismatchException
     */
    private void LoadMiBFile(string moduleName, RteConfigModuleTypes moduleType)
    {
      string pathToMibFile = Path.GetDirectoryName(absPathToRteConfiguartionFile) + rteConfig.PathToBaseProjectFolder +
        "./" + moduleType + "./" + moduleName + RteProjectStructure.PathToMibFileFromModuleBaseFolder;

      // Check if file exists
      if(false == File.Exists(pathToMibFile))
      {
        throw new FileNotFoundException("Requested MiB file to imported not found! FilePath: " + pathToMibFile +
          " Check project structure recommended.");
      }

      // Create MiB Object
      mibConfig = new RteModuleInternalBehavior.RteModuleInternalBehavior();
      mibConfig.ReadActiveRteModuleInternalBehaviorToXml(pathToMibFile);

      // Check if config match's with MiB File
      if((moduleName != mibConfig.ModuleName) || (moduleType != mibConfig.ModuleType))
      {
        throw new OsarResources.Generic.OsarConfigMismatchException("Requested module to be imported: " +
          moduleType.ToString() + " " + moduleName + " >> Does not fit with MiB configuration: " +
          mibConfig.ModuleType.ToString() + " " + mibConfig.ModuleName);
      }
    }

    /**
     * @brief     Helper function to check if the MiB module is already available in rte config
     * @param[in] moduleName: Name of the module
     * @param[in] moduleType: Type of the module
     * @details   ATTENTION: Only Server Port part is considered!
     *            Check of port is available and identical!
     * @exception - OsarMergeException
     */
    private bool CheckIfModuleAlreadyConfigured(string moduleName, RteConfigModuleTypes moduleType)
    {
      bool retVal = false;
      foreach(RteConfigUsedModuleCfg rteModule in rteConfig.RteUsedModules)
      {
        if( (rteModule.ModuleName == moduleName) && (rteModule.ModuleType == moduleType ) )
        {
          retVal = true;
          break;
        }
      }

      return retVal;
    }


    /**
     * @brief     Helper function to check if the MiB server port is already available in rte config
     * @param[in] MiB Server Port which shall be imported
     * @details   ATTENTION: Only Server Port part is considered!
     *            Check of port is available and identical!
     * @exception - OsarMergeException
     */
    private bool CheckIfServerPortIsAlreadyConfigured(RteAvailableServerPort mibServerPort)
    {
      bool retVal = false;

      foreach (RteClientServerPortConnection clientServerPortConnection in rteConfig.RteClientServerConnections)
      {
        /* Check if UUID exists */
        if(clientServerPortConnection.RteServerPort.UUID == mibServerPort.ServerPort.InterfacePrototype.UUID)
        {
          // Check if object is identical
          if(clientServerPortConnection.RteServerPort == mibServerPort.ServerPort.InterfacePrototype)
          {
            // Element found and identical!
            retVal = true;
            break;
          }
          else
          {
            // UUID found but element is not identical
            throw new OsarResources.Generic.OsarMergeException("Merge of available Server-Port not possible." +
              "Rte Config Port: " + clientServerPortConnection.RteServerPort.InterfaceName + " and MiB Port: " +
              mibServerPort.ServerPort.InterfacePrototype.InterfaceName + " have the same UUID but are NOT equal!");
          }
        }
      }

      return retVal;
    }

    /**
     * @brief     Helper function to check if the MiB sender port is already available in rte config
     * @param[in] MiB Sender Port which shall be imported
     * @details   ATTENTION: Only Sender Port parts is considered!
     *            Check of port is available and identical!
     * @exception - OsarMergeException
     */
    private bool CheckIfSenderPortIsAlreadyConfigured(RteAvailableSRPort mibSenderPort)
    {
      bool retVal = false;


      foreach (RteSenderReceiverPortConnection senderReceiverPortConnection in rteConfig.RteSenderReceiverConnections)
      {
        /* Check if UUID exists */
        if (senderReceiverPortConnection.RteSenderPort.UUID == mibSenderPort.SenderReceiverPort.InterfacePrototype.UUID)
        {
          // Check if object is identical
          if (senderReceiverPortConnection.RteSenderPort == mibSenderPort.SenderReceiverPort.InterfacePrototype)
          {
            // Element found and identical!
            retVal = true;
            break;
          }
          else
          {
            // UUID found but element is not identical
            throw new OsarResources.Generic.OsarMergeException("Import of available Server-Port not possible." +
              "Rte Config Port: " + senderReceiverPortConnection.RteSenderPort.InterfaceName + " and MiB Port: " +
              mibSenderPort.SenderReceiverPort.InterfacePrototype.InterfaceName + " have the same UUID but are NOT equal!");
          }
        }
      }

      return retVal;
    }

    #endregion
  }
}
/**
* @}
*/
