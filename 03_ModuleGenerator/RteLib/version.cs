/******************************************************************************
 * @file      version.cs
 * @author    S.Reinemuth
 * @proj      Library
 * @date      Sunday, January 17, 2021
 * @version   Application v. 1.0.1.9
 * @version   Generator   v. 1.2.3.9
 * @brief     Controls the assembly Version
 *****************************************************************************/
using System;
using System.Reflection;

[assembly: AssemblyVersion("1.0.1.9")]

namespace RteLib
{
  static public class VersionClass
	{
		public static int major { get; set; }	 //Version of the program
		public static int minor { get; set; }	 //Sub version of the program
		public static int patch { get; set; }	 //Debug patch of the program
		public static int build { get; set; }	 //Count program builds

    static VersionClass()
    {
			major = 1;
			minor = 0;
			patch = 1;
			build = 9;
    }

		public static string getCurrentVersion()
    {
      return major.ToString() + '.' + minor.ToString() + '.' + patch.ToString() + '.' + build.ToString();
    }
	}
}
//!< @version 0.1.0	->	Release of first RteTypes implementation.
//!< @version 0.1.1	->	Adding basic RTE Config file structure
//!< @version 0.1.2	->	Adding RteLib database validation and advanced data type sort functionality.
//!< @version 0.1.3	->	Adding new RteInterface APIs. Adding initial RteModuleInternalBehavior functionality.
//!< @version 0.1.4	->	Adapting interface structure. Adding new RteDataTypeHelper class.
//!< @version 0.1.5	->	Adding New Config functionality >> Client Server Connections and Sender Receiver Connections.
//!< @version 0.1.6	->	Add new UUID Resource File for Rte defined UUIDs
//!< @version 0.1.7	->	Adding base configuration for a System Application
//!< @version 0.1.8	->	Add backup interfaces into the module internal behavior data base file.
//!< @version 0.1.9	->	Adding backup Advanced Data Type List
//!< @version 0.1.10	->	Adding attribute getter interfaces for rte standard types as initialized objects.
//!< @version 0.1.11	->	Update used source file lib
//!< @version 0.1.12	->	Add bug-fixes for reading xml files. Close files after read operation has been done.
//!< @version 0.2.0	->	Adding to data structure equal and non equal operator overload
//!< @version 0.2.1	->	Adding RteConfig / RteInterface / RteTypes importer functionality.
//!< @version 0.2.2	->	Adding RteConfigTypes usedConfigModule access properties.
//!< @version 0.2.3	->	Added Attribute Getter and Setter for Rte Data Types (Name only)
//!< @version 0.2.4	->	Bugfix for import handler. Use now absolute path to rte config file as reference.
//!< @version 0.3.0	->	Adding Attribute interfaces for all data structures.
//!< @version 0.3.1	->	Adding additional type name add property, directly to rteAdvancedDataTypes struct.
//!< @version 0.3.2	->	Change base data type structure from struct to class
//!< @version 0.3.3	->	Adding default and copy constructors to data structure classes.
//!< @version 0.3.4	->	Fix naming issue in RteDataAccessType
//!< @version 0.3.5	->	Fix naming issue in RteAdvancedDataTypeType
//!< @version 0.3.6	->	Adding validation for SR and CS interface blueprints
//!< @version 0.3.7	->	Adding validation functionality to module internal behavior structure
//!< @version 1.0.0	->	Refactoring database structure and update to xml file version 2.x
//!< @version 1.0.1	->	Adding request APIs to rte-config structure
