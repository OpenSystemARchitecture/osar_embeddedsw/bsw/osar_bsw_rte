﻿/*****************************************************************************************************************************
 * @file        RteInterfaces.cs                                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        05.01.2019                                                                                                   *
 * @brief       Definition of the RTE Interfaces                                                                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RteLib.RteInterface
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using OsarResources.XML;
using RteLib.RteTypes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RteLib.RteInterface
{
  /**
  * @brief      RTE Interface XML File Structure
  * @note       This class is used for the Interface File Structure with additional functionalities
  */
  public class RteInterfaces
  {
    private XmlFileVersion xmlFileVersion;
    private List<RteCSInterfaceBlueprint> clientServerBlueprintList;
    private List<RteSRInterfaceBlueprint> senderReceiverBlueprintList;

    /**
      * @brief      Constructor
      */
    public RteInterfaces()
    {
      xmlFileVersion = new XmlFileVersion();
      xmlFileVersion.MajorVersion = Convert.ToUInt16(RteInterfaceResources.RteInterfaceXmlMajorVersion);
      xmlFileVersion.MinorVersion = Convert.ToUInt16(RteInterfaceResources.RteInterfaceXmlMinorVersion);
      xmlFileVersion.PatchVersion = Convert.ToUInt16(RteInterfaceResources.RteInterfaceXmlPatchVersion);
      clientServerBlueprintList = new List<RteCSInterfaceBlueprint>();
      senderReceiverBlueprintList = new List<RteSRInterfaceBlueprint>();
    }

    /**
      * @brief      Destructor
      */
    ~RteInterfaces()
    {
    }

    #region Attributes
    /// <summary>
    /// Getter and Setter for the xmlFileVersion
    /// </summary>
    

    public XmlFileVersion XmlFileVersion
    {
      get { return xmlFileVersion; }
      set { xmlFileVersion = value; }
    }

    /// <summary>
    /// Getter and Setter for the clientServerBlueprintList
    /// </summary>
    
    public List<RteCSInterfaceBlueprint> ClientServerBlueprintList
    {
      get { return clientServerBlueprintList; }
      set { clientServerBlueprintList = value; }
    }

    /// <summary>
    /// Getter and Setter for the clientServerBlueprintList
    /// </summary>
    
    public List<RteSRInterfaceBlueprint> SenderReceiverBlueprintList
    {
      get { return senderReceiverBlueprintList; }
      set { senderReceiverBlueprintList = value; }
    }
    #endregion

    /**
      * @brief      Interface to add a client server Rte Interface
      * @param      New user Client Server Interface
      * @retval     None
      * @note       Dose not check interface data types.
      */
    public void AddRteCSInterface(RteCSInterfaceBlueprint usrRteCSInterface)
    {
      Regex regex = new Regex("^[a-zA-Z0-9_]*$", RegexOptions.Compiled);

      /* Check if new interface already exist */
      if (true == IsCSInterfaceAvailable(usrRteCSInterface.InterfaceInfo))
      {
        /* Data type already exist >> Could not be added to database */
        throw new ArgumentException("Client Server Interface is already available.");
      }

      /* Check if new interface name has been set */
      if ("" == usrRteCSInterface.InterfaceInfo.InterfaceName)
      {
        throw new ArgumentException("Client Server Interface name is not set.");
      }

      /* Check if new interface name is valid */
      if (false == regex.IsMatch(usrRteCSInterface.InterfaceInfo.InterfaceName))
      {
        throw new ArgumentException("Client Server Interface name contains invalid chars >> Interface Name: " + usrRteCSInterface.InterfaceInfo.InterfaceName + " Interface UUID: " + usrRteCSInterface.InterfaceInfo.UUID);
      }

      /* Check if new interface name already exist */
      if (true == IsCSInterfaceNameAvailable(usrRteCSInterface.InterfaceInfo))
      {
        /* Data type already exist >> Could not be added to database */
        throw new ArgumentException("Client Server Interface name is already available.");
      }

      /* Evaluate / Validate user interface */
      foreach(RteFunctionBlueprint function in usrRteCSInterface.Functions)
      {
        /* Check return value has been set */
        if ( (null == function.ReturnValue.UUID ) || ( null == function.ReturnValue.DataTypeName ) ||
            ( ""   == function.ReturnValue.UUID ) || ( "" == function.ReturnValue.DataTypeName ))
        {
          /* Data type already exist >> Could not be added to database */
          throw new ArgumentException("Client Server Interface return type has not been set or is invalid >> Interface Name: " 
            + usrRteCSInterface.InterfaceInfo.InterfaceName 
            + " Interface UUID: " + usrRteCSInterface.InterfaceInfo.UUID 
            + " Function name " + function.FunctionName);
        }

        //TODO Validate if data type exist

        /* Check if new function name has been set */
        if ("" == function.FunctionName)
        {
          throw new ArgumentException("Client Server Interface function name is not set. >> Interface Name: "
            + usrRteCSInterface.InterfaceInfo.InterfaceName
            + " Interface UUID: " + usrRteCSInterface.InterfaceInfo.UUID);
        }

        /* Check if function name is valid */
        if (false == regex.IsMatch(function.FunctionName))
        {
          throw new ArgumentException("Client Server Interface function name contains invalid chars >> Interface Name: " 
            + usrRteCSInterface.InterfaceInfo.InterfaceName 
            + " Interface UUID: " + usrRteCSInterface.InterfaceInfo.UUID
            + " Function name " + function.FunctionName);
        }

        foreach(RteFunctionArgumentBlueprint argument in function.ArgList)
        {
          /* Check argument data type has been set */
          if (( null == argument.ArgumentType.UUID ) || ( null == argument.ArgumentType.DataTypeName ) ||
              ( "" == argument.ArgumentType.UUID ) || ( "" == argument.ArgumentType.DataTypeName ))
          {
            /* Data type already exist >> Could not be added to database */
            throw new ArgumentException("Client Server Interface return type has not been set or is invalid >> Interface Name: "
              + usrRteCSInterface.InterfaceInfo.InterfaceName
              + " Interface UUID: " + usrRteCSInterface.InterfaceInfo.UUID
              + " Function name " + function.FunctionName);
          }

          //TODO Validate if data type exist

          /* Check if new argument name has been set */
          if ("" == argument.ArgumentName)
          {
            throw new ArgumentException("Client Server Interface function argument name is not set. >> Interface Name: "
              + usrRteCSInterface.InterfaceInfo.InterfaceName
              + " Interface UUID: " + usrRteCSInterface.InterfaceInfo.UUID
              + " Function name " + function.FunctionName);
          }

          /* Check if argument name is valid */
          if (false == regex.IsMatch(argument.ArgumentName))
          {
            throw new ArgumentException("Client Server Interface function argument name contains invalid chars  >> Interface Name: "
              + usrRteCSInterface.InterfaceInfo.InterfaceName
              + " Interface UUID: " + usrRteCSInterface.InterfaceInfo.UUID
              + " Function name " + function.FunctionName 
              + " Argument name " + argument.ArgumentName);
          }
        }
      }

      /* Interface is valid >> Add to data base */
      clientServerBlueprintList.Add(usrRteCSInterface);
    }

    /**
      * @brief      Interface to add a sender receiver Rte Interface
      * @param      New user advanced Rte Type
      * @retval     None
      * @note       Dose not check interface data types.
      */
    public void AddRteSRInterface(RteSRInterfaceBlueprint usrRteSRInterface)
    {
      Regex regex = new Regex("^[a-zA-Z0-9_]*$", RegexOptions.Compiled);

      /* Check if new interface already exist */
      if (true == IsSRInterfaceAvailable(usrRteSRInterface.InterfaceInfo))
      {
        /* Data type already exist >> Could not be added to database */
        throw new ArgumentException("Sender Receiver Interface is already available.");
      }

      /* Check if new interface name has been set */
      if ("" == usrRteSRInterface.InterfaceInfo.InterfaceName)
      {
        throw new ArgumentException("Sender Receiver Interface name is not set.");
      }

      /* Check if new interface name is valid */
      if (false == regex.IsMatch(usrRteSRInterface.InterfaceInfo.InterfaceName))
      {
        throw new ArgumentException("Sender Receiver Interface name contains invalid chars >> Interface Name: " + usrRteSRInterface.InterfaceInfo.InterfaceName);
      }

      /* Check if new interface name already exist */
      if (true == IsSRInterfaceNameAvailable(usrRteSRInterface.InterfaceInfo))
      {
        /* Data type already exist >> Could not be added to database */
        throw new ArgumentException("Sender Receiver Interface name is already available.");
      }

      /* Evaluate / Validate user interface */
      foreach (RteElementBlueprint element in usrRteSRInterface.Elements)
      {
        /* Check argument data type has been set */
        if (( null == element.ElementType.UUID ) || ( null == element.ElementType.DataTypeName ) ||
            ( "" == element.ElementType.UUID ) || ( "" == element.ElementType.DataTypeName ))
        {
          /* Data type already exist >> Could not be added to database */
          throw new ArgumentException("Sender Receiver Interface element data type has not been set or is invalid >> Interface Name: "
            + usrRteSRInterface.InterfaceInfo.InterfaceName
            + " Interface UUID: " + usrRteSRInterface.InterfaceInfo.UUID
            + " Element name " + element.ElementName);
        }

        //TODO Validate if data type exist

        /* Check if new argument name has been set */
        if ("" == element.ElementName)
        {
          throw new ArgumentException("Client Server Interface function argument name is not set. >> Interface Name: "
            + usrRteSRInterface.InterfaceInfo.InterfaceName
            + " Interface UUID: " + usrRteSRInterface.InterfaceInfo.UUID);
        }

        /* Check if argument name is valid */
        if (false == regex.IsMatch(element.ElementName))
        {
          throw new ArgumentException("Client Server Interface function argument name contains invalid chars  >> Interface Name: "
            + usrRteSRInterface.InterfaceInfo.InterfaceName
            + " Interface UUID: " + usrRteSRInterface.InterfaceInfo.UUID
            + " Element name " + element.ElementName);
        }
      }

      /* Interface is valid >> Add to data base */
      senderReceiverBlueprintList.Add(usrRteSRInterface);
    }

    /**
      * @brief      Interface to check if a client server interface is available in RteInterface data base.
      * @param[in]  RteGeneralInterfaceInfo interface info to be checked
      * @param[out] Index of list where the element is located
      * @retval     bool true == interface is available
      * @note       Check will be performed based on the UUID string.
      */
    public bool IsCSInterfaceAvailable(RteGeneralInterfaceInfo checkInterface, out int listIndex)
    {
      bool retVal = false;
      listIndex = 0;

      /* Check for each sender receiver interface in data base */
      for (int idx = 0; idx < clientServerBlueprintList.Count(); idx++)
      {
        if (clientServerBlueprintList[idx].InterfaceInfo.UUID == checkInterface.UUID)
        {
          listIndex = idx;
          retVal = true;
          break;
        }
      }

      return retVal;
    }

    /**
      * @brief      Interface to check if a client server interface is available in RteInterface data base.
      * @param[in]  RteGeneralInterfaceInfo interface info to be checked
      * @retval     bool true == interface is available
      * @note       Check will be performed based on the UUID string.
      */
    public bool IsCSInterfaceAvailable(RteGeneralInterfaceInfo checkInterface)
    {
      int dummy;
      return IsCSInterfaceAvailable(checkInterface, out dummy);
    }

    /**
      * @brief      Interface to check if a client server interface is available in RteInterface data base.
      * @param[in]  RteGeneralInterfaceInfo interface info to be checked
      * @retval     bool true == interface is available
      * @note       Check will be performed based on the name string.
      */
    public bool IsCSInterfaceNameAvailable(RteGeneralInterfaceInfo checkInterface)
    {
      bool retVal = false;

      /* Check for each sender receiver interface in data base */
      for (int idx = 0; idx < clientServerBlueprintList.Count(); idx++)
      {
        if (clientServerBlueprintList[idx].InterfaceInfo.InterfaceName == checkInterface.InterfaceName)
        {
          retVal = true;
          break;
        }
      }

      return retVal;
    }

    /**
      * @brief      Interface to check if a sender receiver interface is available in RteInterface data base.
      * @param[in]  RteGeneralInterfaceInfo interface info to be checked
      * @param[out] Index of list where the element is located
      * @retval     bool true == interface is available
      * @note       Check will be performed based on the UUID string.
      */
    public bool IsSRInterfaceAvailable(RteGeneralInterfaceInfo checkInterface, out int listIndex)
    {
      bool retVal = false;
      listIndex = 0;

      /* Check for each sender receiver interface in data base */
      for (int idx = 0; idx < senderReceiverBlueprintList.Count(); idx++)
      {
        if (senderReceiverBlueprintList[idx].InterfaceInfo.UUID == checkInterface.UUID)
        {
          listIndex = idx;
          retVal = true;
          break;
        }
      }

      return retVal;
    }

    /**
      * @brief      Interface to check if a sender receiver interface is available in RteInterface data base.
      * @param[in]  RteGeneralInterfaceInfo interface info to be checked
      * @retval     bool true == interface is available
      * @note       Check will be performed based on the UUID string.
      */
    public bool IsSRInterfaceAvailable(RteGeneralInterfaceInfo checkInterface)
    {
      int dummy;
      return IsSRInterfaceAvailable(checkInterface, out dummy);
    }

    /**
      * @brief      Interface to check if a sender receiver interface is available in RteInterface data base.
      * @param[in]  RteGeneralInterfaceInfo interface info to be checked
      * @retval     bool true == interface is available
      * @note       Check will be performed based on the name string.
      */
    public bool IsSRInterfaceNameAvailable(RteGeneralInterfaceInfo checkInterface)
    {
      bool retVal = false;

      /* Check for each sender receiver interface in data base */
      for (int idx = 0; idx < senderReceiverBlueprintList.Count(); idx++)
      {
        if (senderReceiverBlueprintList[idx].InterfaceInfo.InterfaceName == checkInterface.InterfaceName)
        {
          retVal = true;
          break;
        }
      }

      return retVal;
    }

    /**
      * @brief      Interface to validate the Rte Interface Blueprint List
      * @param      Reference to Rte Types data base with all data types
      * @retval     List<string> >> String list with validation errors
      * @details    The validation function would check if each referenced data type is available.
      *             Check all Sender and Receiver elements
      *             Check all Client and Server elements
      */
    public List<string> ValidateRteInterfaceBlueprintList(ref RteLib.RteTypes.RteTypes typesDb)
    {
      List<string> validationErrors = new List<string>();

      foreach(RteSRInterfaceBlueprint srInterfaceBlueprint in senderReceiverBlueprintList)
      {
        validationErrors.AddRange(ValidateRteSRInterfaceBlueprintElement(
          srInterfaceBlueprint, ref typesDb));
      }

      foreach (RteCSInterfaceBlueprint csInterfaceBlueprint in clientServerBlueprintList)
      {
        validationErrors.AddRange(ValidateRteCSInterfaceBlueprintElement(csInterfaceBlueprint, ref typesDb));
      }

      return validationErrors;
    }

    /**
      * @brief      Interface to validate the Rte Sender Receiver Interface Blueprint List Element
      * @param      Sender Receiver Interface Blueprint to be checked
      * @param      Reference to Rte Types data base with all data types
      * @retval     List<string> >> String list with validation errors
      * @details    The validation function would check if each referenced data type is available.
      */
    public List<string> ValidateRteSRInterfaceBlueprintElement(RteSRInterfaceBlueprint checkSRInterfaceBlueprint,
      ref RteLib.RteTypes.RteTypes typesDb)
    {
      string errorString = "";
      List<string> validationErrors = new List<string>();
      Regex regex = new Regex("^[a-zA-Z0-9_]*$", RegexOptions.Compiled);

      /* Check if Name is set */
      if ("" == checkSRInterfaceBlueprint.InterfaceInfo.InterfaceName)
      {
        errorString = "";
        errorString += "Validation error on Sender Receiver Interface Blueprint \"" +
          checkSRInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
          checkSRInterfaceBlueprint.InterfaceInfo.UUID + "\n";
        errorString += "  >> Sender Receiver Interface Blueprint Name not set!";
        validationErrors.Add(errorString);
      }

      //Validate interface name
      if (false == regex.IsMatch(checkSRInterfaceBlueprint.InterfaceInfo.InterfaceName))
      {
        errorString = "";
        errorString += "Validation error on Sender Receiver Interface Blueprint \"" +
          checkSRInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
          checkSRInterfaceBlueprint.InterfaceInfo.UUID + "\n";
        errorString += "  >> Sender Receiver Interface Blueprint name \"" + 
          checkSRInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" contains invalid characters!";
        validationErrors.Add(errorString);
      }

      /* Check if UUID is set */
      if ("" == checkSRInterfaceBlueprint.InterfaceInfo.UUID)
      {
        errorString = "";
        errorString += "Validation error on Sender Receiver Interface Blueprint \"" +
          checkSRInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
          checkSRInterfaceBlueprint.InterfaceInfo.UUID + "\n";
        errorString += "  >> Sender Receiver Interface Blueprint UUID not set!";
        validationErrors.Add(errorString);
      }

      // Check if element list is empty
      if(0 == checkSRInterfaceBlueprint.Elements.Count)
      {
        errorString = "";
        errorString += "Validation error on Sender Receiver Interface Blueprint \"" +
          checkSRInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
          checkSRInterfaceBlueprint.InterfaceInfo.UUID + "\n";
        errorString += "  >> Sender Receiver Interface Blueprint dose not contain SR elements";
        validationErrors.Add(errorString);
      }

      // Check if element data types are available
      foreach (RteElementBlueprint element in checkSRInterfaceBlueprint.Elements)
      {
        if(false == typesDb.IsDataTypeAvailable(element.ElementType))
        {
          errorString = "";
          errorString += "Validation error on Sender Receiver Interface Blueprint \"" +
            checkSRInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
            checkSRInterfaceBlueprint.InterfaceInfo.UUID + "\n";
          errorString += "  >> Sender Receiver Interface Blueprint Element \"" +
            element.ElementName +  "\" use an data type which is not available in the data type database >> " +
            "Used data type \"" + element.ElementType.DataTypeName + "\" with UUID \"" +
            element.ElementType.UUID + "\"";
          validationErrors.Add(errorString);
        }
      }

      return validationErrors;
    }

    /**
      * @brief      Interface to validate the Rte Client Server Interface Blueprint List Element
      * @param      Client Server Interface Blueprint to be checked
      * @param      Reference to Rte Types data base with all data types
      * @retval     List<string> >> String list with validation errors
      * @details    The validation function would check if each referenced data type is available.
      */
    public List<string> ValidateRteCSInterfaceBlueprintElement(RteCSInterfaceBlueprint checkCSInterfaceBlueprint,
      ref RteLib.RteTypes.RteTypes typesDb)
    {
      string errorString = "";
      List<string> validationErrors = new List<string>();
      Regex regex = new Regex("^[a-zA-Z0-9_]*$", RegexOptions.Compiled);


      /* Check if Name is set */
      if ("" == checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName)
      {
        errorString = "";
        errorString += "Validation error on Client Server Interface Blueprint \"" +
          checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
          checkCSInterfaceBlueprint.InterfaceInfo.UUID + "\n";
        errorString += "  >> Client Server Interface Blueprint Name not set!";
        validationErrors.Add(errorString);
      }

      //Validate interface name
      if (false == regex.IsMatch(checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName))
      {
        errorString = "";
        errorString += "Validation error on Client Server Interface Blueprint \"" +
          checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
          checkCSInterfaceBlueprint.InterfaceInfo.UUID + "\n";
        errorString += "  >> Client Server Interface Blueprint name \"" +
          checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" contains invalid characters!";
        validationErrors.Add(errorString);
      }

      /* Check if UUID is set */
      if ("" == checkCSInterfaceBlueprint.InterfaceInfo.UUID)
      {
        errorString = "";
        errorString += "Validation error on Client Server Interface Blueprint \"" +
          checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
          checkCSInterfaceBlueprint.InterfaceInfo.UUID + "\n";
        errorString += "  >> Client Server Interface Blueprint UUID not set!";
        validationErrors.Add(errorString);
      }

      // Check if element list is empty
      if (0 == checkCSInterfaceBlueprint.Functions.Count)
      {
        errorString = "";
        errorString += "Validation error on Client Server Interface Blueprint \"" +
          checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
          checkCSInterfaceBlueprint.InterfaceInfo.UUID + "\n";
        errorString += "  >> Client Server Interface Blueprint dose not contain CS functions";
        validationErrors.Add(errorString);
      }


      // Check each function
      foreach(RteFunctionBlueprint fnc in checkCSInterfaceBlueprint.Functions)
      {
        //+++++ Check function name +++++
        /* Check if Name is set */
        if ("" == fnc.FunctionName)
        {
          errorString = "";
          errorString += "Validation error on Client Server Interface Blueprint \"" +
            checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
            checkCSInterfaceBlueprint.InterfaceInfo.UUID + "\n";
          errorString += "  >> Function name not set!";
          validationErrors.Add(errorString);
        }

        //Validate function name
        if (false == regex.IsMatch(fnc.FunctionName))
        {
          errorString = "";
          errorString += "Validation error on Client Server Interface Blueprint \"" +
            checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
            checkCSInterfaceBlueprint.InterfaceInfo.UUID + "\n";
          errorString += "  >> Function name \"" + fnc.FunctionName + "\" contains invalid characters!";
          validationErrors.Add(errorString);
        }

        // Check each argument
        foreach(RteFunctionArgumentBlueprint arg in fnc.ArgList)
        {
          /*Check if Argument Name is set */
          if ("" == arg.ArgumentName)
          {
            errorString = "";
            errorString += "Validation error on Client Server Interface Blueprint \"" +
              checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
              checkCSInterfaceBlueprint.InterfaceInfo.UUID + "\n";
            errorString += "  >> In Function \"" + fnc.FunctionName + "\" Argument name not set!";
            validationErrors.Add(errorString);
          }

          //Validate argument name
          if (false == regex.IsMatch(arg.ArgumentName))
          {
            errorString = "";
            errorString += "Validation error on Client Server Interface Blueprint \"" +
              checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
              checkCSInterfaceBlueprint.InterfaceInfo.UUID + "\n";
            errorString += "  >> In Function \"" + fnc.FunctionName + 
              "\" Argument: \"" + arg.ArgumentName + "\" contains invalid characters!";
            validationErrors.Add(errorString);
          }

          // Check if argument type is available
          if (false == typesDb.IsDataTypeAvailable(arg.ArgumentType))
          {
            errorString = "";
            errorString += "Validation error on Client Server Interface Blueprint \"" +
              checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
              checkCSInterfaceBlueprint.InterfaceInfo.UUID + "\n";
            errorString += "  >> Client Server Interface Blueprint function \"" +
              fnc.FunctionName + "." + arg.ArgumentName +  "\" use an data type which is not available in the data type database >> " +
              "Used data type \"" + arg.ArgumentType.DataTypeName + "\" with UUID \"" +
              arg.ArgumentType.UUID + "\"";
            validationErrors.Add(errorString);
          }

        }

        // Check each error type
        if( (null == fnc.AvailableErrorTypes) || (0 == fnc.AvailableErrorTypes.Count))
        {
          errorString = "";
          errorString += "Validation error on Client Server Interface Blueprint \"" +
            checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
            checkCSInterfaceBlueprint.InterfaceInfo.UUID + "\n";
          errorString += "  >> Function name \"" + fnc.FunctionName + "\" contains no error return types!";
          validationErrors.Add(errorString);
        }

        // Check each error return type
        foreach(string errorType in fnc.AvailableErrorTypes)
        {
          //Validate error type name
          if (false == regex.IsMatch(fnc.FunctionName))
          {
            errorString = "";
            errorString += "Validation error on Client Server Interface Blueprint \"" +
              checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
              checkCSInterfaceBlueprint.InterfaceInfo.UUID + "\n";
            errorString += "  >> Function name \"" + fnc.FunctionName + "\" Error type \"" + 
              errorType + "\" contains invalid characters!";
            validationErrors.Add(errorString);
          }
        }

        // Check for return type
        if (false == typesDb.IsDataTypeAvailable(fnc.ReturnValue))
        {
          errorString = "";
          errorString += "Validation error on Client Server Interface Blueprint \"" +
            checkCSInterfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " +
            checkCSInterfaceBlueprint.InterfaceInfo.UUID + "\n";
          errorString += "  >> Client Server Interface Blueprint function \"" +
            fnc.FunctionName + "\" use an return type which is not available in the data type database >> " +
            "Used data type \"" + fnc.ReturnValue.DataTypeName + "\" with UUID \"" +
            fnc.ReturnValue.UUID + "\"";
          validationErrors.Add(errorString);
        }
      }

      return validationErrors;
    }

    /*
     * @brief       Function to store the current rte interface data base to an xml file
     * @param       string Path to RteInterface Database file
     * @retval      none
     */
    public void SaveActiveRteInterfaceDataBaseToXml(string pathToXmlFile)
    {
      XmlSerializer writer = new XmlSerializer(this.GetType());
      StreamWriter file = new StreamWriter(pathToXmlFile);

      /* Setup XML File Version */
      xmlFileVersion.MajorVersion = Convert.ToUInt16(RteInterfaceResources.RteInterfaceXmlMajorVersion);
      xmlFileVersion.MinorVersion = Convert.ToUInt16(RteInterfaceResources.RteInterfaceXmlMinorVersion);
      xmlFileVersion.PatchVersion = Convert.ToUInt16(RteInterfaceResources.RteInterfaceXmlPatchVersion);

      writer.Serialize(file, this);
      file.Close();
    }

    /*
     * @brief       Function to read the current interface structure from an xml file
     * @param       string Path to RteInterface Database file
     * @retval      none
     */
    public void ReadActiveRteInterfaceDataBaseFromXml(string pathToXmlFile)
    {
      RteInterfaces tempRteInterface = new RteInterfaces();
      XmlSerializer reader = new XmlSerializer(this.GetType());
      StreamReader file = new StreamReader(pathToXmlFile);
      tempRteInterface = (RteInterfaces)reader.Deserialize(file);

      this.clientServerBlueprintList = tempRteInterface.clientServerBlueprintList;
      this.senderReceiverBlueprintList = tempRteInterface.senderReceiverBlueprintList;
      this.xmlFileVersion = tempRteInterface.xmlFileVersion;

      file.Close();
    }

  }
}
/**
* @}
*/
