﻿/*****************************************************************************************************************************
 * @file        RteInterfaceTypes.cs                                                                                         *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        05.01.2019                                                                                                   *
 * @brief       Definition of the RTE Interface data types                                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RteLib.RteInterface
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using RteLib.RteTypes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RteLib.RteInterface
{
  /**
  * @brief      RTE Struct of basic Interface Informations
  * @param      string interface Name
  * @param      string interface UUID 
  */
  public class RteGeneralInterfaceInfo : IEquatable<RteGeneralInterfaceInfo>
  {
    private string interfaceName = "";
    private string uuid = "";

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteGeneralInterfaceInfo() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteGeneralInterfaceInfo(RteGeneralInterfaceInfo obj)
    {
      this.InterfaceName = obj.InterfaceName;
      this.UUID = obj.UUID;
    }

    #region Attributes
    
    public string InterfaceName
    {
      get { return interfaceName; }
      set { interfaceName = value; }
    }

    
    public string UUID
    {
      get { return uuid; }
      set { uuid = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteGeneralInterfaceInfo lhs, RteGeneralInterfaceInfo rhs)
    {
      bool retVal = false;

      if (( lhs.UUID == rhs.UUID ) && ( lhs.interfaceName == rhs.interfaceName ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteGeneralInterfaceInfo lhs, RteGeneralInterfaceInfo rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteGeneralInterfaceInfo rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /******************************************************************************/
  /****************************** Client / Server *******************************/
  /******************************************************************************/

  /**
  * @brief      RTE struct of an Client Server Interface function
  * @param      RteBaseDataTypeStructure return data type
  * @param      string list with available error types
  * @param      string function name
  * @param      string function UUID
  * @param      List<RteFunctionArgumentBlueprint> function argument list
  */
  public class RteFunctionBlueprint : IEquatable<RteFunctionBlueprint>
  {
    private RteBaseDataTypeStructure returnValue = new RteBaseDataTypeStructure();
    private List<String> availableErrorTypes = new List<string>();
    private string functionName = "";
    private string uuid = "";
    private List<RteFunctionArgumentBlueprint> argList = new List<RteFunctionArgumentBlueprint>();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteFunctionBlueprint() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteFunctionBlueprint(RteFunctionBlueprint obj)
    {
      this.ReturnValue = new RteBaseDataTypeStructure(obj.ReturnValue);
      this.AvailableErrorTypes = new List<string>(obj.AvailableErrorTypes);
      this.FunctionName = obj.FunctionName;
      this.UUID = obj.UUID;
      this.ArgList = new List<RteFunctionArgumentBlueprint>(obj.ArgList);
    }

    #region Attributes
    
    public RteBaseDataTypeStructure ReturnValue
    {
      get { return returnValue; }
      set { returnValue = value; }
    }

    
    public List<String> AvailableErrorTypes
    {
      get { return availableErrorTypes; }
      set { availableErrorTypes = value; }
    }

    
    public string FunctionName
    {
      get { return functionName; }
      set { functionName = value; }
    }

    
    public string UUID
    {
      get { return uuid; }
      set { uuid = value; }
    }

    
    public List<RteFunctionArgumentBlueprint> ArgList
    {
      get { return argList; }
      set { argList = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteFunctionBlueprint lhs, RteFunctionBlueprint rhs)
    {
      bool retVal = false;

      if (( lhs.returnValue         == rhs.returnValue )                              &&
          ( lhs.functionName        == rhs.functionName )                             &&
          ( lhs.UUID            == rhs.UUID )                                 &&
          ( true == lhs.availableErrorTypes.SequenceEqual(rhs.availableErrorTypes) ) &&
          ( true == lhs.argList.SequenceEqual(rhs.argList)))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteFunctionBlueprint lhs, RteFunctionBlueprint rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteFunctionBlueprint rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE struct of an Client Server function argument
  * @param      string argument name
  * @param      RteBaseDataTypeStructure argument data type
  */
  public class RteFunctionArgumentBlueprint : IEquatable<RteFunctionArgumentBlueprint>
  {
    private string argumentName = "";
    private RteBaseDataTypeStructure argumentType = new RteBaseDataTypeStructure();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteFunctionArgumentBlueprint() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteFunctionArgumentBlueprint(RteFunctionArgumentBlueprint obj)
    {
      this.ArgumentType = new RteBaseDataTypeStructure(obj.ArgumentType);
      this.ArgumentName = obj.ArgumentName;
    }

    #region Attributes
    
    public string ArgumentName
    {
      get { return argumentName; }
      set { argumentName = value; }
    }

    
    public RteBaseDataTypeStructure ArgumentType
    {
      get { return argumentType; }
      set { argumentType = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteFunctionArgumentBlueprint lhs, RteFunctionArgumentBlueprint rhs)
    {
      bool retVal = false;

      if (( lhs.argumentName == rhs.argumentName ) && ( lhs.argumentType == rhs.argumentType ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteFunctionArgumentBlueprint lhs, RteFunctionArgumentBlueprint rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteFunctionArgumentBlueprint rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }


  /**
  * @brief      RTE struct of an Client Server Interface Blueprint
  * @param      RteGeneralInterfaceInfo General Interface Informations
  * @param      List of RteFunctionBlueprint function configuration
  */
  public class RteCSInterfaceBlueprint : IEquatable<RteCSInterfaceBlueprint>
  {
    private RteGeneralInterfaceInfo interfaceInfo = new RteGeneralInterfaceInfo();
    private List<RteFunctionBlueprint> functions = new List<RteFunctionBlueprint>();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteCSInterfaceBlueprint() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteCSInterfaceBlueprint(RteCSInterfaceBlueprint obj)
    {
      this.InterfaceInfo = new RteGeneralInterfaceInfo(obj.InterfaceInfo);
      this.Functions = new List<RteFunctionBlueprint>(obj.Functions);
    }

    #region Attributes
    
    public RteGeneralInterfaceInfo InterfaceInfo
    {
      get { return interfaceInfo; }
      set { interfaceInfo = value; }
    }

    
    public List<RteFunctionBlueprint> Functions
    {
      get { return functions; }
      set { functions = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteCSInterfaceBlueprint lhs, RteCSInterfaceBlueprint rhs)
    {
      bool retVal = false;

      if (( lhs.interfaceInfo == rhs.interfaceInfo ) && ( true == lhs.functions.SequenceEqual(rhs.functions)))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteCSInterfaceBlueprint lhs, RteCSInterfaceBlueprint rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteCSInterfaceBlueprint rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE Enumeration of the different Rte CLient Server Interface implementation
  * @note       CLIENT   >> RTE Client Interface Implementation
  *             SERVER   >> RTE Server Interface Implementation
  */
  public enum RteCSInterfaceImplementationType
  {
    CLIENT,
    SERVER
  }

  /**
  * @brief      RTE struct of an Client Server Interface Prototype
  * @param      RteGeneralInterfaceInfo General Interface Informations
  * @param      RteFunctionBlueprint function configuration
  * @param      RteCSInterfaceImplementationType interface implementation type
  */
  public class RteCSInterfacePrototype : IEquatable<RteCSInterfacePrototype>
  {
    private RteGeneralInterfaceInfo interfacePrototype = new RteGeneralInterfaceInfo();
    private RteGeneralInterfaceInfo interfaceBlueprint = new RteGeneralInterfaceInfo();
    private RteCSInterfaceImplementationType interfaceType = new RteCSInterfaceImplementationType();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteCSInterfacePrototype() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteCSInterfacePrototype(RteCSInterfacePrototype obj)
    {
      this.InterfacePrototype = new RteGeneralInterfaceInfo(obj.InterfacePrototype);
      this.InterfaceBlueprint = new RteGeneralInterfaceInfo(obj.InterfaceBlueprint);
      this.InterfaceType = obj.InterfaceType;
    }

    #region Attributes
    
    public RteGeneralInterfaceInfo InterfacePrototype
    {
      get { return interfacePrototype; }
      set { interfacePrototype = value; }
    }

    
    public RteGeneralInterfaceInfo InterfaceBlueprint
    {
      get { return interfaceBlueprint; }
      set { interfaceBlueprint = value; }
    }

    
    public RteCSInterfaceImplementationType InterfaceType
    {
      get { return interfaceType; }
      set { interfaceType = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteCSInterfacePrototype lhs, RteCSInterfacePrototype rhs)
    {
      bool retVal = false;

      if (( lhs.interfacePrototype == rhs.interfacePrototype ) &&
          ( lhs.interfaceBlueprint == rhs.interfaceBlueprint ) &&
          ( lhs.interfaceType == rhs.interfaceType ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteCSInterfacePrototype lhs, RteCSInterfacePrototype rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteCSInterfacePrototype rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }


  /******************************************************************************/
  /***************************** Sender / Receiver ******************************/
  /******************************************************************************/
  /**
  * @brief      RTE struct of an Sender Receiver Element
  * @param      string data element name
  * @param      RteBaseDataTypeStructure data element data type
  * @param      List<RteBaseDataTypeStructure> function argument list
  */
  public class RteElementBlueprint : IEquatable<RteElementBlueprint>
  {
    private string elementName = "";
    private string uuid = "";
    private RteBaseDataTypeStructure elementType = new RteBaseDataTypeStructure();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteElementBlueprint() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteElementBlueprint(RteElementBlueprint obj)
    {
      this.ElementType = new RteBaseDataTypeStructure(obj.ElementType);
      this.ElementName = obj.ElementName;
      this.UUID = obj.UUID;
    }

    #region Attributes
    
    public string ElementName
    {
      get { return elementName; }
      set { elementName = value; }
    }

    
    public string UUID
    {
      get { return uuid; }
      set { uuid = value; }
    }

    
    public RteBaseDataTypeStructure ElementType
    {
      get { return elementType; }
      set { elementType = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteElementBlueprint lhs, RteElementBlueprint rhs)
    {
      bool retVal = false;

      if (( lhs.elementName == rhs.elementName )  &&
          ( lhs.UUID    == rhs.UUID )     &&
          ( lhs.elementType == rhs.elementType ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteElementBlueprint lhs, RteElementBlueprint rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteElementBlueprint rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE struct of an Sender Receiver Interface Blueprint
  * @param      RteGeneralInterfaceInfo General Interface Informations
  * @param      List of RteElementBlueprint elements
  */
  public class RteSRInterfaceBlueprint : IEquatable<RteSRInterfaceBlueprint>
  {
    private RteGeneralInterfaceInfo interfaceInfo = new RteGeneralInterfaceInfo();
    private List<RteElementBlueprint> elements = new List<RteElementBlueprint>();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteSRInterfaceBlueprint() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteSRInterfaceBlueprint(RteSRInterfaceBlueprint obj)
    {
      this.InterfaceInfo = new RteGeneralInterfaceInfo(obj.InterfaceInfo);
      this.Elements = new List<RteElementBlueprint>(obj.Elements);
    }

    #region Attributes
    
    public RteGeneralInterfaceInfo InterfaceInfo
    {
      get { return interfaceInfo; }
      set { interfaceInfo = value; }
    }

    
    public List<RteElementBlueprint> Elements
    {
      get { return elements; }
      set { elements = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteSRInterfaceBlueprint lhs, RteSRInterfaceBlueprint rhs)
    {
      bool retVal = false;

      if (( lhs.interfaceInfo == rhs.interfaceInfo ) && ( true == lhs.elements.SequenceEqual(rhs.elements)))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteSRInterfaceBlueprint lhs, RteSRInterfaceBlueprint rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteSRInterfaceBlueprint rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE Enumeration of the different Rte CLient Server Interface implementation
  * @note       SENDER   >> RTE Sender Interface Implementation
  *             RECEIVER >> RTE Receiver Interface Implementation
  */
  public enum RteSRInterfaceImplementationType
  {
    SENDER,
    RECEIVER
  }

  /**
  * @brief      RTE struct of an Sender Receiver Interface Prototype
  * @param      RteGeneralInterfaceInfo interface prototype information
  * @param      RteGeneralInterfaceInfo used interface blueprint
  * @param      RteSRInterfaceImplementationType interface implementation type
  */
  public class RteSRInterfacePrototype : IEquatable<RteSRInterfacePrototype>
  {
    private RteGeneralInterfaceInfo interfacePrototype = new RteGeneralInterfaceInfo();
    private RteGeneralInterfaceInfo interfaceBlueprint = new RteGeneralInterfaceInfo();
    private RteSRInterfaceImplementationType interfaceType = new RteSRInterfaceImplementationType();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteSRInterfacePrototype() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteSRInterfacePrototype(RteSRInterfacePrototype obj)
    {
      this.InterfacePrototype = new RteGeneralInterfaceInfo(obj.InterfacePrototype);
      this.InterfaceBlueprint = new RteGeneralInterfaceInfo(obj.InterfaceBlueprint);
      this.InterfaceType = obj.InterfaceType;
    }

    #region Attributes
    
    public RteGeneralInterfaceInfo InterfacePrototype
    {
      get { return interfacePrototype; }
      set { interfacePrototype = value; }
    }

    
    public RteGeneralInterfaceInfo InterfaceBlueprint
    {
      get { return interfaceBlueprint; }
      set { interfaceBlueprint = value; }
    }

    
    public RteSRInterfaceImplementationType InterfaceType
    {
      get { return interfaceType; }
      set { interfaceType = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteSRInterfacePrototype lhs, RteSRInterfacePrototype rhs)
    {
      bool retVal = false;

      if (( lhs.interfacePrototype == rhs.interfacePrototype ) &&
          ( lhs.interfaceBlueprint == rhs.interfaceBlueprint ) &&
          ( lhs.interfaceType      == rhs.interfaceType ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteSRInterfacePrototype lhs, RteSRInterfacePrototype rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteSRInterfacePrototype rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }
}
/**
* @}
*/
