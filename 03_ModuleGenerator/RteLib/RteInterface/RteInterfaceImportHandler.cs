﻿/*****************************************************************************************************************************
 * @file        RteInterfaceImportHandler.cs                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        13.12.2020                                                                                                   *
 * @brief       Implementation of the import handler for the rte interface. In this class all import operation into the      *
 *              Rte Interfaces are available.                                                                                *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RteLib.RteConfig
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RteLib.RteConfig;
using OsarResources.Generic;
using RteLib.RteResources;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RteLib.RteInterface
{
  public class RteInterfaceImportHandler
  {
    private RteConfig.RteConfig rteConfig;                                  //!< Reference to the Rte Configuration
    private RteInterfaces rteInterfaces;                                    //!< Reference to the Rte Configuration
    private RteModuleInternalBehavior.RteModuleInternalBehavior mibConfig;  //!< actualUsedMibConfig
    private string absPathToRteConfiguartionFile;                           //!< Absolute Path to Rte Configuration File

    /**
     * @brief     Constructor
     * @param[in] Rte configuration object
     * @param[in] Reference to the rte interface object
     */
    public RteInterfaceImportHandler(RteConfig.RteConfig config, ref RteInterfaces interfaces, string absPathToRteConfigFile)
    {
      rteConfig = config;
      rteInterfaces = interfaces;
      absPathToRteConfiguartionFile = absPathToRteConfigFile;
    }

    #region Import functions
    /**
     * @brief     Import the MiB backup blueprint interfaces into the rte interface object
     * @param[in] Module to be imported
     * @return    None
     * @exception - FileNotFoundException
     *            - OsarConfigMismatchException
     *            - OsarMergeException
     *            - ArgumentException
     * @details   Pre-Actions:
     *            - Check of module internal behavior file exists and load file.
     *            Following informations are imported:
     *            - Backup client server interfaces blueprints
     *            - Backup sender receiver interface blueprints
     */
    public void ImportMiBFileIntoRteInterface(RteConfigUsedModuleCfg module)
    {
      try
      {
        LoadMiBFile(module);

        // Import all client server interface backup blueprints from MiB into rte interface data base
        foreach(RteCSInterfaceBlueprint backupCSInterfaceBlueprint in mibConfig.BackupCSBlueprintList)
        {
          if(false == CheckIfClientServerInterfaceBlueprintIsAlreadyConfigured(backupCSInterfaceBlueprint))
          {
            // Client server interface is not available in rte interface >> Import it into rte interface
            rteInterfaces.AddRteCSInterface(backupCSInterfaceBlueprint);
          }
        }

        // Import all sender receiver interface backup blueprints from MiB into rte interface data base
        foreach (RteSRInterfaceBlueprint backupSRInterfaceBlueprint in mibConfig.BackupSRBlueprintList)
        {
          if (false == CheckIfSenderReceiverInterfaceBlueprintIsAlreadyConfigured(backupSRInterfaceBlueprint))
          {
            // Sender receiver interface is not available in rte interface >> Import it into rte interface
            rteInterfaces.AddRteSRInterface(backupSRInterfaceBlueprint);
          }
        }
      }
      catch (Exception)
      {
        throw; // Forward the exception
      }
    }

    #endregion

    #region Helper functions
    /**
     * @brief     Helper function which loads the module internal behavior file
     * @param[in] module: Rte basis module information
     * @return    None
     * @exception - FileNotFoundException
     *            - OsarConfigMismatchException
     */
    private void LoadMiBFile(RteConfigUsedModuleCfg module)
    {
      string pathToMibFile = Path.GetDirectoryName(absPathToRteConfiguartionFile) + rteConfig.PathToBaseProjectFolder +
        "./" + module.ModuleType + "./" + module.ModuleName + RteProjectStructure.PathToMibFileFromModuleBaseFolder;

      // Check if file exists
      if (false == File.Exists(pathToMibFile))
      {
        throw new FileNotFoundException("Requested MiB file to merged not found! FilePath: " + pathToMibFile +
          " Check project structure recommended.");
      }

      // Create MiB Object
      mibConfig = new RteModuleInternalBehavior.RteModuleInternalBehavior();
      mibConfig.ReadActiveRteModuleInternalBehaviorToXml(pathToMibFile);

      // Check if config match's with MiB File
      if (( module.ModuleName != mibConfig.ModuleName ) || ( module.ModuleType != mibConfig.ModuleType ))
      {
        throw new OsarResources.Generic.OsarConfigMismatchException("Requested module to be merged: " +
          module.ModuleType.ToString() + " " + module.ModuleName + " >> Does not fit with MiB configuration: " +
          mibConfig.ModuleType.ToString() + " " + mibConfig.ModuleName);
      }
    }

    /**
     * @brief     Helper function to check if the MiB client server interface blueprint is already available in rte interface
     * @param[in] MiB client server interface blueprint which shall be imported
     * @details   Check of port is available and identical!
     * @exception - OsarMergeException
     */
    private bool CheckIfClientServerInterfaceBlueprintIsAlreadyConfigured(RteCSInterfaceBlueprint mibClientServerInterfaceBlueprint)
    {
      bool retVal = false;

      foreach (RteCSInterfaceBlueprint rteClientServerPortBlueprint in rteInterfaces.ClientServerBlueprintList)
      {
        /* Check if UUID exists */
        if (rteClientServerPortBlueprint.InterfaceInfo.UUID == mibClientServerInterfaceBlueprint.InterfaceInfo.UUID)
        {
          // Check if object is identical
          if (rteClientServerPortBlueprint == mibClientServerInterfaceBlueprint)
          {
            // Element found and identical!
            retVal = true;
            break;
          }
          else
          {
            // UUID found but element is not identical
            throw new OsarResources.Generic.OsarMergeException("Import of available Client-Server-Interface-Blueprint not possible." +
              "Rte Client-Server-Interface-Blueprint: " + rteClientServerPortBlueprint.InterfaceInfo.InterfaceName + " and MiB Interface: " +
              mibClientServerInterfaceBlueprint.InterfaceInfo.InterfaceName + " have the same UUID but are NOT equal!");
          }
        }
      }

      return retVal;
    }

    /**
     * @brief     Helper function to check if the MiB sender receiver interface blueprint is already available in rte interface
     * @param[in] MiB sender receiver interface blueprint which shall be imported
     * @details   Check of port is available and identical!
     * @exception - OsarMergeException
     */
    private bool CheckIfSenderReceiverInterfaceBlueprintIsAlreadyConfigured(RteSRInterfaceBlueprint mibSenderReceiverInterfaceBlueprint)
    {
      bool retVal = false;

      foreach (RteSRInterfaceBlueprint rteSenderReceiverPortBlueprint in rteInterfaces.SenderReceiverBlueprintList)
      {
        /* Check if UUID exists */
        if (rteSenderReceiverPortBlueprint.InterfaceInfo.UUID == mibSenderReceiverInterfaceBlueprint.InterfaceInfo.UUID)
        {
          // Check if object is identical
          if (rteSenderReceiverPortBlueprint == mibSenderReceiverInterfaceBlueprint)
          {
            // Element found and identical!
            retVal = true;
            break;
          }
          else
          {
            // UUID found but element is not identical
            throw new OsarResources.Generic.OsarMergeException("Import of available Sender-Receiver-Interface-Blueprint not possible." +
              "Rte Sender-Receiver-Interface-Blueprint: " + rteSenderReceiverPortBlueprint.InterfaceInfo.InterfaceName + " and MiB Interface: " +
              rteSenderReceiverPortBlueprint.InterfaceInfo.InterfaceName + " have the same UUID but are NOT equal!");
          }
        }
      }

      return retVal;
    }
    #endregion
  }
}
/**
* @}
*/
