﻿/*****************************************************************************************************************************
 * @file        RteModuleInternalBehaviorTypes.cs                                                                            *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        05.01.2019                                                                                                   *
 * @brief       Definition of the RTE Module internal behavior data types                                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RteLib.RteModuleInternalBehavior
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RteLib.RteModuleInternalBehavior
{
  /**
  * @brief      RTE Generic Runnable Information
  * @note       This struct is used to identify the different used objects
  */
  public class RteGenericRunnableObject : IEquatable<RteGenericRunnableObject>
  {
    private string name = "";
    private string uuid = "";
    private RteGeneralInterfaceInfo basePrototype;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteGenericRunnableObject() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteGenericRunnableObject(RteGenericRunnableObject obj)
    {
      this.Name = obj.Name;
      this.UUID =obj.UUID;
      this.basePrototype = new RteGeneralInterfaceInfo(obj.BasePrototype);
    }

    #region Attributes
    public string Name
    {
      get { return name; }
      set { name = value; }
    }

    public string UUID
    {
      get { return uuid; }
      set { uuid = value; }
    }

    public RteGeneralInterfaceInfo BasePrototype
    {
      get { return basePrototype; }
      set { basePrototype = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteGenericRunnableObject lhs, RteGenericRunnableObject rhs)
    {
      bool retVal = false;

      if (( lhs.UUID == rhs.UUID ) && ( lhs.Name == rhs.Name ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteGenericRunnableObject lhs, RteGenericRunnableObject rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteGenericRunnableObject rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE Init Runnable Information
  * @note       This struct is used to identify a runnable configuration.
  *             It contains the runnable identification also as the internal runnable port access information.
  */
  public class RteRunnable : IEquatable<RteRunnable>
  {
    private RteGeneralInterfaceInfo runnableInfo = new RteGeneralInterfaceInfo();
    private List<RteGenericRunnableObject> accessToClientPortFunctions = new List<RteGenericRunnableObject>();
    private List<RteGenericRunnableObject> accessToSenderReceiverPortElements = new List<RteGenericRunnableObject>();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteRunnable() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteRunnable(RteRunnable obj)
    {
      this.runnableInfo = new RteGeneralInterfaceInfo(obj.RunnableInfo);
      this.accessToClientPortFunctions = new List<RteGenericRunnableObject>(obj.AccessToClientPortFunctions);
      this.accessToSenderReceiverPortElements = new List<RteGenericRunnableObject>(obj.AccessToSenderReceiverPortElements);
    }

    #region Attributes
    public RteGeneralInterfaceInfo RunnableInfo
    {
      get { return runnableInfo; }
      set { runnableInfo = value; }
    }

    public List<RteGenericRunnableObject> AccessToClientPortFunctions
    {
      get { return accessToClientPortFunctions; }
      set { accessToClientPortFunctions = value; }
    }

    public List<RteGenericRunnableObject> AccessToSenderReceiverPortElements
    {
      get { return accessToSenderReceiverPortElements; }
      set { accessToSenderReceiverPortElements = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteRunnable lhs, RteRunnable rhs)
    {
      bool retVal = false;

      if (( lhs.RunnableInfo                       == rhs.RunnableInfo )                              && 
          ( true == lhs.AccessToClientPortFunctions.SequenceEqual(rhs.AccessToClientPortFunctions) )  &&
          ( true == lhs.AccessToSenderReceiverPortElements.SequenceEqual(rhs.AccessToSenderReceiverPortElements) ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteRunnable lhs, RteRunnable rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteRunnable rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE Cyclic Runnable Information
  * @note       This struct is used to identify the cyclic runnable configuration.
  *             cycleTime Cycle time must > 0 and set as ms.
  */
  public class RteCyclicRunnable : IEquatable<RteCyclicRunnable>
  {
    private RteRunnable runnable = new RteRunnable();
    private UInt16 cycleTime = 0;

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteCyclicRunnable() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteCyclicRunnable(RteCyclicRunnable obj)
    {
      this.Runnable = new RteRunnable(obj.Runnable);
      this.CycleTime = obj.CycleTime;
    }

    #region Attributes
    
    public RteRunnable Runnable
    {
      get { return runnable; }
      set { runnable = value; }
    }

    public UInt16 CycleTime
    {
      get { return cycleTime; }
      set { cycleTime = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteCyclicRunnable lhs, RteCyclicRunnable rhs)
    {
      bool retVal = false;

      if (( lhs.runnable == rhs.runnable ) && ( lhs.cycleTime == rhs.cycleTime ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteCyclicRunnable lhs, RteCyclicRunnable rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteCyclicRunnable rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE Module available Server Port Configuration
  * @note       This struct is used to identify the a server port with all its server runnable
  */
  public class RteAvailableServerPort : IEquatable<RteAvailableServerPort>
  {
    private RteCSInterfacePrototype serverPort = new RteCSInterfacePrototype();
    private List<RteRunnable> serverPortRunnables = new List<RteRunnable>();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteAvailableServerPort() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteAvailableServerPort(RteAvailableServerPort obj)
    {
      this.ServerPort = new RteCSInterfacePrototype(obj.ServerPort);
      this.ServerPortRunnables = new List<RteRunnable>(obj.ServerPortRunnables);
    }

    #region Attributes
    
    public RteCSInterfacePrototype ServerPort
    {
      get { return serverPort; }
      set { serverPort = value; }
    }

    
    public List<RteRunnable> ServerPortRunnables
    {
      get { return serverPortRunnables; }
      set { serverPortRunnables = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteAvailableServerPort lhs, RteAvailableServerPort rhs)
    {
      bool retVal = false;

      if (( lhs.serverPort == rhs.serverPort ) && 
          ( true == lhs.serverPortRunnables.SequenceEqual(rhs.serverPortRunnables) ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteAvailableServerPort lhs, RteAvailableServerPort rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteAvailableServerPort rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE Module available Client Port Configuration
  * @note       This struct is used to identify the a client port with all its client runnables
  */
  public class RteAvailableClientPort : IEquatable<RteAvailableClientPort>
  {
    private RteCSInterfacePrototype clientPort = new RteCSInterfacePrototype();
    private List<RteGenericRunnableObject> clientPortFunctions = new List<RteGenericRunnableObject>();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteAvailableClientPort() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteAvailableClientPort(RteAvailableClientPort obj)
    {
      this.ClientPort = new RteCSInterfacePrototype(obj.ClientPort);
      this.ClientPortFunctions = new List<RteGenericRunnableObject>(obj.ClientPortFunctions);
    }

    #region Attributes
    
    public RteCSInterfacePrototype ClientPort
    {
      get { return clientPort; }
      set { clientPort = value; }
    }

    
    public List<RteGenericRunnableObject> ClientPortFunctions
    {
      get { return clientPortFunctions; }
      set { clientPortFunctions = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteAvailableClientPort lhs, RteAvailableClientPort rhs)
    {
      bool retVal = false;

      if (( lhs.clientPort == rhs.clientPort ) && 
          ( true == lhs.clientPortFunctions.SequenceEqual(rhs.clientPortFunctions) ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteAvailableClientPort lhs, RteAvailableClientPort rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteAvailableClientPort rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE Module available sender receiver Port Configuration
  * @note       This struct is used to identify the a sender receiver port with all its sender receiver elements
  */
  public class RteAvailableSRPort : IEquatable<RteAvailableSRPort>
  {
    private RteSRInterfacePrototype senderReceiverPort = new RteSRInterfacePrototype();
    private List<RteGenericRunnableObject> senderReceiverPortElements = new List<RteGenericRunnableObject>();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteAvailableSRPort() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteAvailableSRPort(RteAvailableSRPort obj)
    {
      this.SenderReceiverPort = new RteSRInterfacePrototype( obj.SenderReceiverPort );
      this.SenderReceiverPortElements = new List<RteGenericRunnableObject>( obj.SenderReceiverPortElements );
    }

    #region Attributes
    
    public RteSRInterfacePrototype SenderReceiverPort
    {
      get { return senderReceiverPort; }
      set { senderReceiverPort = value; }
    }

    public List<RteGenericRunnableObject> SenderReceiverPortElements
    {
      get { return senderReceiverPortElements; }
      set { senderReceiverPortElements = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteAvailableSRPort lhs, RteAvailableSRPort rhs)
    {
      bool retVal = false;

      if (( lhs.senderReceiverPort == rhs.senderReceiverPort ) && 
          ( true == lhs.senderReceiverPortElements.SequenceEqual(rhs.senderReceiverPortElements) ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteAvailableSRPort lhs, RteAvailableSRPort rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteAvailableSRPort rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }
}
/**
* @}
*/
