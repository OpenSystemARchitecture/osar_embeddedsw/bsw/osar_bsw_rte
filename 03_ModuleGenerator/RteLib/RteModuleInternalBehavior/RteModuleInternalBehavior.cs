﻿/*****************************************************************************************************************************
 * @file        RteModuleInternalBehavior.cs                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        05.01.2019                                                                                                   *
 * @brief       Definition of the RTE Module Internal Behavior xml file structure                                            *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RteLib.RteModuleInternalBehavior
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using OsarResources.XML;
using RteLib.RteConfig;
using RteLib.RteInterface;
using RteLib.RteTypes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RteLib.RteModuleInternalBehavior
{
  /**
  * @brief      RTE  Module Internal Behavior XML File Structure
  * @note       This class is used for the  Module Internal Behavior File Structure with additional functionalities
  */
  public class RteModuleInternalBehavior
  {
    private XmlFileVersion xmlFileVersion;
    private string moduleName = "";
    private RteConfigModuleTypes moduleType = RteConfigModuleTypes.SWC;
    private string uuid = "";

    private List<RteRunnable> initRunnables = new List<RteRunnable>();
    private List<RteCyclicRunnable> cyclicRunnables = new List<RteCyclicRunnable>();

    private List<RteAvailableServerPort> availableServerPorts = new List<RteAvailableServerPort>();
    private List<RteAvailableClientPort> availableClientPorts = new List<RteAvailableClientPort>();
    private List<RteAvailableSRPort> availableSRPorts = new List<RteAvailableSRPort>();
    private List<RteCSInterfaceBlueprint> backupCSBlueprintList = new List<RteCSInterfaceBlueprint>();
    private List<RteSRInterfaceBlueprint> backupSRBlueprintList = new List<RteSRInterfaceBlueprint>();
    private List<RteAdvancedDataTypeStructure> backupAdvancedDataTypeList = new List<RteAdvancedDataTypeStructure>();

    // Internal processing elements
    
    Regex regex = new Regex("^[a-zA-Z0-9_]*$", RegexOptions.Compiled);

    /**
      * @brief      Constructor
      */
    public RteModuleInternalBehavior()
    {
      xmlFileVersion = new XmlFileVersion();
      xmlFileVersion.MajorVersion = Convert.ToUInt16(RteModuleInternalBehaviorResources.RteModuleInternalBehaviorXmlMajorVersion);
      xmlFileVersion.MinorVersion = Convert.ToUInt16(RteModuleInternalBehaviorResources.RteModuleInternalBehaviorXmlMinorVersion);
      xmlFileVersion.PatchVersion = Convert.ToUInt16(RteModuleInternalBehaviorResources.RteModuleInternalBehaviorXmlPatchVersion);
    }

    /**
      * @brief      Destructor
      */
    ~RteModuleInternalBehavior()
    {
    }

    #region Attributes
    public XmlFileVersion XmlFileVersion
    {
      get { return xmlFileVersion; }
      set { xmlFileVersion = value; }
    }

    public string ModuleName
    {
      get { return moduleName; }
      set { moduleName = value; }
    }

    public RteConfigModuleTypes ModuleType
    {
      get { return moduleType; }
      set { moduleType = value; }
    }

    public string UUID
    {
      get { return uuid; }
      set { uuid = value; }
    }

    public List<RteRunnable> InitRunnables
    {
      get { return initRunnables; }
      set { initRunnables = value; }
    }

    public List<RteCyclicRunnable> CyclicRunnables
    {
      get { return cyclicRunnables; }
      set { cyclicRunnables = value; }
    }

    public List<RteAvailableServerPort> AvailableServerPorts
    {
      get { return availableServerPorts; }
      set { availableServerPorts = value; }
    }

    public List<RteAvailableClientPort> AvailableClientPorts
    {
      get { return availableClientPorts; }
      set { availableClientPorts = value; }
    }

    public List<RteAvailableSRPort> AvailableSRPorts
    {
      get { return availableSRPorts; }
      set { availableSRPorts = value; }
    }

    public List<RteCSInterfaceBlueprint> BackupCSBlueprintList
    {
      get { return backupCSBlueprintList; }
      set { backupCSBlueprintList = value; }
    }

    public List<RteSRInterfaceBlueprint> BackupSRBlueprintList
    {
      get { return backupSRBlueprintList; }
      set { backupSRBlueprintList = value; }
    }

    public List<RteAdvancedDataTypeStructure> BackupAdvancedDataTypeList
    {
      get { return backupAdvancedDataTypeList; }
      set { backupAdvancedDataTypeList = value; }
    }
    #endregion

    #region Request Functions
    /// <summary>
    /// Interface to check if a usable client runnable object is available
    /// >> Check in available client ports
    /// </summary>
    /// <param name="runnableObject"></param>
    /// <returns></returns>
    public bool IsClientRunnableObjectAvailableAndUsable(RteGenericRunnableObject runnableObject)
    {
      bool elementFound = false;
      foreach (RteAvailableClientPort availableClientPort in availableClientPorts)
      {
        if (true == availableClientPort.ClientPortFunctions.Contains(runnableObject))
        {
          elementFound = true;
          break;
        }
      }

      return elementFound;
    }

    /// <summary>
    /// Interface to check if a usable sender receiver runnable object is available
    /// >> Check in available sender receiver ports
    /// </summary>
    /// <param name="runnableObject"></param>
    /// <returns></returns>
    public bool IsSRRunnableObjectAvailableAndUsable(RteGenericRunnableObject runnableObject)
    {
      bool elementFound = false;

      foreach (RteAvailableSRPort availableSRPort in availableSRPorts)
      {
        if (true == availableSRPort.SenderReceiverPortElements.Contains(runnableObject))
        {
          elementFound = true;
          break;
        }
      }

      return elementFound;
    }

    /// <summary>
    /// Interface to check if the provided client server interface blueprint is used
    /// by any of the configured ports
    /// </summary>
    /// <param name="csIfBlueprint"></param>
    /// <returns></returns>
    public bool IsCSIfBlueprintUsed(RteCSInterfaceBlueprint csIfBlueprint)
    {
      bool elementFound = false;

      foreach(RteAvailableServerPort port in AvailableServerPorts)
      {
        if(port.ServerPort.InterfaceBlueprint.UUID ==
          csIfBlueprint.InterfaceInfo.UUID)
        {
          // Interface blueprint in use
          elementFound = true;
          break;
        }
      }

      if(false == elementFound)
      {
        foreach (RteAvailableClientPort port in AvailableClientPorts)
        {
          if (port.ClientPort.InterfaceBlueprint.UUID ==
            csIfBlueprint.InterfaceInfo.UUID)
          {
            // Interface blueprint in use
            elementFound = true;
            break;
          }
        }
      }

      return elementFound;
    }

    /// <summary>
    /// Interface to check if the provided sender receiver interface blueprint is used
    /// by any of the configured ports
    /// </summary>
    /// <param name="srIfBlueprint"></param>
    /// <returns></returns>
    public bool IsSRIfBlueprintUsed(RteSRInterfaceBlueprint srIfBlueprint)
    {
      bool elementFound = false;

      foreach (RteAvailableSRPort port in AvailableSRPorts)
      {
        if (port.SenderReceiverPort.InterfaceBlueprint.UUID ==
          srIfBlueprint.InterfaceInfo.UUID)
        {
          // Interface blueprint in use
          elementFound = true;
          break;
        }
      }

      return elementFound;
    }

    /// <summary>
    /// Interface to check if the provided advanced data type receiver interface blueprint is used
    /// by any of the backup blueprint interfaces
    /// </summary>
    /// <param name="advacedDataType"></param>
    /// <returns></returns>
    public bool IsAdvancedDataTypeUsed(RteAdvancedDataTypeStructure advacedDataType)
    {
      bool elementFound = false;

      // Check client server blueprints
      foreach(RteCSInterfaceBlueprint blueprintIf in BackupCSBlueprintList)
      {
        //Check each function
        foreach(RteFunctionBlueprint fnc in blueprintIf.Functions)
        {
          // Check return value
          if(fnc.ReturnValue == advacedDataType.DataType)
          {
            elementFound = true;
            break;
          }

          // Check each argument
          foreach(RteFunctionArgumentBlueprint arg in fnc.ArgList)
          {
            if (arg.ArgumentType == advacedDataType.DataType)
            {
              elementFound = true;
              break;
            }
          }
        }

        if(true == elementFound)
        { break; }
      }

      // If data type was not fond jet continue with sender receiver ports
      if (false == elementFound)
      {
        // Check sender receiver blueprints
        foreach (RteSRInterfaceBlueprint blueprintIf in BackupSRBlueprintList)
        {

          // Check each argument
          foreach (RteElementBlueprint arg in blueprintIf.Elements)
          {
            if (arg.ElementType == advacedDataType.DataType)
            {
              elementFound = true;
              break;
            }
          }

          if (true == elementFound)
          { break; }
        }
      }

      return elementFound;
    }

    /// <summary>
    /// Interface to check if in the sever port prototype is available with the provided RteGeneralInterfaceInfo UUID
    /// </summary>
    /// <param name="checkInterface"></param>
    /// <param name="listIndex"></param>
    /// <returns></returns>
    public bool ContainServerPortPrototype(RteGeneralInterfaceInfo checkInterface, out int listIndex)
    {
      bool elementFound = false;
      listIndex = 0;

      if (0 != AvailableServerPorts.Count)
      {
        for (int idx = 0; idx < AvailableServerPorts.Count(); idx++)
        {
          if (AvailableServerPorts[idx].ServerPort.InterfacePrototype.UUID == checkInterface.UUID)
          {
            elementFound = true;
            listIndex = idx;
            break;
          }
        }
      }

      return elementFound;
    }

    /// <summary>
    /// Interface to check if in the client port prototype is available with the provided RteGeneralInterfaceInfo UUID
    /// </summary>
    /// <param name="checkInterface"></param>
    /// <param name="listIndex"></param>
    /// <returns></returns>
    public bool ContainClientPortPrototype(RteGeneralInterfaceInfo checkInterface, out int listIndex)
    {
      bool elementFound = false;
      listIndex = 0;

      if (0 != AvailableClientPorts.Count)
      {
        for (int idx = 0; idx < AvailableClientPorts.Count(); idx++)
        {
          if (AvailableClientPorts[idx].ClientPort.InterfacePrototype.UUID == checkInterface.UUID)
          {
            elementFound = true;
            listIndex = idx;
            break;
          }
        }
      }

      return elementFound;
    }

    /// <summary>
    /// Interface to check if in the Sender port prototype is available with the provided RteGeneralInterfaceInfo UUID
    /// </summary>
    /// <param name="checkInterface"></param>
    /// <param name="listIndex"></param>
    /// <returns></returns>
    public bool ContainSenderPortPrototype(RteGeneralInterfaceInfo checkInterface, out int listIndex)
    {
      bool elementFound = false;
      listIndex = 0;

      if (0 != AvailableSRPorts.Count)
      {
        for (int idx = 0; idx < AvailableSRPorts.Count(); idx++)
        {
          if (( AvailableSRPorts[idx].SenderReceiverPort.InterfacePrototype.UUID == checkInterface.UUID ) &&
                ( AvailableSRPorts[idx].SenderReceiverPort.InterfaceType == RteSRInterfaceImplementationType.SENDER ))
          {
            elementFound = true;
            listIndex = idx;
            break;
          }
        }
      }
      return elementFound;
    }

    /// <summary>
    /// Interface to check if in the Sender port prototype is available with the provided RteGeneralInterfaceInfo UUID
    /// </summary>
    /// <param name="checkInterface"></param>
    /// <param name="listIndex"></param>
    /// <returns></returns>
    public bool ContainReceiverPortPrototype(RteGeneralInterfaceInfo checkInterface, out int listIndex)
    {
      bool elementFound = false;
      listIndex = 0;

      if (0 != AvailableSRPorts.Count)
      {
        for (int idx = 0; idx < AvailableSRPorts.Count(); idx++)
        {
          if (( AvailableSRPorts[idx].SenderReceiverPort.InterfacePrototype.UUID == checkInterface.UUID ) &&
                ( AvailableSRPorts[idx].SenderReceiverPort.InterfaceType == RteSRInterfaceImplementationType.RECEIVER ))
          {
            elementFound = true;
            listIndex = idx;
            break;
          }
        }
      }

      return elementFound;
    }

    /// <summary>
    /// Interface to check if in the backup cs list is an interface with the provided RteGeneralInterfaceInfo UUID
    /// </summary>
    /// <param name="checkInterface"></param>
    /// <param name="listIndex"></param>
    /// <returns></returns>
    public bool ContainCSBlueprint(RteGeneralInterfaceInfo checkInterface, out int listIndex)
    {
      bool elementFound = false;
      listIndex = 0;

      if (0 != BackupCSBlueprintList.Count)
      {
        for (int idx = 0; idx < BackupCSBlueprintList.Count(); idx++)
        {
          if (BackupCSBlueprintList[idx].InterfaceInfo.UUID == checkInterface.UUID)
          {
            elementFound = true;
            listIndex = idx;
            break;
          }
        }
      }

      return elementFound;
    }

    /// <summary>
    /// Interface to check if in the backup sr list is an interface with the provided RteGeneralInterfaceInfo UUID
    /// </summary>
    /// <param name="uuid"></param>
    /// <param name="listIndex"></param>
    /// <returns></returns>
    public bool ContainSRBlueprint(RteGeneralInterfaceInfo checkInterface, out int listIndex)
    {
      bool elementFound = false;
      listIndex = 0;

      if (0 != BackupCSBlueprintList.Count)
      {
        for (int idx = 0; idx < BackupSRBlueprintList.Count(); idx++)
        {
          if (BackupSRBlueprintList[idx].InterfaceInfo.UUID == checkInterface.UUID)
          {
            elementFound = true;
            listIndex = idx;
            break;
          }
        }
      }

      return elementFound;
    }

    /// <summary>
    /// Interface to check if in the backup data type list is an data type with the RteGeneralInterfaceInfo UUID
    /// </summary>
    /// <param name="uuid"></param>
    /// <param name="listIndex"></param>
    /// <returns></returns>
    public bool ContainBackupDataType(RteGeneralInterfaceInfo checkInterface, out int listIndex)
    {
      bool elementFound = false;
      listIndex = 0;

      if (0 != BackupCSBlueprintList.Count)
      {
        for (int idx = 0; idx < BackupAdvancedDataTypeList.Count(); idx++)
        {
          if (BackupAdvancedDataTypeList[idx].DataType.UUID == checkInterface.UUID)
          {
            elementFound = true;
            listIndex = idx;
            break;
          }
        }
      }

      return elementFound;
    }
    #endregion

    #region Actions
    /**
      * @brief      Interface to validate the Rte Module Internal Behavior
      * @param      Reference to Rte Config data base
      * @param      Reference to Rte Types data base with all data types
      * @param      Reference to Rte Interface data base with all interfaces
      * @retval     List<string> >> String list with validation errors
      * @details    The validation function would check if each referenced data type and 
      *             interface blueprint / prototype for correctness.
      *             Check also if data types and interfaces from the backup lists are available in the
      *             data bases
      */
    public List<string> ValidateModuleInternalBehavior(
      ref RteConfig.RteConfig rteConfigDb,
      ref RteInterface.RteInterfaces rteInterfaceDb,
      ref RteTypes.RteTypes rteTypesDb)
    {
      List<string> validationErrors = new List<string>();
      string errorString = "";

      // Check internal behavior settings
      validationErrors.AddRange(ValidateModuleInternalBehavior());

      /* Validate backup client server blueprint list */
      foreach(RteCSInterfaceBlueprint interfaceBlueprint in backupCSBlueprintList)
      {
        if(false == rteInterfaceDb.IsCSInterfaceAvailable(interfaceBlueprint.InterfaceInfo))
        {
          errorString = "";
          errorString += "Validation error on Module Internal Behavior >> Backup Client Server Interface Blueprint \"" +
            interfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " + interfaceBlueprint.InterfaceInfo.UUID + 
            "\n  >> Not found in database!";
          validationErrors.Add(errorString);
        }
        else
        {
          if(false == rteInterfaceDb.ClientServerBlueprintList.Contains(interfaceBlueprint))
          {
            errorString = "";
            errorString += "Validation error on Module Internal Behavior >> Backup Client Server Interface Blueprint \"" +
              interfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " + interfaceBlueprint.InterfaceInfo.UUID +
              "\n  >> Out of sync with database!";
            validationErrors.Add(errorString);
          }
        }
      }

      /* Validate backup sender receiver blueprint list */
      foreach (RteSRInterfaceBlueprint interfaceBlueprint in backupSRBlueprintList)
      {
        if (false == rteInterfaceDb.IsSRInterfaceAvailable(interfaceBlueprint.InterfaceInfo))
        {
          errorString = "";
          errorString += "Validation error on Module Internal Behavior >> Backup Sender Receiver Interface Blueprint \"" +
            interfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " + interfaceBlueprint.InterfaceInfo.UUID +
            "\n  >> Not found in database!";
          validationErrors.Add(errorString);
        }
        else
        {
          if (false == rteInterfaceDb.SenderReceiverBlueprintList.Contains(interfaceBlueprint))
          {
            errorString = "";
            errorString += "Validation error on Module Internal Behavior >> Backup Sender Receiver Interface Blueprint \"" +
              interfaceBlueprint.InterfaceInfo.InterfaceName + "\" with UUID: " + interfaceBlueprint.InterfaceInfo.UUID +
              "\n  >> Out of sync with database!";
            validationErrors.Add(errorString);
          }
        }
      }

      /* Validate backup types list */
      foreach (RteAdvancedDataTypeStructure advancedDataType in backupAdvancedDataTypeList)
      {
        if (false == rteTypesDb.IsDataTypeAvailable(advancedDataType.DataType))
        {
          errorString = "";
          errorString += "Validation error on Module Internal Behavior >> Backup Data Type \"" +
            advancedDataType.DataType.DataTypeName + "\" with UUID: " + advancedDataType.DataType.UUID +
            "\n  >> Not found in database!";
          validationErrors.Add(errorString);
        }
        else
        {
          if (false == rteTypesDb.AdvancedDataTypeList.Contains(advancedDataType))
          {
            errorString = "";
            errorString += "Validation error on Module Internal Behavior >> Backup Data Type \"" +
              advancedDataType.DataType.DataTypeName + "\" with UUID: " + advancedDataType.DataType.UUID +
              "\n  >> Out of sync with database!";
            validationErrors.Add(errorString);
          }
        }
      }

      return validationErrors;
    }

    /**
      * @brief      Interface to validate the Rte Module Internal Behavior
      * @retval     List<string> >> String list with validation errors
      * @details    The validation function would check if each referenced data type and 
      *             interface blueprint / prototype for correctness.
      */
    public List<string> ValidateModuleInternalBehavior( )
    {
      List<string> validationErrors = new List<string>();
      string errorString = "";

      /* Check Module Name */
      if ("" == ModuleName)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior  \"" +
          ModuleName + "\" with UUID: " + UUID + "\n  >> Module Name not set!";
        validationErrors.Add(errorString);
      }

      //Validate interface name
      if (false == regex.IsMatch(ModuleName))
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior \"" +
          ModuleName + "\" with UUID: " + UUID + "\n  >> Module name \"" +
          ModuleName + "\" contains invalid characters!";
        validationErrors.Add(errorString);
      }

      /* Check if UUID is set */
      if ("" == UUID)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior \"" +
          ModuleName + "\" with UUID: " + UUID + "\n";
        errorString += "  >> Module Internal Behavior UUID not set!";
        validationErrors.Add(errorString);
      }

      /* Validate init runnables */
      foreach (RteRunnable runnable in InitRunnables)
      {
        validationErrors.AddRange(ValidateModuleInternalBehavior_RteRunnables(
          runnable));
      }

      /* Validate cyclic runnables */
      foreach (RteCyclicRunnable runnable in CyclicRunnables)
      {
        validationErrors.AddRange(ValidateModuleInternalBehavior_CyclicRunnables(
          runnable));
      }

      /* Validate available sender receiver ports */
      foreach (RteAvailableSRPort sRPort in availableSRPorts)
      {
        validationErrors.AddRange(ValidateModuleInternalBehavior_AvailableSRPort(
          sRPort));
      }


      /* Validate available server ports */
      foreach (RteAvailableServerPort serverPort in availableServerPorts)
      {
        validationErrors.AddRange(ValidateModuleInternalBehavior_AvailableServerPort(
          serverPort));
      }

      /* Validate available client ports */
      foreach (RteAvailableClientPort clientPort in availableClientPorts)
      {
        validationErrors.AddRange(ValidateModuleInternalBehavior_AvailableClientPort(
          clientPort));
      }

      return validationErrors;
    }


    #region Sub-Validation Functions

    /**
      * @brief      Interface to validate the Rte Module Internal Behavior Init-Runnable-Element
      * @param      Rte runnable to be evaluated
      * @details    The validation function would check if the naming are correct and the assigned 
      *             elements are available
      */
    private List<string> ValidateModuleInternalBehavior_RteRunnables( RteRunnable runnable )
    {
      string errorString = "";
      List<string> validationErrors = new List<string>();

      // Check if runnable name is set
      if ("" == runnable.RunnableInfo.InterfaceName)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Init-Runnable \"" +
          runnable.RunnableInfo.InterfaceName + "\" with UUID: " + runnable.RunnableInfo.UUID + "\n  >> Runnable name not set!";
        validationErrors.Add(errorString);
      }

      //Validate interface name
      if (false == regex.IsMatch(runnable.RunnableInfo.InterfaceName))
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Rte-Runnable \"" +
          runnable.RunnableInfo.InterfaceName + "\" with UUID: " + runnable.RunnableInfo.UUID + "\n  >> Runnable name \"" +
          runnable.RunnableInfo.InterfaceName + "\" contains invalid characters!";
        validationErrors.Add(errorString);
      }

      /* Check if UUID is set */
      if ("" == runnable.RunnableInfo.UUID)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Rte-Runnable \"" +
          runnable.RunnableInfo.InterfaceName + "\" with UUID: " + runnable.RunnableInfo.UUID + "\n";
        errorString += "  >> Runnable UUID not set!";
        validationErrors.Add(errorString);
      }

      // Check if reference client port access exists
      foreach(RteGenericRunnableObject runnableObject in runnable.AccessToClientPortFunctions)
      {
        if(false == IsClientRunnableObjectAvailableAndUsable(runnableObject))
        {
          errorString = "";
          errorString += "Validation error on Module Internal Behavior >> Rte-Runnable \"" +
            runnable.RunnableInfo.InterfaceName + "\" with UUID: " + runnable.RunnableInfo.UUID + "\n";
          errorString += "  >> Referenced access to client port function not possible >> Client port function not available : \""
            + runnableObject.Name + "\"";
          validationErrors.Add(errorString);
        }
      }

      // Check if reference sender receiver port access exists
      foreach (RteGenericRunnableObject runnableObject in runnable.AccessToSenderReceiverPortElements)
      {
        if (false == IsSRRunnableObjectAvailableAndUsable(runnableObject))
        {
          errorString = "";
          errorString += "Validation error on Module Internal Behavior >> Rte-Runnable \"" +
            runnable.RunnableInfo.InterfaceName + "\" with UUID: " + runnable.RunnableInfo.UUID + "\n";
          errorString += "  >> Referenced access to sender-receiver port element not possible >> "+
            "Sender receiver port element not available : \""
            + runnableObject.Name + "\"";
          validationErrors.Add(errorString);
        }
      }

      return validationErrors;
    }

    /**
      * @brief      Interface to validate the Rte Module Internal Behavior Cyclic-Runnable-Element
      * @param      Rte runnable to be evaluated
      * @retval     List<string> >> String list with validation errors
      * @details    The validation function would check if the naming are correct and the assigned 
      *             elements are available
      */
    private List<string> ValidateModuleInternalBehavior_CyclicRunnables( RteCyclicRunnable runnable )
    {
      string errorString = "";
      List<string> validationErrors = new List<string>();

      // Check runnable cycle time
      if (0 == runnable.CycleTime)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Cyclic-Runnable \"" +
          runnable.Runnable.RunnableInfo.InterfaceName + "\" with UUID: " + runnable.Runnable.RunnableInfo.UUID + 
          "\n  >> Runnable cycle time 0 zero!";
        validationErrors.Add(errorString);
      }

      validationErrors.AddRange(ValidateModuleInternalBehavior_RteRunnables(
        runnable.Runnable ));

      return validationErrors;
    }

    /**
      * @brief      Interface to validate the Rte Module Internal Behavior Available Sender-Receiver-Port
      * @param      Available SR Port to be evaluated to be evaluated
      * @retval     List<string> >> String list with validation errors
      * @details    The validation function would check if the naming are correct and the assigned 
      *             elements are available
      */
    private List<string> ValidateModuleInternalBehavior_AvailableSRPort( RteAvailableSRPort availablePort )
    {
      string errorString = "";
      List<string> validationErrors = new List<string>();

      /* Validate Port Prototype */
      // Check if port name is set
      if ("" == availablePort.SenderReceiverPort.InterfacePrototype.InterfaceName)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Available Sender-Receiver-Port \"" +
          availablePort.SenderReceiverPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
          availablePort.SenderReceiverPort.InterfacePrototype.UUID + "\n  >> Port name not set!";
        validationErrors.Add(errorString);
      }

      //Validate port name
      if (false == regex.IsMatch(availablePort.SenderReceiverPort.InterfacePrototype.InterfaceName))
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Available Sender-Receiver-Port \"" +
          availablePort.SenderReceiverPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
          availablePort.SenderReceiverPort.InterfacePrototype.UUID + "\n  >> Port name \"" +
          availablePort.SenderReceiverPort.InterfacePrototype.InterfaceName + "\" contains invalid characters!";
        validationErrors.Add(errorString);
      }

      /* Check if UUID is set */
      if ("" == availablePort.SenderReceiverPort.InterfacePrototype.UUID)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Available Sender-Receiver-Port \"" +
          availablePort.SenderReceiverPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
          availablePort.SenderReceiverPort.InterfacePrototype.UUID + "\n";
        errorString += "  >> Port UUID not set!";
        validationErrors.Add(errorString);
      }

      // Check if interface blueprint is available
      bool blueprintFound = false;
      RteSRInterfaceBlueprint foundPortBlueprint = new RteSRInterfaceBlueprint();
      foreach (RteSRInterfaceBlueprint sRInterfaceBlueprint in backupSRBlueprintList)
      {
        if( ( sRInterfaceBlueprint.InterfaceInfo.UUID == availablePort.SenderReceiverPort.InterfaceBlueprint.UUID ) &&
            ( sRInterfaceBlueprint.InterfaceInfo.InterfaceName == availablePort.SenderReceiverPort.InterfaceBlueprint.InterfaceName ))
        {
          blueprintFound = true;
        }
      }
      if(false == blueprintFound)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Available Sender-Receiver-Port \"" +
          availablePort.SenderReceiverPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
          availablePort.SenderReceiverPort.InterfacePrototype.UUID + "\n";
        errorString += "  >> Port Blueprint not found! Configured Port Blueprint Name \"" +
          availablePort.SenderReceiverPort.InterfaceBlueprint.InterfaceName + "\" with UUID: " +
          availablePort.SenderReceiverPort.InterfaceBlueprint.UUID;
        validationErrors.Add(errorString);
      }

      // Check port element
      if (true == blueprintFound)
      {
        bool functionFound = false;
        foreach (RteGenericRunnableObject portElement in availablePort.SenderReceiverPortElements)
        {
          functionFound = false;

          foreach (RteElementBlueprint element in foundPortBlueprint.Elements)
          {
            if (( element.ElementName == portElement.Name ) &&
                ( element.UUID == portElement.UUID ))
            {
              functionFound = true;
              break;
            }

            // Check if found has been found
            if (false == functionFound)
            {
              errorString = "";
              errorString += "Validation error on Module Internal Behavior >> Available Sender-Receiver-Port \"" +
                availablePort.SenderReceiverPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
                availablePort.SenderReceiverPort.InterfacePrototype.UUID + "\n";
              errorString += "  >> Port element not found! Configured port element name \"" +
                portElement.Name + "\" with UUID: " + portElement.UUID;
              validationErrors.Add(errorString);
            }
          }
        }
      }

      return validationErrors;
    }

    /**
      * @brief      Interface to validate the Rte Module Internal Behavior Available Server-Port
      * @param      Available Server Port to be evaluated to be evaluated
      * @details    The validation function would check if the naming are correct and the assigned 
      *             elements are available
      */
    private List<string> ValidateModuleInternalBehavior_AvailableServerPort( RteAvailableServerPort availablePort )
    {
      string errorString = "";
      List<string> validationErrors = new List<string>();

      /* Validate Port Prototype */
      // Check if port name is set
      if ("" == availablePort.ServerPort.InterfacePrototype.InterfaceName)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Available Server-Port \"" +
          availablePort.ServerPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
          availablePort.ServerPort.InterfacePrototype.UUID + "\n  >> Port name not set!";
        validationErrors.Add(errorString);
      }

      //Validate port name
      if (false == regex.IsMatch(availablePort.ServerPort.InterfacePrototype.InterfaceName))
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Available Server-Port \"" +
          availablePort.ServerPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
          availablePort.ServerPort.InterfacePrototype.UUID + "\n  >> Port name \"" +
          availablePort.ServerPort.InterfacePrototype.InterfaceName + "\" contains invalid characters!";
        validationErrors.Add(errorString);
      }

      /* Check if UUID is set */
      if ("" == availablePort.ServerPort.InterfacePrototype.UUID)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Available Server-Port \"" +
          availablePort.ServerPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
          availablePort.ServerPort.InterfacePrototype.UUID + "\n";
        errorString += "  >> Port UUID not set!";
        validationErrors.Add(errorString);
      }

      // Check if interface blueprint is available
      bool blueprintFound = false;
      RteCSInterfaceBlueprint foundPortBlueprint = new RteCSInterfaceBlueprint();
      foreach (RteCSInterfaceBlueprint cSInterfaceBlueprint in backupCSBlueprintList)
      {
        if (( cSInterfaceBlueprint.InterfaceInfo.UUID == availablePort.ServerPort.InterfaceBlueprint.UUID ) &&
            ( cSInterfaceBlueprint.InterfaceInfo.InterfaceName == availablePort.ServerPort.InterfaceBlueprint.InterfaceName ))
        {
          blueprintFound = true;
          foundPortBlueprint = cSInterfaceBlueprint;
        }
      }
      if (false == blueprintFound)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Available Server-Port \"" +
          availablePort.ServerPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
          availablePort.ServerPort.InterfacePrototype.UUID + "\n";
        errorString += "  >> Port Blueprint not found! Configured Port Blueprint Name \"" +
          availablePort.ServerPort.InterfaceBlueprint.InterfaceName + "\" with UUID: " +
          availablePort.ServerPort.InterfaceBlueprint.UUID;
        validationErrors.Add(errorString);
      }

      // Check port runnables
      if (true == blueprintFound)
      {
        bool functionFound = false;
        foreach (RteRunnable portRunnable in availablePort.ServerPortRunnables)
        {
          functionFound = false;

          foreach (RteFunctionBlueprint function in foundPortBlueprint.Functions)
          {
            if (( function.FunctionName == portRunnable.RunnableInfo.InterfaceName )) // Check only function name >> Blueprint and prototype UUID are different
            {
              functionFound = true;
              break;
            }
          }

          // Check if function has been found
          if (false == functionFound)
          {
            errorString = "";
            errorString += "Validation error on Module Internal Behavior >> Available Server-Port \"" +
              availablePort.ServerPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
              availablePort.ServerPort.InterfacePrototype.UUID + "\n";
            errorString += "  >> Port runnable not found! Configured port runnable name \"" +
              portRunnable.RunnableInfo.InterfaceName + "\" with UUID: " + portRunnable.RunnableInfo.UUID;
            validationErrors.Add(errorString);
          }
        }
      }

      return validationErrors;
    }

    /**
      * @brief      Interface to validate the Rte Module Internal Behavior Available Client-Port
      * @param      Available Client Port to be evaluated to be evaluated
      * @retval     List<string> >> String list with validation errors
      * @details    The validation function would check if the naming are correct and the assigned 
      *             elements are available
      */
    private List<string> ValidateModuleInternalBehavior_AvailableClientPort( RteAvailableClientPort availablePort )
    {
      string errorString = "";
      List<string> validationErrors = new List<string>();

      /* Validate Port Prototype */
      // Check if port name is set
      if ("" == availablePort.ClientPort.InterfacePrototype.InterfaceName)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Available Client-Port \"" +
          availablePort.ClientPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
          availablePort.ClientPort.InterfacePrototype.UUID + "\n  >> Port name not set!";
        validationErrors.Add(errorString);
      }

      //Validate port name
      if (false == regex.IsMatch(availablePort.ClientPort.InterfacePrototype.InterfaceName))
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Available Client-Port \"" +
          availablePort.ClientPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
          availablePort.ClientPort.InterfacePrototype.UUID + "\n  >> Port name \"" +
          availablePort.ClientPort.InterfacePrototype.InterfaceName + "\" contains invalid characters!";
        validationErrors.Add(errorString);
      }

      /* Check if UUID is set */
      if ("" == availablePort.ClientPort.InterfacePrototype.UUID)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Available Client-Port \"" +
          availablePort.ClientPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
          availablePort.ClientPort.InterfacePrototype.UUID + "\n";
        errorString += "  >> Port UUID not set!";
        validationErrors.Add(errorString);
      }

      // Check if interface blueprint is available
      bool blueprintFound = false;
      RteCSInterfaceBlueprint foundPortBlueprint = new RteCSInterfaceBlueprint();
      foreach (RteCSInterfaceBlueprint cSInterfaceBlueprint in backupCSBlueprintList)
      {
        if (( cSInterfaceBlueprint.InterfaceInfo.UUID == availablePort.ClientPort.InterfaceBlueprint.UUID ) &&
            ( cSInterfaceBlueprint.InterfaceInfo.InterfaceName == availablePort.ClientPort.InterfaceBlueprint.InterfaceName ))
        {
          blueprintFound = true;
          foundPortBlueprint = cSInterfaceBlueprint;
        }
      }
      if (false == blueprintFound)
      {
        errorString = "";
        errorString += "Validation error on Module Internal Behavior >> Available Client-Port \"" +
          availablePort.ClientPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
          availablePort.ClientPort.InterfacePrototype.UUID + "\n";
        errorString += "  >> Port Blueprint not found! Configured Port Blueprint Name \"" +
          availablePort.ClientPort.InterfaceBlueprint.InterfaceName + "\" with UUID: " +
          availablePort.ClientPort.InterfaceBlueprint.UUID;
        validationErrors.Add(errorString);
      }


      // Check port functions
      if (true == blueprintFound)
      {
        bool functionFound = false;
        foreach (RteGenericRunnableObject portFunction in availablePort.ClientPortFunctions)
        {
          functionFound = false;

          foreach (RteFunctionBlueprint function in foundPortBlueprint.Functions)
          {
            if( ( function.FunctionName == portFunction.Name) ) // Check only function name >> Blueprint and prototype UUID are different
            {
              functionFound = true;
              break;
            }
          }

          // Check if found has been found
          if (false == functionFound)
          {
            errorString = "";
            errorString += "Validation error on Module Internal Behavior >> Available Client-Port \"" +
              availablePort.ClientPort.InterfacePrototype.InterfaceName + "\" with UUID: " +
              availablePort.ClientPort.InterfacePrototype.UUID + "\n";
            errorString += "  >> Port function not found! Configured port function name \"" +
              portFunction.Name + "\" with UUID: " + portFunction.UUID;
            validationErrors.Add(errorString);
          }
        }
      }

      return validationErrors;
    }

    #endregion

    /*
    * @brief       Function to store the current rte module internal behavior data to an xml file
    * @param       string Path to Rte module internal behavior file
    * @retval      none
    */
    public void SaveActiveRteModuleInternalBehaviorToXml(string pathToXmlFile)
    {
      XmlSerializer writer = new XmlSerializer(this.GetType());
      StreamWriter file = new StreamWriter(pathToXmlFile);
      writer.Serialize(file, this);
      file.Close();
    }

    /*
     * @brief       Function to read the current rte module internal behavior data to an xml file
     * @param       string Path to Rte module internal behavior file
     * @retval      none
     */
    public void ReadActiveRteModuleInternalBehaviorToXml(string pathToXmlFile)
    {
      RteModuleInternalBehavior tempModuleInternalbehavior = new RteModuleInternalBehavior();
      XmlSerializer reader = new XmlSerializer(this.GetType());
      StreamReader file = new StreamReader(pathToXmlFile);
      tempModuleInternalbehavior = (RteModuleInternalBehavior)reader.Deserialize(file);

      this.xmlFileVersion = tempModuleInternalbehavior.xmlFileVersion;
      this.ModuleName = tempModuleInternalbehavior.ModuleName;
      this.ModuleType = tempModuleInternalbehavior.ModuleType;
      this.initRunnables = tempModuleInternalbehavior.initRunnables;
      this.cyclicRunnables = tempModuleInternalbehavior.cyclicRunnables;
      this.availableServerPorts = tempModuleInternalbehavior.availableServerPorts;
      this.availableClientPorts = tempModuleInternalbehavior.availableClientPorts;
      this.availableSRPorts = tempModuleInternalbehavior.availableSRPorts;
      this.backupCSBlueprintList = tempModuleInternalbehavior.backupCSBlueprintList;
      this.backupSRBlueprintList = tempModuleInternalbehavior.backupSRBlueprintList;
      this.backupAdvancedDataTypeList = tempModuleInternalbehavior.backupAdvancedDataTypeList;

      file.Close();
    }
    #endregion
  }
}
/**
* @}
*/
