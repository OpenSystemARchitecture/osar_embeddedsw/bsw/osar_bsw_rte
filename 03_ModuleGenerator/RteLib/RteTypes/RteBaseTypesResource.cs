﻿/*****************************************************************************************************************************
 * @file        RteBaseTypesResource.cs                                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the constant base types resource                                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RteLib.RteResources;

namespace RteLib.RteTypes
{
  /****************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
  /****************************************************************************************************************************/
  /// <summary>
  /// Class which provides the base data types as usable attribute
  /// </summary>
  public class RteBaseTypesResource
  {
    RteBaseDataTypeStructure uint8_Type;
    RteBaseDataTypeStructure uint16_Type;
    RteBaseDataTypeStructure uint32_Type;
    RteBaseDataTypeStructure uint64_Type;

    RteBaseDataTypeStructure sint8_Type;
    RteBaseDataTypeStructure sint16_Type;
    RteBaseDataTypeStructure sint32_Type;
    RteBaseDataTypeStructure sint64_Type;

    RteBaseDataTypeStructure float32_Type;
    RteBaseDataTypeStructure float64_Type;

    RteBaseDataTypeStructure boolean_Type;

    RteBaseDataTypeStructure void_Type;

    RteAdvancedDataTypeStructure rte_ErrorType_Type;

    /// <summary>
    /// Constructor
    /// </summary>
    public RteBaseTypesResource()
    {
      uint8_Type = new RteBaseDataTypeStructure();
      uint8_Type.DataAccessType = RteDataAccessType.STANDARD;
      uint8_Type.DataTypeName = "uint8";
      uint8_Type.UUID = RteDefinedElementUUIDs.RteDataTypeUint8;

      sint8_Type = new RteBaseDataTypeStructure();
      sint8_Type.DataAccessType = RteDataAccessType.STANDARD;
      sint8_Type.DataTypeName = "sint8";
      sint8_Type.UUID = RteDefinedElementUUIDs.RteDataTypeSint8;


      uint16_Type = new RteBaseDataTypeStructure();
      uint16_Type.DataAccessType = RteDataAccessType.STANDARD;
      uint16_Type.DataTypeName = "uint16";
      uint16_Type.UUID = RteDefinedElementUUIDs.RteDataTypeUint16;

      sint16_Type = new RteBaseDataTypeStructure();
      sint16_Type.DataAccessType = RteDataAccessType.STANDARD;
      sint16_Type.DataTypeName = "sint16";
      sint16_Type.UUID = RteDefinedElementUUIDs.RteDataTypeSint16;



      uint32_Type = new RteBaseDataTypeStructure();
      uint32_Type.DataAccessType = RteDataAccessType.STANDARD;
      uint32_Type.DataTypeName = "uint32";
      uint32_Type.UUID = RteDefinedElementUUIDs.RteDataTypeUint32;

      sint32_Type = new RteBaseDataTypeStructure();
      sint32_Type.DataAccessType = RteDataAccessType.STANDARD;
      sint32_Type.DataTypeName = "sint32";
      sint32_Type.UUID = RteDefinedElementUUIDs.RteDataTypeSint32;


      uint64_Type = new RteBaseDataTypeStructure();
      uint64_Type.DataAccessType = RteDataAccessType.STANDARD;
      uint64_Type.DataTypeName = "uint64";
      uint64_Type.UUID = RteDefinedElementUUIDs.RteDataTypeUint64;

      sint64_Type = new RteBaseDataTypeStructure();
      sint64_Type.DataAccessType = RteDataAccessType.STANDARD;
      sint64_Type.DataTypeName = "sint64";
      sint64_Type.UUID = RteDefinedElementUUIDs.RteDataTypeSint64;


      float32_Type = new RteBaseDataTypeStructure();
      float32_Type.DataAccessType = RteDataAccessType.STANDARD;
      float32_Type.DataTypeName = "float32";
      float32_Type.UUID = RteDefinedElementUUIDs.RteDataTypeFloat32;

      float64_Type = new RteBaseDataTypeStructure();
      float64_Type.DataAccessType = RteDataAccessType.STANDARD;
      float64_Type.DataTypeName = "float64";
      float64_Type.UUID = RteDefinedElementUUIDs.RteDataTypeFloat64;


      boolean_Type = new RteBaseDataTypeStructure();
      boolean_Type.DataAccessType = RteDataAccessType.STANDARD;
      boolean_Type.DataTypeName = "boolean";
      boolean_Type.UUID = RteDefinedElementUUIDs.RteDataTypeBoolean;


      void_Type = new RteBaseDataTypeStructure();
      void_Type.DataAccessType = RteDataAccessType.STANDARD;
      void_Type.DataTypeName = "void";
      void_Type.UUID = RteDefinedElementUUIDs.RteDataTypeVoid;


      rte_ErrorType_Type = new RteAdvancedDataTypeStructure();
      rte_ErrorType_Type.BaseDataType = Uint16_Type;
      rte_ErrorType_Type.DataType.DataAccessType = RteDataAccessType.STANDARD;
      rte_ErrorType_Type.DataType.DataTypeName = "Rte_ErrorType";
      rte_ErrorType_Type.DataType.UUID = RteDefinedElementUUIDs.RteDataTypeRteErrorType;
      rte_ErrorType_Type.DataTypeType = RteAdvancedDataTypeType.STANDARD;
    }

    #region Attribute Interface
    public RteBaseDataTypeStructure Uint8_Type { get => uint8_Type; }
    public RteBaseDataTypeStructure Uint16_Type { get => uint16_Type; }
    public RteBaseDataTypeStructure Uint32_Type { get => uint32_Type; }
    public RteBaseDataTypeStructure Uint64_Type { get => uint64_Type; }
    public RteBaseDataTypeStructure Sint8_Type { get => sint8_Type; }
    public RteBaseDataTypeStructure Sint16_Type { get => sint16_Type; }
    public RteBaseDataTypeStructure Sint32_Type { get => sint32_Type; }
    public RteBaseDataTypeStructure Sint64_Type { get => sint64_Type; }
    public RteBaseDataTypeStructure Float32_Type { get => float32_Type; }
    public RteBaseDataTypeStructure Float64_Type { get => float64_Type; }
    public RteBaseDataTypeStructure Boolean_Type { get => boolean_Type; }
    public RteBaseDataTypeStructure Void_Type { get => void_Type; }
    public RteAdvancedDataTypeStructure Rte_ErrorType_Type { get => rte_ErrorType_Type; }
    #endregion
  }
}
