﻿/*****************************************************************************************************************************
 * @file        RteDataTypeStructure.cs                                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        05.01.2019                                                                                                   *
 * @brief       Definition of the RTE Data Type Structure                                                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RteLib.RteTypes
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RteLib.RteTypes
{

  /**
  * @brief      RTE Enumeration of the different RTE Data Access Types
  * @note       STDANDARD               >> <dataType>
  *             POINTER                 >> <dataType>*
  *             CONST_POINTER           >> <dataType>* const
  *             POINTER_TO_CONST        >> const <dataType>*
  *             CONST_POINTER_TO_CONST  >> const <dataType>* const
  */
  public enum RteDataAccessType
  {
    STANDARD,
    POINTER,
    CONST_POINTER,
    POINTER_TO_CONST,
    CONST_POINTER_TO_CONST
  }

  /**
  * @brief      RTE Base Data Type Structure
  * @note       This class is used to identify the different data sets.
  *             It would also be used for the standard data types.
  * @details    Including the unique ID and the data type name
  */
  public class RteBaseDataTypeStructure : IEquatable<RteBaseDataTypeStructure>
  {
    private string uuid = "";
    private string dataTypeName = "";
    private RteDataAccessType dataAccessType = new RteDataAccessType();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteBaseDataTypeStructure() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteBaseDataTypeStructure(RteBaseDataTypeStructure obj)
    {
      this.UUID = obj.UUID;
      this.DataTypeName = obj.DataTypeName;
      this.DataAccessType = obj.DataAccessType;
    }

    #region Attributes
    public string UUID
    {
      get { return uuid; }
      set { uuid = value; }
    }

    public string DataTypeName
    {
      get { return dataTypeName; }
      set { dataTypeName = value; }
    }

    public RteDataAccessType DataAccessType
    {
      get { return dataAccessType; }
      set { dataAccessType = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteBaseDataTypeStructure lhs, RteBaseDataTypeStructure rhs)
    {
      bool retVal = false;

      if((lhs.UUID == rhs.UUID) && ( lhs.DataTypeName == rhs.DataTypeName ) && ( lhs.DataAccessType == rhs.DataAccessType ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteBaseDataTypeStructure lhs, RteBaseDataTypeStructure rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteBaseDataTypeStructure rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE Enumeration of the different RTE Advances Data Types
  * @note       Standard    >> Redefinition of an existing data type
  *             Struct      >> Definition of an new struct data type
  *             Enumeration >> Definition of an new enum data type
  */
  public enum RteAdvancedDataTypeType
  {
    STANDARD,
    STRUCT,
    ENUMERATION
  }

  /**
  * @brief      RTE advanced structure data type structure
  * @note       Including the base data type and the struct element name
  */
  public class RteAdvancedStructDataTypeStructure : IEquatable<RteAdvancedStructDataTypeStructure>
  {
    private RteBaseDataTypeStructure baseDataType = new RteBaseDataTypeStructure();
    private string structElementName = "";

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteAdvancedStructDataTypeStructure() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteAdvancedStructDataTypeStructure(RteAdvancedStructDataTypeStructure obj)
    {
      this.BaseDataType = new RteBaseDataTypeStructure(obj.BaseDataType);
      this.StructElementName = obj.StructElementName;
    }

    #region Attributes
    
    public RteBaseDataTypeStructure BaseDataType
    {
      get { return baseDataType; }
      set { baseDataType = value; }
    }

    
    public string StructElementName
    {
      get { return structElementName; }
      set { structElementName = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteAdvancedStructDataTypeStructure lhs, RteAdvancedStructDataTypeStructure rhs)
    {
      bool retVal = false;

      if (( lhs.BaseDataType == rhs.BaseDataType ) && ( lhs.StructElementName == rhs.StructElementName ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteAdvancedStructDataTypeStructure lhs, RteAdvancedStructDataTypeStructure rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteAdvancedStructDataTypeStructure rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE advanced enumeration data type structure
  * @note       Including the base data type and the enumeration element name 
  *             and an additional enumeration id
  */
  public class RteAdvancedEnumDataTypeStructure : IEquatable<RteAdvancedEnumDataTypeStructure>
  {
    private string enumElementName = "";
    private string enumId = "";

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteAdvancedEnumDataTypeStructure() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteAdvancedEnumDataTypeStructure(RteAdvancedEnumDataTypeStructure obj)
    {
      this.enumElementName = obj.enumElementName;
      this.EnumId = obj.EnumId;
    }

    #region Attributes
    
    public string EnumElementName
    {
      get { return enumElementName; }
      set { enumElementName = value; }
    }

    
    public string EnumId
    {
      get { return enumId; }
      set { enumId = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteAdvancedEnumDataTypeStructure lhs, RteAdvancedEnumDataTypeStructure rhs)
    {
      bool retVal = false;

      if (( lhs.enumElementName == rhs.enumElementName ) && ( lhs.EnumId == rhs.EnumId ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteAdvancedEnumDataTypeStructure lhs, RteAdvancedEnumDataTypeStructure rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteAdvancedEnumDataTypeStructure rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }

  /**
  * @brief      RTE advanced data type structure
  * @note       Definition structure of an advances data type
  *             Type selection is done by the dataTypeType
  */
  public class RteAdvancedDataTypeStructure : IEquatable<RteAdvancedDataTypeStructure>
  {
    private RteBaseDataTypeStructure dataType = new RteBaseDataTypeStructure();
    private RteAdvancedDataTypeType dataTypeType = new RteAdvancedDataTypeType();
    private RteBaseDataTypeStructure baseDataType = new RteBaseDataTypeStructure();
    private List<RteAdvancedStructDataTypeStructure> structElements = new List<RteAdvancedStructDataTypeStructure>();
    private List<RteAdvancedEnumDataTypeStructure> enumElements = new List<RteAdvancedEnumDataTypeStructure>();

    /// <summary>
    /// Default Constructor
    /// </summary>
    public RteAdvancedDataTypeStructure() { }

    /// <summary>
    /// Copy Constructor
    /// </summary>
    /// <param name="obj"></param>
    public RteAdvancedDataTypeStructure(RteAdvancedDataTypeStructure obj)
    {
      this.DataType = new RteBaseDataTypeStructure(obj.DataType);
      this.DataTypeType = obj.DataTypeType;
      this.BaseDataType = new RteBaseDataTypeStructure(obj.BaseDataType);

      this.StructElements = new List<RteAdvancedStructDataTypeStructure>(obj.StructElements);
      this.EnumElements = new List<RteAdvancedEnumDataTypeStructure>(obj.EnumElements);
    }

    #region Attributes
    
    public RteBaseDataTypeStructure DataType
    {
      get { return dataType; }
      set { dataType = value; }
    }

    
    public RteAdvancedDataTypeType DataTypeType
    {
      get { return dataTypeType; }
      set { dataTypeType = value; }
    }

    
    public RteBaseDataTypeStructure BaseDataType
    {
      get { return baseDataType; }
      set { baseDataType = value; }
    }

    
    public List<RteAdvancedStructDataTypeStructure> StructElements
    {
      get { return structElements; }
      set { structElements = value; }
    }

    
    public List<RteAdvancedEnumDataTypeStructure> EnumElements
    {
      get { return enumElements; }
      set { enumElements = value; }
    }

    // Custom Elements which are useful
    
    public string DataTypeName
    {
      get { return dataType.DataTypeName; }
      set { dataType.DataTypeName = value; }
    }
    #endregion

    #region Operator Overload
    /**
     * @brief     Equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects equal >> false == objects not equal
     */
    public static bool operator ==(RteAdvancedDataTypeStructure lhs, RteAdvancedDataTypeStructure rhs)
    {
      bool retVal = false;

      if (( lhs.DataType == rhs.DataType )                                  &&
          ( lhs.DataTypeType == rhs.DataTypeType )                          &&
          ( lhs.BaseDataType == rhs.BaseDataType )                          &&
          ( true == lhs.StructElements.SequenceEqual(rhs.StructElements) )  &&
          ( true == lhs.EnumElements.SequenceEqual(rhs.EnumElements) ))
      { retVal = true; }
      else
      { retVal = false; }

      return retVal;
    }

    /**
     * @brief     Not equal operator overload
     * @param[in] lhs: Left hand side to be checked
     * @param[in] rhs: Right hand side to be checked
     * @return    bool: true == objects not equal >> false == objects equal
     */
    public static bool operator !=(RteAdvancedDataTypeStructure lhs, RteAdvancedDataTypeStructure rhs)
    {
      return !( lhs == rhs );
    }

    /**
     * @brief     Implement interface equal operator to check if the content of objects is equal.
     * @param[in] rhs: object to be compared
     * @return    bool: true if objects are same instance
     * @details   Implementation of this interface to be .Net compatible
     */
    public bool Equals(RteAdvancedDataTypeStructure rhs)
    {
      return this == rhs;
    }

    /**
     * @brief     Override equal operator to check if the instance of the objects is the same
     * @param[in] obj: Object to be checked
     * @return    bool: true if objects are same instance
     */
    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }

    /**
     * @brief     Override hash code function
     * @return    int: hash code
     */
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
    #endregion
  }
}
/**
* @}
*/
