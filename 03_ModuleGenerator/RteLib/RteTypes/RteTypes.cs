﻿/*****************************************************************************************************************************
 * @file        RteTypes.cs                                                                                                  *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        05.01.2019                                                                                                   *
 * @brief       Definition of the RTE data types                                                                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RteLib.RteTypes
* @{
*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using OsarResources.XML;
using OsarResources.Generic;
using System.Text.RegularExpressions;
using RteLib.RteResources;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RteLib.RteTypes
{
  /**
  * @brief      RTE Types File Structure
  * @note       This class is used for the Types File Structure with additional functionalities
  */
  public class RteTypes
  {
    private XmlFileVersion xmlFileVersion;
    private List<RteBaseDataTypeStructure> baseDataTypeList;
    private List<RteAdvancedDataTypeStructure> advancedDataTypeList;

    /**
      * @brief      Constructor
      */
    public RteTypes()
    {
      xmlFileVersion = new XmlFileVersion();
      xmlFileVersion.MajorVersion = Convert.ToUInt16(RteTypesResources.RteTypesXmlMajorVersion);
      xmlFileVersion.MinorVersion = Convert.ToUInt16(RteTypesResources.RteTypesXmlMinorVersion);
      xmlFileVersion.PatchVersion = Convert.ToUInt16(RteTypesResources.RteTypesXmlPatchVersion);
      baseDataTypeList = new List<RteBaseDataTypeStructure>();
      advancedDataTypeList = new List<RteAdvancedDataTypeStructure>();
    }

    /**
      * @brief      Destructor
      */
    ~RteTypes()
    {
    }

    #region Attributes
    public XmlFileVersion XmlFileVersion
    {
      get { return xmlFileVersion; }
      set { xmlFileVersion = value; }
    }
    public List<RteBaseDataTypeStructure> BaseDataTypeList
    {
      get { return baseDataTypeList; }
      set { baseDataTypeList = value; }
    }
    public List<RteAdvancedDataTypeStructure> AdvancedDataTypeList
    {
      get { return advancedDataTypeList; }
      set { advancedDataTypeList = value; }
    }
    #endregion

    /**
      * @brief      Initialize base type list with default data
      * @param      None
      * @retval     None
      */
    public void SetupDefaultConfiguration()
    {
      /* Setup Base Rte Data Types */
      RteBaseTypesResource baseTypeRes = new RteBaseTypesResource();
      baseDataTypeList = new List<RteBaseDataTypeStructure>();

      baseDataTypeList.Add(baseTypeRes.Uint8_Type);
      baseDataTypeList.Add(baseTypeRes.Sint8_Type);
      baseDataTypeList.Add(baseTypeRes.Uint16_Type);
      baseDataTypeList.Add(baseTypeRes.Sint16_Type);
      baseDataTypeList.Add(baseTypeRes.Uint32_Type);
      baseDataTypeList.Add(baseTypeRes.Sint32_Type);
      baseDataTypeList.Add(baseTypeRes.Uint64_Type);
      baseDataTypeList.Add(baseTypeRes.Sint64_Type);
      baseDataTypeList.Add(baseTypeRes.Float32_Type);
      baseDataTypeList.Add(baseTypeRes.Float64_Type);
      baseDataTypeList.Add(baseTypeRes.Boolean_Type);
      baseDataTypeList.Add(baseTypeRes.Void_Type);

      /* Add Rte defined advanced data types */
      AddAdvancedRteType(baseTypeRes.Rte_ErrorType_Type);
    }

    /**
      * @brief      Interface to add a advanced user Rte Type
      * @param      New user advanced Rte Type
      * @retval     None
      */
    public void AddAdvancedRteType(RteAdvancedDataTypeStructure usrRteType)
    {
      Regex regex = new Regex("^[a-zA-Z0-9_]*$", RegexOptions.Compiled);

      /* Check if new data already exist */
      if (true == IsDataTypeAvailable(usrRteType.DataType))
      {
        /* Data type already exist >> Could not be added to database */
        throw new ArgumentException("Data type is already available.");
      }

      /* Check if new data type name has been set */
      if ("" == usrRteType.DataType.DataTypeName)
      {
        throw new ArgumentException("Data type name is not set.");
      }

      /* Check if new data type name is valid */
      if (false == regex.IsMatch(usrRteType.DataType.DataTypeName))
      {
        throw new ArgumentException("Data type name contains invalid chars >> Type Name: " + usrRteType.DataType.DataTypeName);
      }

      /* Check if new data name already exist */
      if (true == IsDataTypeNameAvailable(usrRteType.DataType))
      {
        /* Data type already exist >> Could not be added to database */
        throw new ArgumentException("Data type name is already available.");
      }

      /* Evaluate / Validate user types */
      switch (usrRteType.DataTypeType)
      {
        /***********************************************************************************/
        /*************************** Evaluate standard data type ***************************/
        /***********************************************************************************/
        case RteAdvancedDataTypeType.STANDARD:
        {
          /* Check if base data type is available */
          if (false == IsDataTypeAvailable(usrRteType.BaseDataType))
          {
            throw new ArgumentException("Base data type is not available.");
          }
        }
        break;

        /***********************************************************************************/
        /***************************** Evaluate struct data type ***************************/
        /***********************************************************************************/
        case RteAdvancedDataTypeType.STRUCT:
        {
          /* Check if struct element base data type is available */
          for (int idx = 0; idx < usrRteType.StructElements.Count; idx++)
          {
            /*Check if base data type exist */
            if (false == IsDataTypeAvailable(usrRteType.StructElements[idx].BaseDataType))
            {
              throw new ArgumentException("Struct element base data type is not available.");
            }

            /* Check if element data name has been set */
            if ("" == usrRteType.StructElements[idx].StructElementName)
            {
              throw new ArgumentException("Struct element base data type name is not set");
            }

            /* Check if new data type name is valid */
            if (false == regex.IsMatch(usrRteType.StructElements[idx].StructElementName))
            {
              throw new ArgumentException("Struct element data type name contains invalid chars >> Element Name: " + usrRteType.StructElements[idx].StructElementName);
            }
          }
        }
        break;

        /***********************************************************************************/
        /****************************** Evaluate enum data type ****************************/
        /***********************************************************************************/
        case RteAdvancedDataTypeType.ENUMERATION:
        {
          /* Check if struct element base data type is available */
          for (int idx = 0; idx < usrRteType.EnumElements.Count; idx++)
          {
            /* Check if element data name has been set */
            if ("" == usrRteType.EnumElements[idx].EnumElementName)
            {
              throw new ArgumentException("Enum element name is not set");
            }

            /* Check if new data type name is valid */
            if (false == regex.IsMatch(usrRteType.EnumElements[idx].EnumElementName))
            {
              throw new ArgumentException("Enum element name contains invalid chars >> Element Name: " + usrRteType.EnumElements[idx].EnumElementName);
            }

            /* Check if enum element id valid */
            if ("" == usrRteType.EnumElements[idx].EnumId)
            {
              if (false == regex.IsMatch(usrRteType.EnumElements[idx].EnumId))
              {
                throw new ArgumentException("Enum element id contains invalid chars >> Element Id: " + usrRteType.EnumElements[idx].EnumId);
              }
            }
          }
        }
        break;

        default:
        throw new InvalidDataException();
      }

      /* Store new data type */
      advancedDataTypeList.Add(usrRteType);
    }

    /**
      * @brief      Interface to check if a data type is available in RteTypes data base.
      * @param[in]  RteBaseDataTypeStructure base data type to be checked
      * @param[out] Index of list where the element is located
      *             index == -1 >> Element is in base data type list located
      * @retval     bool true == data type is available
      * @note       Check will be performed based on the UUID string.
      */
    public bool IsDataTypeAvailable(RteBaseDataTypeStructure dataType, out int listIndex)
    {
      bool retVal = false;
      listIndex = 0;

      /* Check for base data types */
      foreach (RteBaseDataTypeStructure rteDataType in baseDataTypeList)
      {
        if (rteDataType.UUID == dataType.UUID)
        {
          listIndex = -1;
          retVal = true;
          break;
        }
      }

      /* Check for advanced data types */
      if (true != retVal)
      {
        for (int idx = 0; idx < advancedDataTypeList.Count(); idx++)
        {
          if (advancedDataTypeList[idx].DataType.UUID == dataType.UUID)
          {
            listIndex = idx;
            retVal = true;
            break;
          }
        }
      }

      return retVal;
    }

    /**
      * @brief      Interface to check if a data type is available in RteTypes data base.
      * @param      RteBaseDataTypeStructure base data type to be checked
      * @retval     bool true == data type is available
      * @note       Check will be performed based on the UUID string.
      */
    public bool IsDataTypeAvailable(RteBaseDataTypeStructure dataType)
    {
      int referenceId;

      return IsDataTypeAvailable(dataType, out referenceId);
    }

    /**
      * @brief      Interface to check if a data type name is available in RteTypes data base.
      * @param      RteBaseDataTypeStructure base data type to be checked
      * @retval     bool true == data type name is available
      * @note       Check will be performed based on the name string.
      */
    public bool IsDataTypeNameAvailable(RteBaseDataTypeStructure dataType)
    {
      bool retVal = false;

      /* Check for base data types */
      foreach (RteBaseDataTypeStructure rteDataType in baseDataTypeList)
      {
        if (rteDataType.DataTypeName == dataType.DataTypeName)
        {
          retVal = true;
          break;
        }
      }

      if (true != retVal)
      {
        for (int idx = 0; idx < advancedDataTypeList.Count(); idx++)
        {
          if (advancedDataTypeList[idx].DataType.DataTypeName == dataType.DataTypeName)
          {
            retVal = true;
            break;
          }
        }
      }

      return retVal;
    }

    /**
      * @brief      Interface to validate the Rte Type List
      * @param      None
      * @retval     List<string> >> String list with validation errors
      * @details    The validation function would check if each referenced data type is available.
      */
    public List<string> ValidateRteTypesList()
    {
      List<string> validationErrors = new List<string>();


      /* Check if each referenced data type is available */
      foreach (RteAdvancedDataTypeStructure checkDataType in advancedDataTypeList)
      {
        validationErrors.AddRange(ValidateRteDataTypeElement(checkDataType));
      }

      return validationErrors;
    }

    /**
      * @brief      Interface to validate the Rte Type List Element
      * @param      Data type to be checked
      * @retval     List<string> >> String list with validation errors
      * @details    The validation function would check if each referenced data type is available.
      */
    public List<string> ValidateRteDataTypeElement(RteAdvancedDataTypeStructure checkDataType)
    {
      string errorString;
      List<string> validationErrors = new List<string>();
      Regex regex = new Regex("^[a-zA-Z0-9_]*$", RegexOptions.Compiled);

      // Validate DataType element
      /* Check data type name */
      if (false == regex.IsMatch(checkDataType.DataType.DataTypeName))
      {
        errorString = "";
        errorString += "Validation error on data type \"" + checkDataType.DataType.DataTypeName + "\" with UUID: " + checkDataType.DataType.UUID + "\n";
        errorString += "  >> Data type name \"" + checkDataType.DataType.DataTypeName + "\" contains invalid characters!";
        validationErrors.Add(errorString);
      }

      /* Check if UUID is set */
      if("" == checkDataType.DataType.UUID)
      {
        errorString = "";
        errorString += "Validation error on data type \"" + checkDataType.DataType.DataTypeName + "\" with UUID: " + checkDataType.DataType.UUID + "\n";
        errorString += "  >> Data type UUID not set!";
        validationErrors.Add(errorString);
      }

      /* Check data type elements */
      switch (checkDataType.DataTypeType)
      {
        case RteAdvancedDataTypeType.STANDARD:
          {
            /* Check base data type */
            if (false == IsDataTypeAvailable(checkDataType.BaseDataType))
            {
              errorString = "";
              errorString += "Validation error on standard data type \"" + checkDataType.DataType.DataTypeName + "\" with UUID: " + checkDataType.DataType.UUID + "\n";
              errorString += "  >> Base data type \"" + checkDataType.BaseDataType.DataTypeName + "\" with UUID: " + checkDataType.BaseDataType.UUID + " not found!";
              validationErrors.Add(errorString);
            }
          }
          break;

        case RteAdvancedDataTypeType.ENUMERATION:
          {
            foreach (RteAdvancedEnumDataTypeStructure checkEnumElement in checkDataType.EnumElements)
            {
              // Check enum element name
              if (false == regex.IsMatch(checkEnumElement.EnumElementName))
              {
                errorString = "";
                errorString += "Validation error on enum data type \"" + checkDataType.DataType.DataTypeName + "\" with UUID: " + checkDataType.DataType.UUID + "\n";
                errorString += "  >> Enum element name \"" + checkEnumElement.EnumElementName + "\" contains invalid characters!";
                validationErrors.Add(errorString);
              }

              // Check enum element value
              if (false == regex.IsMatch(checkEnumElement.EnumId))
              {
                errorString = "";
                errorString += "Validation error on enum data type \"" + checkDataType.DataType.DataTypeName + "\" with UUID: " + checkDataType.DataType.UUID + "\n";
                errorString += "  >> Enum element name \"" + checkEnumElement.EnumElementName +
                               "  >> Enum element value \"" + checkEnumElement.EnumId + "\" contains invalid characters!";
                validationErrors.Add(errorString);
              }
            }
          }
          break;

        case RteAdvancedDataTypeType.STRUCT:
          {
            /* Check struct elements */
            foreach (RteAdvancedStructDataTypeStructure checkStructElement in checkDataType.StructElements)
            {
              /* Check for base data type */
              if (false == IsDataTypeAvailable(checkStructElement.BaseDataType))
              {
                errorString = "";
                errorString += "Validation error on struct data type \"" + checkDataType.DataType.DataTypeName + "\" with UUID: " + checkDataType.DataType.UUID + "\n";
                errorString += "  >> Struct element base data type \"" + checkStructElement.BaseDataType.DataTypeName + "\" with UUID: " + checkStructElement.BaseDataType.UUID + " not found!";
                validationErrors.Add(errorString);
              }

              /* Check Element name */
              if (false == regex.IsMatch(checkStructElement.StructElementName))
              {
                errorString = "";
                errorString += "Validation error on struct data type \"" + checkDataType.DataType.DataTypeName + "\" with UUID: " + checkDataType.DataType.UUID + "\n";
                errorString += "  >> Struct element name \"" + checkStructElement.StructElementName + "\" contains invalid characters!";
                validationErrors.Add(errorString);
              }
            }
          }
          break;

        default:
        throw new InvalidDataException();
      }

      return validationErrors;
    }

    /**
      * @brief      Interface to sort the RteTypes list so data-types are available until it would be referenced.
      * @param      None
      * @retval     None
      */
    public void SortRteTypesList()
    {
      int idx = 0, idx2 = 0, referencedListIdx;
      string errorString;

      /* Check each data element within the advanced data type list */
      for (idx = 0; idx < advancedDataTypeList.Count(); idx++)
      {
        /* Select data type */
        switch (advancedDataTypeList[idx].DataTypeType)
        {
          case RteAdvancedDataTypeType.STANDARD:
          {
            /* Check if data type is available and the list position */
            if(true == IsDataTypeAvailable(advancedDataTypeList[idx].BaseDataType, out referencedListIdx))
            {
              /* Check if element was a base data type element >> If not continue advanced processing */
              if(-1 != referencedListIdx)
              {
                if (idx == referencedListIdx)
                {
                  errorString = "";
                  errorString += "Data type error >> Self-Reference detected \"" + advancedDataTypeList[idx].DataType.DataTypeName + "\" with UUID: " + advancedDataTypeList[idx].DataType.UUID + "\n";
                  throw new InvalidDataException(errorString);
                }

                  /* Check reference position */
                  if (referencedListIdx > idx)
                {
                  SwapAdvancedListElements(idx, referencedListIdx);

                  /* Modify counter to check swapped element */
                  idx--;
                }
              }
            }
          }
          break;

          case RteAdvancedDataTypeType.STRUCT:
          {
            /* Check for each struct element */
            for (idx2 = 0; idx2 < advancedDataTypeList[idx].StructElements.Count(); idx2++)
            {
              if (true == IsDataTypeAvailable(advancedDataTypeList[idx].StructElements[idx2].BaseDataType, out referencedListIdx))
              {
                /* Check if element was a base data type element >> If not continue advanced processing */
                if (-1 != referencedListIdx)
                {
                  if (idx == referencedListIdx)
                  {
                    errorString = "";
                    errorString += "Data type error >> Self-Reference detected \"" + advancedDataTypeList[idx].DataType.DataTypeName + "\" with UUID: " + advancedDataTypeList[idx].DataType.UUID + "\n";
                    throw new InvalidDataException(errorString);
                  }

                    /* Check reference position */
                    if (referencedListIdx > idx)
                  {
                    SwapAdvancedListElements(idx, referencedListIdx);

                    /* Modify counter to check swapped element */
                    idx--;
                    break;
                  }
                }
              }
            }
          }
          break;

          case RteAdvancedDataTypeType.ENUMERATION:
          {
            //Nothing to do
          }
          break;

          default:
          {
            throw new InvalidDataException();
          }
        }
      }
    }

    /*
     * @brief       Function to store the current rte type data base to an xml file
     * @param       string Path to RteType Database file
     * @retval      none
     */
    public void SaveActiveRteTypesDataBaseToXml(string pathToXmlFile)
    {
      XmlSerializer writer = new XmlSerializer(this.GetType());
      StreamWriter file = new StreamWriter(pathToXmlFile);

      /* Setup XML File Version */
      xmlFileVersion.MajorVersion = Convert.ToUInt16(RteTypesResources.RteTypesXmlMajorVersion);
      xmlFileVersion.MinorVersion = Convert.ToUInt16(RteTypesResources.RteTypesXmlMinorVersion);
      xmlFileVersion.PatchVersion = Convert.ToUInt16(RteTypesResources.RteTypesXmlPatchVersion);

      writer.Serialize(file, this);
      file.Close();
    }

    /*
     * @brief       Function to read the current configuration structure from an xml file
     * @param       string Path to RteType Database file
     * @retval      none
     */
    public void ReadActiveRteTypesDataBaseFromXml(string pathToXmlFile)
    {
      RteTypes tempRteTypes = new RteTypes();
      XmlSerializer reader = new XmlSerializer(this.GetType());
      StreamReader file = new StreamReader(pathToXmlFile);
      tempRteTypes = (RteTypes)reader.Deserialize(file);

      this.BaseDataTypeList = tempRteTypes.BaseDataTypeList;
      this.advancedDataTypeList = tempRteTypes.advancedDataTypeList;
      this.xmlFileVersion = tempRteTypes.xmlFileVersion;

      file.Close();
    }

    /*
     * @brief       Internal Helper function to swap two advanced data type list elements 
     * @param[in]   Element Index 1
     * @param[in]   Element Index 2
     * @retval      none
     */
    private void SwapAdvancedListElements(int elementIdx1, int elementIdx2)
    {
      RteAdvancedDataTypeStructure element1, element2;

      /* Referenced types to be moved in its position */
      element1 = advancedDataTypeList[elementIdx1];
      element2 = advancedDataTypeList[elementIdx2];

      /* Swap elements */
      advancedDataTypeList.RemoveAt(elementIdx2);
      advancedDataTypeList.Insert(elementIdx2, element1);
      advancedDataTypeList.RemoveAt(elementIdx1);
      advancedDataTypeList.Insert(elementIdx1, element2);
    }

  }
}
/**
* @}
*/
