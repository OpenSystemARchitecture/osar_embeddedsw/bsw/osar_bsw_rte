﻿/*****************************************************************************************************************************
 * @file        RteTypesImportHandler.cs                                                                                     *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        13.12.2020                                                                                                   *
 * @brief       Implementation of the import handler for the rte types. In this class all import operation into the          *
 *              Rte Types are available.                                                                                     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup RteLib.RteTypes
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteConfig;
using RteLib.RteResources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace RteLib.RteTypes
{
  public class RteTypesImportHandler
  {
    private RteConfig.RteConfig rteConfig;                                  //!< Reference to the Rte Configuration
    private RteTypes rteTypes;                                              //!< Reference to the Rte Types
    private RteModuleInternalBehavior.RteModuleInternalBehavior mibConfig;  //!< actualUsedMibConfig
    private string absPathToRteConfiguartionFile;                           //!< Absolute Path to Rte Configuration File

    /**
     * @brief     Constructor
     * @param[in] Rte configuration object
     * @param[in] Reference to the rte types object
     */
    public RteTypesImportHandler(RteConfig.RteConfig config, ref RteTypes types, string absPathToRteConfigFile)
    {
      rteConfig = config;
      rteTypes = types;
      absPathToRteConfiguartionFile = absPathToRteConfigFile;
    }

    #region Import functions
    /**
     * @brief     Import the MiB backup types into the rte types object
     * @param[in] Module to be imported
     * @return    None
     * @exception - FileNotFoundException
     *            - OsarConfigMismatchException
     *            - OsarMergeException
     *            - ArgumentException
     * @details   Pre-Actions:
     *            - Check of module internal behavior file exists and load file.
     *            Following informations are imported:
     *            - Backup types
     */
    public void ImportMiBFileIntoRteTypes(RteConfigUsedModuleCfg module)
    {
      try
      {
        LoadMiBFile(module);

        // Import all backup types from MiB into rte types data base
        foreach (RteAdvancedDataTypeStructure backuptypes in mibConfig.BackupAdvancedDataTypeList)
        {
          if (false == CheckIfTypeIsAlreadyConfigured(backuptypes))
          {
            // Client server interface is not available in rte interface >> Import it into rte interface
            rteTypes.AddAdvancedRteType(backuptypes);
          }
        }
      }
      catch (Exception)
      {
        throw; // Forward the exception
      }
    }

    #endregion

    #region Helper functions
    /**
     * @brief     Helper function which loads the module internal behavior file
     * @param[in] module: Rte basis module information
     * @return    None
     * @exception - FileNotFoundException
     *            - OsarConfigMismatchException
     */
    private void LoadMiBFile(RteConfigUsedModuleCfg module)
    {
      string pathToMibFile = Path.GetDirectoryName(absPathToRteConfiguartionFile) + rteConfig.PathToBaseProjectFolder + "./" +
        module.ModuleType + "./" + module.ModuleName + RteProjectStructure.PathToMibFileFromModuleBaseFolder;

      // Check if file exists
      if (false == File.Exists(pathToMibFile))
      {
        throw new FileNotFoundException("Requested MiB file to merged not found! FilePath: " + pathToMibFile +
          " Check project structure recommended.");
      }

      // Create MiB Object
      mibConfig = new RteModuleInternalBehavior.RteModuleInternalBehavior();
      mibConfig.ReadActiveRteModuleInternalBehaviorToXml(pathToMibFile);

      // Check if config match's with MiB File
      if (( module.ModuleName != mibConfig.ModuleName ) || ( module.ModuleType != mibConfig.ModuleType ))
      {
        throw new OsarResources.Generic.OsarConfigMismatchException("Requested module to be merged: " +
          module.ModuleType.ToString() + " " + module.ModuleName + " >> Does not fit with MiB configuration: " +
          mibConfig.ModuleType.ToString() + " " + mibConfig.ModuleName);
      }
    }

    /**
     * @brief     Helper function to check if the MiB type is already available in rte types
     * @param[in] MiB type which shall be imported
     * @details   Check of type is available and identical!
     * @exception - OsarMergeException
     */
    private bool CheckIfTypeIsAlreadyConfigured(RteAdvancedDataTypeStructure mibType)
    {
      bool retVal = false;

      foreach (RteAdvancedDataTypeStructure rteType in rteTypes.AdvancedDataTypeList)
      {
        /* Check if UUID exists */
        if (rteType.DataType.UUID == mibType.DataType.UUID)
        {
          // Check if object is identical
          if (rteType == mibType)
          {
            // Element found and identical!
            retVal = true;
            break;
          }
          else
          {
            // UUID found but element is not identical
            throw new OsarResources.Generic.OsarMergeException("Import of available Data-Type not possible." +
              "Rte Data-Type: " + rteType.DataType.DataTypeName + " and MiB Data-Type: " +
              mibType.DataType.DataTypeName + " have the same UUID but are NOT equal!");
          }
        }
      }

      return retVal;
    }

    #endregion
  }
}
