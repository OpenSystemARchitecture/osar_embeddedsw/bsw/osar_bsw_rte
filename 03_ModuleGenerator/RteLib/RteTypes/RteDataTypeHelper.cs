﻿/******************************************************************************
  * @file    RteDataTypeHelper.cs
  * @author  Reinemuth Sebastian
  * @date    05-01-2019
  * @brief   Definiton of the RTE Data Type Helper functions
  * $Author: sreinemuth $ @ $Date: 2017-02-18 12:00:30 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 12 $
  *****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RteLib.RteTypes
{
  /**
  * @brief      Implementation of Rte data type helper functionalities 
  */
  public static class RteDataTypeHelper
  {
    /**
    * @brief      Helper function to generate the data type with access macro
    * @param[in]  data type name string
    * @param[in]  RteDataAccessType access type
    * @retval     string with data type in access macro 
    */
    public static string GetDataTypeWithAccessString(string dataType, RteDataAccessType accessType)
    {
      string retVal = "";

      switch (accessType)
      {
        case RteDataAccessType.STANDARD:
        retVal = RteTypesResources.RteTypeAccessStandardMacro + "(" + dataType + ")";
        break;

        case RteDataAccessType.POINTER:
        retVal = RteTypesResources.RteTypeAccessPointerMacro + "(" + dataType + ")";
        break;

        case RteDataAccessType.CONST_POINTER:
        retVal = RteTypesResources.RteTypeAccessConstPointerMacro + "(" + dataType + ")";
        break;

        case RteDataAccessType.POINTER_TO_CONST:
        retVal = RteTypesResources.RteTypeAccessPointerConstMacro + "(" + dataType + ")";
        break;

        case RteDataAccessType.CONST_POINTER_TO_CONST:
        retVal = RteTypesResources.RteTypeAccessConstPointerConstMacro + "(" + dataType + ")";
        break;
      }

      return retVal;
    }
  }
}
