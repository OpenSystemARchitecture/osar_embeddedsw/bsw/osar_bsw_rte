/*****************************************************************************************************************************
 * @file        NvmHdl.c                                                                                                     *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.08.2018 15:58:25                                                                                          *
 * @brief       Implementation of functionalities from the "NvmHdl" module.                                                  *
 *                                                                                                                           *
 * @details     Implementation of an automatic nvm block handling statemachine.                                              *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/

/**
* @addtogroup OSAR_BSW
* @{
*/
/**
 * @addtogroup Nvm
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "NvmHdl.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Nvm_START_SEC_CONST
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_CONST
#include "Nvm_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Nvm_START_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Nvm_START_SEC_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_INIT_VAR
#include "Nvm_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Nvm_START_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
/*-------------------------------------------------- External defined variables --------------------------------------------*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Nvm_START_SEC_CONST
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_CONST
#include "Nvm_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Nvm_START_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Nvm_START_SEC_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_INIT_VAR
#include "Nvm_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Nvm_START_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_ZERO_INIT_VAR
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define Nvm_START_SEC_CODE
#include "Nvm_MemMap.h"
#define Nvm_STOP_SEC_CODE
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define Nvm_START_SEC_CODE
#include "Nvm_MemMap.h"
/**
 * @brief           API struct object initialization function.
 * @param[in/out]   NvmHdl_Type pointer to corresponding NvmHdl
 * @param[in]       uint16 correspondending Nvm Block Id
 * @retval          Std_ReturnType
 *                  > E_OK
 *                  > E_NOT_OK
 * @details         This function shall be called before using the object.
 */
Std_ReturnType NvmHdl_Init( NvmHdl_Type* nvmHdl, uint16 nvmBlockId )
{
  Std_ReturnType retVal = E_NOT_OK;

  /* Check input parameter */
  if(NULL_PTR == nvmHdl)
  {
    #if(STD_ON == NVM_MODULE_USE_DET)
    Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_ARGUMENT);
    #endif
    retVal = E_NOT_OK;
  }
  else
  {
    /* Initialize the handler variable */
    nvmHdl->nvmBlockHdlId = nvmBlockId;
    nvmHdl->nvmReadStatemachineState = NVMHDL_SM_REQUESTBLOCKSTATUS;
    nvmHdl->nvmWriteStatemachineState = NVMHDL_SM_REQUESTBLOCKSTATUS;
    retVal = E_OK;
  }

  return retVal;
}

/**
 * @brief           API Nvm Handler Read Nvm Block Item 
 * @param[in/out]   NvmHdl_Type pointer to corresponding NvmHdl
 * @param[out]      uint8* pointer to the read data
 * @retval          Std_ReturnType
 *                  > E_OK          >> Request processing finished
 *                  > E_PENDING     >> Request still in progress
 *                  > E_SKIPPED     >> Request skipped >> Data invalid
 *                  > E_NOT_OK      >> Request failed
 * @details         This function is an handler function which abstracts the general Nvm APIs
 *                  It is used to read an Nvm Block Item
 */
Std_ReturnType NvmHdl_ReadBlockItem( NvmHdl_Type* nvmHdl, uint8* pData )
{
  Std_ReturnType retVal = E_NOT_OK, nvmRetVal;
  Nvm_BlockStatusType nvmBlkStatus;

  /* Check input parameter */
  if ( (NULL_PTR == nvmHdl) || (NULL_PTR == pData))
  {
    #if(STD_ON == NVM_MODULE_USE_DET)
    Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_ARGUMENT);
    #endif
    retVal = E_NOT_OK;
  }
  else
  {
    /* Process the read statemachine */
    switch(nvmHdl->nvmReadStatemachineState)
    {
      /************************************************************************************************************************/
      /* State NVMHDL_SM_REQUESTBLOCKSTATUS: Check active block status and write till the nvm block is ready                  */
      /************************************************************************************************************************/
      case NVMHDL_SM_REQUESTBLOCKSTATUS:
      {
        nvmRetVal = Nvm_GetErrorStatus(nvmHdl->nvmBlockHdlId , &nvmBlkStatus);
        if(E_OK != nvmRetVal)
        {
          /* Request failed >> Report status */
          retVal = E_NOT_OK;
          break;
        }
        else if( (NVM_BLK_OK == nvmBlkStatus) || (NVM_BLK_SKIPPED == nvmBlkStatus) )
        {
          /* Nvm Block is ready to be read */
          nvmHdl->nvmReadStatemachineState = NVMHDL_SM_SET_NVM_REQUEST;
          retVal = E_PENDING;
          break;
        }
        else if( (NVM_BLK_INTEGRITY_FAILED == nvmBlkStatus) || (NVM_BLK_INVALIDATE == nvmBlkStatus) )
        {
          /* Data is not valid >> Report it */
          retVal = E_SKIPPED;
          break;
        }
        else
        {
          /* Data is still pending check next cycle */
          retVal = E_PENDING;
          break;
        }
      }
      break;

      /************************************************************************************************************************/
      /* State NVMHDL_SM_SET_NVM_REQUEST: Set the Read Request                                                                */
      /************************************************************************************************************************/
      case NVMHDL_SM_SET_NVM_REQUEST:
      {
        nvmRetVal = Nvm_ReadBlock(nvmHdl->nvmBlockHdlId, pData);
        if(E_OK == nvmRetVal)
        {
          /* Request accepted >> continue with waiting for request */
          nvmHdl->nvmReadStatemachineState = NVMHDL_SM_WAIT_FOR_NVM_REQUEST;
          retVal = E_PENDING;
          break;
        }
        else
        {
          /* Request not accepted >> Report status and reset statemachine */
          nvmHdl->nvmReadStatemachineState = NVMHDL_SM_REQUESTBLOCKSTATUS;
          retVal = E_NOT_OK;
          break;
        }
      }
      break;

      /************************************************************************************************************************/
      /* State NVMHDL_SM_WAIT_FOR_NVM_REQUEST: Wait till the read request has been processed                                  */
      /************************************************************************************************************************/
      case NVMHDL_SM_WAIT_FOR_NVM_REQUEST:
      {
        nvmRetVal = Nvm_GetErrorStatus(nvmHdl->nvmBlockHdlId , &nvmBlkStatus);
        if(E_OK != nvmRetVal)
        {
          /* Request failed >> Report status */
          nvmHdl->nvmReadStatemachineState = NVMHDL_SM_REQUESTBLOCKSTATUS;
          retVal = E_NOT_OK;
          break;
        }
        else if( NVM_BLK_PENDING == nvmBlkStatus)
        {
          /* Data is still pending check next cycle */
          retVal = E_PENDING;
          break;
        }
        else if( NVM_BLK_OK == nvmBlkStatus)
        {
          /* Nvm Block is ready to be read */
          nvmHdl->nvmReadStatemachineState = NVMHDL_SM_REQUESTBLOCKSTATUS;
          retVal = E_OK;
          break;
        }
        else
        {
          /* Data is not valid >> Report it */
          nvmHdl->nvmReadStatemachineState = NVMHDL_SM_REQUESTBLOCKSTATUS;
          retVal = E_SKIPPED;
          break;
        }
      }
      break;

      /************************************************************************************************************************/
      /* State default: Critical system error >> Report the error                                                             */
      /************************************************************************************************************************/
      default:
      #if(STD_ON == NVM_MODULE_USE_DET)
      Det_ReportError(NVM_DET_MODULE_ID, NVM_E_GENERIC_PROGRAMMING_FAILURE);
      #endif
      nvmHdl->nvmReadStatemachineState = NVMHDL_SM_REQUESTBLOCKSTATUS;
      retVal = E_PENDING;
      break;
    }
  }

  return retVal;
}

/**
 * @brief           API Nvm Handler Write Nvm Block Item 
 * @param[in/out]   NvmHdl_Type pointer to corresponding NvmHdl
 * @param[in]       uint8* pointer to the write data
 * @retval          Std_ReturnType
 *                  > E_OK          >> Request processing finished
 *                  > E_PENDING     >> Request still in progress
 *                  > E_SKIPPED     >> Request skipped >> Data invalid
 *                  > E_NOT_OK      >> Request failed
 * @details         This function is an handler function which abstracts the general Nvm APIs
 *                  It is used to write an Nvm Block Item
 */
Std_ReturnType NvmHdl_WriteBlockItem( NvmHdl_Type* nvmHdl, uint8* pData )
{
  Std_ReturnType retVal = E_NOT_OK, nvmRetVal;
  Nvm_BlockStatusType nvmBlkStatus;

  /* Check input parameter */
  if ( (NULL_PTR == nvmHdl) || (NULL_PTR == pData))
  {
    #if(STD_ON == NVM_MODULE_USE_DET)
    Det_ReportError(NVM_DET_MODULE_ID, NVM_E_INVALID_ARGUMENT);
    #endif
    retVal = E_NOT_OK;
  }
  else
  {
    /* Process the write statemachine */
    switch(nvmHdl->nvmWriteStatemachineState)
    {
      /************************************************************************************************************************/
      /* State NVMHDL_SM_REQUESTBLOCKSTATUS: Check active block status and write till the nvm block is ready                  */
      /************************************************************************************************************************/
      case NVMHDL_SM_REQUESTBLOCKSTATUS:
      {
        nvmRetVal = Nvm_GetErrorStatus(nvmHdl->nvmBlockHdlId , &nvmBlkStatus);
        if(E_OK != nvmRetVal)
        {
          /* Request failed >> Report status */
          retVal = E_NOT_OK;
          break;
        }
        else if( (NVM_BLK_OK == nvmBlkStatus) || (NVM_BLK_SKIPPED == nvmBlkStatus) || (NVM_BLK_INTEGRITY_FAILED == nvmBlkStatus) || (NVM_BLK_INVALIDATE == nvmBlkStatus))
        {
          /* Nvm Block is ready to be written */
          nvmHdl->nvmWriteStatemachineState = NVMHDL_SM_SET_NVM_REQUEST;
          retVal = E_PENDING;
          break;
        }
        else
        {
          /* Data is still pending check next cycle */
          retVal = E_PENDING;
          break;
        }

      }
      break;

      /************************************************************************************************************************/
      /* State NVMHDL_SM_SET_NVM_REQUEST: Set the Write Request                                                                */
      /************************************************************************************************************************/
      case NVMHDL_SM_SET_NVM_REQUEST:
      {
        nvmRetVal = Nvm_WriteBlock(nvmHdl->nvmBlockHdlId, pData);
        if(E_OK == nvmRetVal)
        {
          /* Request accepted >> continue with waiting for request */
          nvmHdl->nvmWriteStatemachineState = NVMHDL_SM_WAIT_FOR_NVM_REQUEST;
          retVal = E_PENDING;
          break;
        }
        else
        {
          /* Request not accepted >> Report status and reset statemachine */
          nvmHdl->nvmWriteStatemachineState = NVMHDL_SM_REQUESTBLOCKSTATUS;
          retVal = E_NOT_OK;
          break;
        }
      }
      break;

      /************************************************************************************************************************/
      /* State NVMHDL_SM_WAIT_FOR_NVM_REQUEST: Wait till the write request has been processed                                  */
      /************************************************************************************************************************/
      case NVMHDL_SM_WAIT_FOR_NVM_REQUEST:
      {
        nvmRetVal = Nvm_GetErrorStatus(nvmHdl->nvmBlockHdlId , &nvmBlkStatus);
        if(E_OK != nvmRetVal)
        {
          /* Request failed >> Report status */
          nvmHdl->nvmWriteStatemachineState = NVMHDL_SM_REQUESTBLOCKSTATUS;
          retVal = E_NOT_OK;
          break;
        }
        else if( NVM_BLK_PENDING == nvmBlkStatus)
        {
          /* Data is still pending check next cycle */
          retVal = E_PENDING;
          break;
        }
        else if( NVM_BLK_OK == nvmBlkStatus)
        {
          /* Nvm Block is ready to be read */
          nvmHdl->nvmWriteStatemachineState = NVMHDL_SM_REQUESTBLOCKSTATUS;
          retVal = E_OK;
          break;
        }
        else
        {
          /* Data is not valid >> Report it */
          nvmHdl->nvmWriteStatemachineState = NVMHDL_SM_REQUESTBLOCKSTATUS;
          retVal = E_SKIPPED;
          break;
        }
      }
      break;

      /************************************************************************************************************************/
      /* State default: Critical system error >> Report the error                                                             */
      /************************************************************************************************************************/
      default:
      #if(STD_ON == NVM_MODULE_USE_DET)
      Det_ReportError(NVM_DET_MODULE_ID, NVM_E_GENERIC_PROGRAMMING_FAILURE);
      #endif
      nvmHdl->nvmWriteStatemachineState = NVMHDL_SM_REQUESTBLOCKSTATUS;
      retVal = E_PENDING;
      break;
    }
  }

  return retVal;
}
#define Nvm_STOP_SEC_CODE
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */
