/*****************************************************************************************************************************
 * @file        FlsEep_Int.c                                                                                                     *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.08.2018 08:29:23                                                                                          *
 * @brief       Implementation of functionalities from the "FlsEep" module.                                                  *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup FlsEep
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "FlsEep_Int.h"
#include "Det.h"
#include "Crc.h"
#include "Fls.h"
#include "FlsEep_RequestList.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define FlsEep_START_SEC_CONST
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_CONST
#include "FlsEep_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define FlsEep_START_SEC_NOINIT_VAR
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_NOINIT_VAR
#include "FlsEep_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define FlsEep_START_SEC_INIT_VAR
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_INIT_VAR
#include "FlsEep_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define FlsEep_START_SEC_ZERO_INIT_VAR
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_ZERO_INIT_VAR
#include "FlsEep_MemMap.h"
/*------------------------------------------------ External configuration data ---------------------------------------------*/
extern const FlsEep_PageCfgType flsEep_PageConfig[];
extern const FlsEep_BlkCfgType flsEepConfiguredBlocks[];
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define FlsEep_START_SEC_CONST
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_CONST
#include "FlsEep_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define FlsEep_START_SEC_NOINIT_VAR
#include "FlsEep_MemMap.h"
FlsEep_InitSm_StateType flsEep_InitSmState;
FlsEep_ManagementBlkType flsEep_MgmtFlashBlock;   /*!< FlsEep Management block stored in flash*/

FlsEep_RuntimeFlsPageMgmtType flsEep_Mgmt;
#define FlsEep_STOP_SEC_NOINIT_VAR
#include "FlsEep_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define FlsEep_START_SEC_INIT_VAR
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_INIT_VAR
#include "FlsEep_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define FlsEep_START_SEC_ZERO_INIT_VAR
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_ZERO_INIT_VAR
#include "FlsEep_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Helper function to fill the management block with valid default values
 * @param[in]       None
 * @retval          None
 */
void FlsEep_Int_SetDefaultManagementBlock(void);

/**
 * @brief           Processing the write request in the request list
 * @param[in]       None
 * @retval          None
 */
void FlsEep_Int_ProcessWriteRequests( void );

/**
 * @brief           Processing the write data to flash
 * @param[in]       uint8* pToData
 * @param[in]       uint16 length of Data 
 * @retval          None
 */
void FlsEep_Int_WriteData( uint8* pToData, uint16 lengthOfData );

/**
 * @brief           Processing the write request in the request list
 * @param[in]       None
 * @retval          None
 */
void FlsEep_Int_ProcessReadRequests( void );

/**
 * @brief           Restore management block from the flash
 * @param[in]       uint8* pointer to the last byte of the management block 
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_NOT_OK     >> Mgmt blk not valid
 *                  > FLSEEP_E_OK         >> Mgmt blk valid and restored
 * 
 * 
 *                      Offset >>          -11     -10     -9      -8      -7      -6      -5      -4       -3       -2       -1        0
 * @note            mgmtBlk LSB |...|...|Length3|Length2|Length1|Length0|CfgCrc3|CfgCrc2|CfgCrc1|CfgCrc0|MgmtCrc3|MgmtCrc2|MgmtCrc1|MgmtCrc0|
 *                                                                                                                                      ^
 *                                                                                                          argument pointer offset ----|
 */
FlsEep_ReturnType FlsEep_Int_RestoreFlsMgmtBlock(uint8* pToLastMgmtBlkElement);

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define FlsEep_START_SEC_CODE
#include "FlsEep_MemMap.h"
/**
 * @brief           Helper function to fill the management block with valid default values
 * @param[in]       None
 * @retval          None
 */
void FlsEep_Int_SetDefaultManagementBlock(void)
{
  uint16 idx;
  Crc_ReturnType crcRetVal;

  /* Set Default Block Data Addresses */
  for(idx = 0; idx < FLSEEP_CNT_OF_CONFIGURED_FLS_BLOCKS; idx++)
  {
    flsEep_MgmtFlashBlock.blkAddressList[idx].flsEepDataBlkAddress = (uint32)NULL_PTR;
    flsEep_MgmtFlashBlock.blkAddressList[idx].flsEepDataBlkId = (uint32)0xFFFFFFFF;
  }

  /* Set Management fls address */
  flsEep_MgmtFlashBlock.blkAddressList[0].flsEepDataBlkAddress = flsEep_PageConfig[flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId].pageStartAddress + 2;
  flsEep_MgmtFlashBlock.blkAddressList[0].flsEepDataBlkId = FLSEEP_MGMT_BLK_ID;

  /* Set Management block configuration checksum */
  flsEep_MgmtFlashBlock.mgmtBlkCfgChecksum = FLSEEP_CONFIGURATION_CHECKSUM;
  flsEep_MgmtFlashBlock.mgmtBlkEntries = FLSEEP_CNT_OF_CONFIGURED_FLS_BLOCKS;

  /* Calculate management block CRC */
  crcRetVal = Crc32_0x04C11DB7_Compute((uint8*)&flsEep_MgmtFlashBlock, (sizeof(FlsEep_ManagementBlkType) - 4) , &flsEep_MgmtFlashBlock.mgmtBlkCrc);
  if(CRC_E_OK != crcRetVal)
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_GENERIC_PROGRAMMING_FAILURE);
    #endif
  }
}

/**
 * @brief           Processing the write request in the request list
 * @param[in]       None
 * @retval          None
 */
void FlsEep_Int_ProcessWriteRequests( void )
{
  FlsEep_ReturnType fncRetVal;
  FlsEep_RequestListEntryType requestItem;

  /* Request Request Item */
  fncRetVal = FlsEep_RequestList_GetActiveRequest(&requestItem);

  /* Check item data */
  if( (fncRetVal != FLSEEP_E_OK ) || (requestItem.requestType != FLSEEP_REQUEST_WRITE))
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_INVALID_CONDITION);
    #endif
  }
  else
  {
    /* Update flash management block */
    flsEep_MgmtFlashBlock.blkAddressList[requestItem.blkCfgIdx].flsEepDataBlkId = flsEepConfiguredBlocks[requestItem.blkCfgIdx].flsEepBlkId;
    flsEep_MgmtFlashBlock.blkAddressList[requestItem.blkCfgIdx].flsEepDataBlkAddress = flsEep_Mgmt.flsEep_Mgmt_ActiveFlsAddress;

    /* Write Data */
    FlsEep_Int_WriteData(requestItem.pBlkRamDataAddress, flsEepConfiguredBlocks[requestItem.blkCfgIdx].flsEepBlkLength);

    /* Set new request status */
    FlsEep_RequestList_SetActiveRequestStatus(FLSEEP_E_OK);
  }
}

/**
 * @brief           Processing the write data to flash
 * @param[in]       uint8* pToData
 * @param[in]       uint16 length of Data 
 * @retval          None
 */
void FlsEep_Int_WriteData( uint8* pToData, uint16 lengthOfData )
{
  uint8 *pToRamData = pToData;
  sint32 cnt = lengthOfData;

  /* Use byte processing to be independent from hw voltage */
  while(cnt > 0)
  {
    /* Program data to flash */
    Fls_UnLockFlash();
    Fls_SyncProgramByte( *pToRamData,  flsEep_Mgmt.flsEep_Mgmt_ActiveFlsAddress);

    /* Update management data */
    flsEep_Mgmt.flsEep_Mgmt_ActiveFlsAddress += 1;
    flsEep_Mgmt.flsEep_Mgmt_FlsPageMemoryFree -= 1;
    pToRamData += 1;
    cnt -= 1;
  }

}

/**
 * @brief           Processing the write request in the request list
 * @param[in]       None
 * @retval          None
 */
void FlsEep_Int_ProcessReadRequests( void )
{
  FlsEep_ReturnType fncRetVal;
  FlsEep_RequestListEntryType requestItem;
  uint8* pToFlashData;
  uint16 idx;

  /* Request Request Item */
  fncRetVal = FlsEep_RequestList_GetActiveRequest(&requestItem);

  /* Check item data */
  if( (fncRetVal != FLSEEP_E_OK ) || (requestItem.requestType != FLSEEP_REQUEST_READ))
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_INVALID_CONDITION);
    #endif
  }
  else
  {
    /* Check if flash data is available */
    if(NULL != flsEep_MgmtFlashBlock.blkAddressList[requestItem.blkCfgIdx].flsEepDataBlkAddress)
    { 
      /* Flash data available >>  Copy data from flash into ram */
      pToFlashData = (uint8*)flsEep_MgmtFlashBlock.blkAddressList[requestItem.blkCfgIdx].flsEepDataBlkAddress;

      for(idx = 0; idx < flsEepConfiguredBlocks[requestItem.blkCfgIdx].flsEepBlkLength; idx++)
      {
        requestItem.pBlkRamDataAddress[idx] = pToFlashData[idx];
      }

      /* Set new request status */
      FlsEep_RequestList_SetActiveRequestStatus(FLSEEP_E_OK);
    }
    else
    {
      /* Flash data not available >> Skip element */
      FlsEep_RequestList_SetActiveRequestStatus(FLSEEP_E_INVALIDATE);
    }
  }
}

/**
 * @brief           Restore management block from the flash
 * @param[in]       uint8* pointer to the last byte of the management block 
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_NOT_OK     >> Mgmt blk not valid
 *                  > FLSEEP_E_OK         >> Mgmt blk valid and restored
 * 
 * 
 *                      Offset >>          -11     -10     -9      -8      -7      -6      -5      -4       -3       -2       -1        0
 * @note            mgmtBlk LSB |...|...|Length3|Length2|Length1|Length0|CfgCrc3|CfgCrc2|CfgCrc1|CfgCrc0|MgmtCrc3|MgmtCrc2|MgmtCrc1|MgmtCrc0|
 *                                                                                                                                      ^
 *                                                                                                          argument pointer offset ----|
 */
FlsEep_ReturnType FlsEep_Int_RestoreFlsMgmtBlock(uint8* pToLastMgmtBlkElement)
{
  FlsEep_ReturnType retVal = FLSEEP_E_NOT_OK;
  Crc_ReturnType crcRetVal;
  uint32 mgmtCrc;
  uint8 *bpToRamMgmtBlk, *bpToFlsMgmtBlk;
  uint16 idx, idx2;

  uint32 mgmtFlsCrc = *(uint32*)&pToLastMgmtBlkElement[-3];
  uint32 mgmtFlsCfgChecksum = *(uint32*)&pToLastMgmtBlkElement[-7];
  uint32 mgmtFlsLutLength = *(uint16*)&pToLastMgmtBlkElement[-11];

  /* length =  lengthOfBlockLut - lengthOfLength(4) - lengthOfChecksum(4)] - lengthOfCrc(4)*/
  uint16 mgmtLength = (mgmtFlsLutLength*sizeof(FlsEep_MgmtDataBlkLutEntryType)) + 4 + 4 + 4;
  
  /* pointer = &pToLastMgmtBlkElement[ - mgmtLength + 1] */
  uint8 *pToFirstMgmtByte = (uint8*)&pToLastMgmtBlkElement[ - mgmtLength + 1 ];

  FlsEep_MgmtDataBlkLutEntryType *pMgmtFirstLutElement = (FlsEep_MgmtDataBlkLutEntryType*)pToFirstMgmtByte;


  /* Calculate Crc of management block */
  crcRetVal = Crc32_0x04C11DB7_Compute(pToFirstMgmtByte, (mgmtLength - 4), &mgmtCrc );

  if(CRC_E_OK != crcRetVal)
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_GENERIC_PROGRAMMING_FAILURE);
    #endif
    retVal = FLSEEP_E_NOT_OK;
  }
  else
  {
    /* Check CRC value of management block */
    if(mgmtFlsCrc != mgmtCrc)
    {
      /* CRC checksum dose not match >> Different management block layout found >> Write new management block */
      retVal = FLSEEP_E_NOT_OK;
    }
    else
    {
      /* Management block is valid >> Set internal active management data */
      flsEep_Mgmt.flsEep_Mgmt_ActiveFlsAddress = (uint32)&pToLastMgmtBlkElement[ +1 ];
      flsEep_Mgmt.flsEep_Mgmt_FlsPageMemoryFree = flsEep_PageConfig[flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId].pageLength - (flsEep_Mgmt.flsEep_Mgmt_ActiveFlsAddress - flsEep_PageConfig[flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId].pageStartAddress);

      /* Check general management block checksum */
      if( mgmtFlsCfgChecksum == FLSEEP_CONFIGURATION_CHECKSUM )
      {
        /* Configuration is identical >> copy management block to ram */
        bpToRamMgmtBlk = (uint8*)&flsEep_MgmtFlashBlock;
        bpToFlsMgmtBlk = pToFirstMgmtByte;

        for(idx = 0; idx < sizeof(FlsEep_ManagementBlkType); idx++)
        {
          *bpToRamMgmtBlk++ = *bpToFlsMgmtBlk++;
        }
      }
      else
      {
        /* New configuration detected >> Search / Sort and Copy management data into block to ram */
        flsEep_MgmtFlashBlock.mgmtBlkEntries = FLSEEP_CNT_OF_CONFIGURED_FLS_BLOCKS;   /* Set new configuration for the new management block */
        flsEep_MgmtFlashBlock.mgmtBlkCfgChecksum = FLSEEP_CONFIGURATION_CHECKSUM;     /* Set new configuration for the new management block */

        /* Copy old block config data from old lut into new lut */
        for(idx = 0; idx < mgmtFlsLutLength; idx++)
        {
          /* Search for each restoreable element the current configured element */
          for(idx2 = 0; idx2 < FLSEEP_CNT_OF_CONFIGURED_FLS_BLOCKS; idx2++ )
          {
            if(pMgmtFirstLutElement[idx].flsEepDataBlkId == flsEepConfiguredBlocks[idx2].flsEepBlkId)
            {
              /* Element found >> Copy old data set into active mgmt block */
              flsEep_MgmtFlashBlock.blkAddressList[idx2].flsEepDataBlkId = flsEepConfiguredBlocks[idx2].flsEepBlkId;
              flsEep_MgmtFlashBlock.blkAddressList[idx2].flsEepDataBlkAddress = pMgmtFirstLutElement[idx].flsEepDataBlkAddress;
              break;
            }
            /* Else >> Element not found >> Do nothing */
          }
          
        }
      }
      retVal = FLSEEP_E_OK;
    }

  }

  return retVal;
}
#define FlsEep_STOP_SEC_CODE
#include "FlsEep_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

#define FlsEep_START_SEC_CODE
#include "FlsEep_MemMap.h"

/**
 * @brief           Module initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module.
 */
void FlsEep_Int_Init( void )
{
  /* Initialize module variables */
  flsEep_InitSmState = FLSEEP_INITSM_CHECK_PAGE_STATUS;
}

/**
 * @brief           Processing function for the FlsEep Init statemachine
 * @param[in]       None
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK
 *                  > FLSEEP_E_NOT_OK
 *                  > FLSEEP_E_PENDING
 *                  > FLSEEP_E_REPEAT_CALL
 */
FlsEep_ReturnType FlsEep_Int_ProcessInitSm( void )
{
  FlsEep_ReturnType retVal = FLSEEP_E_NOT_OK, fncRetVal;
  uint16 pageStatus0 = 6, pageStatus1 = 6;
  // uint16 flashStatus;
  Fls_ReturnType flsRetVal;

  /* Get Page status */
  pageStatus0 = (*(volatile uint16*)flsEep_PageConfig[0].pageStartAddress);
  pageStatus1 = (*(volatile uint16*)flsEep_PageConfig[1].pageStartAddress);

  switch(flsEep_InitSmState)
  {
    /************************************************************************************************************************/
    /* State FLSEEP_INITSM_CHECK_PAGE_STATUS: Read and check the page status >> Decide which action is needed for a correct */
    /*                                        initialization                                                                */
    /************************************************************************************************************************/
    case FLSEEP_INITSM_CHECK_PAGE_STATUS:
    {
      if((FLSEEP_ERASED == pageStatus0) && (FLSEEP_ERASED == pageStatus1))
      {
        /* Both Pages are erased >> Set page 0 to FLSEEP_VALID_PAGE and start processing */
        flsRetVal = Fls_UnLockFlash();
        flsRetVal |= Fls_SyncProgramHalfWord(FLSEEP_VALID_PAGE, flsEep_PageConfig[0].pageStartAddress);
        if(flsRetVal != FLS_E_OK)
        {
          #if(STD_ON == FLSEEP_MODULE_USE_DET)
          Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_GENERIC_PROGRAMMING_FAILURE);
          #endif
          retVal = FLSEEP_E_NOT_OK;
        }

        /* Set the active page */
        flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId = 0;

        /* Switch to set default management block */
        flsEep_InitSmState = FLSEEP_INITSM_WRITE_DEFAULT_MGMTBLK;
        retVal = FLSEEP_E_REPEAT_CALL;
      }
      else if((FLSEEP_VALID_PAGE == pageStatus0) && (FLSEEP_ERASED == pageStatus1))
      {
        /* Page zero is valid >> Search for active management block */
        /* Set the active page */
        flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId = 0;

        /* Switch to restor management block */
        flsEep_InitSmState = FLSEEP_INITSM_RESTORE_MGMTBLOCK;
        retVal = FLSEEP_E_REPEAT_CALL;
      }
      else if((FLSEEP_ERASED == pageStatus0) && (FLSEEP_VALID_PAGE == pageStatus1))
      {
        /* Page one is valid >> Search for active management block */
        /* Set the active page */
        flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId = 1;

        /* Switch to restor management block */
        flsEep_InitSmState = FLSEEP_INITSM_RESTORE_MGMTBLOCK;
        retVal = FLSEEP_E_REPEAT_CALL;
      }
      else if((FLSEEP_VALID_PAGE == pageStatus0) && (FLSEEP_VALID_PAGE == pageStatus1))
      {
        /* Both pages are valid >> Unexpected state >> formate flash */
        flsEep_InitSmState = FLSEEP_INITSM_FORMAT_FLASH;
        retVal = FLSEEP_E_REPEAT_CALL;
      }
      else
      {
        /* Unexpected state >> formate flash */
        flsEep_InitSmState = FLSEEP_INITSM_FORMAT_FLASH;
        retVal = FLSEEP_E_REPEAT_CALL;
      }
    }
    break;

    /************************************************************************************************************************/
    /* State FLSEEP_INITSM_RESTORE_MGMTBLOCK: Read the management block from flash                                          */
    /************************************************************************************************************************/
    case FLSEEP_INITSM_RESTORE_MGMTBLOCK:
    {
      uint8 *pToFlsPage;
      uint32 idx;
      boolean dataFound = FALSE;

      /* Set active last page address >> Searching from top down */
      pToFlsPage = (uint8*)flsEep_PageConfig[flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId].pageStartAddress;

      /* Search for unerased memory */
      for(idx = (flsEep_PageConfig[flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId].pageLength - 1); idx != (sizeof(FlsEep_ManagementBlkType)); idx--)
      {
        if(pToFlsPage[idx] != FLSEE_ERASED_FLS_VALUE)
        {
          dataFound = TRUE;
          break;
        }
      }

      /* Check if data has been found */
      if(TRUE == dataFound)
      {
        fncRetVal = FlsEep_Int_RestoreFlsMgmtBlock((uint8*)&pToFlsPage[idx]);
        if(FLSEEP_E_OK == fncRetVal)
        {
          flsEep_InitSmState = FLSEEP_INITSM_CHECK_PAGE_STATUS;
          retVal = FLSEEP_E_OK;
        }
        else
        {
          flsEep_InitSmState = FLSEEP_INITSM_FORMAT_FLASH;
          retVal = FLSEEP_E_REPEAT_CALL;
        }
      }
      else
      {
        /* Management block not found >> Set default management block */
        flsEep_InitSmState = FLSEEP_INITSM_WRITE_DEFAULT_MGMTBLK;
        retVal = FLSEEP_E_REPEAT_CALL;
      }
    }
    break;

    /************************************************************************************************************************/
    /* State FLSEEP_INITSM_FORMAT_FLASH: Flash invalid >> Format flash memory                                               */
    /************************************************************************************************************************/
    case FLSEEP_INITSM_FORMAT_FLASH:
    {
      flsRetVal = Fls_UnLockFlash();
      flsRetVal |= Fls_SyncEraseFlsSector(flsEep_PageConfig[0].pageFlsSectorId);
      flsRetVal |= Fls_SyncEraseFlsSector(flsEep_PageConfig[1].pageFlsSectorId);
      if(FLS_E_OK != flsRetVal)
      {
        #if(STD_ON == FLSEEP_MODULE_USE_DET)
        Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_GENERIC_PROGRAMMING_FAILURE);
        #endif
        flsEep_InitSmState = FLSEEP_INITSM_CHECK_PAGE_STATUS;
        retVal = FLSEEP_E_NOT_OK;
      }
      else
      {
        flsEep_InitSmState = FLSEEP_INITSM_CHECK_PAGE_STATUS;
        retVal = FLSEEP_E_REPEAT_CALL;
      }
    }
    break;

    /************************************************************************************************************************/
    /* State FLSEEP_INITSM_WRITE_DEFAULT_MGMTBLK: Writing a valid default management block                                  */
    /************************************************************************************************************************/
    case FLSEEP_INITSM_WRITE_DEFAULT_MGMTBLK:
    {
      uint8* pToMgmtBlock = (uint8*)&flsEep_MgmtFlashBlock;
      uint16 idx;
      uint32 activeFlsAddress = flsEep_PageConfig[flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId].pageStartAddress + 2;

      /* Set Ram default management block */
      FlsEep_Int_SetDefaultManagementBlock();

      /* Program management block to Fls */
      Fls_UnLockFlash();
      for(idx = 0; idx < sizeof(FlsEep_ManagementBlkType); idx++)
      {
        flsRetVal = Fls_SyncProgramByte(*pToMgmtBlock, activeFlsAddress);
        pToMgmtBlock++;
        activeFlsAddress++;

        if(FLS_E_OK != flsRetVal)
        {
          #if(STD_ON == FLSEEP_MODULE_USE_DET)
          Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_GENERIC_PROGRAMMING_FAILURE);
          #endif
          flsEep_InitSmState = FLSEEP_INITSM_CHECK_PAGE_STATUS;
          retVal = FLSEEP_E_NOT_OK;
          break;
        }

      }

      /* Check if error occurred */
      if(FLSEEP_INITSM_CHECK_PAGE_STATUS != flsEep_InitSmState)
      {
        /* Set internal active management data */
        flsEep_Mgmt.flsEep_Mgmt_ActiveFlsAddress = flsEep_PageConfig[flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId].pageStartAddress  + sizeof(FlsEep_ManagementBlkType) + 2;
        flsEep_Mgmt.flsEep_Mgmt_FlsPageMemoryFree = flsEep_PageConfig[flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId].pageLength - (sizeof(FlsEep_ManagementBlkType) + 2);

        /* No Error occurred >> Retrun ok */
        flsEep_InitSmState = FLSEEP_INITSM_CHECK_PAGE_STATUS;
        retVal = FLSEEP_E_OK;
      }
    }
    break;

    /************************************************************************************************************************/
    /* State default: Unexpected system state >> Report error!!!                                                            */
    /************************************************************************************************************************/
    default:
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_GENERIC_PROGRAMMING_FAILURE);
    #endif
    flsEep_InitSmState = FLSEEP_INITSM_CHECK_PAGE_STATUS;
    retVal = FLSEEP_E_REPEAT_CALL;
    break;
  }

  return retVal;
}

/**
 * @brief           Processing all request in active request list
 * @param[in]       None
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK         >> Request performed 
 *                  > FLSEEP_E_SKIPPED    >> Request skipped >> Not enough memory free >> Refactor flash memory
 */
FlsEep_ReturnType FlsEep_Int_ProcessRequests( void )
{
  FlsEep_ReturnType retVal = FLSEEP_E_NOT_OK;
  FlsEep_RequestType requestType;
  uint8 activeListLoad;
  boolean writeRequestProcessed = FALSE;
  uint8 idx;
  Crc_ReturnType crcRetVal;

  /* Check if active page has enough memory free */
  if(flsEep_Mgmt.flsEep_Mgmt_FlsPageMemoryFree > (FlsEep_RequestList_GetCompleteListWriteDataLoad() + sizeof(FlsEep_ManagementBlkType)))
  {
    activeListLoad = FlsEep_RequestList_GetRequestLoad();
    for(idx = 0; idx < activeListLoad; idx++)
    {
      requestType = FlsEep_RequestList_GetActiveRequestType();
      if(FLSEEP_REQUEST_WRITE == requestType )
      {
        writeRequestProcessed = TRUE;
        FlsEep_Int_ProcessWriteRequests();
      }
      else
      {
        FlsEep_Int_ProcessReadRequests();
      }
    }

    if(TRUE == writeRequestProcessed )
    {
      /* Update flash management block */
      flsEep_MgmtFlashBlock.blkAddressList[0].flsEepDataBlkAddress = flsEep_Mgmt.flsEep_Mgmt_ActiveFlsAddress;

      /* Calculate management block CRC */
      crcRetVal = Crc32_0x04C11DB7_Compute((uint8*)&flsEep_MgmtFlashBlock, (sizeof(FlsEep_ManagementBlkType) - 4) , &flsEep_MgmtFlashBlock.mgmtBlkCrc);
      if(CRC_E_OK != crcRetVal)
      {
        #if(STD_ON == FLSEEP_MODULE_USE_DET)
        Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_GENERIC_PROGRAMMING_FAILURE);
        #endif
      }

      /* Write Data */
      FlsEep_Int_WriteData( (uint8*)&flsEep_MgmtFlashBlock, sizeof(FlsEep_ManagementBlkType));
    }

    retVal = FLSEEP_E_OK;
    
  }
  else
  {
    /* Not enough memory free >> first reformat the flash memory */
    retVal = FLSEEP_E_SKIPPED;
  }
  return retVal;
}

/**
 * @brief           Processing function to refactor the flash pages
 * @param[in]       None
 * @retval          None
 */
void FlsEep_Int_ProcessRefactorFlsPages( void )
{
  uint16 validFlashValue = FLSEEP_VALID_PAGE, blockIdx;
  uint32 tempFlsAddress;
  uint8 oldPageCfgId;
  Crc_ReturnType crcRetVal;

  /* Step 1: Select new flash page */
  if(0 == flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId)
  {
    /* Set new page config id */
    flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId = 1;
    oldPageCfgId = 0;
  }
  else
  {
    /* Set new page config id */
    flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId = 0;
    oldPageCfgId = 1;
  }

  /* Step 2: Set new effective flash address  */
  flsEep_Mgmt.flsEep_Mgmt_ActiveFlsAddress = flsEep_PageConfig[flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId].pageStartAddress;
  flsEep_Mgmt.flsEep_Mgmt_FlsPageMemoryFree = flsEep_PageConfig[flsEep_Mgmt.flsEep_Mgmt_ActivePageCfgId].pageLength;

  /* Step 3: Set new flash page to valid  */
  FlsEep_Int_WriteData((uint8*)&validFlashValue, 2);

  /* Step 4: Copy all data blocks (except the management block) from last flash page to new flash page */
  for(blockIdx = 1; blockIdx <= (FLSEEP_CNT_OF_CONFIGURED_FLS_BLOCKS -1); blockIdx++)
  {
    /* Store active flash address */
    tempFlsAddress = flsEep_Mgmt.flsEep_Mgmt_ActiveFlsAddress;

    /* Copy block data to new flash address */
    FlsEep_Int_WriteData((uint8*)flsEep_MgmtFlashBlock.blkAddressList[blockIdx].flsEepDataBlkAddress, flsEepConfiguredBlocks[blockIdx].flsEepBlkLength);

    /* Update management block */
    flsEep_MgmtFlashBlock.blkAddressList[blockIdx].flsEepDataBlkAddress = tempFlsAddress;
  }

  /* Step 3: Write new management block */
  /* Store active flash address */
  flsEep_MgmtFlashBlock.blkAddressList[0].flsEepDataBlkAddress = flsEep_Mgmt.flsEep_Mgmt_ActiveFlsAddress;
  flsEep_MgmtFlashBlock.blkAddressList[0].flsEepDataBlkId = flsEepConfiguredBlocks[0].flsEepBlkId;
  
  /* Calculate management block CRC */
  crcRetVal = Crc32_0x04C11DB7_Compute((uint8*)&flsEep_MgmtFlashBlock, (sizeof(FlsEep_ManagementBlkType) - 4) , &flsEep_MgmtFlashBlock.mgmtBlkCrc);
  if(CRC_E_OK != crcRetVal)
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_GENERIC_PROGRAMMING_FAILURE);
    #endif
  }
  /* Copy block data to new flash address */
  FlsEep_Int_WriteData((uint8*)&flsEep_MgmtFlashBlock, sizeof(FlsEep_ManagementBlkType));

  /* Step 4: Format old flash block */
  Fls_UnLockFlash();
  Fls_SyncEraseFlsSector(flsEep_PageConfig[oldPageCfgId].pageFlsSectorId);

}

/**
 * @brief           Search in configured blocks for the actual block id and return the configuration index
 * @param[in]       uint32 >> Block Id which shall be validated
 * @param[out]      uint16 >> index of block id in block config buffer 
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK         >> Block found 
 *                  > FLSEEP_E_NOT_OK     >> Block not found 
 */
FlsEep_ReturnType FlsEep_Int_SearchCfgIndexFromBlkId( uint32 blkId, uint16 *cfgIdx )
{
  FlsEep_ReturnType retVal = FLSEEP_E_NOT_OK;
  uint16 idx;

  for(idx = 1; idx < FLSEEP_CNT_OF_CONFIGURED_FLS_BLOCKS; idx++)
  {
    if(flsEepConfiguredBlocks[idx].flsEepBlkId == blkId)
    {
      *cfgIdx = idx;
      retVal = FLSEEP_E_OK;
      break;
    }
  }

  return retVal;
}
#define FlsEep_STOP_SEC_CODE
#include "FlsEep_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */
