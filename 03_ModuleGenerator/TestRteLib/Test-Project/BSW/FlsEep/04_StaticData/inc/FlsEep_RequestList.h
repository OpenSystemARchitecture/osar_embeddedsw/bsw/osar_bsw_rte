/*****************************************************************************************************************************
 * @file        FlsEep_RequestList.h                                                                                         *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.08.2018 08:29:23                                                                                          *
 * @brief       Implementation of module global datatypes from the "FlsEep" module.                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __FLSEEP_REQUESTLIST_H
#define __FLSEEP_REQUESTLIST_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup FlsEep
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "FlsEep_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Module initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module
 */
void FlsEep_RequestList_Init(void);

/**
 * @brief           Add a new request entry to user request list
 * @param[in]       uint16 FlsEep block config index
 * @param[in]       uint8* pointer to ram block data
 * @param[in]       FlsEep_RequestType request type
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK            >> Request accepted
 *                  > FLSEEP_E_NOT_OK        >> Request not accepted
 */
FlsEep_ReturnType FlsEep_RequestList_AddNewRequest( uint16 blkCfgIdx, uint8* pBlkRamDataAddress, FlsEep_RequestType requestType );

/**
 * @brief           Get the pointer to the active request element
 * @param[out]      const FlsEep_RequestListEntryType* const pointer to active element
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK            >> Request accepted
 *                  > FLSEEP_E_NOT_OK        >> Request not accepted
 */
FlsEep_ReturnType FlsEep_RequestList_GetActiveRequest( FlsEep_RequestListEntryType* activeRequestElement );

/**
 * @brief           Get the status of a requested block
 * @param[in]       uint16 FlsEep block config index
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK            >> Request processed correctly 
 *                  > FLSEEP_E_NOT_OK        >> Request processing failed
 *                  > FLSEEP_E_PENDING       >> Request still in progres 
 *                  > FLSEEP_E_INVALIDATE    >> Request skipped because of invalidate flash data
 * @note            Is requested is is not in list, the status would be reported as ok.
 */
FlsEep_ReturnType FlsEep_RequestList_GetRequestStatus( uint16 blkCfgIdx );

/**
 * @brief           Get the status of the active requested block
 * @param[in]       None
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK            >> Request processed correctly 
 *                  > FLSEEP_E_NOT_OK        >> Request processing failed
 *                  > FLSEEP_E_PENDING       >> Request still in progres 
 *                  > FLSEEP_E_INVALIDATE    >> Request skipped because of invalidate flash data
 */
FlsEep_ReturnType FlsEep_RequestList_GetActiveRequestStatus( void );

/**
 * @brief           Get the active request type
 * @param[in]       None
 * @retval          FlsEep_RequestType
 *                  > FLSEEP_REQUEST_WRITE       >> Request write
 *                  > FLSEEP_REQUEST_READ        >> Request read
 */
FlsEep_RequestType FlsEep_RequestList_GetActiveRequestType( void );

/**
 * @brief           Set a new request processing status
 * @param[in]       FlsEep_ReturnType new processing status
 *                  > FLSEEP_E_OK
 *                  > FLSEEP_E_NOT_OK
 *                  > FLSEEP_E_PENDING
 *                  > FLSEEP_E_INVALIDATE
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK            >> Request accepted
 *                  > FLSEEP_E_NOT_OK        >> Request not accepted
 * @note            If the new status is OK / Not_Ok / Skipped the system would change the active status element.
 */
FlsEep_ReturnType FlsEep_RequestList_SetActiveRequestStatus( FlsEep_ReturnType requestProcessingStatus );

/**
 * @brief           Get a request list load
 * @param[in]       None
 * @retval          uint8 list load
 */
uint8 FlsEep_RequestList_GetRequestLoad( void );

/**
 * @brief           Get complete data load of all pending list elements 
 * @param[in]       None
 * @retval          uint32 list data load
 */
uint32 FlsEep_RequestList_GetCompleteListWriteDataLoad(void);

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __FLSEEP_REQUESTLIST_H*/
