/*****************************************************************************************************************************
 * @file        FlsEep.c                                                                                                     *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        21.03.2019 07:43:30                                                                                          *
 * @brief       Generated Rte Module Interface file: FlsEep.c                                                                *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup FlsEep 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_FlsEep.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of USER include and definition area           << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "FlsEep.h"
#include "FlsEep_Int.h"
#include "FlsEep_RequestList.h"

/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define FlsEep_START_SEC_NOINIT_VAR
#include "FlsEep_MemMap.h"
FlsEep_MainSm_StateType flsEep_MainSmState;
#define FlsEep_STOP_SEC_NOINIT_VAR
#include "FlsEep_MemMap.h"

/*------------------------------------------------ Extern defined data -----------------------------------------------------*/
extern FlsEep_RuntimeFlsPageMgmtType flsEep_Mgmt;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           End of USER include and definition area            << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map functions into code memory ---------------------------------------------*/
#define FlsEep_START_SEC_CODE
#include "FlsEep_MemMap.h"
/**
 * @brief           Rte generated module initialization function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: System Init
 * @uuid            58929ec3fafe4149a668a028038b25a3
 */
VAR(void) FlsEep_Init( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  /* Initialize Sub modules */
  FlsEep_Int_Init();
  FlsEep_RequestList_Init();
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module cyclic function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: Cyclic 10ms
 * @uuid            269ceade94654760aba908370d711b4d
 */
VAR(void) FlsEep_Mainfunction( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  FlsEep_ReturnType fncRetVal;

  switch(flsEep_MainSmState)
  {
    /************************************************************************************************************************/
    /* State FLSEEP_MAINSM_INIT: Trigger the init statemachine until the flash has been complete initalized.                */
    /************************************************************************************************************************/
    case FLSEEP_MAINSM_INIT:
    do{
      fncRetVal = FlsEep_Int_ProcessInitSm();
    }while(FLSEEP_E_REPEAT_CALL == fncRetVal);

    if(FLSEEP_E_OK == fncRetVal)
    {
      /* Ok >> Continue next cycle */
      flsEep_MainSmState = FLSEEP_MAINSM_READY_FOR_OPERATION;
      break;
    }
    else if(FLSEEP_E_PENDING == fncRetVal)
    {
      /* Pending repeat next cycle */
      break;
    }
    else
    {
      /* Failure occurred */
      flsEep_MainSmState = FLSEEP_MAINSM_SAFE_STATE;
      break;
    }
    break;

    /************************************************************************************************************************/
    /* State FLSEEP_MAINSM_INIT: Wait for user requests of actions                                                          */
    /************************************************************************************************************************/
    case FLSEEP_MAINSM_READY_FOR_OPERATION:
    /* Check if active page has enough memory free */
    if(flsEep_Mgmt.flsEep_Mgmt_FlsPageMemoryFree <= (FlsEep_RequestList_GetCompleteListWriteDataLoad() + sizeof(FlsEep_ManagementBlkType)))
    {
      flsEep_MainSmState = FLSEEP_MAINSM_REFACTOR_FLSPAGES;
      break;
    }

    /* Check if new data is available */
    if( 0 != FlsEep_RequestList_GetRequestLoad())
    {
      flsEep_MainSmState = FLSEEP_MAINSM_PROCESS_REQUEST;
      break;
    }

    break;

    /************************************************************************************************************************/
    /* State FLSEEP_MAINSM_PROCESS_REQUEST: Trigger the processing request state machine.                                   */
    /************************************************************************************************************************/
    case FLSEEP_MAINSM_PROCESS_REQUEST:
    fncRetVal = FlsEep_Int_ProcessRequests();
    if(FLSEEP_E_OK ==  fncRetVal)
    {
      /* Request performed >> Go back to FLSEEP_MAINSM_READY_FOR_OPERATION */
      flsEep_MainSmState = FLSEEP_MAINSM_READY_FOR_OPERATION;
    }
    else
    {
      /* Request skipped >> Not enough memory free >> Refactor flash pages */
      flsEep_MainSmState = FLSEEP_MAINSM_REFACTOR_FLSPAGES;
    }
    break;

    /************************************************************************************************************************/
    /* State FLSEEP_MAINSM_REFACTOR_FLSPAGES: Page is full so refactor pages                                                */
    /************************************************************************************************************************/
    case FLSEEP_MAINSM_REFACTOR_FLSPAGES:
    /* Trigger refactoration of flash pages */
    FlsEep_Int_ProcessRefactorFlsPages();

    /* Request performed >> Go back to FLSEEP_MAINSM_READY_FOR_OPERATION */
    flsEep_MainSmState = FLSEEP_MAINSM_READY_FOR_OPERATION;
    break;

    /************************************************************************************************************************/
    /* State FLSEEP_MAINSM_PROCESS_WRITE: Trigger the write state machine.                                                  */
    /************************************************************************************************************************/
    case FLSEEP_MAINSM_SAFE_STATE:
    //TODO: Add Implementation in State FLSEEP_MAINSM_SAFE_STATE
    break;

    /************************************************************************************************************************/
    /* State default: Unexpected system state >> Report error!!!                                                            */
    /************************************************************************************************************************/
    default:
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_GENERIC_PROGRAMMING_FAILURE);
    #endif
    flsEep_MainSmState = FLSEEP_MAINSM_INIT;
    break;
  }
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

#define FlsEep_STOP_SEC_CODE
#include "FlsEep_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of USER function area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define FlsEep_START_SEC_CODE
#include "FlsEep_MemMap.h"
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfunction.
 */
void FlsEep_InitMemory( void )
{
  flsEep_MainSmState = FLSEEP_MAINSM_INIT;
}


/**
 * @brief           Trigger a write action into the Flash
 * @param[in]       uint32 FlsEep configured block id
 * @param[in]       uin8* pointer to data which shall be written
 * @retval          Std_ReturnType
 *                  > E_OK        >> Request accepted
 *                  > E_NOT_OK    >> Request failed.
 *                  > E_SKIPPED   >> Request has not been accepted.
 * @details         Interface function to trigger a write action. This Function dose not perform the write action. The status
 *                  of the active block has to be additional requested.
 */
Std_ReturnType FlsEep_RequestWriteData( uint32 flsBlockId, uint8* pWriteData )
{
  Std_ReturnType stdRetVal;
  FlsEep_ReturnType fncRetVal;
  uint16 blkCfgIdx;
  /* Check input data */
  if(NULL_PTR == pWriteData)
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_NULL_POINTER);
    #endif
    stdRetVal = E_NOT_OK;
  }
  else if( FLSEEP_E_NOT_OK == FlsEep_Int_SearchCfgIndexFromBlkId(flsBlockId, &blkCfgIdx) )
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_INVALID_ARGUMENT);
    #endif
    stdRetVal = E_NOT_OK;
  }
  else
  {
    /* Input data valid >> Store request */
    fncRetVal = FlsEep_RequestList_AddNewRequest(blkCfgIdx, pWriteData, FLSEEP_REQUEST_WRITE);

    if(FLSEEP_E_OK == fncRetVal)
    {
      /* Request accepted */
      stdRetVal = E_OK;
    }
    else if(FLSEEP_E_SKIPPED == fncRetVal)
    {
      /* Request has been skipped */
      stdRetVal = E_SKIPPED;
    }
    else
    {
      /* Request not accepted >> Invalid data of buffer full */
      stdRetVal = E_NOT_OK;
    }
  }
  return stdRetVal;
}

/**
 * @brief           Trigger a read action into the Flash
 * @param[in]       uint32 FlsEep configured block id
 * @param[in]       uint8* pointer to data where the read data shall be stored
 * @retval          Std_ReturnType
 *                  > E_OK        >> Request accepted
 *                  > E_NOT_OK    >> Request failed.
 *                  > E_SKIPPED   >> Request has not been accepted.
 * @details         Interface function to trigger a read action. This Function dose not perform the write action. The status
 *                  of the active block has to be additional requested. The FlsEep module would write the read data into the given pointer location.
 */
Std_ReturnType FlsEep_RequestReadData( uint32 flsBlockId, uint8* pReadData )
{
  Std_ReturnType stdRetVal;
  FlsEep_ReturnType fncRetVal;
  uint16 blkCfgIdx;

  /* Check input data */
  if(NULL_PTR == pReadData)
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_NULL_POINTER);
    #endif
    stdRetVal = E_NOT_OK;
  }
  else if( FLSEEP_E_NOT_OK == FlsEep_Int_SearchCfgIndexFromBlkId(flsBlockId, &blkCfgIdx) )
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_INVALID_ARGUMENT);
    #endif
    stdRetVal = E_NOT_OK;
  }
  else
  {
    /* Input data valid >> Store request */
    fncRetVal = FlsEep_RequestList_AddNewRequest(blkCfgIdx, pReadData, FLSEEP_REQUEST_READ);

    if(FLSEEP_E_OK == fncRetVal)
    {
      /* Request accepted */
      stdRetVal = E_OK;
    }
    else if(FLSEEP_E_SKIPPED == fncRetVal)
    {
      /* Request has been skipped */
      stdRetVal = E_SKIPPED;
    }
    else
    {
      /* Request not accepted >> Invalid data of buffer full */
      stdRetVal = E_NOT_OK;
    }
  }
  return stdRetVal;
}



/**
 * @brief           Request the actual block status
 * @param[in]       uint32 FlsEep configured block id
 * @retval          Data_StatusType
 *                  > DATA_E_OK            >> Block is valid
 *                  > DATA_E_PENDING       >> Block processing is still in progress
 *                  > DATA_E_INVALIDATE    >> Block data is not available
 *                  > DATA_E_SKIPPED       >> Request failed / not accepted
 * @details         Interface function to request the actual block status.
 */
Data_StatusType FlsEep_RequestBlockStatus( uint32 flsBlockId )
{
  FlsEep_ReturnType flsEepRetVal;
  Data_StatusType dataStatus = DATA_E_OK;
  uint16 cfgIdx;

  /* Search for corresponding block config index */
  if( FLSEEP_E_NOT_OK == FlsEep_Int_SearchCfgIndexFromBlkId(flsBlockId, &cfgIdx) )
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_INVALID_ARGUMENT);
    #endif
    dataStatus = DATA_E_SKIPPED;
  }
  else
  {
    flsEepRetVal = FlsEep_RequestList_GetRequestStatus(cfgIdx);

    switch(flsEepRetVal)
    {
      case FLSEEP_E_OK:
      dataStatus = DATA_E_OK;
      break;

      case FLSEEP_E_NOT_OK:
      dataStatus = DATA_E_SKIPPED;
      break;

      case FLSEEP_E_PENDING:
      dataStatus = DATA_E_PENDING;
      break;

      case FLSEEP_E_INVALIDATE:
      dataStatus = DATA_E_INVALIDATE;
      break;

      default:
      #if(STD_ON == FLSEEP_MODULE_USE_DET)
      Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_GENERIC_PROGRAMMING_FAILURE);
      #endif
      break;
    }
  }

  return dataStatus;
}

/**
 * @brief           Request the actual module status
 * @param[in]       None
 * @retval          FlsEep_ReturnType
 *                  > E_OK            >> Module is in idle mode
 *                  > E_PENDING       >> Module is still processing data
 *                  > E_NOT_OK        >> Module is in safe state
 * @details         Interface function to request the actual module status.
 */
Std_ReturnType FlsEep_RequestModuleStatus( void )
{
  Std_ReturnType retVal;

  /* Check statemachine state */
  if(FLSEEP_MAINSM_SAFE_STATE == flsEep_MainSmState)
  {
    retVal = E_NOT_OK;
  }
  else if(FLSEEP_MAINSM_READY_FOR_OPERATION == flsEep_MainSmState)
  {
    retVal = E_OK;
  }
  else
  {
    retVal = E_PENDING;
  }
  return retVal;
}

#define FlsEep_STOP_SEC_CODE
#include "FlsEep_MemMap.h"

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of USER function area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

