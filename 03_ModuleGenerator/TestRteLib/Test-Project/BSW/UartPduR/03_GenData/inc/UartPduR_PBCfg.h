/*****************************************************************************************************************************
 * @file        UartPduR_PBCfg.h                                                                                             *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        12.12.2020 17:15:21                                                                                          *
 * @brief       Generated header file data of the UartPduR module.                                                           *
 * @version     v.1.1.1                                                                                                      *
 * @generator   OSAR UartPduR Generator v.1.3.1.4                                                                            *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.7.1                                                                           *
*****************************************************************************************************************************/

#ifndef __UARTPDUR_PBCFG_H
#define __UARTPDUR_PBCFG_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup UartPduR 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "UartPduR.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*-------------------------------------------- UartPduR Module Det Information --------------------------------------------*/
#define UARTPDUR_DET_MODULE_ID                       3
#define UARTPDUR_MODULE_USE_DET                      STD_ON
/*---------------------------------------- General module configuration parameters ----------------------------------------*/
#define UARTPDUR_CNT_PDU_END_PATTERN                 2
#define UARTPDUR_CNT_OF_USED_UART_MODULES            1
#define UARTPDUR_CNT_OF_CONFIGURED_PDUS              1
#define UARTPDUR_CNT_OF_GLOBAL_MAX_DATA_BYTES        100
#define UARTPDUR_USE_CRC32ETHERNET_E2E_COMMUNICATION STD_OFF
/*------------------------------------------ Pdu specific transmission functions ------------------------------------------*/
#define UartPduR_SendUartPdu_0_DummyPdu(pData)       UartPduR_SendUartPdu(0, pData, 1)
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*-------------------------------------------- Uart PduR configuration buffers --------------------------------------------*/
extern const uint8 pduEndPattern[];

extern const uint8 listOfUsedUartModules[];

extern const UartPduR_PduConfigType uartPduR_PduConfigList[];

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------ Provide extern function notification prototypes ------------------------------------*/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __UARTPDUR_PBCFG_H*/
