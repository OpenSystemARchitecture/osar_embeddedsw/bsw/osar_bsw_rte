/*****************************************************************************************************************************
 * @file        UartPduR.h                                                                                                   *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        25.03.2018 12:37:53                                                                                          *
 * @brief       Implementation of definitions / interface function prototypes / datatypes and generic module interface       *
 *              informations of the "UartPduR" module.                                                                       *
 *                                                                                                                           *
 * @details     The Uart Pdu Router Module (UartPduR) is an abstraction module of the Uart Interface module. It shall be     *
 *              used to distribute Uart Frames to different Software module. So the Uart Interface could be used to          *
 *              realize different services. Therefor a special PDU / Frame layout muss be used.                              *
 *                                                                                                                           *
 * @version     1.0.3                                                                                                        *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#ifndef __UARTPDUR_H
#define __UARTPDUR_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup UartPduR
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
#include "UartPduR_Types.h"
#include "UartPduR_PBCfg.h"
#include "UartPduR_LoIf.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfuntion.
 */
void UartPduR_InitMemory( void );

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void UartPduR_Init( void );

/**
 * @brief           Module global mainfunction.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called form the actual runtime environment within an fixed cycle.
 */
void UartPduR_Mainfunction( void );

/* ######################################################################################################################## */
/* ################################## UARTPDUR PROVIDED GENERIC HIGH LEVEL INTERFACES ##################################### */
/* ######################################################################################################################## */
/**
* @brief           Module generic transmit function for UART Pdus
* @param[in]       uint16 unique UART PDU ID
* @param[in]       uint8 input pointer to transmitted Data
* @param[in]       uint16 length of data to be transmitted
* @retval          UartIf_ReturnType
*                  > UARTPDUR_E_NULL_POINTER
*                  > UARTPDUR_E_INVALID_PDU_LENGTH
*                  > UARTPDUR_E_ID_UNKNOWN
*                  > UARTPDUR_E_TRANSMITBUFFER_OVERFLOW
*                  > UARTPDUR_E_GENERIC_PROGRAMMING_FAILURE
*                  > UARTPDUR_E_OK
* @details         This function shall note be called from any other application. The equal interface functions would be generated
*/
UartPduR_ReturnType UartPduR_SendUartPdu(uint16 pduId, uint8 *pData, uint16 dataLength);

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __UARTPDUR_H*/
