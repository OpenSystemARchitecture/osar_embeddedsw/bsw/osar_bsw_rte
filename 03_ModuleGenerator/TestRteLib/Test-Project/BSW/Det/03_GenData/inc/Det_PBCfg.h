/*****************************************************************************************************************************
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        12.12.2020 17:15:20                                                                                          *
 * @brief       Generated header file data of the det module.                                                                *
 * @version     v.1.3.0                                                                                                      *
 * @generator   OSAR DET Generator v.1.3.1.8                                                                                 *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.7.1                                                                           *
*****************************************************************************************************************************/

#ifndef __DET_PBCFG_H
#define __DET_PBCFG_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Det 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Available Errors within the Det Module
 */
typedef enum{
  DET_E_DET_DEVIATIONS_FULL = 0
}DET_E_ERROR;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define DET_MODULE_ID               0
#define DET_MODULE_ENABLED          STD_ON
#define DET_MODULE_USE_DET          STD_ON
#define DET_MAX_DET_USER_DEVIATIONS 10
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map functions into code memory ---------------------------------------------*/
#define Det_START_SEC_CODE
#include "Det_MemMap.h"
/**
 * @brief           Enter Det_ReportError function.
 * @param[in]       Module Id
 * @param[in]       Error Id
 * @retval          None
 */
void Det_EnterReportErrorFnc( uint16 ModuleId, uint16 ErrorId );

/**
 * @brief           Leave Det_ReportError function.
 * @param[in]       Module Id
 * @param[in]       Error Id
 * @retval          None
 */
void Det_LeaveReportErrorFnc( uint16 ModuleId, uint16 ErrorId );

#define Det_STOP_SEC_CODE
#include "Det_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __DET_PBCFG_H*/
