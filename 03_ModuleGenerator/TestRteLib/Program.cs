﻿/******************************************************************************
  * @file    Program.cs
  * @author  Reinemuth Sebastian
  * @date    05-01-2019
  * @brief   Test RteLib
  * $Author: sreinemuth $ @ $Date: 2017-02-18 12:00:30 +0100 (Sa, 18 Feb 2017) $ , Revision: $Rev: 12 $
  *****************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RteLib.RteTypes;
using RteLib.RteConfig;
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;

namespace TestRteLib
{
  class Program
  {
    static RteAvailableClientPort clientPort = new RteAvailableClientPort();
    static RteAvailableServerPort serverPort = new RteAvailableServerPort();
    static RteAvailableSRPort senderPort = new RteAvailableSRPort();
    static RteAvailableSRPort receiverPort = new RteAvailableSRPort();

    static void Main(string[] args)
    {
      string pathToRteConfigFile;
      createDummyRteTypesFile();
      createDummyRteInterfaceFile();
      createDummyRteInternalBehaviourFile1();
      createDummyConfigFile();


      // Import config
      RteConfig rteConfigFile = new RteConfig();
      pathToRteConfigFile = "C:\\Repos\\git\\osar\\OSAR_EmbeddedSW\\BSW\\OSAR_BSW_Rte\\03_ModuleGenerator\\TestRteLib\\" +
        "Test-Project\\BSW\\Rte\\01_Generator\\RteModuleConfig.xml";

      rteConfigFile.ReadActiveRteConfigDataBaseFromXml(pathToRteConfigFile);
      RteConfigImportHandler rteConfigImporter = new RteConfigImportHandler(ref rteConfigFile, pathToRteConfigFile);


      rteConfigImporter.ImportMiBFileIntoRteConfig("Nvm", RteConfigModuleTypes.BSW);
      rteConfigImporter.ImportMiBFileIntoRteConfig("UartPduR", RteConfigModuleTypes.BSW);
      rteConfigImporter.ImportMiBFileIntoRteConfig("Testing", RteConfigModuleTypes.BSW);
      rteConfigImporter.ImportMiBFileIntoRteConfig("Testing2", RteConfigModuleTypes.BSW);

      rteConfigFile.SaveActiveRteConfigDataBaseToXml("C:\\Repos\\git\\osar\\OSAR_EmbeddedSW\\BSW\\OSAR_BSW_Rte\\03_ModuleGenerator\\TestRteLib\\" +
        "Test-Project\\BSW\\Rte\\01_Generator\\RteModuleConfig.xml");


      // Import interfaces
      RteInterfaces rteInterfacesFile = new RteInterfaces();
      rteInterfacesFile.ReadActiveRteInterfaceDataBaseFromXml("C:\\Repos\\git\\osar\\OSAR_EmbeddedSW\\BSW\\OSAR_BSW_Rte\\03_ModuleGenerator\\TestRteLib\\" +
        "Test-Project\\BSW\\Rte\\01_Generator\\RteInterfaces.xml");
      RteInterfaceImportHandler rteInterfaceImporter = new RteInterfaceImportHandler(rteConfigFile, ref rteInterfacesFile, pathToRteConfigFile);
      foreach(RteConfigUsedModuleCfg module in rteConfigFile.RteUsedModules)
      {
        rteInterfaceImporter.ImportMiBFileIntoRteInterface(module);
      }

      rteInterfacesFile.SaveActiveRteInterfaceDataBaseToXml("C:\\Repos\\git\\osar\\OSAR_EmbeddedSW\\BSW\\OSAR_BSW_Rte\\03_ModuleGenerator\\TestRteLib\\" +
        "Test-Project\\BSW\\Rte\\01_Generator\\RteInterfaces.xml");

      //Import Types
      RteTypes rteTypesFile = new RteTypes();
      rteTypesFile.ReadActiveRteTypesDataBaseFromXml("C:\\Repos\\git\\osar\\OSAR_EmbeddedSW\\BSW\\OSAR_BSW_Rte\\03_ModuleGenerator\\TestRteLib\\" +
        "Test-Project\\BSW\\Rte\\01_Generator\\RteTypes.xml");
      RteTypesImportHandler rteTypesImporter = new RteTypesImportHandler(rteConfigFile, ref rteTypesFile, pathToRteConfigFile);
      foreach (RteConfigUsedModuleCfg module in rteConfigFile.RteUsedModules)
      {
        rteTypesImporter.ImportMiBFileIntoRteTypes(module);
      }
      rteTypesFile.SaveActiveRteTypesDataBaseToXml("C:\\Repos\\git\\osar\\OSAR_EmbeddedSW\\BSW\\OSAR_BSW_Rte\\03_ModuleGenerator\\TestRteLib\\" +
        "Test-Project\\BSW\\Rte\\01_Generator\\RteTypes.xml");

    }

    static private void createDummyRteTypesFile()
    {
      RteTypes rteTypesFile = new RteTypes();
      RteTypes rteTypesFile2 = new RteTypes();
      RteAdvancedDataTypeStructure usrDataElement, usrDataElement2;
      RteAdvancedStructDataTypeStructure structElement;
      RteAdvancedEnumDataTypeStructure enumElement;

      rteTypesFile.SetupDefaultConfiguration();

      /* Add User Elements Standard */
      usrDataElement2 = new RteAdvancedDataTypeStructure();
      usrDataElement2.DataType = new RteBaseDataTypeStructure();
      usrDataElement2.DataType.DataAccessType = RteDataAccessType.STANDARD;
      usrDataElement2.DataType.DataTypeName = "TestStandardType_UINT8_WrongRef";
      usrDataElement2.DataType.UUID = Guid.NewGuid().ToString("N");
      usrDataElement2.DataTypeType = RteAdvancedDataTypeType.STANDARD;
      usrDataElement2.BaseDataType = rteTypesFile.BaseDataTypeList[0];
      rteTypesFile.AddAdvancedRteType(usrDataElement2);

      usrDataElement = new RteAdvancedDataTypeStructure();
      usrDataElement.DataType = new RteBaseDataTypeStructure();
      usrDataElement.DataType.DataAccessType = RteDataAccessType.STANDARD;
      usrDataElement.DataType.DataTypeName = "TestStandardType_UINT8";
      usrDataElement.DataType.UUID = Guid.NewGuid().ToString("N");
      usrDataElement.DataTypeType = RteAdvancedDataTypeType.STANDARD;
      usrDataElement.BaseDataType = usrDataElement2.DataType;
      rteTypesFile.AddAdvancedRteType(usrDataElement);

      /* Add User Elements Struct */
      usrDataElement = new RteAdvancedDataTypeStructure();
      usrDataElement.DataType = new RteBaseDataTypeStructure();
      usrDataElement.DataType.DataAccessType = RteDataAccessType.STANDARD;
      usrDataElement.DataType.DataTypeName = "TestStructType_Struct";
      usrDataElement.DataType.UUID = Guid.NewGuid().ToString("N");
      usrDataElement.StructElements = new List<RteAdvancedStructDataTypeStructure>();

      usrDataElement.DataTypeType = RteAdvancedDataTypeType.STRUCT;

      structElement = new RteAdvancedStructDataTypeStructure();
      structElement.BaseDataType = rteTypesFile.BaseDataTypeList[0];
      structElement.StructElementName = "StructElementU8";
      usrDataElement.StructElements.Add(structElement);

      structElement = new RteAdvancedStructDataTypeStructure();
      structElement.BaseDataType = rteTypesFile.BaseDataTypeList[2];
      structElement.StructElementName = "StructElementU16";
      usrDataElement.StructElements.Add(structElement);

      structElement = new RteAdvancedStructDataTypeStructure();
      structElement.BaseDataType = usrDataElement2.DataType;
      structElement.StructElementName = "StructElementWrongRef";
      usrDataElement.StructElements.Add(structElement);
      rteTypesFile.AddAdvancedRteType(usrDataElement);


      /* Add User Elements Enum */
      usrDataElement = new RteAdvancedDataTypeStructure();
      usrDataElement.DataType = new RteBaseDataTypeStructure();
      usrDataElement.DataType.DataAccessType = RteDataAccessType.STANDARD;
      usrDataElement.DataType.DataTypeName = "TestStructType_Enum";
      usrDataElement.DataType.UUID = Guid.NewGuid().ToString("N");
      usrDataElement.EnumElements = new List<RteAdvancedEnumDataTypeStructure>();

      usrDataElement.DataTypeType = RteAdvancedDataTypeType.ENUMERATION;

      enumElement = new RteAdvancedEnumDataTypeStructure();
      enumElement.EnumElementName = "EnumElement1";
      enumElement.EnumId = "0";
      usrDataElement.EnumElements.Add(enumElement);

      enumElement = new RteAdvancedEnumDataTypeStructure();
      enumElement.EnumElementName = "EnumElement2";
      usrDataElement.EnumElements.Add(enumElement);

      enumElement = new RteAdvancedEnumDataTypeStructure();
      enumElement.EnumElementName = "EnumElement3";
      usrDataElement.EnumElements.Add(enumElement);

      rteTypesFile.AddAdvancedRteType(usrDataElement);

      /* Store and Readback */
      rteTypesFile.SaveActiveRteTypesDataBaseToXml("C:\\Temp\\RteTypes.xml");
      rteTypesFile2.ReadActiveRteTypesDataBaseFromXml("C:\\Temp\\RteTypes.xml");

      /* Sort List */
      rteTypesFile2.SortRteTypesList();

      /* Validate List */
      List<string> validation;
      validation = rteTypesFile2.ValidateRteTypesList();

    }

    static private void createDummyConfigFile()
    {
      /* Create a config file */
      RteConfigUsedModuleCfg usedRteMod;
      RteConfig rteConfigFile = new RteConfig();
      rteConfigFile.PathToBaseProjectFolder = ".\\..\\..\\..\\";
      rteConfigFile.PathToRteInterfaceFile = ".\\RteInterfaces.xml";
      rteConfigFile.PathToRteTypeFile = ".\\RteTypes.xml";

      usedRteMod = new RteConfigUsedModuleCfg();
      usedRteMod.ModuleName = "Rte";
      usedRteMod.ModuleType = RteConfigModuleTypes.BSW;
      rteConfigFile.RteUsedModules.Add(usedRteMod);

      usedRteMod = new RteConfigUsedModuleCfg();
      usedRteMod.ModuleName = "ApSystemHeartBeat";
      usedRteMod.ModuleType = RteConfigModuleTypes.SWC;
      rteConfigFile.RteUsedModules.Add(usedRteMod);

      /* Adding Client Server Port connection */
      RteClientServerPortConnection clientServerPortConnection = new RteClientServerPortConnection();
      clientServerPortConnection.RteServerPort = serverPort.ServerPort.InterfacePrototype;
      clientServerPortConnection.RteClientPorts = new List<RteGeneralInterfaceInfo>();
      clientServerPortConnection.RteClientPorts.Add(clientPort.ClientPort.InterfacePrototype);
      
      rteConfigFile.RteClientServerConnections.Add(clientServerPortConnection);


      /* Adding Sender Receiver Port connection */
      RteSenderReceiverPortConnection senderReceiverPortConnection = new RteSenderReceiverPortConnection();
      senderReceiverPortConnection.RteSenderPort = senderPort.SenderReceiverPort.InterfacePrototype;
      senderReceiverPortConnection.RteReceiverPorts = new List<RteGeneralInterfaceInfo>();
      senderReceiverPortConnection.RteReceiverPorts.Add(receiverPort.SenderReceiverPort.InterfacePrototype);

      rteConfigFile.RteSenderReceiverConnections.Add(senderReceiverPortConnection);

      /* Adding Rte System Application */
      RteSystemApplication dummyApplication = new RteSystemApplication();
      dummyApplication.RteModule = new List<RteConfigUsedModuleCfg>();

      RteConfigUsedModuleCfg dummyModule = new RteConfigUsedModuleCfg();
      dummyModule.ModuleName = "ApSystemHeartBeat";
      dummyModule.ModuleType = RteConfigModuleTypes.SWC;

      dummyApplication.RteModule.Add(dummyModule);
      dummyApplication.RteSystemApplicationName = "DummyApplication";
      dummyApplication.UUID = Guid.NewGuid().ToString("N");

      rteConfigFile.RteSystemApplications.Add(dummyApplication);

      rteConfigFile.SaveActiveRteConfigDataBaseToXml("C:\\Temp\\RteConfig.xml");
    }

    static private void createDummyRteInterfaceFile()
    {
      RteInterfaces rteInterface1 = new RteInterfaces();
      RteInterfaces rteInterface2 = new RteInterfaces();
      RteBaseDataTypeStructure voidDataType = new RteBaseDataTypeStructure();
      RteBaseDataTypeStructure u8DataType = new RteBaseDataTypeStructure();
      RteBaseDataTypeStructure pU8DataType = new RteBaseDataTypeStructure();
      RteBaseDataTypeStructure U64DataType = new RteBaseDataTypeStructure();

      voidDataType.DataAccessType = RteDataAccessType.STANDARD;
      voidDataType.DataTypeName = "void";
      voidDataType.UUID = "afa6d4b6e5b24c039a86a7867d5c0fb8";

      u8DataType.DataAccessType = RteDataAccessType.STANDARD;
      u8DataType.DataTypeName = "uint8";
      u8DataType.UUID = "1433f660d6ae4a5c85e2222f77a7846e";

      pU8DataType.DataAccessType = RteDataAccessType.POINTER;
      pU8DataType.DataTypeName = "uint8";
      pU8DataType.UUID = "1433f660d6ae4a5c85e2222f77a7846e";

      U64DataType.DataAccessType = RteDataAccessType.STANDARD;
      U64DataType.DataTypeName = "uint64";
      U64DataType.UUID = "e08693433c954c0d822b2ddbbb1ee2ba";

      /* >>>> Create dummy Client Server Interface <<<< */
      RteCSInterfaceBlueprint rteCSBlueprint = new RteCSInterfaceBlueprint();
      rteCSBlueprint.InterfaceInfo.InterfaceName = "DummyCSInterface";
      rteCSBlueprint.InterfaceInfo.UUID = Guid.NewGuid().ToString("N");
      rteCSBlueprint.Functions = new List<RteFunctionBlueprint>();
      RteFunctionBlueprint rteFnc = new RteFunctionBlueprint();
      rteFnc.FunctionName = "dummyFncName";
      rteFnc.UUID = Guid.NewGuid().ToString("N");
      rteFnc.ReturnValue = voidDataType;
      rteFnc.ArgList = new List<RteFunctionArgumentBlueprint>();

      RteFunctionArgumentBlueprint fncArg = new RteFunctionArgumentBlueprint();
      fncArg.ArgumentName = "fncArg1";
      fncArg.ArgumentType = u8DataType;
      rteFnc.ArgList.Add(fncArg);

      fncArg = new RteFunctionArgumentBlueprint();
      fncArg.ArgumentName = "pFncArg2";
      fncArg.ArgumentType = pU8DataType;
      rteFnc.ArgList.Add(fncArg);
      rteFnc.AvailableErrorTypes = new List<string>();
      rteFnc.AvailableErrorTypes.Add("NotImplemented");
      rteFnc.AvailableErrorTypes.Add("Generic_Programming_Failure");
      rteFnc.AvailableErrorTypes.Add("NullPointer");

      /* Add function to port */
      rteCSBlueprint.Functions.Add(rteFnc);

      /* Add CS Port to blueprint list */
      rteInterface1.AddRteCSInterface(rteCSBlueprint);

      /* >>>> Create dummy Sender Receiver Interface <<<< */
      RteSRInterfaceBlueprint rteSRBlueprint = new RteSRInterfaceBlueprint();
      rteSRBlueprint.InterfaceInfo.InterfaceName = "DummySRInterface";
      rteSRBlueprint.InterfaceInfo.UUID = Guid.NewGuid().ToString("N");

      rteSRBlueprint.Elements = new List<RteElementBlueprint>();

      RteElementBlueprint rteSrElement = new RteElementBlueprint();
      rteSrElement.ElementName = "dummySRElement1";
      rteSrElement.UUID = Guid.NewGuid().ToString("N");

      rteSrElement.ElementType = u8DataType;
      rteSRBlueprint.Elements.Add(rteSrElement);

      rteSrElement = new RteElementBlueprint();
      rteSrElement.ElementName = "dummySRElement2";
      rteSrElement.UUID = Guid.NewGuid().ToString("N");
      rteSrElement.ElementType = U64DataType;
      rteSRBlueprint.Elements.Add(rteSrElement);

      /* Add SR Port to blueprint list */
      rteInterface1.AddRteSRInterface(rteSRBlueprint);

      /* Store and Readback */
      rteInterface1.SaveActiveRteInterfaceDataBaseToXml("C:\\Temp\\RteInterface.xml");
      rteInterface2.ReadActiveRteInterfaceDataBaseFromXml("C:\\Temp\\RteInterface.xml");


    }

    static private void createDummyRteInternalBehaviourFile1()
    {
      RteModuleInternalBehavior dummyRteInternalBehavior = new RteModuleInternalBehavior();
      RteModuleInternalBehavior dummyRteInternalBehavior1 = new RteModuleInternalBehavior();

      /* Create available Client Ports */
      clientPort.ClientPort.InterfaceBlueprint.InterfaceName = "DummyCSInterface";
      clientPort.ClientPort.InterfaceBlueprint.UUID = "a976318d7d73414f9b05db8688f42aa9";
      clientPort.ClientPort.InterfacePrototype.InterfaceName = "PpDummyCSInterface";
      clientPort.ClientPort.InterfacePrototype.UUID = Guid.NewGuid().ToString("N");

      clientPort.ClientPort.InterfaceType = RteCSInterfaceImplementationType.CLIENT;

      RteGenericRunnableObject clientPortFnc = new RteGenericRunnableObject();
      clientPortFnc.Name = "dummyFncName";
      clientPortFnc.UUID = "a4267b84dfdb40a4a196661338cefe0b";
      clientPort.ClientPortFunctions = new List<RteGenericRunnableObject>();
      clientPort.ClientPortFunctions.Add(clientPortFnc);

      /* Create available Server Port */
      serverPort.ServerPort.InterfaceBlueprint.InterfaceName = "DummyCSInterface";
      serverPort.ServerPort.InterfaceBlueprint.UUID = "a976318d7d73414f9b05db8688f42aa9";
      serverPort.ServerPort.InterfaceType = RteCSInterfaceImplementationType.SERVER;
      serverPort.ServerPort.InterfacePrototype.InterfaceName = "PpDummyCSInterface2";
      serverPort.ServerPort.InterfacePrototype.UUID = Guid.NewGuid().ToString("N");

      serverPort.ServerPortRunnables = new List<RteRunnable>();

      RteRunnable serverPortRunnabe = new RteRunnable();
      RteGeneralInterfaceInfo newRunableInfo = new RteGeneralInterfaceInfo();
      newRunableInfo.InterfaceName = "TestRunnable";
      newRunableInfo.UUID = "a976318d7d73414f9b05ab8688f42aa9";
      serverPortRunnabe.RunnableInfo = newRunableInfo;
      serverPort.ServerPortRunnables.Add(serverPortRunnabe);

      dummyRteInternalBehavior.AvailableClientPorts.Add(clientPort);
      dummyRteInternalBehavior.AvailableServerPorts.Add(serverPort);

      /* Create available Sender Ports */
      senderPort.SenderReceiverPort.InterfaceBlueprint.InterfaceName = "DummySRInterface";
      senderPort.SenderReceiverPort.InterfaceBlueprint.UUID = "e1be12729e5c4aab95ddebe34610eaa1";
      senderPort.SenderReceiverPort.InterfaceType = RteSRInterfaceImplementationType.SENDER;
      senderPort.SenderReceiverPort.InterfacePrototype.InterfaceName = "PpDummySRInterface";
      senderPort.SenderReceiverPort.InterfacePrototype.UUID = Guid.NewGuid().ToString("N");

      senderPort.SenderReceiverPortElements = new List<RteGenericRunnableObject>();

      RteGenericRunnableObject senderElement = new RteGenericRunnableObject();
      senderElement.Name = "dummySRElement1";
      senderElement.UUID = "ae9f64d29e324c8088a66904c8b47be1";
      senderPort.SenderReceiverPortElements.Add(senderElement);

      senderElement = new RteGenericRunnableObject();
      senderElement.Name = "dummySRElement2";
      senderElement.UUID = "ddb6d1a47945499d9ee252c5e884145b";
      senderPort.SenderReceiverPortElements.Add(senderElement);

      dummyRteInternalBehavior.AvailableSRPorts.Add(senderPort);

      /* Create available Receiver Ports */
      receiverPort.SenderReceiverPort.InterfacePrototype.InterfaceName = "DummySRInterfaceReceiver";
      receiverPort.SenderReceiverPort.InterfacePrototype.UUID = Guid.NewGuid().ToString("N");
      receiverPort.SenderReceiverPort.InterfaceType = RteSRInterfaceImplementationType.RECEIVER;
      receiverPort.SenderReceiverPortElements = senderPort.SenderReceiverPortElements;

      dummyRteInternalBehavior.AvailableSRPorts.Add(receiverPort);



      //Create Dummy Module
      dummyRteInternalBehavior.ModuleName = "TestDummy";
      dummyRteInternalBehavior.ModuleType = RteConfigModuleTypes.SWC;

      //Create Dummy Init Runnable
      RteRunnable initRunnable = new RteRunnable();
      initRunnable.RunnableInfo.InterfaceName = "";
      initRunnable.RunnableInfo.UUID = Guid.NewGuid().ToString("N");
      dummyRteInternalBehavior.InitRunnables.Add(initRunnable);

      //Create Dummy Cyclic Runnable
      RteCyclicRunnable cyclicRunnable = new RteCyclicRunnable();
      cyclicRunnable.Runnable.RunnableInfo.InterfaceName = "Mainfunction";
      cyclicRunnable.Runnable.RunnableInfo.UUID = Guid.NewGuid().ToString("N");
      cyclicRunnable.CycleTime = 100;


      cyclicRunnable.Runnable.AccessToClientPortFunctions = new List<RteGenericRunnableObject>();
      cyclicRunnable.Runnable.AccessToClientPortFunctions.Add(clientPortFnc);
      cyclicRunnable.Runnable.AccessToSenderReceiverPortElements = new List<RteGenericRunnableObject>();
      cyclicRunnable.Runnable.AccessToSenderReceiverPortElements.Add(senderElement);

      dummyRteInternalBehavior.CyclicRunnables.Add(cyclicRunnable);



      /* Add backup CS Interface */
      RteInterfaces rteInterface1 = new RteInterfaces();
      rteInterface1.ReadActiveRteInterfaceDataBaseFromXml("C:\\Temp\\RteInterface.xml");
      dummyRteInternalBehavior.BackupCSBlueprintList.Add(rteInterface1.ClientServerBlueprintList[0]);
      dummyRteInternalBehavior.BackupSRBlueprintList.Add(rteInterface1.SenderReceiverBlueprintList[0]);

      RteTypes rteTypesFile = new RteTypes();
      rteTypesFile.ReadActiveRteTypesDataBaseFromXml("C:\\Temp\\RteTypes.xml");
      dummyRteInternalBehavior.BackupAdvancedDataTypeList.Add(rteTypesFile.AdvancedDataTypeList[1]);

      dummyRteInternalBehavior.SaveActiveRteModuleInternalBehaviorToXml("C:\\Temp\\RteModuleInternalBehaviour.xml");
      dummyRteInternalBehavior1.ReadActiveRteModuleInternalBehaviorToXml("C:\\Temp\\RteModuleInternalBehaviour.xml");
    }
  }
}
