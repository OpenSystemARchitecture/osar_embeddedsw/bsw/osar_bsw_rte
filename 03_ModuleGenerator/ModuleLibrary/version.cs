/******************************************************************************
 * @file      version.cs
 * @author    OSAR S.Reinemuth
 * @proj      ModuleLibrary
 * @date      Sunday, January 17, 2021
 * @version   Application v. 1.0.1.1
 * @version   Generator   v. 1.2.5.1
 * @brief     Controls the assembly Version
 *****************************************************************************/
using System;
using System.Reflection;

[assembly: AssemblyVersion("1.0.1.1")]

namespace ModuleLibrary
{
  static public class ModuleLibraryVersionClass
  {
    public static int major { get; set; }  //Version of the program
    public static int minor { get; set; }  //Sub version of the program
    public static int patch { get; set; }  //Debug patch of the program
    public static int build { get; set; }  //Count program builds

    static ModuleLibraryVersionClass()
    {
			major = 1;
			minor = 0;
			patch = 1;
			build = 1;
    }

    public static string getCurrentVersion()
    {
      return major.ToString() + '.' + minor.ToString() + '.' + patch.ToString() + '.' + build.ToString();
    }
  }
}
//!< @version 0.0.1	->	Initial Project Setup
//!< @version 0.1.0	->	Add initial version of the module generator library
//!< @version 0.1.1	->	Update UI to configure the System Applications
//!< @version 0.1.2	->	Fix rte advanced "enum" type generation issue.
//!< @version 1.0.0	->	Use new RteLib and Data with database xml file version 2.x
//!< @version 1.0.1	->	Fix bug in Rte-SystemApplication view. Delete of SysApp now reflected to config file.
