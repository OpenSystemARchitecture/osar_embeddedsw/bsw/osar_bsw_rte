﻿/*****************************************************************************************************************************
 * @file        HyperspaceProjectConfig.cs                                                                                   *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.01.2020                                                                                                   *
 * @brief       Implementation of the hyperspace project config model                                                        *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Models
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleLibrary.Versions.v_2_0_0.Models
{
  /// <summary>
  /// Implementation of the Data Model for the Used Module Structure
  /// </summary>
  public class CfgSpace_ModuleCfg
  {
    /// <summary>
    /// Interface for Module Name
    /// </summary>
    public String ModuleName { get; set; }

    /// <summary>
    /// Interface for Module Type
    /// </summary>
    public RteConfigModuleTypes ModuleType { get; set; }

    /// <summary>
    /// Interface for the module base folder
    /// </summary>
    public String ModuleBaseFolder { get; set; }

    /// <summary>
    /// Interface for the Module Library Path
    /// </summary>
    public String ModuleLibPath { get; set; }

    /// <summary>
    /// Interface for the Configuration File Path
    /// </summary>
    public String CfgFilePath { get; set; }

    /// <summary>
    /// Interface to select if the module shall be generated and validated
    /// </summary>
    public bool ValidateAndGenerateModule { get; set; }
  }

  /// <summary>
  /// Hyperspace config
  /// </summary>
  public struct HyperspaceProjectConfig
  {
    public string BaseProjectFolderPath;
    public string ProjectName;
    public List<CfgSpace_ModuleCfg> CfgSpaceModuleList;
  }
}
  /**
 * @}
 */
   