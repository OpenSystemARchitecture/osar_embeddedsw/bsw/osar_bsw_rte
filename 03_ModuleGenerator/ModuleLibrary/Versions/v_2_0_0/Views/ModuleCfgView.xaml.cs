﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ModuleLibrary.Versions.v_2_0_0.ViewModels;

namespace ModuleLibrary.Versions.v_2_0_0.Views
{
  /// <summary>
  /// Interaction logic for the ModuleCfgView.xaml
  /// </summary>
  public partial class ModuleCfgView : UserControl
  {
    ViewModels.Module_ViewModel newCfg;
    string pathToConfigFile;
    string absPathModuleBaseFolder;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile">Absolute Path to configuration file</param>
    /// <param name="absPathToModuleBaseFolder">Absolute Path to module base folder</param>
    public ModuleCfgView(string pathToCfgFile, string absPathToModuleBaseFolder)
    {
      /* Check if config file path is an rooted one */
      if (true == System.IO.Path.IsPathRooted(pathToCfgFile))
      {
        pathToConfigFile = pathToCfgFile;
      }
      else
      {
        pathToConfigFile = absPathToModuleBaseFolder + pathToCfgFile;
      }

      absPathModuleBaseFolder = absPathToModuleBaseFolder;

      newCfg = new ViewModels.Module_ViewModel(pathToCfgFile, absPathToModuleBaseFolder);

      InitializeComponent();

      /* Set Data Context where data is binded to*/
      this.DataContext = newCfg;

      /* Setup System Application List */
      SetupSystemAppViewList();

    }




    #region UI Actions
    /// <summary>
    /// Button to select the Hyperspace project file
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_HyperspaceProjectFilePath_Click(object sender, RoutedEventArgs e)
    {
      using (var openFileDialog = new System.Windows.Forms.OpenFileDialog())
      {
        openFileDialog.InitialDirectory = absPathModuleBaseFolder;
        openFileDialog.Filter = "Hyperspace Project File |*" + ".hsprj";

        if (System.Windows.Forms.DialogResult.OK == openFileDialog.ShowDialog())
        {
          // Set path in view
          TB_HyperspaceProjectFile.Text = openFileDialog.FileName;

          // Set path in project file
          newCfg.HyperspaceProjectFilePath = openFileDialog.FileName;
        }
      }
    }

    // <summary>
    /// Button to convert absolute path into an relative path of the Hyperspace Project File
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_HyperspaceProjectFileRelativePath_Click(object sender, RoutedEventArgs e)
    {
      if (true == System.IO.Path.IsPathRooted(TB_HyperspaceProjectFile.Text))
      {
        var folderUri = new Uri(TB_HyperspaceProjectFile.Text, UriKind.Absolute);

        //string moduleBaseFolder = absPathModuleBaseFolder + "/";
        
        var referenceUri = new Uri(pathToConfigFile, UriKind.Absolute);

        string relativePath = "./" + referenceUri.MakeRelativeUri(folderUri).ToString();

        // Set path in view
        TB_HyperspaceProjectFile.Text = relativePath;

        // Set path in project file
        newCfg.HyperspaceProjectFilePath = relativePath;
      }
      else
      {
        MessageBox.Show("Could not convert path. Path is already relative!", "Conversion failure", MessageBoxButton.OK);
      }
    }

    /// <summary>
    /// Button clicked interface for creating a new system application
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_CreateSysApp_Click(object sender, RoutedEventArgs e)
    {
      ContentControl contentControlElement;
      newCfg.AddANewSystemApplication( out contentControlElement);
      SP_SystemApplication.Children.Add(contentControlElement);
    }

    /// <summary>
    /// Button clicked interface for deleting a system application
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_DeleteSysApp_Click(object sender, RoutedEventArgs e)
    {
      ContentControl contentControlElement;
      if( true == newCfg.DeleteASystemApplication(out contentControlElement))
      {
        SP_SystemApplication.Children.Remove(contentControlElement);
      }
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function to setup the System Application List
    /// </summary>
    private void SetupSystemAppViewList()
    {
      /* Set System App List */
      foreach(ContentControl sysApp in newCfg.SysAppContentControlList)
      {
        SP_SystemApplication.Children.Add(sysApp);
      }
    }

    #endregion


  }
}
