﻿/*****************************************************************************************************************************
 * @file        SystemApplicationElementV.cs                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.12.2020                                                                                                   *
 * @brief       Implementation of the View for the SystemApplicationElement                                                  *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_2_0_0.Views
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ModuleLibrary.Versions.v_2_0_0.ViewModels;
using RteLib.RteConfig;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Views
{
  /// <summary>
  /// Interaction logic for SystemApplicationElementV.xaml
  /// </summary>
  public partial class SystemApplicationElementV : UserControl
  {
    SystemApplicationElementVM usedVM;

    StoreConfiguration storeConfiguration;
    /// <summary>
    /// Constructor
    /// </summary>
    public SystemApplicationElementV(StoreConfiguration storeCfg)
    {
      InitializeComponent();
      storeConfiguration = storeCfg;
      usedVM = new SystemApplicationElementVM(storeCfg);

      this.DataContext = usedVM;
    }

    #region UI Actions
    /// <summary>
    /// Event handler to identify a drag operation
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    ListBox dragSource = null;
    private void ListBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
      ListBox parent = (ListBox)sender;
      dragSource = parent;
      object data = GetDataFromListBox(dragSource, e.GetPosition(parent));

      if (data != null)
      {
        DragDropEffects retVal = DragDrop.DoDragDrop(parent, data, DragDropEffects.Copy | DragDropEffects.None | DragDropEffects.Move);

        if (retVal == DragDropEffects.Move)
        {
          // Move in own system detected. Do nothing
        }

        if (retVal == DragDropEffects.Copy)
        {
          // Data copied. Delete is in own section
          usedVM.DeleteModule((RteConfigUsedModuleCfg)data);
        }

        if (retVal == DragDropEffects.None)
        {
          // Move in invalid areas detected. Do nothing
        }
      }
    }

    /// <summary>
    /// Interface to get the ListBox object, the mouse is pointing to.
    /// </summary>
    /// <param name="source"></param>
    /// <param name="point"></param>
    /// <returns></returns>
    private static object GetDataFromListBox(ListBox source, Point point)
    {
      UIElement element = source.InputHitTest(point) as UIElement;
      if (element != null)
      {
        object data = DependencyProperty.UnsetValue;
        while (data == DependencyProperty.UnsetValue)
        {
          data = source.ItemContainerGenerator.ItemFromContainer(element);

          if (data == DependencyProperty.UnsetValue)
          {
            element = VisualTreeHelper.GetParent(element) as UIElement;
          }

          if (element == source)
          {
            return null;
          }
        }

        if (data != DependencyProperty.UnsetValue)
        {
          return data;
        }
      }

      return null;
    }

    /// <summary>
    /// Event handler for dropping data into list
    /// Copy element into internal list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void ListBox_Drop(object sender, DragEventArgs e)
    {
      ListBox parent = (ListBox)sender;
      object data = e.Data.GetData(typeof(RteConfigUsedModuleCfg));
      if( true == usedVM.AddNewModule((RteConfigUsedModuleCfg)data))
      {
        // Data copied
        e.Effects = DragDropEffects.Copy;
      }
      else
      {
        // Data not copied >> Only a move event occurred
        e.Effects = DragDropEffects.Move;
      }
    }

    private void ListBox_Enter(object sender, DragEventArgs e)
    {
      e.Effects = DragDropEffects.None;
    }
    #endregion

    #region Attributes
    /// <summary>
    /// Getter for the used view model
    /// </summary>
    public SystemApplicationElementVM UsedVM
    {
      get { return usedVM; }
    }
    #endregion
  }
}
/**
 * @}
 */
