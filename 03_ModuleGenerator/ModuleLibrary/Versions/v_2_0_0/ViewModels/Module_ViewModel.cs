﻿/*****************************************************************************************************************************
 * @file        Module_ViewModel.cs                                                                                          *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the View Model from the Module Xml Cfg Class                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_2_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using ModuleLibrary.Versions.v_2_0_0.Models;
using ModuleLibrary.Versions.v_2_0_0.Views;
using OsarResources.Generator;
using OsarResources.Generic;
using OsarResources.XML;
using RteLib.RteConfig;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Controls;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.ViewModels
{
  /// <summary>
  /// Delegate to store the Configuration
  /// </summary>
  public delegate void StoreConfiguration();

  /// <summary>
  /// Delegate to inform handler about an UI data update
  /// </summary>
  //public delegate void UiDataUpdate();

  public class Module_ViewModel : BaseViewModel, OsarModuleGeneratorInterface
  {
    private static RteConfig RteXmlCfg = new RteConfig();
    private static string pathToConfigurationFile;
    private static string pathToModuleBaseFolder;
    private static StoreConfiguration storeCfg = new StoreConfiguration(SaveConfigToXml);
    //private static SystemApplicationElementV usedModulesView;
    private static SystemApplicationElementV unUsedModulesView;
    private static List<SystemApplicationElementV> availableSystemAppViewList;
    private static List<ContentControl> systemApplicationContentControlList;

    private static string userSelectedSysAppName = "";

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"> Should be a absolute Path</param>
    /// <param name="absPathToBaseModuleFolder"> Should be a absolute Path</param>
    public Module_ViewModel(string pathToCfgFile, string absPathToBaseModuleFolder) : base(storeCfg)
    {

      /* Check if config file path is an rooted one */
      if (true == Path.IsPathRooted(pathToCfgFile))
      { pathToConfigurationFile = pathToCfgFile; }
      else
      { pathToConfigurationFile = absPathToBaseModuleFolder + pathToCfgFile; }

      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      /* Read Configuration File */
      if (!File.Exists(pathToConfigurationFile))
      { // If config file does not exist, create a default file
        SetDefaultConfiguration();
      }

      /* Read Configuration File */
      ReadConfigFromXml();

      /*---------- Setup UI Elements ----------*/
      SetupSystemAppViewList();

    }

    #region public attributes
    /// <summary>
    /// Interface for the XML File Version
    /// </summary>
    public XmlFileVersion XmlFileVersion
    {
      get { return RteXmlCfg.XmlFileVersion; }
      set { RteXmlCfg.XmlFileVersion = value; }
    }

    /// <summary>
    /// Interface for the XML File Version string
    /// </summary>
    public string XmlFileVersion_String
    {
      get
      {
        return ( "v." + RteXmlCfg.XmlFileVersion.MajorVersion.ToString() + "." +
          RteXmlCfg.XmlFileVersion.MinorVersion.ToString() + "." +
          RteXmlCfg.XmlFileVersion.PatchVersion.ToString() );
      }
    }

    /// <summary>
    /// Interface for the Module Version
    /// </summary>
    public string ModuleVersion
    {
      get { return Generator.DefResources.ModuleVersion; }
    }

    /// <summary>
    /// Interface for the Det Module Id
    /// </summary>
    public UInt16 DetModuleID
    {
      get { return RteXmlCfg.DetModuleID; }
      set { RteXmlCfg.DetModuleID = value; }
    }

    /// <summary>
    /// Interface for the Det Module Usage
    /// </summary>
    public SystemState DetModuleUsage
    {
      get { return RteXmlCfg.DetModuleUsage; }
      set { RteXmlCfg.DetModuleUsage = value; }
    }

    /// <summary>
    /// Interface for the Hyperspace Project File Path
    /// </summary>
    public String HyperspaceProjectFilePath
    {
      get { return RteXmlCfg.PathToHyperspaceProjectFile; }
      set { RteXmlCfg.PathToHyperspaceProjectFile = value; }
    }

    /// <summary>
    /// Interface to get the view of the unused modules
    /// </summary>
    public SystemApplicationElementV UnUsedModuleListView
    {
      get { return unUsedModulesView; }
    }

    /// <summary>
    /// Interface to get the complete list of available system applications
    /// </summary>
    public List<SystemApplicationElementV> AvailableSystemAppList
    {
      get { return availableSystemAppViewList; }
    }

    /// <summary>
    /// Interface to get the content control list of the system application views
    /// </summary>
    public List<ContentControl> SysAppContentControlList
    {
      get { return systemApplicationContentControlList; }
    }

    /// <summary>
    /// Getter and setter for the user selected system application name
    /// </summary>
    public string UserSelectedSysAppName
    {
      get { return userSelectedSysAppName; }
      set { userSelectedSysAppName = value; }
    }
    #endregion

    #region UI Action Interface
    /// <summary>
    /// UI Action to create a new a system application
    /// </summary>
    /// <param name="newCC"></param>
    public void AddANewSystemApplication(out ContentControl newCC)
    {
      CreateANewSysAppView(userSelectedSysAppName);
      newCC = systemApplicationContentControlList.Last();
    }

    /// <summary>
    /// UI Action to delete a system application
    /// </summary>
    /// <param name="removedCC"></param>
    /// <returns bool>True if element has been deleted</returns>
    public bool DeleteASystemApplication(out ContentControl removedCC)
    {
      //TODO synchronize available elements
      return DeleteASysAppView(userSelectedSysAppName, out removedCC);
    }
    #endregion

    #region Configuration Load and Storage
    /*
     * @brief       Function to store the current configuration structure to an xml file
     * @param       none
     * @retval      none
     */
    private static void SaveConfigToXml()
    {
      /* First synchronize data before store operation */
      SynchronizeUiDataWithCfgData();

      RteXmlCfg.SaveActiveRteConfigDataBaseToXml(pathToConfigurationFile);
    }

    /*
     * @brief       Function to read the current configuration structure from an xml file
     * @param       none
     * @retval      none
     */
    private static void ReadConfigFromXml()
    {
      RteXmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToConfigurationFile);
    }
    #endregion

    #region Generic Osar Generator
    /// <summary>
    /// Used to validate the active configuration of its correctness.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType ValidateConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfo2 = new GenInfoType(), genInfoMerge = new GenInfoType();

      /* Ensure a correct version */
      if (null == RteXmlCfg)
      {
        genInfo1 = SetDefaultConfiguration();
      }

      /* Call Validator */
      Generator.ModuleValidator modVal = new Generator.ModuleValidator(pathToConfigurationFile);
      genInfo2 = modVal.ValidateConfiguration();

      /* Merge outputs */
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfo1, genInfo2);
      return genInfoMerge;
    }

    /// <summary>
    /// Used to generate the active configuration.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType GenerateConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfo2 = new GenInfoType(), genInfoMerge = new GenInfoType();

      /* Validate configuration */
      genInfo1 = ValidateConfiguration();

      /* If no validation error has been detected >> Start generation */
      if (0U == genInfo1.error.Count)
      {
        Generator.ModuleGenerator modGen = new Generator.ModuleGenerator(pathToConfigurationFile, pathToModuleBaseFolder);
        genInfo2 = modGen.GenerateConfiguration();
      }

      /* Merge outputs */
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfo1, genInfo2);

      return genInfoMerge;
    }

    /// <summary>
    /// Used to set the default configuration.
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType SetDefaultConfiguration()
    {
      GenInfoType genInfo;
      Generator.DefaultCfgGenerator defGen = new Generator.DefaultCfgGenerator(pathToConfigurationFile);
      genInfo = defGen.CreateDefaultConfiguration();
      return genInfo;
    }

    /// <summary>
    /// Interface to update an outdated configuration to an up to date configuration version
    /// </summary>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs
    /// </returns>
    public GenInfoType UpdateConfigurationFileVersion()
    {
      GenInfoType genInfo;
      ReadConfigFromXml();
      Generator.CfgVersionUpdater versUpdater = new Generator.CfgVersionUpdater(pathToConfigurationFile);
      genInfo = versUpdater.UpdateCfgVersion();
      SaveConfigToXml();
      return genInfo;
    }
    /// <summary>
    /// Interface to update an outdated configuration to an specific configuration version
    /// </summary>
    /// <param name="updateToVersion"> Xml file version to be updated </param>
    /// <returns>
    /// >> Return of standardized information <<
    /// List<string> info >> General validation information
    /// List<string> warnings >> General validation warnings
    /// List<string> errors >> General validation errors
    /// List<string> errors >> Validation logs</returns>
    public GenInfoType UpdateConfigurationFileVersion(XmlFileVersion updateToVersion)
    {
      GenInfoType genInfo;
      ReadConfigFromXml();
      Generator.CfgVersionUpdater versUpdater = new Generator.CfgVersionUpdater(pathToConfigurationFile);
      genInfo = versUpdater.UpdateCfgVersion(updateToVersion);
      SaveConfigToXml();
      return genInfo;
    }
    #endregion

    #region Helper functions
    /// <summary>
    /// Helper function to setup the System Application List
    /// 
    /// Note:
    /// Must only be called during initialization
    /// </summary>
    private void SetupSystemAppViewList()
    {
      systemApplicationContentControlList = new List<ContentControl>();
      SetupUnusedElementList();

      /* Setup lists for system applications */
      availableSystemAppViewList = new List<SystemApplicationElementV>();
      foreach (RteSystemApplication sysApp in RteXmlCfg.RteSystemApplications)
      {
        SystemApplicationElementV sysAppElement = new SystemApplicationElementV(storeCfg);
        sysAppElement.UsedVM.Name = sysApp.RteSystemApplicationName;
        foreach (RteConfigUsedModuleCfg usedSysAppModule in sysApp.RteModule)
        { sysAppElement.UsedVM.Modules.Add(usedSysAppModule); }

        availableSystemAppViewList.Add(sysAppElement);
      }

      /* Setup content control list */
      foreach (SystemApplicationElementV sysApp in availableSystemAppViewList)
      {
        ContentControl contentC = new ContentControl();
        contentC.Content = sysApp;
        systemApplicationContentControlList.Add(contentC);
      }
    }

    /// <summary>
    /// Helper function to create a new system application
    /// </summary>
    /// <param name="systemAppName"></param>
    private void CreateANewSysAppView(string systemAppName)
    {
      RteSystemApplication newRteSystemApplication = new RteSystemApplication();
      newRteSystemApplication.RteModule = new List<RteConfigUsedModuleCfg>();
      newRteSystemApplication.RteSystemApplicationName = systemAppName;
      newRteSystemApplication.UUID = OsarResources.Generic.OsarGenericHelper.GenerateANewUUID();
      RteXmlCfg.RteSystemApplications.Add(newRteSystemApplication);
      SaveConfigToXml();

      SystemApplicationElementV sysAppElement = new SystemApplicationElementV(storeCfg);
      sysAppElement.UsedVM.Name = systemAppName;
      availableSystemAppViewList.Add(sysAppElement);

      ContentControl contentC = new ContentControl();
      contentC.Content = sysAppElement;
      systemApplicationContentControlList.Add(contentC);
    }

    /// <summary>
    /// Helper function to delete a new system application
    /// </summary>
    /// <param name="systemAppName"></param>
    /// <param name="removedCC"></param>
    /// <returns bool></returns>
    private bool DeleteASysAppView(string systemAppName, out ContentControl removedCC)
    {
      bool elementFound = false;
      removedCC = null;

      /* Delete from content control list */
      foreach (SystemApplicationElementV sysAppElement in availableSystemAppViewList)
      {
        if (sysAppElement.UsedVM.Name == systemAppName)
        {
          foreach (ContentControl contentControlElement in systemApplicationContentControlList)
          {
            /* Delete Content Control Element */
            if (contentControlElement.Content == sysAppElement)
            {
              /* Element found >> Delete them now from the lists */
              removedCC = contentControlElement;
              systemApplicationContentControlList.Remove(contentControlElement);
              elementFound = true;
              break;
            }
          }

          /* Delete System Application element from Model */
          if (true == elementFound)
          {
            foreach (RteSystemApplication rteSysApp in RteXmlCfg.RteSystemApplications)
            {
              if (rteSysApp.RteSystemApplicationName == systemAppName)
              {
                RteXmlCfg.RteSystemApplications.Remove(rteSysApp);
                SaveConfigToXml();
                break;
              }
            }
          }

          /* Delete View Element */
          availableSystemAppViewList.Remove(sysAppElement);
          break;
        }
      }

      SetupUnusedElementList();
      OnPropertyChanged(new PropertyChangedEventArgs("UnUsedModuleListView"));

      return elementFound;
    }

    /// <summary>
    /// Helper function to synchronize the UI data with the configuration data.
    /// Replace the config data with the new UI data
    /// </summary>
    static private void SynchronizeUiDataWithCfgData()
    {
      foreach(SystemApplicationElementV sysAppV in availableSystemAppViewList)
      {
        for(int idx = 0; idx < RteXmlCfg.RteSystemApplications.Count; idx++)
        {
          if(sysAppV.UsedVM.Name == RteXmlCfg.RteSystemApplications[idx].RteSystemApplicationName)
          { 
            /* Create new module list*/
            List<RteConfigUsedModuleCfg> newRteModuleList = new List<RteConfigUsedModuleCfg>();
            foreach(RteConfigUsedModuleCfg rteModuleInView in sysAppV.UsedVM.Modules)
            {
              newRteModuleList.Add(rteModuleInView);
            }

            /* Create copy of existing object */
            RteSystemApplication activeSystemAppObject = RteXmlCfg.RteSystemApplications[idx];
            RteXmlCfg.RteSystemApplications.Remove(activeSystemAppObject); // delete active on in config
            
            // Update configuration
            activeSystemAppObject.RteModule = newRteModuleList;

            // Store new configuration
            RteXmlCfg.RteSystemApplications.Add(activeSystemAppObject);

            break;
          }
        }
      }
    }

    /// <summary>
    /// Helper function to setup the currently unused elements in the System Applications
    /// </summary>
    private void SetupUnusedElementList()
    {
      bool elementFound = false;
      unUsedModulesView = new SystemApplicationElementV(storeCfg);
      unUsedModulesView.UsedVM.Name = UI_Resources.UI_UnUsedModuleListName;
      foreach (RteConfigUsedModuleCfg usedRteModule in RteXmlCfg.RteUsedModules)
      {
        elementFound = false;
        foreach (RteSystemApplication sysApp in RteXmlCfg.RteSystemApplications)
        {
          if(true == sysApp.RteModule.Contains(usedRteModule))
          {
            elementFound = true;
          }
        }

        if(false == elementFound)
        {
          unUsedModulesView.UsedVM.Modules.Add(usedRteModule);
        }
      }
    }

    #endregion
  }
}
/**
 * @}
 */
