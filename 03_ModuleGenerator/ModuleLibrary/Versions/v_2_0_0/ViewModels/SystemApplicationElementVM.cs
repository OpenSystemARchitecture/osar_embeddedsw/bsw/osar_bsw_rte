﻿/*****************************************************************************************************************************
 * @file        SystemApplicationElementVM.cs                                                                                *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.12.2020                                                                                                   *
 * @brief       Implementation of the View Model for the SystemApplicationElement view                                       *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_2_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using RteLib.RteConfig;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.ViewModels
{
  public class SystemApplicationElementVM
  {
    StoreConfiguration storeConfiguration;
    ObservableCollection<RteConfigUsedModuleCfg> modules = new ObservableCollection<RteConfigUsedModuleCfg>();
    string name = "";

    /// <summary>
    /// Constructor
    /// </summary>
    public SystemApplicationElementVM(StoreConfiguration storeCfg)
    {
      storeConfiguration = storeCfg;
    }

    #region Attributes
    /// <summary>
    /// Getter and setter for the modules list
    /// </summary>
    public ObservableCollection<RteConfigUsedModuleCfg> Modules
    { 
      get { return modules; }
      set { modules = value; }
    }

    /// <summary>
    /// Getter and setter for the name
    /// </summary>
    public string Name
    {
      get { return name; }
      set { name = value; }
    }
    #endregion

    #region Operations
    /// <summary>
    /// Interface to add a new module. If module already exists, do nothing
    /// </summary>
    /// <param name=""></param>
    public bool AddNewModule(RteConfigUsedModuleCfg rteModule)
    {
      bool retVal = false;
      if(false == modules.Contains(rteModule))
      {
        modules.Add(rteModule);
        storeConfiguration();
        retVal = true;
      }
      else
      {
        retVal = false;
      }
      return retVal;
    }

    /// <summary>
    /// Interface to remove an element
    /// </summary>
    /// <param name="rteModule"></param>
    public void DeleteModule(RteConfigUsedModuleCfg rteModule)
    {
      modules.Remove(rteModule);
      storeConfiguration();
    }
    #endregion

  }
}
/**
 * @}
 */
