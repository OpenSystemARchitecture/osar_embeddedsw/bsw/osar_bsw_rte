﻿/*****************************************************************************************************************************
 * @file        RteSystemApplicationGenerator.cs                                                                             *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.01.2020                                                                                                   *
 * @brief       Implementation of the rte system application file generator                                                  *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RteLib.RteConfig;
using RteLib.RteTypes;
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.DocumentationObjects.General;
using OsarResources.Generator;
using System.IO;
using OsarResources.XML;
using OsarResources.Generator.Resources;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public struct RteSaCyclicRunnableInfo
  {
    public string runnableName;
    public UInt16 cycleTime;
  }

  public class RteSystemApplicationGenerator
  {
    RteConfig xmlCfg = new RteConfig();
    string pathToConfiguratioFile;
    string projectBaseFolder;
    string pathToModuleBaseFolder;
    private GenInfoType info = new GenInfoType();
    List<RteModuleInternalBehavior> rteMibList;

    private List<string> mcalInitRunnables;
    private List<string> bswInitRunnables;
    private List<string> cdlInitRunnables;
    private List<string> swcInitRunnables;
    private List<RteSaCyclicRunnableInfo> idleRunnables;
    private List<RteSaCyclicRunnableInfo> prio1Runnables;
    private List<RteSaCyclicRunnableInfo> prio2Runnables;
    private List<RteSaCyclicRunnableInfo> prio3Runnables;
    private List<RteSaCyclicRunnableInfo> prio4Runnables;
    private List<RteSaCyclicRunnableInfo> prio5Runnables;
    private List<RteSaCyclicRunnableInfo> prio6Runnables;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    /// <param name="absPathToBaseModuleFolder"></param>
    /// <param name="mibList"></param>
    public RteSystemApplicationGenerator(string pathToCfgFile, string absPathToBaseModuleFolder, List<RteModuleInternalBehavior> mibList)
    {
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;
      rteMibList = mibList;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();

      xmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToCfgFile);

      projectBaseFolder = Path.GetDirectoryName(pathToConfiguratioFile) + xmlCfg.PathToBaseProjectFolder;
    }

    /// <summary>
    /// Interface to generate the system application files
    /// </summary>
    /// <returns></returns>
    public GenInfoType GenerateSystemApplicationFiles()
    {
      info.AddLogMsg("Start generation of the Rte System Application files.");

      generatingRteSystemApplicationInternalHeaderFile();
      generatingRteSystemApplicationInternalSourceFile();

      generatingRteSystemApplicationHeaderFiles();
      generatingRteSystemApplicationSourceFiles();

      return info;
    }


    /**
    * @brief    Generate the needed Rte System Application Header Files
    * @details  Rte Config Xml Data Base Config file used for file generation
    * @retval   none
    */
    private void generatingRteSystemApplicationHeaderFiles()
    {
      CHeaderFile rteGenModuleFile = new CHeaderFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileFunctionObjects genFncObject = new CFileFunctionObjects();
      CFileDefinitionObjects headerDefines = new CFileDefinitionObjects();
      CFileVariableObjects genVariable = new CFileVariableObjects();

      GeneralStartGroupObject groupMappingOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject groupMappingCloser = new GeneralEndGroupObject();

      foreach (RteSystemApplication systemApplication in xmlCfg.RteSystemApplications)
      {
        string fileName = GenResources.GenRteModuleSystemApplicationBaseFileName + systemApplication.RteSystemApplicationName + ".h";

        /*++++++++++++++++++++++++++++++++++++++++ Create doxygen file header ++++++++++++++++++++++++++++++++++++++++*/
        doxygenFileHeader.AddFileBrief("Generated Rte System Application Header File: " + fileName);
        doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
        doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
        doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
        doxygenFileHeader.AddFileName(fileName);
        doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());

        doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupBSW);
        doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);


        /* ++++++++++++++++++++++++++++++++++++++++ Process include objects ++++++++++++++++++++++++++++++++++++++++ */
        CFileIncludeObjects includes = new CFileIncludeObjects();
        includes.AddIncludeFile(GenResources.GenRteConfigHeaderFileName + ".h");
        includes.AddIncludeFile(GenResources.GenRteTypesSourceFileName);

        foreach (RteConfigUsedModuleCfg module in systemApplication.RteModule)
        {
          includes.AddIncludeFile(DefResources.ModuleName + "_" + module.ModuleName + ".h");
        }

        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++ Process all Tasks +++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        doxygenDescription.AddElementBrief("Rte generated system application init task.");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplicationInitTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), doxygenDescription);

        doxygenDescription.AddElementBrief("Rte generated system application idle task.");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplicationIdleTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), doxygenDescription);

        doxygenDescription.AddElementBrief("Rte generated system application low priority (1) task.");
        doxygenDescription.AddElementDetails("Generated task scheduling: 10000ms < runnable < --- \n" +
          "Scheduling time: " + GenResources.GenRteSystemApplication1PrioTaskSchedulingTime + "ms");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplication1PrioTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), doxygenDescription);

        doxygenDescription.AddElementBrief("Rte generated system application below normal priority (2) task.");
        doxygenDescription.AddElementDetails("Generated task scheduling: 1000ms < runnable < 10000ms\n" +
          "Scheduling time: " + GenResources.GenRteSystemApplication2PrioTaskSchedulingTime + "ms");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplication2PrioTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), doxygenDescription);

        doxygenDescription.AddElementBrief("Rte generated system application normal priority (3) task.");
        doxygenDescription.AddElementDetails("Generated task scheduling: 500ms < runnable < 1000ms\n" +
          "Scheduling time: " + GenResources.GenRteSystemApplication3PrioTaskSchedulingTime + "ms");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplication3PrioTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), doxygenDescription);

        doxygenDescription.AddElementBrief("Rte generated system application above normal priority (4) task.");
        doxygenDescription.AddElementDetails("Generated task scheduling: 100ms < runnable < 500ms\n" +
          "Scheduling time: " + GenResources.GenRteSystemApplication4PrioTaskSchedulingTime + "ms");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplication4PrioTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), doxygenDescription);

        doxygenDescription.AddElementBrief("Rte generated system application high priority (5) task.");
        doxygenDescription.AddElementDetails("Generated task scheduling: 10ms < runnable < 100ms\n" +
          "Scheduling time: " + GenResources.GenRteSystemApplication5PrioTaskSchedulingTime + "ms");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplication5PrioTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), doxygenDescription);

        doxygenDescription.AddElementBrief("Rte generated system application ultra high priority (6) task.");
        doxygenDescription.AddElementDetails("Generated task scheduling: 1ms < runnable < 10ms\n" +
          "Scheduling time: " + GenResources.GenRteSystemApplication6PrioTaskSchedulingTime + "ms");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplication6PrioTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), doxygenDescription);

        /*++++++++++++++++++++++++++++++++++++++++ Generate File ++++++++++++++++++++++++++++++++++++++++*/
        rteGenModuleFile.AddFileCommentHeader(doxygenFileHeader);
        rteGenModuleFile.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
        rteGenModuleFile.AddFileName(fileName);
        rteGenModuleFile.AddFilePath(pathToModuleBaseFolder + GenResources.GenHeaderFilePath);
        rteGenModuleFile.AddGlobalFunctionPrototypeObject(genFncObject);
        rteGenModuleFile.AddIncludeObject(includes);
        rteGenModuleFile.AddDefinitionObject(headerDefines);
        rteGenModuleFile.AddGlobalVariableObject(genVariable);
        rteGenModuleFile.GenerateSourceFile();
      }
    }

    /**
    * @brief    Generate the needed Rte System Application Source Files
    * @details  Rte Config Xml Data Base Config file used for RteInterface file generation
    * @retval   none
    */
    private void generatingRteSystemApplicationSourceFiles()
    {
      CSourceFile rteGenModuleFile = new CSourceFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileFunctionObjects genFncObject = new CFileFunctionObjects();
      CFileVariableObjects genVariable = new CFileVariableObjects();

      GeneralStartGroupObject groupMappingOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject groupMappingCloser = new GeneralEndGroupObject();
      int fncGroupIdx;

      //Trace.WriteLine(OsarResources.Generator.Resources.generatorOutputs.Std_Output + "Start generation of the Rte System Application Source files.");
      foreach (RteSystemApplication systemApplication in xmlCfg.RteSystemApplications)
      {
        string fileName = GenResources.GenRteModuleSystemApplicationBaseFileName + systemApplication.RteSystemApplicationName + ".c";

        /*++++++++++++++++++++++++++++++++++++++++ Create doxygen file header ++++++++++++++++++++++++++++++++++++++++*/
        doxygenFileHeader.AddFileBrief("Generated Rte System Application Source File: " + fileName);
        doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
        doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
        doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
        doxygenFileHeader.AddFileName(fileName);
        doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());

        doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupBSW);
        doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);


        /* ++++++++++++++++++++++++++++++++++++++++ Process include objects ++++++++++++++++++++++++++++++++++++++++ */
        CFileIncludeObjects includes = new CFileIncludeObjects();
        includes.AddIncludeFile(GenResources.GenRteTypesSourceFileName);
        includes.AddIncludeFile(GenResources.GenRteModuleSystemApplicationBaseFileName + systemApplication.RteSystemApplicationName + ".h");

        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++ Process all Tasks +++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        groupMappingOpener.AddGroupName("Map functions into OSAR Code sections");
        groupMappingOpener.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CODE);
        groupMappingCloser.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CODE);
        fncGroupIdx = genFncObject.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);

        /* Sort module elements into List */
        hlpSortModulesRunnablesIntoInternalProcessingList(systemApplication);

        doxygenDescription.AddElementBrief("Rte generated system application init task.");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplicationInitTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), fncGroupIdx, doxygenDescription,
          hlpGenerateInitTaskFncContent());

        doxygenDescription.AddElementBrief("Rte generated system application idle task.");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplicationIdleTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), fncGroupIdx, doxygenDescription);

        doxygenDescription.AddElementBrief("Rte generated system application low priority (1) task.");
        doxygenDescription.AddElementDetails("Generated task scheduling: 10000ms < runnable < --- \n" +
          "Scheduling time: " + GenResources.GenRteSystemApplication1PrioTaskSchedulingTime + "ms");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplication1PrioTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), fncGroupIdx, doxygenDescription,
          hlpGeneratePrio1TaskFncContent());

        doxygenDescription.AddElementBrief("Rte generated system application below normal priority (2) task.");
        doxygenDescription.AddElementDetails("Generated task scheduling: 1000ms < runnable < 10000ms\n" +
          "Scheduling time: " + GenResources.GenRteSystemApplication2PrioTaskSchedulingTime + "ms");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplication2PrioTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), fncGroupIdx, doxygenDescription,
          hlpGeneratePrio2TaskFncContent());

        doxygenDescription.AddElementBrief("Rte generated system application normal priority (3) task.");
        doxygenDescription.AddElementDetails("Generated task scheduling: 500ms < runnable < 1000ms\n" +
          "Scheduling time: " + GenResources.GenRteSystemApplication3PrioTaskSchedulingTime + "ms");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplication3PrioTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), fncGroupIdx, doxygenDescription,
          hlpGeneratePrio3TaskFncContent());

        doxygenDescription.AddElementBrief("Rte generated system application above normal priority (4) task.");
        doxygenDescription.AddElementDetails("Generated task scheduling: 100ms < runnable < 500ms\n" +
          "Scheduling time: " + GenResources.GenRteSystemApplication4PrioTaskSchedulingTime + "ms");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplication4PrioTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), fncGroupIdx, doxygenDescription,
          hlpGeneratePrio4TaskFncContent());

        doxygenDescription.AddElementBrief("Rte generated system application high priority (5) task.");
        doxygenDescription.AddElementDetails("Generated task scheduling: 10ms < runnable < 100ms\n" +
          "Scheduling time: " + GenResources.GenRteSystemApplication5PrioTaskSchedulingTime + "ms");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplication5PrioTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), fncGroupIdx, doxygenDescription,
          hlpGeneratePrio5TaskFncContent());

        doxygenDescription.AddElementBrief("Rte generated system application ultra high priority (6) task.");
        doxygenDescription.AddElementDetails("Generated task scheduling: 1ms < runnable < 10ms\n" +
          "Scheduling time: " + GenResources.GenRteSystemApplication6PrioTaskSchedulingTime + "ms");
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
          GenResources.GenRteSystemApplication6PrioTaskPrefixName + systemApplication.RteSystemApplicationName,
          RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), fncGroupIdx, doxygenDescription,
          hlpGeneratePrio6TaskFncContent());

        /*++++++++++++++++++++++++++++++++++++++++ Generate File ++++++++++++++++++++++++++++++++++++++++*/
        rteGenModuleFile.AddFileCommentHeader(doxygenFileHeader);
        rteGenModuleFile.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
        rteGenModuleFile.AddFileName(fileName);
        rteGenModuleFile.AddFilePath(pathToModuleBaseFolder + GenResources.GenSourceFilePath);
        rteGenModuleFile.AddIncludeObject(includes);
        rteGenModuleFile.AddGlobalVariableObject(genVariable);
        rteGenModuleFile.AddGlobalFunctionObject(genFncObject);
        rteGenModuleFile.GenerateSourceFile();

        /*++++++++++++++++++++++++++++++++++++++++ Generate User Information ++++++++++++++++++++++++++++++++++++++++*/
        info.AddInfoMsg("Rte Os Task with Priority 0 required >> \"" +
          GenResources.GenRteSystemApplicationIdleTaskPrefixName + systemApplication.RteSystemApplicationName + "\"");

        info.AddInfoMsg("Rte Os Task with Priority 1 required >> \"" +
          GenResources.GenRteSystemApplication1PrioTaskPrefixName + systemApplication.RteSystemApplicationName + "\"" +
          " >> Function must be scheduled each " + GenResources.GenRteSystemApplication1PrioTaskSchedulingTime + " ms.");

        info.AddInfoMsg("Rte Os Task with Priority 2 required >> \"" +
          GenResources.GenRteSystemApplication2PrioTaskPrefixName + systemApplication.RteSystemApplicationName + "\"" +
          " >> Function must be scheduled each " + GenResources.GenRteSystemApplication2PrioTaskSchedulingTime + " ms.");

        info.AddInfoMsg("Rte Os Task with Priority 3 required >> \"" +
          GenResources.GenRteSystemApplication3PrioTaskPrefixName + systemApplication.RteSystemApplicationName + "\"" +
          " >> Function must be scheduled each " + GenResources.GenRteSystemApplication3PrioTaskSchedulingTime + " ms.");

        info.AddInfoMsg("Rte Os Task with Priority 4 required >> \"" +
          GenResources.GenRteSystemApplication4PrioTaskPrefixName + systemApplication.RteSystemApplicationName + "\"" +
          " >> Function must be scheduled each " + GenResources.GenRteSystemApplication4PrioTaskSchedulingTime + " ms.");

        info.AddInfoMsg("Rte Os Task with Priority 5 required >> \"" +
          GenResources.GenRteSystemApplication5PrioTaskPrefixName + systemApplication.RteSystemApplicationName + "\"" +
          " >> Function must be scheduled each " + GenResources.GenRteSystemApplication5PrioTaskSchedulingTime + " ms.");

        info.AddInfoMsg("Rte Os Task with Priority 6 required >> \"" +
          GenResources.GenRteSystemApplication6PrioTaskPrefixName + systemApplication.RteSystemApplicationName + "\"" +
          " >> Function must be scheduled each " + GenResources.GenRteSystemApplication6PrioTaskSchedulingTime + " ms.");
      }
    }

    /**
      * @brief    Generate the needed Rte System Application Header Files
      * @details  Rte Config Xml Data Base Config file used for file generation
      * @retval   none
      */
    private void generatingRteSystemApplicationInternalHeaderFile()
    {
      CHeaderFile rteGenModuleFile = new CHeaderFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileFunctionObjects genFncObject = new CFileFunctionObjects();
      CFileDefinitionObjects headerDefines = new CFileDefinitionObjects();
      CFileVariableObjects genVariable = new CFileVariableObjects();

      GeneralStartGroupObject groupMappingOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject groupMappingCloser = new GeneralEndGroupObject();

      //Trace.WriteLine(OsarResources.Generator.Resources.generatorOutputs.Std_Output + "Start generation of the Rte System Application Internal Header files.");

      string fileName = GenResources.GenRteModuleSystemApplicationBaseFileName + GenResources.GenRteSystemApplicationInternal_Name + ".h";

      /*++++++++++++++++++++++++++++++++++++++++ Create doxygen file header ++++++++++++++++++++++++++++++++++++++++*/
      doxygenFileHeader.AddFileBrief("Generated Rte System Application Internal Header File: " + fileName);
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileHeader.AddFileName(fileName);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());

      doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupBSW);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);


      /* ++++++++++++++++++++++++++++++++++++++++ Process include objects ++++++++++++++++++++++++++++++++++++++++ */
      CFileIncludeObjects includes = new CFileIncludeObjects();
      includes.AddIncludeFile(GenResources.GenRteConfigHeaderFileName + ".h");
      includes.AddIncludeFile(GenResources.GenRteTypesSourceFileName);

      foreach (RteSystemApplication systemApplication in xmlCfg.RteSystemApplications)
      {
        string appFileName = GenResources.GenRteModuleSystemApplicationBaseFileName + systemApplication.RteSystemApplicationName + ".h";
        includes.AddIncludeFile(appFileName);
      }


      /* ++++++++++++++++++++++++++++++++++++++++ Process function objects ++++++++++++++++++++++++++++++++++++++++ */
      doxygenDescription.AddElementBrief("Rte internal init function collector for all system applications");
      genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
        GenResources.GenRteSystemApplicationInitTaskPrefixName + GenResources.GenRteSystemApplicationInternal_Name,
        RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), doxygenDescription);

      /*++++++++++++++++++++++++++++++++++++++++ Generate File ++++++++++++++++++++++++++++++++++++++++*/
      rteGenModuleFile.AddFileCommentHeader(doxygenFileHeader);
      rteGenModuleFile.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      rteGenModuleFile.AddFileName(fileName);
      rteGenModuleFile.AddFilePath(pathToModuleBaseFolder + GenResources.GenHeaderFilePath);
      rteGenModuleFile.AddGlobalFunctionPrototypeObject(genFncObject);
      rteGenModuleFile.AddIncludeObject(includes);
      rteGenModuleFile.AddDefinitionObject(headerDefines);
      rteGenModuleFile.AddGlobalVariableObject(genVariable);
      rteGenModuleFile.GenerateSourceFile();
    }

    /**
    * @brief    Generate the needed Rte System Application Source Files
    * @details  Rte Config Xml Data Base Config file used for RteInterface file generation
    * @retval   none
    */
    private void generatingRteSystemApplicationInternalSourceFile()
    {
      CSourceFile rteGenModuleFile = new CSourceFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileFunctionObjects genFncObject = new CFileFunctionObjects();
      CFileVariableObjects genVariable = new CFileVariableObjects();

      GeneralStartGroupObject groupMappingOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject groupMappingCloser = new GeneralEndGroupObject();
      int fncGroupIdx;

      //Trace.WriteLine(OsarResources.Generator.Resources.generatorOutputs.Std_Output + "Start generation of the Rte System Application Internal Source files.");
      string fileName = GenResources.GenRteModuleSystemApplicationBaseFileName + GenResources.GenRteSystemApplicationInternal_Name + ".c";

      /*++++++++++++++++++++++++++++++++++++++++ Create doxygen file header ++++++++++++++++++++++++++++++++++++++++*/
      doxygenFileHeader.AddFileBrief("Generated Rte System Application Internal Source File: " + fileName);
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileHeader.AddFileName(fileName);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());

      doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupBSW);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);


      /* ++++++++++++++++++++++++++++++++++++++++ Process include objects ++++++++++++++++++++++++++++++++++++++++ */
      CFileIncludeObjects includes = new CFileIncludeObjects();
      includes.AddIncludeFile(GenResources.GenRteTypesSourceFileName);
      includes.AddIncludeFile(GenResources.GenRteModuleSystemApplicationBaseFileName + GenResources.GenRteSystemApplicationInternal_Name + ".h");

      /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++++++++++ Process all Tasks +++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      groupMappingOpener.AddGroupName("Map functions into OSAR Code sections");
      groupMappingOpener.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CODE);
      groupMappingCloser.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CODE);
      fncGroupIdx = genFncObject.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);

      doxygenDescription.AddElementBrief("Rte internal init function collector for all system applications");
      string fncContent = "";
      foreach (RteSystemApplication systemApplication in xmlCfg.RteSystemApplications)
      {
        fncContent += hlpAddNewFncContentLine(GenResources.GenRteSystemApplicationInitTaskPrefixName + systemApplication.RteSystemApplicationName + "();");
      }

      genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
        GenResources.GenRteSystemApplicationInitTaskPrefixName + GenResources.GenRteSystemApplicationInternal_Name,
        RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), fncGroupIdx, doxygenDescription,
        fncContent);

      /*++++++++++++++++++++++++++++++++++++++++ Generate File ++++++++++++++++++++++++++++++++++++++++*/
      rteGenModuleFile.AddFileCommentHeader(doxygenFileHeader);
      rteGenModuleFile.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      rteGenModuleFile.AddFileName(fileName);
      rteGenModuleFile.AddFilePath(pathToModuleBaseFolder + GenResources.GenSourceFilePath);
      rteGenModuleFile.AddIncludeObject(includes);
      rteGenModuleFile.AddGlobalVariableObject(genVariable);
      rteGenModuleFile.AddGlobalFunctionObject(genFncObject);
      rteGenModuleFile.GenerateSourceFile();
    }

    /**
    * @brief    Helper function to sort all runnables into corresponding List
    * @retval   none
    */
    private void hlpSortModulesRunnablesIntoInternalProcessingList(RteSystemApplication systemApplication)
    {
      RteModuleInternalBehavior processModule = new RteModuleInternalBehavior();
      bool elementFound = false;
      string functionName;

      mcalInitRunnables = new List<string>();
      bswInitRunnables = new List<string>();
      cdlInitRunnables = new List<string>();
      swcInitRunnables = new List<string>();
      idleRunnables = new List<RteSaCyclicRunnableInfo>();
      prio1Runnables = new List<RteSaCyclicRunnableInfo>();
      prio2Runnables = new List<RteSaCyclicRunnableInfo>();
      prio3Runnables = new List<RteSaCyclicRunnableInfo>();
      prio4Runnables = new List<RteSaCyclicRunnableInfo>();
      prio5Runnables = new List<RteSaCyclicRunnableInfo>();
      prio6Runnables = new List<RteSaCyclicRunnableInfo>();

      /* Process each module */
      foreach (RteConfigUsedModuleCfg usedModule in systemApplication.RteModule)
      {
        elementFound = false;
        /* Search for corresponding Module in modules list */
        foreach (RteModuleInternalBehavior availablemodule in rteMibList)
        {
          if (usedModule.ModuleName == availablemodule.ModuleName)
          {
            processModule = availablemodule;
            elementFound = true;
            break;
          }
        }

        /* Check if module has been found */
        if (false == elementFound)
        {
          throw new ArgumentOutOfRangeException("Assigned Rte module in system application not found!");
        }

        /* Sort init runnable into init runnable List */
        foreach (RteRunnable initRunnable in processModule.InitRunnables)
        {
          if (( "" == initRunnable.RunnableInfo.InterfaceName ) || ( null == initRunnable.RunnableInfo.InterfaceName ))
          {
             functionName = processModule.ModuleName + GenResources.GenRteGeneratorModuleInitFncPrefix;
          }
          else
          {
            functionName = processModule.ModuleName +
              GenResources.GenRteGeneratorModuleInitFncPrefix + "_" + initRunnable.RunnableInfo.InterfaceName;
          }

          /* Sort Init runnables into correct Init List*/
          switch (processModule.ModuleType)
          {
            case RteConfigModuleTypes.MCAL:
            mcalInitRunnables.Add(functionName);
            break;

            case RteConfigModuleTypes.CDD:
            mcalInitRunnables.Add(functionName);
            break;

            case RteConfigModuleTypes.BSW:
            bswInitRunnables.Add(functionName);
            break;

            case RteConfigModuleTypes.CD:
            cdlInitRunnables.Add(functionName);
            break;

            default:
            swcInitRunnables.Add(functionName);
            break;
          }
        }

        /* Sort cyclic runnable into init runnable List */
        foreach (RteCyclicRunnable cyclicRunnable in processModule.CyclicRunnables)
        {
          functionName = processModule.ModuleName + "_" + cyclicRunnable.Runnable.RunnableInfo.InterfaceName;

          /* Check if runnable exists */
          if((null == cyclicRunnable.Runnable.RunnableInfo.InterfaceName ) || (null == cyclicRunnable.Runnable.RunnableInfo.UUID))
          {
            continue;
          }

          /* Check task asignment due to rate monotonic scheduling */
          if (10000 <= cyclicRunnable.CycleTime)
          {
            RteSaCyclicRunnableInfo runnable = new RteSaCyclicRunnableInfo();
            runnable.runnableName = functionName;
            runnable.cycleTime = cyclicRunnable.CycleTime;
            prio1Runnables.Add(runnable);
          }
          else if (( 1000 <= cyclicRunnable.CycleTime ) && ( 10000 > cyclicRunnable.CycleTime ))
          {
            RteSaCyclicRunnableInfo runnable = new RteSaCyclicRunnableInfo();
            runnable.runnableName = functionName;
            runnable.cycleTime = cyclicRunnable.CycleTime;
            prio2Runnables.Add(runnable);
          }
          else if (( 500 <= cyclicRunnable.CycleTime ) && ( 1000 > cyclicRunnable.CycleTime ))
          {
            RteSaCyclicRunnableInfo runnable = new RteSaCyclicRunnableInfo();
            runnable.runnableName = functionName;
            runnable.cycleTime = cyclicRunnable.CycleTime;
            prio3Runnables.Add(runnable);
          }
          else if (( 100 <= cyclicRunnable.CycleTime ) && ( 500 > cyclicRunnable.CycleTime ))
          {
            RteSaCyclicRunnableInfo runnable = new RteSaCyclicRunnableInfo();
            runnable.runnableName = functionName;
            runnable.cycleTime = cyclicRunnable.CycleTime;
            prio4Runnables.Add(runnable);
          }
          else if (( 10 <= cyclicRunnable.CycleTime ) && ( 100 > cyclicRunnable.CycleTime ))
          {
            RteSaCyclicRunnableInfo runnable = new RteSaCyclicRunnableInfo();
            runnable.runnableName = functionName;
            runnable.cycleTime = cyclicRunnable.CycleTime;
            prio5Runnables.Add(runnable);
          }
          else if (( 1 <= cyclicRunnable.CycleTime ) && ( 10 > cyclicRunnable.CycleTime ))
          {
            RteSaCyclicRunnableInfo runnable = new RteSaCyclicRunnableInfo();
            runnable.runnableName = functionName;
            runnable.cycleTime = cyclicRunnable.CycleTime;
            prio6Runnables.Add(runnable);
          }
          else
          {
            info.AddWaringMsg(Convert.ToUInt16(GenErrorWarningCodes.Warning_5000), GenErrorWarningCodes.Warning_5000_Msg +
            processModule.ModuleName + " Module Runnable: " + cyclicRunnable.Runnable.RunnableInfo.InterfaceName +
            " Runnable cycle time: " + cyclicRunnable.CycleTime.ToString());
          }
        }
      }
    }

    /**
    * @brief    Helper function to generate init task function content
    * @retval   string function content
    */
    private string hlpGenerateInitTaskFncContent()
    {
      string fncContent = "";

      /* Sort and create init runnable list */
      foreach (string initRunnable in mcalInitRunnables)
      {
        fncContent += hlpAddNewFncContentLine(initRunnable + "();");
      }
      foreach (string initRunnable in bswInitRunnables)
      {
        fncContent += hlpAddNewFncContentLine(initRunnable + "();");
      }
      foreach (string initRunnable in cdlInitRunnables)
      {
        fncContent += hlpAddNewFncContentLine(initRunnable + "();");
      }
      foreach (string initRunnable in swcInitRunnables)
      {
        fncContent += hlpAddNewFncContentLine(initRunnable + "();");
      }

      return fncContent;
    }

    /**
    * @brief    Helper function to generate prio 1 task function content
    * @retval   string function content
    */
    private string hlpGeneratePrio1TaskFncContent()
    {
      string fncContent = "";
      int compareValue;
      /* ===================================================================================== */
      /* ================================== Sort runnables =================================== */
      /* ===================================================================================== */
      /* Do nothing here */

      /* ===================================================================================== */
      /* ============================ Generate function content ============================== */
      /* ===================================================================================== */
      /* Generate counter */
      fncContent += hlpAddNewFncContentLine("static uint64 cnt = 0;\n");

      foreach (RteSaCyclicRunnableInfo runnable in prio1Runnables)
      {
        compareValue = runnable.cycleTime / Convert.ToUInt16(GenResources.GenRteSystemApplication1PrioTaskSchedulingTime);
        fncContent += hlpAddNewFncContentLine("/* Trigger each: " + runnable.cycleTime.ToString() + "ms */");
        fncContent += hlpAddNewFncContentLine("if (0 == ( cnt % " + compareValue.ToString() + " ))");
        fncContent += hlpAddNewFncContentLine("{");
        fncContent += hlpAddNewFncContentLine("  " + runnable.runnableName + "();");
        fncContent += hlpAddNewFncContentLine("}\n");
      }

      /* Increment Counter */
      fncContent += hlpAddNewFncContentLine("cnt++;");
      return fncContent;
    }

    /**
    * @brief    Helper function to generate prio 2 task function content
    * @retval   string function content
    */
    private string hlpGeneratePrio2TaskFncContent()
    {
      string fncContent = "";
      int compareValue, arrayOffset;

      List<string>[] sheduling = new List<string>[100];
      for (int idx = 0; idx < 100; idx++)
      {
        sheduling[idx] = new List<string>();
      }

      /* ===================================================================================== */
      /* ================================== Sort runnables =================================== */
      /* ===================================================================================== */
      foreach (RteSaCyclicRunnableInfo runnable in prio2Runnables)
      {
        /* Calculate Array offset */
        arrayOffset = ( ( runnable.cycleTime / Convert.ToUInt16(GenResources.GenRteSystemApplication2PrioTaskSchedulingTime) ) - 10 );

        /* Add runnable into scheduling array */
        sheduling[arrayOffset].Add(runnable.runnableName);
      }

      /* ===================================================================================== */
      /* ============================ Generate function content ============================== */
      /* ===================================================================================== */
      /* Generate counter */
      fncContent += hlpAddNewFncContentLine("static uint64 cnt = 0;\n");

      for (int idx = 0; idx < 100; idx++)
      {
        compareValue = ( ( idx * Convert.ToUInt16(GenResources.GenRteSystemApplication2PrioTaskSchedulingTime) ) + 1000 ) / Convert.ToUInt16(GenResources.GenRteSystemApplication2PrioTaskSchedulingTime);

        /* Check if scheduing element is needed */
        if (0 != sheduling[idx].Count())
        {
          fncContent += hlpAddNewFncContentLine("/* Trigger each: " + ( ( idx * Convert.ToUInt16(GenResources.GenRteSystemApplication2PrioTaskSchedulingTime) ) + 1000 ).ToString() + "ms */");
          fncContent += hlpAddNewFncContentLine("if (0 == ( cnt % " + compareValue.ToString() + " ))");
          fncContent += hlpAddNewFncContentLine("{");
          foreach (string runnable in sheduling[idx])
          {
            fncContent += hlpAddNewFncContentLine("  " + runnable + "();");
          }
          fncContent += hlpAddNewFncContentLine("}\n");
        }
      }

      /* Increment Counter */
      fncContent += hlpAddNewFncContentLine("cnt++;");
      return fncContent;
    }

    /**
    * @brief    Helper function to generate prio 3 task function content
    * @retval   string function content
    */
    private string hlpGeneratePrio3TaskFncContent()
    {
      string fncContent = "";
      int compareValue, arrayOffset;

      List<string>[] sheduling = new List<string>[100];
      for (int idx = 0; idx < 100; idx++)
      {
        sheduling[idx] = new List<string>();
      }

      /* ===================================================================================== */
      /* ================================== Sort runnables =================================== */
      /* ===================================================================================== */
      foreach (RteSaCyclicRunnableInfo runnable in prio3Runnables)
      {
        /* Calculate Array offset */
        arrayOffset = ( ( runnable.cycleTime / Convert.ToUInt16(GenResources.GenRteSystemApplication3PrioTaskSchedulingTime) ) - 10 );

        /* Add runnable into scheduling array */
        sheduling[arrayOffset].Add(runnable.runnableName);
      }

      /* ===================================================================================== */
      /* ============================ Generate function content ============================== */
      /* ===================================================================================== */
      /* Generate counter */
      fncContent += hlpAddNewFncContentLine("static uint64 cnt = 0;\n");

      for (int idx = 0; idx < 100; idx++)
      {
        compareValue = ( ( idx * Convert.ToUInt16(GenResources.GenRteSystemApplication3PrioTaskSchedulingTime) ) + 100 ) / Convert.ToUInt16(GenResources.GenRteSystemApplication3PrioTaskSchedulingTime);

        /* Check if scheduing element is needed */
        if (0 != sheduling[idx].Count())
        {
          fncContent += hlpAddNewFncContentLine("/* Trigger each: " + ( ( idx * Convert.ToUInt16(GenResources.GenRteSystemApplication3PrioTaskSchedulingTime) ) + 100 ).ToString() + "ms */");
          fncContent += hlpAddNewFncContentLine("if (0 == ( cnt % " + compareValue.ToString() + " ))");
          fncContent += hlpAddNewFncContentLine("{");
          foreach (string runnable in sheduling[idx])
          {
            fncContent += hlpAddNewFncContentLine("  " + runnable + "();");
          }
          fncContent += hlpAddNewFncContentLine("}\n");
        }
      }

      /* Increment Counter */
      fncContent += hlpAddNewFncContentLine("cnt++;");
      return fncContent;
    }

    /**
    * @brief    Helper function to generate prio 4 task function content
    * @retval   string function content
    */
    private string hlpGeneratePrio4TaskFncContent()
    {
      string fncContent = "";
      int compareValue, arrayOffset;

      List<string>[] sheduling = new List<string>[100];
      for (int idx = 0; idx < 100; idx++)
      {
        sheduling[idx] = new List<string>();
      }

      /* ===================================================================================== */
      /* ================================== Sort runnables =================================== */
      /* ===================================================================================== */
      foreach (RteSaCyclicRunnableInfo runnable in prio4Runnables)
      {
        /* Calculate Array offset */
        arrayOffset = ( ( runnable.cycleTime / Convert.ToUInt16(GenResources.GenRteSystemApplication4PrioTaskSchedulingTime) ) - 10 );

        /* Add runnable into scheduling array */
        sheduling[arrayOffset].Add(runnable.runnableName);
      }

      /* ===================================================================================== */
      /* ============================ Generate function content ============================== */
      /* ===================================================================================== */
      /* Generate counter */
      fncContent += hlpAddNewFncContentLine("static uint64 cnt = 0;\n");

      for (int idx = 0; idx < 100; idx++)
      {
        compareValue = ( ( idx * Convert.ToUInt16(GenResources.GenRteSystemApplication4PrioTaskSchedulingTime) ) + 100 ) / Convert.ToUInt16(GenResources.GenRteSystemApplication4PrioTaskSchedulingTime);

        /* Check if scheduing element is needed */
        if (0 != sheduling[idx].Count())
        {
          fncContent += hlpAddNewFncContentLine("/* Trigger each: " + ( ( idx * Convert.ToUInt16(GenResources.GenRteSystemApplication4PrioTaskSchedulingTime) ) + 100 ).ToString() + "ms */");
          fncContent += hlpAddNewFncContentLine("if (0 == ( cnt % " + compareValue.ToString() + " ))");
          fncContent += hlpAddNewFncContentLine("{");
          foreach (string runnable in sheduling[idx])
          {
            fncContent += hlpAddNewFncContentLine("  " + runnable + "();");
          }
          fncContent += hlpAddNewFncContentLine("}\n");
        }
      }

      /* Increment Counter */
      fncContent += hlpAddNewFncContentLine("cnt++;");
      return fncContent;
    }

    /**
    * @brief    Helper function to generate prio 5 task function content
    * @retval   string function content
    */
    private string hlpGeneratePrio5TaskFncContent()
    {
      string fncContent = "";
      int compareValue, arrayOffset;

      List<string>[] sheduling = new List<string>[100];
      for (int idx = 0; idx < 100; idx++)
      {
        sheduling[idx] = new List<string>();
      }

      /* ===================================================================================== */
      /* ================================== Sort runnables =================================== */
      /* ===================================================================================== */
      foreach (RteSaCyclicRunnableInfo runnable in prio5Runnables)
      {
        /* Calculate Array offset */
        arrayOffset = ( ( runnable.cycleTime / Convert.ToUInt16(GenResources.GenRteSystemApplication5PrioTaskSchedulingTime) ) - 10 );

        /* Add runnable into scheduling array */
        sheduling[arrayOffset].Add(runnable.runnableName);
      }

      /* ===================================================================================== */
      /* ============================ Generate function content ============================== */
      /* ===================================================================================== */
      /* Generate counter */
      fncContent += hlpAddNewFncContentLine("static uint64 cnt = 0;\n");

      for (int idx = 0; idx < 100; idx++)
      {
        compareValue = ( ( idx * Convert.ToUInt16(GenResources.GenRteSystemApplication5PrioTaskSchedulingTime) ) + 10 ) / Convert.ToUInt16(GenResources.GenRteSystemApplication5PrioTaskSchedulingTime);

        /* Check if scheduing element is needed */
        if (0 != sheduling[idx].Count())
        {
          fncContent += hlpAddNewFncContentLine("/* Trigger each: " + ( ( idx * Convert.ToUInt16(GenResources.GenRteSystemApplication5PrioTaskSchedulingTime) ) + 10 ).ToString() + "ms */");
          fncContent += hlpAddNewFncContentLine("if (0 == ( cnt % " + compareValue.ToString() + " ))");
          fncContent += hlpAddNewFncContentLine("{");
          foreach (string runnable in sheduling[idx])
          {
            fncContent += hlpAddNewFncContentLine("  " + runnable + "();");
          }
          fncContent += hlpAddNewFncContentLine("}\n");
        }
      }

      /* Increment Counter */
      fncContent += hlpAddNewFncContentLine("cnt++;");
      return fncContent;
    }


    /**
    * @brief    Helper function to generate prio 6 task function content
    * @retval   string function content
    */
    private string hlpGeneratePrio6TaskFncContent()
    {
      string fncContent = "";
      int compareValue, arrayOffset;

      List<string>[] sheduling = new List<string>[10];
      for (int idx = 0; idx < 10; idx++)
      {
        sheduling[idx] = new List<string>();
      }

      /* ===================================================================================== */
      /* ================================== Sort runnables =================================== */
      /* ===================================================================================== */
      foreach (RteSaCyclicRunnableInfo runnable in prio6Runnables)
      {
        /* Calculate Array offset */
        arrayOffset = ( ( runnable.cycleTime / Convert.ToUInt16(GenResources.GenRteSystemApplication6PrioTaskSchedulingTime) ) - 1 );

        /* Add runnable into scheduling array */
        sheduling[arrayOffset].Add(runnable.runnableName);
      }

      /* ===================================================================================== */
      /* ============================ Generate function content ============================== */
      /* ===================================================================================== */
      /* Generate counter */
      fncContent += hlpAddNewFncContentLine("static uint64 cnt = 0;\n");

      for (int idx = 0; idx < 10; idx++)
      {
        compareValue = ( ( idx * Convert.ToUInt16(GenResources.GenRteSystemApplication6PrioTaskSchedulingTime) ) + 1 ) / Convert.ToUInt16(GenResources.GenRteSystemApplication6PrioTaskSchedulingTime);

        /* Check if scheduing element is needed */
        if (0 != sheduling[idx].Count())
        {
          fncContent += hlpAddNewFncContentLine("/* Trigger each: " + ( ( idx * Convert.ToUInt16(GenResources.GenRteSystemApplication6PrioTaskSchedulingTime) ) + 1 ).ToString() + "ms */");
          fncContent += hlpAddNewFncContentLine("if (0 == ( cnt % " + compareValue.ToString() + " ))");
          fncContent += hlpAddNewFncContentLine("{");
          foreach (string runnable in sheduling[idx])
          {
            fncContent += hlpAddNewFncContentLine("  " + runnable + "();");
          }
          fncContent += hlpAddNewFncContentLine("}\n");
        }
      }

      /* Increment Counter */
      fncContent += hlpAddNewFncContentLine("cnt++;");
      return fncContent;
    }

    /**
    * @brief    Helper function to add a new function content line
    * @retval   string function content line
    * @note     Adding previous spaces and line feed.
    */
    private string hlpAddNewFncContentLine(string fncLine)
    {
      return ( "  " + fncLine + "\n" );
    }
  }
}
/**
 * @}
 */
