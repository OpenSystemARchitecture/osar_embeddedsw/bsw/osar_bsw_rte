﻿/*****************************************************************************************************************************
 * @file        RteCfgValidator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        03.01.2020                                                                                                   *
 * @brief       Implementation of the Validator of the Rte Config                                                            *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generator.Resources;
using OsarResources.XML;
using RteLib.RteConfig;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class RteCfgValidator
  {
    RteConfig rteXmlCfg = new RteConfig();
    string pathToConfigurationFile;
    private GenInfoType info = new GenInfoType();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    public RteCfgValidator(string pathToCfgFile)
    {
      pathToConfigurationFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();

      rteXmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToCfgFile);
    }

    /// <summary>
    /// Interface to validate the module configuration file
    /// </summary>
    /// <return> GenInfoType </return>
    public GenInfoType ValidateRteModuleConfiguration()
    {
      info.AddLogMsg(ValidateResources.LogMsg_StartOfModuleConfigValidation);

      /* Validate configuration file version */
      XmlFileVersion defaultVersion = new XmlFileVersion();
      defaultVersion.MajorVersion = Convert.ToUInt16(DefResources.RteCfgFileMajorVersion);
      defaultVersion.MinorVersion = Convert.ToUInt16(DefResources.RteCfgFileMinorVersion);
      defaultVersion.PatchVersion = Convert.ToUInt16(DefResources.RteCfgFilePatchVersion);
      if (false == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(defaultVersion, rteXmlCfg.XmlFileVersion))
      {
        info.AddErrorMsg(Convert.ToUInt16(ErrorWarningCodes.Error_0000), ErrorWarningCodes.Error_0000_Msg);
      }

      // Check module base path for its correctness
      if(null == rteXmlCfg.PathToBaseProjectFolder)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1000), GenErrorWarningCodes.Error_1000_Msg);
      }
      else if("" == rteXmlCfg.PathToBaseProjectFolder)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1000), GenErrorWarningCodes.Error_1000_Msg);
      }

      // Check rte types file path for its correctness
      if (null == rteXmlCfg.PathToRteTypeFile)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1001), GenErrorWarningCodes.Error_1001_Msg);
      }
      else if ("" == rteXmlCfg.PathToRteTypeFile)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1001), GenErrorWarningCodes.Error_1001_Msg);
      } else
      {
        string directoryPath = Path.GetDirectoryName(pathToConfigurationFile) + rteXmlCfg.PathToRteTypeFile;
        if (!File.Exists(directoryPath))
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1002), GenErrorWarningCodes.Error_1002_Msg +
            directoryPath);
        }
      }

      // Check rte interface file path for its correctness
      if (null == rteXmlCfg.PathToRteInterfaceFile)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1003), GenErrorWarningCodes.Error_1003_Msg);
      }
      else if ("" == rteXmlCfg.PathToRteInterfaceFile)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1003), GenErrorWarningCodes.Error_1003_Msg);
      } else
      {
        string directoryPath = Path.GetDirectoryName(pathToConfigurationFile) + rteXmlCfg.PathToRteInterfaceFile;
        if (!File.Exists(directoryPath))
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1004), GenErrorWarningCodes.Error_1004_Msg +
            directoryPath);
        }
      }

      // Check hyperspace project file path for its correctness
      if (null == rteXmlCfg.PathToHyperspaceProjectFile)
      {
        info.AddWaringMsg(Convert.ToUInt16(GenErrorWarningCodes.Warning_1000), GenErrorWarningCodes.Warning_1000_Msg);
      } else if("" == rteXmlCfg.PathToHyperspaceProjectFile)
      {
        info.AddWaringMsg(Convert.ToUInt16(GenErrorWarningCodes.Warning_1000), GenErrorWarningCodes.Warning_1000_Msg);
      } else
      {
        string pathToHyperspaceCfg = Path.GetDirectoryName(pathToConfigurationFile) + rteXmlCfg.PathToHyperspaceProjectFile;
        if (!File.Exists(pathToHyperspaceCfg))
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1005), GenErrorWarningCodes.Error_1005_Msg +
            pathToHyperspaceCfg);
        }
      }

      //TODO: Add Additional Module evaluation

      info.AddLogMsg(ValidateResources.LogMsg_ModuleConfigValidationDone);

      return info;
    }
  }
}
/**
 * @}
 */
