﻿/*****************************************************************************************************************************
 * @file        RteLayerGenerator.cs                                                                                         *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.01.2020                                                                                                   *
 * @brief       Implementation of the rte layer file generator                                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RteLib.RteConfig;
using RteLib.RteTypes;
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.DocumentationObjects.General;
using OsarResources.Generator;
using System.IO;
using OsarResources.XML;
using OsarResources.Generator.Resources;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class RteLayerGenerator
  {
    RteConfig xmlCfg = new RteConfig();
    string pathToConfiguratioFile;
    string projectBaseFolder;
    string pathToModuleBaseFolder;
    private GenInfoType info = new GenInfoType();
    List<RteModuleInternalBehavior> rteMibList;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    /// <param name="absPathToBaseModuleFolder"></param>
    /// <param name="mibList"></param>
    public RteLayerGenerator(string pathToCfgFile, string absPathToBaseModuleFolder, List<RteModuleInternalBehavior> mibList)
    {
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;
      rteMibList = mibList;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();

      xmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToCfgFile);

      projectBaseFolder = Path.GetDirectoryName(pathToConfiguratioFile) + xmlCfg.PathToBaseProjectFolder;
    }

    /// <summary>
    /// Generate the needed RteInterface File
    /// </summary>
    /// <returns></returns>
    public GenInfoType GenerateRteLayerFiles()
    {
      info.AddLogMsg("Start generation of the Rte Interaction Layer files.");
      generatingRteInteractionLayerHeaderFile();
      generatingRteInteractionLayerSourceFile();
      return info;
    }

    /**
    * @brief    Generate the needed RteInterface Header File
    * @details  Rte Config Xml Data Base Config file used for RteInterface file generation
    * @retval   none
    */
    private void generatingRteInteractionLayerHeaderFile()
    {
      string fileName = GenResources.GenRteInteractionLayerFileName + ".h";

      CHeaderFile rteGenModuleFile = new CHeaderFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileFunctionObjects genFncObject = new CFileFunctionObjects();
      CFileDefinitionObjects headerDefines = new CFileDefinitionObjects();
      CFileVariableObjects genVariable = new CFileVariableObjects();

      GeneralStartGroupObject groupMappingOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject groupMappingCloser = new GeneralEndGroupObject();

      int groupIdx;

      General newGeneral = new General(pathToConfiguratioFile);

      try
      {
        /*++++++++++++++++++++++++++++++++++++++++ Create doxygen file header ++++++++++++++++++++++++++++++++++++++++*/
        doxygenFileHeader.AddFileBrief("Generated Rte Interaction Layer Header File: " + fileName);
        doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
        doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
        doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
        doxygenFileHeader.AddFileName(fileName);
        doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
        doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupBSW);
        doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);


        /* ++++++++++++++++++++++++++++++++++++++++ Process include objects ++++++++++++++++++++++++++++++++++++++++ */
        CFileIncludeObjects includes = new CFileIncludeObjects();
        includes.AddIncludeFile(GenResources.GenRteConfigHeaderFileName + ".h");
        includes.AddIncludeFile(GenResources.GenRteTypesSourceFileName);

        foreach (RteModuleInternalBehavior module in rteMibList)
        {
          includes.AddIncludeFile(DefResources.ModuleName + "_" + module.ModuleName + ".h");
        }

        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++ Process all connected Server Ports objects ++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        foreach (RteClientServerPortConnection serverPortConnection in xmlCfg.RteClientServerConnections)
        {
          groupMappingOpener = new GeneralStartGroupObject();
          groupMappingOpener.AddCustomStartGroupString("/* Connect Server Port : \"" + GenResources.GenRteServerFncInterfaceBaseString + serverPortConnection.RteServerPort.InterfaceName + "\" */");
          groupIdx = headerDefines.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);
          /* Route each Client Port to the corresponding Server Port */
          foreach (RteGeneralInterfaceInfo clientPort in serverPortConnection.RteClientPorts)
          {
            RteCSInterfaceBlueprint csIfBlueprint = newGeneral.hlpGetFromCSIfPrototypeCSIfBlueprint(clientPort.UUID, rteMibList);

            foreach (RteFunctionBlueprint portFnc in csIfBlueprint.Functions)
            {
              headerDefines.AddDefinition(GenResources.GenRteClientFncInterfaceBaseString + clientPort.InterfaceName + "_" + portFnc.FunctionName,
                GenResources.GenRteServerFncInterfaceBaseString + serverPortConnection.RteServerPort.InterfaceName + "_" + portFnc.FunctionName,
                groupIdx);
            }
          }
        }

        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++ Process all unconnected Client Ports objects ++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        doxygenDescription.AddElementBrief("Dummy implementation for unconnected client port function");
        foreach (RteModuleInternalBehavior module in rteMibList)
        {
          foreach (RteAvailableClientPort clientPort in module.AvailableClientPorts)
          {
            if (false == newGeneral.hlpCheckIfPortPrototypeIsConnected(clientPort.ClientPort.InterfacePrototype.UUID))
            {
              RteCSInterfaceBlueprint csIfBlueprint = newGeneral.hlpGetFromCSIfPrototypeCSIfBlueprint(clientPort.ClientPort.InterfacePrototype.UUID, rteMibList);

              foreach (RteFunctionBlueprint portFnc in csIfBlueprint.Functions)
              {
                string functionName = GenResources.GenRteClientFncInterfaceBaseString + clientPort.ClientPort.InterfacePrototype.InterfaceName + "_" + portFnc.FunctionName;
                genFncObject.AddFunction(newGeneral.hlpGenerateFncRetVal(portFnc.FunctionName), functionName, newGeneral.hlpGenerateFncArgList(portFnc.FunctionName), doxygenDescription);
              }
            }
          }
        }

        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++ Process all connected Sender Ports objects ++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        foreach (RteSenderReceiverPortConnection senderPortConnection in xmlCfg.RteSenderReceiverConnections)
        {
          RteSRInterfaceBlueprint srIfBlueprint;
          groupMappingOpener = new GeneralStartGroupObject();
          groupMappingOpener.AddCustomStartGroupString("/* Connect Sender Port : \"" + GenResources.GenRteSenderElementInterfaceBaseString + senderPortConnection.RteSenderPort.InterfaceName + "\" */");
          groupIdx = headerDefines.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);

          /* Create Sender Port variable and access */
          srIfBlueprint = newGeneral.hlpGetFromSRIfPrototypeSRIfBlueprint(senderPortConnection.RteSenderPort.UUID, rteMibList);
          foreach (RteElementBlueprint portElement in srIfBlueprint.Elements)
          {
            headerDefines.AddDefinition(GenResources.GenRteSenderElementInterfaceBaseString + senderPortConnection.RteSenderPort.InterfaceName + "_" + portElement.ElementName + "( x )",
                 "( " + GenResources.GenRteSenderElementInterfaceBaseString + senderPortConnection.RteSenderPort.InterfaceName + "_" + portElement.ElementName + GenResources.GenRteSenderElementVar + " = x )",
                groupIdx);

            genVariable.AddVariable(RteDataTypeHelper.GetDataTypeWithAccessString(portElement.ElementType.DataTypeName, portElement.ElementType.DataAccessType),
              GenResources.GenRteSenderElementInterfaceBaseString + senderPortConnection.RteSenderPort.InterfaceName + "_" + portElement.ElementName + GenResources.GenRteSenderElementVar,
              "", "extern");
          }

          /* Route each Receiver Port to the corresponding Server Port */
          foreach (RteGeneralInterfaceInfo receiverPort in senderPortConnection.RteReceiverPorts)
          {
            srIfBlueprint = newGeneral.hlpGetFromSRIfPrototypeSRIfBlueprint(receiverPort.UUID, rteMibList);

            foreach (RteElementBlueprint portElement in srIfBlueprint.Elements)
            {
              headerDefines.AddDefinition(GenResources.GenRteReceiverElementInterfaceBaseString + receiverPort.InterfaceName + "_" + portElement.ElementName + "()",
                GenResources.GenRteSenderElementInterfaceBaseString + senderPortConnection.RteSenderPort.InterfaceName + "_" + portElement.ElementName + GenResources.GenRteSenderElementVar,
                groupIdx);
            }
          }
        }

        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++ Process all unconnected Sender Receiver Ports objects +++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        groupMappingOpener.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_ZERO_VAR);
        groupMappingCloser.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_ZERO_VAR);
        doxygenDescription.AddElementBrief("Dummy implementation for unconnected Sender Receiver port elements");
        foreach (RteModuleInternalBehavior module in rteMibList)
        {
          foreach (RteAvailableSRPort srPort in module.AvailableSRPorts)
          {
            if (false == newGeneral.hlpCheckIfPortPrototypeIsConnected(srPort.SenderReceiverPort.InterfacePrototype.UUID))
            {
              RteSRInterfaceBlueprint srIfBlueprint = newGeneral.hlpGetFromSRIfPrototypeSRIfBlueprint(srPort.SenderReceiverPort.InterfacePrototype.UUID, rteMibList);

              foreach (RteElementBlueprint portElement in srIfBlueprint.Elements)
              {
                //Separate if Sender or receiver Port is used
                if (RteSRInterfaceImplementationType.SENDER == srPort.SenderReceiverPort.InterfaceType)
                {
                  headerDefines.AddDefinition(GenResources.GenRteSenderElementInterfaceBaseString + srPort.SenderReceiverPort.InterfacePrototype.InterfaceName + "_" + portElement.ElementName + "( x )",
                     "( " + GenResources.GenRteSenderElementInterfaceBaseString + srPort.SenderReceiverPort.InterfacePrototype.InterfaceName + "_" + portElement.ElementName + GenResources.GenRteSenderElementVar + " = x )", doxygenDescription);

                  genVariable.AddVariable(RteDataTypeHelper.GetDataTypeWithAccessString(portElement.ElementType.DataTypeName, portElement.ElementType.DataAccessType),
                    GenResources.GenRteSenderElementInterfaceBaseString + srPort.SenderReceiverPort.InterfacePrototype.InterfaceName + "_" + portElement.ElementName + GenResources.GenRteSenderElementVar,
                    doxygenDescription, "", "extern");
                }
                else
                {
                  headerDefines.AddDefinition(GenResources.GenRteReceiverElementInterfaceBaseString + srPort.SenderReceiverPort.InterfacePrototype.InterfaceName + "_" + portElement.ElementName + "()",
                     GenResources.GenRteReceiverElementInterfaceBaseString + srPort.SenderReceiverPort.InterfacePrototype.InterfaceName + "_" + portElement.ElementName + GenResources.GenRteSenderElementVar, doxygenDescription);

                  genVariable.AddVariable(RteDataTypeHelper.GetDataTypeWithAccessString(portElement.ElementType.DataTypeName, portElement.ElementType.DataAccessType),
                    GenResources.GenRteReceiverElementInterfaceBaseString + srPort.SenderReceiverPort.InterfacePrototype.InterfaceName + "_" + portElement.ElementName + GenResources.GenRteSenderElementVar,
                    doxygenDescription, "", "extern");
                }

              }
            }
          }
        }

        /*++++++++++++++++++++++++++++++++++++++++ Generate File ++++++++++++++++++++++++++++++++++++++++*/
        rteGenModuleFile.AddFileCommentHeader(doxygenFileHeader);
        rteGenModuleFile.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
        rteGenModuleFile.AddFileName(fileName);
        rteGenModuleFile.AddFilePath(pathToModuleBaseFolder + GenResources.GenHeaderFilePath);
        rteGenModuleFile.AddGlobalFunctionPrototypeObject(genFncObject);
        rteGenModuleFile.AddIncludeObject(includes);
        rteGenModuleFile.AddDefinitionObject(headerDefines);
        rteGenModuleFile.AddGlobalVariableObject(genVariable);
        rteGenModuleFile.GenerateSourceFile();
      }
      catch
      {
        // If error occurred >> Store error information
        info = OsarGenHelper.MergeGenInfoType(info, newGeneral.GenInfo);
      }
    }

    /**
  * @brief    Generate the needed RteInterface Source File
  * @details  Rte Config Xml Data Base Config file used for RteInterface file generation
  * @retval   none
  */
    private void generatingRteInteractionLayerSourceFile()
    {
      string fileName = GenResources.GenRteInteractionLayerFileName + ".c";

      CSourceFile rteGenModuleFile = new CSourceFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileFunctionObjects genFncObject = new CFileFunctionObjects();
      CFileVariableObjects genVariable = new CFileVariableObjects();

      GeneralStartGroupObject groupMappingOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject groupMappingCloser = new GeneralEndGroupObject();
      int groupFncIdx, groupVarIdx;

      General newGeneral = new General(pathToConfiguratioFile);
      
      try
      {
        /*++++++++++++++++++++++++++++++++++++++++ Create doxygen file header ++++++++++++++++++++++++++++++++++++++++*/
        doxygenFileHeader.AddFileBrief("Generated Rte Interaction Layer Source File: " + fileName);
        doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
        doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
        doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
        doxygenFileHeader.AddFileName(fileName);
        doxygenFileHeader.AddFileGenerator( DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
        doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupBSW);
        doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);


        /* ++++++++++++++++++++++++++++++++++++++++ Process include objects ++++++++++++++++++++++++++++++++++++++++ */
        CFileIncludeObjects includes = new CFileIncludeObjects();
        includes.AddIncludeFile(GenResources.GenRteTypesSourceFileName);
        includes.AddIncludeFile(GenResources.GenRteInteractionLayerFileName + ".h");


        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++ Process all unconnected Client Ports objects ++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        groupMappingOpener.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CODE);
        groupMappingCloser.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CODE);
        groupFncIdx = genFncObject.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);
        doxygenDescription.AddElementBrief("Dummy implementation for unconnected client port function");
        foreach (RteModuleInternalBehavior module in rteMibList)
        {
          foreach (RteAvailableClientPort clientPort in module.AvailableClientPorts)
          {
            if (false == newGeneral.hlpCheckIfPortPrototypeIsConnected(clientPort.ClientPort.InterfacePrototype.UUID))
            {
              RteCSInterfaceBlueprint csIfBlueprint = newGeneral.hlpGetFromCSIfPrototypeCSIfBlueprint(clientPort.ClientPort.InterfacePrototype.UUID, rteMibList);

              foreach (RteFunctionBlueprint portFnc in csIfBlueprint.Functions)
              {
                string functionName = GenResources.GenRteClientFncInterfaceBaseString + clientPort.ClientPort.InterfacePrototype.InterfaceName + "_" + portFnc.FunctionName;
                genFncObject.AddFunction(newGeneral.hlpGenerateFncRetVal(portFnc.FunctionName), functionName, newGeneral.hlpGenerateFncArgList(portFnc.FunctionName), groupFncIdx, doxygenDescription, GenResources.GenRteUnconnectedClientPortFncContent);
              }
            }
          }
        }

        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++ Process all connected Sender Ports objects ++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        groupMappingOpener.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_ZERO_VAR);
        groupMappingCloser.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_ZERO_VAR);
        groupVarIdx = genVariable.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);
        foreach (RteSenderReceiverPortConnection senderPortConnection in xmlCfg.RteSenderReceiverConnections)
        {
          RteSRInterfaceBlueprint srIfBlueprint;

          /* Create Sender Port variable */
          srIfBlueprint = newGeneral.hlpGetFromSRIfPrototypeSRIfBlueprint(senderPortConnection.RteSenderPort.UUID, rteMibList);
          foreach (RteElementBlueprint portElement in srIfBlueprint.Elements)
          {
            genVariable.AddVariable(RteDataTypeHelper.GetDataTypeWithAccessString(portElement.ElementType.DataTypeName, portElement.ElementType.DataAccessType),
              GenResources.GenRteSenderElementInterfaceBaseString + senderPortConnection.RteSenderPort.InterfaceName + "_" + portElement.ElementName + GenResources.GenRteSenderElementVar, groupVarIdx);
          }
        }

        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++ Process all unconnected Sender Receiver Ports objects +++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        groupMappingOpener.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_ZERO_VAR);
        groupMappingCloser.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_ZERO_VAR);
        groupVarIdx = genVariable.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);
        doxygenDescription.AddElementBrief("Dummy implementation for unconnected Sender Receiver port elements");
        foreach (RteModuleInternalBehavior module in rteMibList)
        {
          foreach (RteAvailableSRPort srPort in module.AvailableSRPorts)
          {
            if (false == newGeneral.hlpCheckIfPortPrototypeIsConnected(srPort.SenderReceiverPort.InterfacePrototype.UUID))
            {
              RteSRInterfaceBlueprint srIfBlueprint = newGeneral.hlpGetFromSRIfPrototypeSRIfBlueprint(srPort.SenderReceiverPort.InterfacePrototype.UUID, rteMibList);

              foreach (RteElementBlueprint portElement in srIfBlueprint.Elements)
              {
                //Separate if Sender or receiver Port is used
                if (RteSRInterfaceImplementationType.SENDER == srPort.SenderReceiverPort.InterfaceType)
                {
                  genVariable.AddVariable(RteDataTypeHelper.GetDataTypeWithAccessString(portElement.ElementType.DataTypeName, portElement.ElementType.DataAccessType),
                  GenResources.GenRteSenderElementInterfaceBaseString + srPort.SenderReceiverPort.InterfacePrototype.InterfaceName + "_" + portElement.ElementName +
                  GenResources.GenRteSenderElementVar, groupVarIdx, doxygenDescription);
                }
                else
                {
                  genVariable.AddVariable(RteDataTypeHelper.GetDataTypeWithAccessString(portElement.ElementType.DataTypeName, portElement.ElementType.DataAccessType),
                  GenResources.GenRteReceiverElementInterfaceBaseString + srPort.SenderReceiverPort.InterfacePrototype.InterfaceName + "_" + portElement.ElementName +
                  GenResources.GenRteSenderElementVar, groupVarIdx, doxygenDescription);
                }

              }
            }
          }
        }

        /*++++++++++++++++++++++++++++++++++++++++ Generate File ++++++++++++++++++++++++++++++++++++++++*/
        rteGenModuleFile.AddFileCommentHeader(doxygenFileHeader);
        rteGenModuleFile.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
        rteGenModuleFile.AddFileName(fileName);
        rteGenModuleFile.AddFilePath(pathToModuleBaseFolder + GenResources.GenSourceFilePath);
        rteGenModuleFile.AddIncludeObject(includes);
        rteGenModuleFile.AddGlobalVariableObject(genVariable);
        rteGenModuleFile.AddGlobalFunctionObject(genFncObject);
        rteGenModuleFile.GenerateSourceFile();
      }
      catch
      {
        // If error occurred >> Store error information
        info = OsarGenHelper.MergeGenInfoType(info, newGeneral.GenInfo);
      }
    }
  }
}
/**
 * @}
 */
