﻿/*****************************************************************************************************************************
 * @file        RteMakefileGenerator.cs                                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.01.2020                                                                                                   *
 * @brief       Implementation of the rte makefile file generator                                                            *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarSourceFileLib.ModuleMakefile;
using RteLib.RteConfig;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class RteMakefileGenerator
  {
    RteConfig xmlCfg = new RteConfig();
    string pathToConfiguratioFile;
    string projectBaseFolder;
    string pathToModuleBaseFolder;
    private GenInfoType info = new GenInfoType();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    /// <param name="absPathToBaseModuleFolder"></param>
    public RteMakefileGenerator(string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();

      xmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToCfgFile);

      projectBaseFolder = Path.GetDirectoryName(pathToConfiguratioFile) + xmlCfg.PathToBaseProjectFolder;
    }

    /// <summary>
    /// Interface to generate the rte makefile
    /// </summary>
    /// <returns></returns>
    public GenInfoType GenerateMakefiles()
    {
      info.AddLogMsg("Start generation of the rte makefile");

      ModuleMakefile makefile = new ModuleMakefile();
      makefile.AddFileName(DefResources.ModuleName + ".mk");
      makefile.AddFilePath(pathToModuleBaseFolder + GenResources.GenMakefileFilePath);
      makefile.AddMakefileModuleName(DefResources.ModuleName);
      makefile.AddMakefileBasePath("BSW");

      /* Add static data sets */
      makefile.AddMakefileIncludePath("02_Mandatory/inc");
      makefile.AddMakefileIncludePath("03_GenData/inc");
      makefile.AddMakefileIncludePath("04_StaticData/inc");
      makefile.AddMakefileSourceFileName("04_StaticData/src/Rte.c");

      /* Add dynamic data sets */
      makefile.AddMakefileSourceFileName("03_GenData/src/Rte_Callout_Stubs.c");
      makefile.AddMakefileSourceFileName("03_GenData/src/Rte_Interfaces.c");
      makefile.AddMakefileSourceFileName("03_GenData/src/RteSystemApplication_Internal.c");

      foreach (RteSystemApplication systemApplication in xmlCfg.RteSystemApplications)
      {
        string fileName = GenResources.GenRteModuleSystemApplicationBaseFileName + systemApplication.RteSystemApplicationName + ".c";
        makefile.AddMakefileSourceFileName("03_GenData/src/" + fileName);
      }

      /* Generate Makefile */
      makefile.GenerateSourceFile();

      return info;
    }

  }
}
/**
 * @}
 */
