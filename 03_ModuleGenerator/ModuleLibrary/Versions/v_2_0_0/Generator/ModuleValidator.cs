﻿/*****************************************************************************************************************************
 * @file        ModuleValidator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Validation Class                                                                *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.XML;
using OsarResources.Generator.Resources;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  internal class ModuleValidator
  {
    private string pathToConfiguratioFile;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public ModuleValidator(string pathToCfgFile)
    {
      pathToConfiguratioFile = pathToCfgFile;
    }

    /// <summary>
    /// Interface to validate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType ValidateConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfoMerge = new GenInfoType();

      RteCfgValidator cfgFileValidator = new RteCfgValidator(pathToConfiguratioFile);
      genInfo1 = cfgFileValidator.ValidateRteModuleConfiguration();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      RteTypesValidator typesFileValidator = new RteTypesValidator(pathToConfiguratioFile);
      genInfo1 = typesFileValidator.ValidateRteTypesConfiguration();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      RteInterfaceValidator interfaceFileValidator = new RteInterfaceValidator(pathToConfiguratioFile);
      genInfo1 = interfaceFileValidator.ValidateRteInterfaceConfiguration();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      return genInfoMerge;
    }
  }
}
/**
 * @}
 */
