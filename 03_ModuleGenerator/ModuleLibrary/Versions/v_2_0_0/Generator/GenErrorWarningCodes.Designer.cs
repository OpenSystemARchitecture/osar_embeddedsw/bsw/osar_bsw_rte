﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModuleLibrary.Versions.v_2_0_0.Generator {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class GenErrorWarningCodes {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal GenErrorWarningCodes() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ModuleLibrary.Versions.v_2_0_0.Generator.GenErrorWarningCodes", typeof(GenErrorWarningCodes).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1000 .
        /// </summary>
        internal static string Error_1000 {
            get {
                return ResourceManager.GetString("Error_1000", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rte module config file: Project base folder path not set!.
        /// </summary>
        internal static string Error_1000_Msg {
            get {
                return ResourceManager.GetString("Error_1000_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1001 .
        /// </summary>
        internal static string Error_1001 {
            get {
                return ResourceManager.GetString("Error_1001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rte module config file: Rte Types file path not set!.
        /// </summary>
        internal static string Error_1001_Msg {
            get {
                return ResourceManager.GetString("Error_1001_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1002 .
        /// </summary>
        internal static string Error_1002 {
            get {
                return ResourceManager.GetString("Error_1002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rte module config file: Rte Types file not found! Path:.
        /// </summary>
        internal static string Error_1002_Msg {
            get {
                return ResourceManager.GetString("Error_1002_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1003 .
        /// </summary>
        internal static string Error_1003 {
            get {
                return ResourceManager.GetString("Error_1003", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rte module config file: Rte Interface file path not set!.
        /// </summary>
        internal static string Error_1003_Msg {
            get {
                return ResourceManager.GetString("Error_1003_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1004 .
        /// </summary>
        internal static string Error_1004 {
            get {
                return ResourceManager.GetString("Error_1004", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rte module config file: Rte Interface file not found! Path:.
        /// </summary>
        internal static string Error_1004_Msg {
            get {
                return ResourceManager.GetString("Error_1004_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1005 .
        /// </summary>
        internal static string Error_1005 {
            get {
                return ResourceManager.GetString("Error_1005", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rte module config file: Specified Hyperspace project file not found! Path:.
        /// </summary>
        internal static string Error_1005_Msg {
            get {
                return ResourceManager.GetString("Error_1005_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1006 .
        /// </summary>
        internal static string Error_1006 {
            get {
                return ResourceManager.GetString("Error_1006", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ....
        /// </summary>
        internal static string Error_1006_Msg {
            get {
                return ResourceManager.GetString("Error_1006_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1007 .
        /// </summary>
        internal static string Error_1007 {
            get {
                return ResourceManager.GetString("Error_1007", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ....
        /// </summary>
        internal static string Error_1007_Msg {
            get {
                return ResourceManager.GetString("Error_1007_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1008 .
        /// </summary>
        internal static string Error_1008 {
            get {
                return ResourceManager.GetString("Error_1008", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ....
        /// </summary>
        internal static string Error_1008_Msg {
            get {
                return ResourceManager.GetString("Error_1008_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1009 .
        /// </summary>
        internal static string Error_1009 {
            get {
                return ResourceManager.GetString("Error_1009", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ....
        /// </summary>
        internal static string Error_1009_Msg {
            get {
                return ResourceManager.GetString("Error_1009_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1010 .
        /// </summary>
        internal static string Error_1010 {
            get {
                return ResourceManager.GetString("Error_1010", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ....
        /// </summary>
        internal static string Error_1010_Msg {
            get {
                return ResourceManager.GetString("Error_1010_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2000 .
        /// </summary>
        internal static string Error_2000 {
            get {
                return ResourceManager.GetString("Error_2000", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Failed to create / store the rte types file! Exception msg: .
        /// </summary>
        internal static string Error_2000_Msg {
            get {
                return ResourceManager.GetString("Error_2000_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2001 .
        /// </summary>
        internal static string Error_2001 {
            get {
                return ResourceManager.GetString("Error_2001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rte Types file validation error occured: .
        /// </summary>
        internal static string Error_2001_Msg {
            get {
                return ResourceManager.GetString("Error_2001_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3000 .
        /// </summary>
        internal static string Error_3000 {
            get {
                return ResourceManager.GetString("Error_3000", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Failed to create / store the rte interface file! Exception msg:.
        /// </summary>
        internal static string Error_3000_Msg {
            get {
                return ResourceManager.GetString("Error_3000_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 5000 .
        /// </summary>
        internal static string Error_5000 {
            get {
                return ResourceManager.GetString("Error_5000", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Interface UUID not found in interface database &gt;&gt; UUID:.
        /// </summary>
        internal static string Error_5000_Msg {
            get {
                return ResourceManager.GetString("Error_5000_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 5001 .
        /// </summary>
        internal static string Error_5001 {
            get {
                return ResourceManager.GetString("Error_5001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Interface function not found in interface data base: Function name: .
        /// </summary>
        internal static string Error_5001_Msg {
            get {
                return ResourceManager.GetString("Error_5001_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 5002 .
        /// </summary>
        internal static string Error_5002 {
            get {
                return ResourceManager.GetString("Error_5002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Client/Server Port Blueprint not found! UUID: .
        /// </summary>
        internal static string Error_5002_Msg {
            get {
                return ResourceManager.GetString("Error_5002_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 5003 .
        /// </summary>
        internal static string Error_5003 {
            get {
                return ResourceManager.GetString("Error_5003", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Sender/Receiver Port Blueprint found to corresponding Sender/Receiver Port Prototype UUDI: .
        /// </summary>
        internal static string Error_5003_Msg {
            get {
                return ResourceManager.GetString("Error_5003_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 5004 .
        /// </summary>
        internal static string Error_5004 {
            get {
                return ResourceManager.GetString("Error_5004", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Client/Server Port Blueprint found to corresponding Client/Server Port Prototype UUDI :.
        /// </summary>
        internal static string Error_5004_Msg {
            get {
                return ResourceManager.GetString("Error_5004_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 5005 .
        /// </summary>
        internal static string Error_5005 {
            get {
                return ResourceManager.GetString("Error_5005", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sender/Receiver Port Blueprint not found! UUID:.
        /// </summary>
        internal static string Error_5005_Msg {
            get {
                return ResourceManager.GetString("Error_5005_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 6000 .
        /// </summary>
        internal static string Error_6000 {
            get {
                return ResourceManager.GetString("Error_6000", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MIB config import failure &quot;Config failure&quot;:.
        /// </summary>
        internal static string Error_6000_Msg {
            get {
                return ResourceManager.GetString("Error_6000_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 6001 .
        /// </summary>
        internal static string Error_6001 {
            get {
                return ResourceManager.GetString("Error_6001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MIB config import failure &quot;Import failure&quot;:.
        /// </summary>
        internal static string Error_6001_Msg {
            get {
                return ResourceManager.GetString("Error_6001_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 6002 .
        /// </summary>
        internal static string Error_6002 {
            get {
                return ResourceManager.GetString("Error_6002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .....
        /// </summary>
        internal static string Error_6002_Msg {
            get {
                return ResourceManager.GetString("Error_6002_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1000 .
        /// </summary>
        internal static string Warning_1000 {
            get {
                return ResourceManager.GetString("Warning_1000", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rte module config file: Hyperspace project file not defined..
        /// </summary>
        internal static string Warning_1000_Msg {
            get {
                return ResourceManager.GetString("Warning_1000_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4000 .
        /// </summary>
        internal static string Warning_4000 {
            get {
                return ResourceManager.GetString("Warning_4000", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MiB: Specified user module not found! Module: .
        /// </summary>
        internal static string Warning_4000_Msg {
            get {
                return ResourceManager.GetString("Warning_4000_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4001 .
        /// </summary>
        internal static string Warning_4001 {
            get {
                return ResourceManager.GetString("Warning_4001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Module internal behavior file not found! Module:.
        /// </summary>
        internal static string Warning_4001_Msg {
            get {
                return ResourceManager.GetString("Warning_4001_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4002 .
        /// </summary>
        internal static string Warning_4002 {
            get {
                return ResourceManager.GetString("Warning_4002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Hyperspace project file found. Skipp merge of Hyperspace-Project-File with Rte-Module-Configuration-File.
        /// </summary>
        internal static string Warning_4002_Msg {
            get {
                return ResourceManager.GetString("Warning_4002_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 4003 .
        /// </summary>
        internal static string Warning_4003 {
            get {
                return ResourceManager.GetString("Warning_4003", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configured system application module not found in Rte used module list. Remove system application module:.
        /// </summary>
        internal static string Warning_4003_Msg {
            get {
                return ResourceManager.GetString("Warning_4003_Msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 5000 .
        /// </summary>
        internal static string Warning_5000 {
            get {
                return ResourceManager.GetString("Warning_5000", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rte Runnable could not be scheduled &gt;&gt; Module:.
        /// </summary>
        internal static string Warning_5000_Msg {
            get {
                return ResourceManager.GetString("Warning_5000_Msg", resourceCulture);
            }
        }
    }
}
