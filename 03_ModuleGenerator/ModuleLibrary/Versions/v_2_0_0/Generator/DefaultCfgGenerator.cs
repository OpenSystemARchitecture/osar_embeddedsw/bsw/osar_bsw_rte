﻿/*****************************************************************************************************************************
 * @file        DefaultCfgGenerator.cs                                                                                                *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Default Module Class                                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generic;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  internal class DefaultCfgGenerator
  {
    private string pathToConfiguratioFile;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public DefaultCfgGenerator(string pathToCfgFile)
    {
      pathToConfiguratioFile = pathToCfgFile;
    }

    /// <summary>
    /// Interface to set default configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType CreateDefaultConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfoMerge = new GenInfoType();

      /* Create default config file */
      RteCfgDefaultGenerator cfgFileGenerator = new RteCfgDefaultGenerator(pathToConfiguratioFile);
      genInfo1 = cfgFileGenerator.GenerateDefaultRteModuleConfiguration();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      /* Create default types file */
      RteTypesDefaultGenerator typesFileGenerator = new RteTypesDefaultGenerator(pathToConfiguratioFile);
      genInfo1 = typesFileGenerator.GenerateDefaultRteTypesConfiguration();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      /* Create default interface file */
      RteInterfaceDefaultGenerator interfaceFileGenerator = new RteInterfaceDefaultGenerator(pathToConfiguratioFile);
      genInfo1 = interfaceFileGenerator.GenerateDefaultRteInterfaceConfiguration();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      return genInfoMerge;
    }
  }
}
/**
 * @}
 */
