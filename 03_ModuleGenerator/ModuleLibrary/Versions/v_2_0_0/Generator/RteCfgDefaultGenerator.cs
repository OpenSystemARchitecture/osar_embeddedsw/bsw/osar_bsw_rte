﻿/*****************************************************************************************************************************
 * @file        RteCfgDefaultGenerator.cs                                                                                    *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        03.01.2020                                                                                                   *
 * @brief       Implementation of the Default Rte Config Generator Class                                                     *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generic;
using RteLib.RteConfig;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class RteCfgDefaultGenerator
  {
    RteConfig xmlCfg = new RteConfig();
    string pathToConfiguratioFile;
    private GenInfoType info = new GenInfoType();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    public RteCfgDefaultGenerator(string pathToCfgFile)
    {
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();
    }

    /// <summary>
    /// Interface to generate a default module configuration file
    /// </summary>
    /// <return> GenInfoType </return>
    public GenInfoType GenerateDefaultRteModuleConfiguration()
    {
      info.AddLogMsg(DefResources.LogMsg_StartDefaultCfgCreation +
        xmlCfg.XmlFileVersion.MajorVersion.ToString() + "." +
        xmlCfg.XmlFileVersion.MinorVersion.ToString() + "." +
        xmlCfg.XmlFileVersion.PatchVersion.ToString());

      xmlCfg.PathToBaseProjectFolder = DefResources.RteCfgDefaultPathToBaseProjectFolder;
      xmlCfg.PathToRteTypeFile = DefResources.RteCfgDefaultRteTypeDataBaseFileName;
      xmlCfg.PathToRteInterfaceFile = DefResources.RteCfgDefaultRteInterfaceDataBaseFileName;

      xmlCfg.DetModuleID = Convert.ToUInt16(DefResources.RteCfgDefaultDetModuleId);
      if (DefResources.RteCfgDefDetModuleEnableUsage == SystemState.STD_ON.ToString())
        xmlCfg.DetModuleUsage = SystemState.STD_ON;
      else
        xmlCfg.DetModuleUsage = SystemState.STD_OFF;

      /* Adding Rte default System Application */
      RteSystemApplication rteSystemApp = new RteSystemApplication();
      rteSystemApp.RteSystemApplicationName = DefResources.RteCfgDefaultSystemApplicationName;
      rteSystemApp.UUID = Guid.NewGuid().ToString("N");
      xmlCfg.RteSystemApplications.Add(rteSystemApp);

      xmlCfg.SaveActiveRteConfigDataBaseToXml(pathToConfiguratioFile);

      info.AddLogMsg(DefResources.LogMsg_DefaultCfgCreationDone);

      return info;
    }
  }
}
/**
 * @}
 */
