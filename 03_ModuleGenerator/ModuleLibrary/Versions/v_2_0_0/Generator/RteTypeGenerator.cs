﻿/*****************************************************************************************************************************
 * @file        RteTypesGenerator.cs                                                                                         *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.01.2020                                                                                                   *
 * @brief       Implementation of the rte types file generator                                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generator.Resources;
using OsarResources.Generic;
using OsarResources.XML;
using RteLib.RteConfig;
using RteLib.RteTypes;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.DocumentationObjects.General;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class RteTypesGenerator
  {
    RteConfig xmlCfg = new RteConfig();
    RteTypes rteXmlTypes = new RteTypes();
    string pathToConfiguratioFile;
    string pathToModuleBaseFolder;
    private GenInfoType info = new GenInfoType();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    /// <param name="absPathToBaseModuleFolder"></param>
    public RteTypesGenerator(string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();

      xmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToCfgFile);
      string pathToRteTypesFile = Path.GetDirectoryName(pathToConfiguratioFile) + xmlCfg.PathToRteTypeFile;
      rteXmlTypes.ReadActiveRteTypesDataBaseFromXml(pathToRteTypesFile);
    }

    /// <summary>
    /// Interface to validate the rte types configuration file
    /// </summary>
    /// <return> GenInfoType </return>
    public GenInfoType GenerateRteTypesConfiguration()
    {
      info.AddLogMsg(GenResources.LogMsg_GenTypesHeaderFile);
      CHeaderFile moduleTypesHeader = new CHeaderFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileDefinitionObjects headerDefinitions = new CFileDefinitionObjects();
      GeneralStartGroupObject generalStartGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject generalEndGroup = new GeneralEndGroupObject();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileVariableObjects headerVariables = new CFileVariableObjects();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      CFileTypeObjects headerTypes = new CFileTypeObjects();
      int actualGroupIdx;
      List<string> tempStringList;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Prepare general data sets ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenFileHeader.AddFileBrief("Generated Rte Data Types.");
      doxygenFileHeader.AddFileName(GenResources.GenRteTypesSourceFileName);
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileGroupOpener.AddToDoxygenMasterGroup(OsarResources.Generator.Resources.genericSource.DoxygenOsarMasterGroupBSW);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Prepare include strings ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      headerIncludes.AddIncludeFile("Std_Types.h");

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++ Prepare data types +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* Generating Advanced Types if available */
      foreach (RteAdvancedDataTypeStructure advancedDataType in rteXmlTypes.AdvancedDataTypeList)
      {
        /* Check advanced data type type */
        switch (advancedDataType.DataTypeType)
        {
          case RteAdvancedDataTypeType.STANDARD:
            {
              doxygenDescription = new DoxygenElementDescription();
              doxygenDescription.AddElementBrief("Rte generated standard user data type >> " + advancedDataType.DataType.DataTypeName);
              doxygenDescription.AddElementUUID(advancedDataType.DataType.UUID);
              doxygenDescription.AddElementDetails("New data type based on standard data type: \"" + advancedDataType.BaseDataType.DataTypeName + "\" with UUID: " + advancedDataType.BaseDataType.UUID);
              headerTypes.AddNewDataTypeDefinition(advancedDataType.BaseDataType.DataTypeName, advancedDataType.DataType.DataTypeName, doxygenDescription);
            }
            break;

          case RteAdvancedDataTypeType.ENUMERATION:
            {
              doxygenDescription = new DoxygenElementDescription();
              doxygenDescription.AddElementBrief("Rte generated user enumeration data type >> " + advancedDataType.DataType.DataTypeName);
              doxygenDescription.AddElementUUID(advancedDataType.DataType.UUID);

              tempStringList = new List<string>();
              foreach (RteAdvancedEnumDataTypeStructure enumElement in advancedDataType.EnumElements)
              {
                /* Check if Id is given */
                if ( (null != enumElement.EnumId) && ("" != enumElement.EnumId) )
                {
                  tempStringList.Add(enumElement.EnumElementName + " = " + enumElement.EnumId);
                }
                else
                {
                  tempStringList.Add(enumElement.EnumElementName);
                }

              }

              headerTypes.AddEnumerationTypeDefinition(advancedDataType.DataType.DataTypeName, tempStringList.ToArray(), doxygenDescription);
            }
            break;

          case RteAdvancedDataTypeType.STRUCT:
            {
              doxygenDescription = new DoxygenElementDescription();
              doxygenDescription.AddElementBrief("Rte generated user struct data type >> " + advancedDataType.DataType.DataTypeName);
              doxygenDescription.AddElementUUID(advancedDataType.DataType.UUID);

              tempStringList = new List<string>();
              foreach (RteAdvancedStructDataTypeStructure structElement in advancedDataType.StructElements)
              {
                doxygenDescription.AddElementParameter("Struct element \"" + structElement.StructElementName + "\" uses \"" +
                  structElement.BaseDataType.DataTypeName + "\" with UUID: " + structElement.BaseDataType.UUID + " as base data type", CParameterType.INPUT_OUTPUT);
                tempStringList.Add(structElement.BaseDataType.DataTypeName + " " + structElement.StructElementName);
              }

              headerTypes.AddStructTypeDefinition(advancedDataType.DataType.DataTypeName, tempStringList.ToArray(), doxygenDescription);
            }
            break;

          default:
          throw new ArgumentOutOfRangeException();
        }
      }
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++ Prepare Definitions +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Header Functions ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      moduleTypesHeader.AddFileName(GenResources.GenRteTypesSourceFileName);
      moduleTypesHeader.AddFilePath(pathToModuleBaseFolder + GenResources.GenHeaderFilePath);
      moduleTypesHeader.AddFileCommentHeader(doxygenFileHeader);
      moduleTypesHeader.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      moduleTypesHeader.AddDefinitionObject(headerDefinitions);
      moduleTypesHeader.AddIncludeObject(headerIncludes);
      moduleTypesHeader.AddGlobalVariableObject(headerVariables);
      moduleTypesHeader.AddTypesObject(headerTypes);
      moduleTypesHeader.GenerateSourceFile();

      info.AddLogMsg(GenResources.LogMsg_TypesHeaderFileGenerated);

      return info;
    }
  }
}
/**
 * @}
 */
