﻿/*****************************************************************************************************************************
 * @file        RteCalloutGenerator.cs                                                                                       *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.01.2020                                                                                                   *
 * @brief       Implementation of the rte callout file generator                                                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RteLib.RteConfig;
using RteLib.RteTypes;
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.DocumentationObjects.General;
using OsarResources.Generator;
using System.IO;
using OsarResources.XML;
using OsarResources.Generator.Resources;
using RteLib.RteResources;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class RteCalloutGenerator
  {
    RteConfig xmlCfg = new RteConfig();
    string pathToConfiguratioFile;
    string projectBaseFolder;
    string pathToModuleBaseFolder;
    private GenInfoType info = new GenInfoType();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    /// <param name="absPathToBaseModuleFolder"></param>
    public RteCalloutGenerator(string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();

      xmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToCfgFile);

      projectBaseFolder = Path.GetDirectoryName(pathToConfiguratioFile) + xmlCfg.PathToBaseProjectFolder;
    }

    /// <summary>
    /// Interface to generate the callout files
    /// </summary>
    /// <returns></returns>
    public GenInfoType GenerateRteCalloutFiles()
    {
      info.AddLogMsg("Start generation of the Rte Callout Stubs files.");

      generatingRteCalloutStubsHeaderFile();
      generatingRteCalloutStubsSourceFile();

      return info;
    }

    /**
    * @brief    Generate the needed RteCalloutStube Header File
    * @details  Rte Config Xml Data Base Config file used for RteInterface file generation
    * @retval   none
    */
    private void generatingRteCalloutStubsHeaderFile()
    {
      string fileName = GenResources.GenRteModuleCalloutStubsFileName + ".h";

      CHeaderFile rteGenModuleFile = new CHeaderFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileFunctionObjects genFncObject = new CFileFunctionObjects();
      CFileDefinitionObjects headerDefines = new CFileDefinitionObjects();
      CFileVariableObjects genVariable = new CFileVariableObjects();

      GeneralStartGroupObject groupMappingOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject groupMappingCloser = new GeneralEndGroupObject();

      /*++++++++++++++++++++++++++++++++++++++++ Create doxygen file header ++++++++++++++++++++++++++++++++++++++++*/
      doxygenFileHeader.AddFileBrief("Generated Rte Callout Stubs Header File: " + fileName);
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileHeader.AddFileName(fileName);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());

      doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupBSW);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);


      /* ++++++++++++++++++++++++++++++++++++++++ Process include objects ++++++++++++++++++++++++++++++++++++++++ */
      CFileIncludeObjects includes = new CFileIncludeObjects();
      includes.AddIncludeFile(GenResources.GenRteTypesSourceFileName);

      /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++ Callout Stubs Functions ++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenDescription.AddElementBrief("Rte Generated user defined startup initialization functions");
      doxygenDescription.AddElementNote("Add here user init functions");
      doxygenDescription.AddElementUUID(RteDefinedElementUUIDs.RteCalloutStubsStartupInitFunction);
      genFncObject.AddFunction("void", GenResources.GenRteCalloutStubsInitFunctionName, "void", doxygenDescription);


      doxygenDescription.AddElementBrief("Rte Generated user defined shutdown de-initialization functions");
      doxygenDescription.AddElementNote("Add here user de-initialization functions");
      doxygenDescription.AddElementUUID(RteDefinedElementUUIDs.RteCalloutStubsShutdownDeinitFunction);
      genFncObject.AddFunction("void", GenResources.GenRteCalloutStubsDeinitFunctionName, "void", doxygenDescription);

      /*++++++++++++++++++++++++++++++++++++++++ Generate File ++++++++++++++++++++++++++++++++++++++++*/
      rteGenModuleFile.AddFileCommentHeader(doxygenFileHeader);
      rteGenModuleFile.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      rteGenModuleFile.AddFileName(fileName);
      rteGenModuleFile.AddFilePath(pathToModuleBaseFolder + GenResources.GenHeaderFilePath);
      rteGenModuleFile.AddGlobalFunctionPrototypeObject(genFncObject);
      rteGenModuleFile.AddIncludeObject(includes);
      rteGenModuleFile.AddDefinitionObject(headerDefines);
      rteGenModuleFile.AddGlobalVariableObject(genVariable);
      rteGenModuleFile.GenerateSourceFile();
    }

    /**
    * @brief    Generate the needed RteCalloutStube Source File
    * @details  Rte Config Xml Data Base Config file used for RteInterface file generation
    * @retval   none
    */
    private void generatingRteCalloutStubsSourceFile()
    {
      string fileName = GenResources.GenRteModuleCalloutStubsFileName + ".c";

      CSourceFile rteGenFile = new CSourceFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileFunctionObjects genFncObject = new CFileFunctionObjects();
      CFileVariableObjects genVariable = new CFileVariableObjects();

      GeneralStartGroupObject groupMappingOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject groupMappingCloser = new GeneralEndGroupObject();
      CSourceFile rteParsedFile = null;
      string parsedFncContent = "";
      int groupIdx;


      /* Check if Rte generated module file dose exist */
      if (File.Exists(GenResources.GenSourceFilePath + "\\" + fileName))
      {
        info.AddLogMsg("Calloutstubs file dose exist >> Start parsing of calloutstubs file.");

        rteParsedFile = new CSourceFile();
        rteParsedFile.AddFileName(fileName);
        rteParsedFile.AddFilePath(GenResources.GenSourceFilePath);
        rteParsedFile.ParseSourceFile();

        /* ++++++++++++++++++++++++++++++++++++++++ Process user code sections ++++++++++++++++++++++++++++++++++++++++ */
        rteGenFile.AddUserFunctionSectionString(rteParsedFile.GetUserFunctionSectionString());
        rteGenFile.AddUserIncludeDefinionSectionString(rteParsedFile.GetUserIncludeDefinionSectionString());
      }


      /*++++++++++++++++++++++++++++++++++++++++ Create doxygen file header ++++++++++++++++++++++++++++++++++++++++*/
      doxygenFileHeader.AddFileBrief("Generated Rte Callout Stubs Source File: " + fileName);
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileHeader.AddFileName(fileName);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());

      doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupBSW);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);


      /* ++++++++++++++++++++++++++++++++++++++++ Process include objects ++++++++++++++++++++++++++++++++++++++++ */
      CFileIncludeObjects includes = new CFileIncludeObjects();
      includes.AddIncludeFile(GenResources.GenRteTypesSourceFileName);
      includes.AddIncludeFile(GenResources.GenRteModuleCalloutStubsFileName + ".h");


      /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++ Callout Stubs Functions ++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      groupMappingOpener.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CODE);
      groupMappingCloser.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CODE);
      groupIdx = genFncObject.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);


      /* Generate Startup Init Function */
      doxygenDescription.AddElementBrief("Rte Generated user defined startup initialization functions");
      doxygenDescription.AddElementNote("Add here user init functions");
      doxygenDescription.AddElementUUID(RteDefinedElementUUIDs.RteCalloutStubsStartupInitFunction);
      parsedFncContent = hlpGetFunctionContentFromParsedSourceFile(RteDefinedElementUUIDs.RteCalloutStubsStartupInitFunction, rteParsedFile);
      if ("" == parsedFncContent)
      {
        parsedFncContent += CFileResources.CFileGeneralAreaString + "\n";
        parsedFncContent += CFileResources.CSourceFileStartOfUserImplementationArea + "\n";
        parsedFncContent += CFileResources.CFileGeneralAreaString + "\n\n";
        parsedFncContent += CFileResources.CFileGeneralAreaString + "\n";
        parsedFncContent += CFileResources.CSourceFileStopOfUserImplementationArea + "\n";
        parsedFncContent += CFileResources.CFileGeneralAreaString;

      }
      genFncObject.AddFunction("void", GenResources.GenRteCalloutStubsInitFunctionName, "void", groupIdx, doxygenDescription, parsedFncContent);

      /* Generate Shutdown Deinit Function */
      doxygenDescription.AddElementBrief("Rte Generated user defined shutdown de-initialization functions");
      doxygenDescription.AddElementNote("Add here user de-initialization functions");
      doxygenDescription.AddElementUUID(RteDefinedElementUUIDs.RteCalloutStubsShutdownDeinitFunction);
      parsedFncContent = hlpGetFunctionContentFromParsedSourceFile(RteDefinedElementUUIDs.RteCalloutStubsShutdownDeinitFunction, rteParsedFile);
      if ("" == parsedFncContent)
      {
        parsedFncContent += CFileResources.CFileGeneralAreaString + "\n";
        parsedFncContent += CFileResources.CSourceFileStartOfUserImplementationArea + "\n";
        parsedFncContent += CFileResources.CFileGeneralAreaString + "\n\n";
        parsedFncContent += CFileResources.CFileGeneralAreaString + "\n";
        parsedFncContent += CFileResources.CSourceFileStopOfUserImplementationArea + "\n";
        parsedFncContent += CFileResources.CFileGeneralAreaString;

      }
      genFncObject.AddFunction("void", GenResources.GenRteCalloutStubsDeinitFunctionName, "void", groupIdx, doxygenDescription, parsedFncContent);

      /*++++++++++++++++++++++++++++++++++++++++ Generate File ++++++++++++++++++++++++++++++++++++++++*/
      rteGenFile.AddUserCodeSections();
      rteGenFile.AddFileCommentHeader(doxygenFileHeader);
      rteGenFile.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      rteGenFile.AddFileName(fileName);
      rteGenFile.AddFilePath(pathToModuleBaseFolder + GenResources.GenSourceFilePath);
      rteGenFile.AddIncludeObject(includes);
      rteGenFile.AddGlobalVariableObject(genVariable);
      rteGenFile.AddGlobalFunctionObject(genFncObject);
      rteGenFile.GenerateSourceFile();
    }

    /**
    * @brief    Gelper function to search for existing UUIDs in parsed file
    * @retval   string >> Parsed function content
    *           "" == If function could not be found
    */
    private string hlpGetFunctionContentFromParsedSourceFile(string UUID, CSourceFile rteParsedFile)
    {
      string parsedFncContent = "";
      DoxygenElementDescription parsedDoxygenDescription = new DoxygenElementDescription();
      List<objectsWithGroups> parsedGroupedFunctObjects;
      List<singleObject> parsedSingeFunctObjects;
      CFileFunctionObjects parsedFncObj;
      bool functionFound = false;

      if (null != rteParsedFile)
      {
        /* ++++++++++ Source file available >> Check data ++++++++++*/
        parsedFncObj = rteParsedFile.GetGlobalFunctionObject();

        /* Check single function objects */
        if (null != parsedFncObj)
        {
          parsedSingeFunctObjects = parsedFncObj.GetSingleObjects();

          foreach (singleObject fnc in parsedSingeFunctObjects)
          {
            /* Check for documentation object */
            if (true == parsedDoxygenDescription.IsParseStringStartObject(fnc.objectDesription))
            {
              parsedDoxygenDescription.ParseString(fnc.objectDesription);
              /* Check UUID */
              if (parsedDoxygenDescription.GetElementUUID() == UUID)
              {
                /* Element found in parsed file */
                functionFound = true;
                parsedFncContent = fnc.objectString[4];
                break;
              }
            }
          }
        }

        /* Check group function */
        if (( null != parsedFncObj ) && ( false == functionFound ))
        {
          parsedGroupedFunctObjects = parsedFncObj.GetGroupedObjects();

          foreach (objectsWithGroups group in parsedGroupedFunctObjects)
          {
            foreach (singleObject fnc in group.objects)
            {
              /* Check for documentation object */
              if (true == parsedDoxygenDescription.IsParseStringStartObject(fnc.objectDesription))
              {
                parsedDoxygenDescription.ParseString(fnc.objectDesription);
                /* Check UUID */
                if (parsedDoxygenDescription.GetElementUUID() == UUID)
                {
                  /* Element found in parsed file */
                  functionFound = true;
                  parsedFncContent = fnc.objectString[4];
                  break;
                }
              }
            }

            if (true == functionFound)
            {
              break;
            }
          }
        }
      }

      return parsedFncContent;
    }
  }
}
/**
 * @}
 */
