﻿/*****************************************************************************************************************************
 * @file        RteMibGenerator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.01.2020                                                                                                   *
 * @brief       Implementation of the rte module internal behavior file generator                                            *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RteLib.RteConfig;
using RteLib.RteTypes;
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.DocumentationObjects.General;
using OsarResources.Generator;
using System.IO;
using OsarResources.XML;
using OsarResources.Generator.Resources;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class RteMibGenerator
  {
    RteConfig rteXmlCfg = new RteConfig();
    RteInterfaces xmlInterface = new RteInterfaces();
    string pathToConfiguratioFile;
    string pathToModuleBaseFolder;
    string projectBaseFolder;
    private GenInfoType info = new GenInfoType();
    List<RteModuleInternalBehavior> rteModuleInternalBehaviorList = new List<RteModuleInternalBehavior>();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    /// <param name="absPathToBaseModuleFolder"> Absolute path to module base folder</param>
    public RteMibGenerator(string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();

      rteXmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToCfgFile);

      projectBaseFolder = Path.GetDirectoryName(pathToConfiguratioFile) + rteXmlCfg.PathToBaseProjectFolder;

      xmlInterface.ReadActiveRteInterfaceDataBaseFromXml(Path.GetDirectoryName(pathToConfiguratioFile) + rteXmlCfg.PathToRteInterfaceFile);
    }

    /// <summary>
    /// Interface for attribute rteModuleInternalBehaviorList
    /// </summary>
    public List<RteModuleInternalBehavior> RteModuleInternalBehaviorList
    {
      get => rteModuleInternalBehaviorList;
    }

    /// <summary>
    /// Interface to generate the "Module Internal Behavior" specific Rte and Module files
    /// </summary>
    /// <returns></returns>
    public GenInfoType GenerateRteMibModuleFiles()
    {
      info = new GenInfoType();

      // Setup list if necessary
      if (0 == rteModuleInternalBehaviorList.Count())
      {
        SetupUsedMibModuleList();
      }

      // Process all configured modules
      foreach (RteModuleInternalBehavior processModule in rteModuleInternalBehaviorList)
      {
        RteConfigUsedModuleCfg module = new RteConfigUsedModuleCfg();
        module.ModuleName = processModule.ModuleName;
        module.ModuleType = processModule.ModuleType;

        /* Generate module "<ModuleName>.c" from internal behavior file */
        generatingRteMibInterfaceFiles(module, processModule);

        /* Generate module interface header file "Rte_<ModuleName>.h" from internal behavior file */
        generatringRteInterfaceHeaderFile(module, processModule);

      }

      return info;
    }

    /// <summary>
    /// Interface to setup the module internal behavior list
    /// </summary>
    /// <returns></returns>
    public GenInfoType SetupUsedMibModuleList()
    {
      RteModuleInternalBehavior processModule;

      // Process all configured modules
      foreach (RteConfigUsedModuleCfg module in rteXmlCfg.RteUsedModules)
      {
        processModule = new RteModuleInternalBehavior();

        /* Check if module dose exist */
        
        if (!Directory.Exists(projectBaseFolder + module.ModuleType + "\\" + module.ModuleName))
        {
          string msg = module.ModuleType.ToString() + " >> " + module.ModuleName + " Skipp processing of this module.";
          info.AddWaringMsg(Convert.ToUInt16(GenErrorWarningCodes.Warning_4000), GenErrorWarningCodes.Warning_4000_Msg + msg);
        }
        else
        {
          /* Parse module internal behavior file */
          string pathToMibFile = projectBaseFolder + module.ModuleType + "\\" + module.ModuleName + "\\" + 
            GenResources.GenRteModuleInternalBehaviorFilePath + "\\" +
            GenResources.GenRteModuleInternalBehaviorFileName;
          processModule.ReadActiveRteModuleInternalBehaviorToXml(pathToMibFile);
          //parsingXmlRteModuleInternalBehaviorFile(module, out processModule);

          /* Check if parsing has been finished successful */
          if (null != processModule)
          {
            /* Validate module internal behavior file */
            validateRteInterfaceFile(module, ref processModule);
          }

          /* Check if validation has been finished successful */
          if (null != processModule)
          {
            /* Add module internal behavior file to general available modules list */
            rteModuleInternalBehaviorList.Add(processModule);
          }
        }
      }

      return info;
    }

    /**
      * @brief          Validate the Rte module internal behavior file
      * @param[in/out]  RteConfig Module usage information
      * @retval         none
      */
    private void validateRteInterfaceFile(RteConfigUsedModuleCfg module, ref RteModuleInternalBehavior moduleInternalBehaviour)
    {
      info.AddLogMsg("Start check of module internal behavior file compatibility of module: " + module.ModuleType + "\\" + module.ModuleName);

      /* Validate configuration file version */
      XmlFileVersion defaultVersion = new XmlFileVersion();
      defaultVersion.MajorVersion = Convert.ToUInt16(DefResources.RteMibFileMajorVersion);
      defaultVersion.MinorVersion = Convert.ToUInt16(DefResources.RteMibFileMinorVersion);
      defaultVersion.PatchVersion = Convert.ToUInt16(DefResources.RteMibFilePatchVersion);
      if (false == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(defaultVersion, moduleInternalBehaviour.XmlFileVersion))
      {
        info.AddErrorMsg(Convert.ToUInt16(ErrorWarningCodes.Error_0000), ErrorWarningCodes.Error_0000_Msg);
        moduleInternalBehaviour = null;
      }

      info.AddLogMsg("Module internal behavior file of module: " + module.ModuleType + "\\" + module.ModuleName + " compatible!");
    }


    /**
    * @brief      Generate data from the Rte module internal behavior file >> Module Source file
    * @param[in]  RteConfig Module usage information
    * @param[out] RteModuleInternalBehavior parsed module internal behavior file
    * @details    Xml Config file based on RteModuleInternalBehavior class
    * @retval     none
    */
    private void generatingRteMibInterfaceFiles(RteConfigUsedModuleCfg module, RteModuleInternalBehavior moduleInternalBehaviour)
    {
      info.AddLogMsg("Start module interface files generation form MiB config for module: " + module.ModuleType + "\\" + module.ModuleName);
      string pathToModuleSourceFile = projectBaseFolder + module.ModuleType + "\\" + module.ModuleName + "\\" +
                                      GenResources.GenRteGeneratorModuleGenSrcFilePath + "\\";
      string pathToModuleHeaderFile = projectBaseFolder + module.ModuleType + "\\" + module.ModuleName + "\\" +
                                      GenResources.GenRteGeneratorModuleGenIncFilePath + "\\";
      string fileName = moduleInternalBehaviour.ModuleName + ".c";
      string fileHeaderName = DefResources.ModuleName + "_" + moduleInternalBehaviour.ModuleName + ".h";

      CSourceFile rteParsedModuleFile = null;
      CSourceFile rteGenModuleFile = new CSourceFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileFunctionObjects genFncObject = new CFileFunctionObjects();
      CFileFunctionObjects parsedFncObj;
      GeneralStartGroupObject groupMappingOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject groupMappingCloser = new GeneralEndGroupObject();
      General newGeneral;

      string parsedFncContent = "";
      int groupIndex;
      string functionName;

      /* Create mem-map groups */
      groupMappingOpener.AddGroupName("Map functions into code memory");
      groupMappingOpener.AddOsarStartMemMapDefine(moduleInternalBehaviour.ModuleName, OsarMemMapDataType.OSAR_CODE);
      groupMappingCloser.AddOsarStopMemMapDefine(moduleInternalBehaviour.ModuleName, OsarMemMapDataType.OSAR_CODE);
      groupIndex = genFncObject.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);

      /* Check if Rte generated module file dose exist */
      if (File.Exists(pathToModuleSourceFile + fileName))
      {
        rteParsedModuleFile = new CSourceFile();
        rteParsedModuleFile.AddFileName(fileName);
        rteParsedModuleFile.AddFilePath(pathToModuleSourceFile);
        rteParsedModuleFile.ParseSourceFile();

        /* ++++++++++++++++++++++++++++++++++++++++ Process user code sections ++++++++++++++++++++++++++++++++++++++++ */
        rteGenModuleFile.AddUserFunctionSectionString(rteParsedModuleFile.GetUserFunctionSectionString());
        rteGenModuleFile.AddUserIncludeDefinionSectionString(rteParsedModuleFile.GetUserIncludeDefinionSectionString());
      }

      /* ++++++++++++++++++++++++++++++++++++++++ Generate new Rte Module File ++++++++++++++++++++++++++++++++++++++++*/
      /*++++++++++++++++++++++++++++++++++++++++ Create doxygen file header ++++++++++++++++++++++++++++++++++++++++*/
      doxygenFileHeader.AddFileBrief("Generated Rte Module Interface file: " + fileName);
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileHeader.AddFileName(fileName);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());

      switch (moduleInternalBehaviour.ModuleType)
      {
        case RteConfigModuleTypes.BSW:
        doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupBSW);
        break;

        case RteConfigModuleTypes.CD:
        doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupCD);
        break;

        case RteConfigModuleTypes.CDD:
        doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupCDD);
        break;

        case RteConfigModuleTypes.MCAL:
        doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupMCAL_STM32F4Cube);
        break;

        case RteConfigModuleTypes.SWC:
        doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupSWC);
        break;

        default:
        throw new ArgumentOutOfRangeException();
      }
      doxygenFileGroupOpener.AddToDoxygenGroup(module.ModuleName);

      /*++++++++++++++++++++++++++++++++++++++++ Process all init runnables  ++++++++++++++++++++++++++++++++++++++++*/
      foreach (RteRunnable initRunnable in moduleInternalBehaviour.InitRunnables)
      {
        doxygenDescription = new DoxygenElementDescription();
        parsedFncContent = "";

        DoxygenElementDescription parsedDoxygenDescription = new DoxygenElementDescription();
        CFileFunctionObjects parsedFunctionObject = new CFileFunctionObjects();
        List<objectsWithGroups> parsedGroupedFunctObjects;
        bool functionFound = false;

        if (null != rteParsedModuleFile)
        {
          /* ++++++++++ Source file available >> Check data ++++++++++*/
          parsedFncObj = rteParsedModuleFile.GetGlobalFunctionObject();

          //Check for existing uuid
          parsedGroupedFunctObjects = parsedFncObj.GetGroupedObjects();
          foreach (objectsWithGroups group in parsedGroupedFunctObjects)
          {
            foreach (singleObject fnc in group.objects)
            {
              /* Check for documentation object */
              if (true == parsedDoxygenDescription.IsParseStringStartObject(fnc.objectDesription))
              {
                parsedDoxygenDescription.ParseString(fnc.objectDesription);
                /* Check UUID */
                if (parsedDoxygenDescription.GetElementUUID() == initRunnable.RunnableInfo.UUID)
                {
                  /* Element found in parsed file */
                  functionFound = true;
                  parsedFncContent = fnc.objectString[4];
                  break;
                }
              }
            }

            if (true == functionFound)
            {
              break;
            }
          }
        }
        if (false == functionFound)
        {
          //Add dummy file content
          parsedFncContent = "";

          parsedFncContent += CFileResources.CFileFunctionInternalGeneralAreaString + "\n";
          parsedFncContent += CFileResources.CSourceFileStartOfUserImplementationArea + "\n";
          parsedFncContent += CFileResources.CFileFunctionInternalGeneralAreaString + "\n\n";
          parsedFncContent += CFileResources.CFileFunctionInternalGeneralAreaString + "\n";
          parsedFncContent += CFileResources.CSourceFileStopOfUserImplementationArea + "\n";
          parsedFncContent += CFileResources.CFileFunctionInternalGeneralAreaString;
        }

        // Setup Doxygen function description
        doxygenDescription.AddElementBrief("Rte generated module initialization function.");
        //TODO Add accessible Port description

        newGeneral = new General(pathToConfiguratioFile);
        string doxString = newGeneral.hlpGenerateFunctionInterfaceInfoString(initRunnable, moduleInternalBehaviour);
        info = OsarGenHelper.MergeGenInfoType(info, newGeneral.GenInfo);

        doxygenDescription.AddElementDetails(doxString);
        doxygenDescription.AddElementNote("Trigger: System Init");
        doxygenDescription.AddElementUUID(initRunnable.RunnableInfo.UUID);
        doxygenDescription.AddElementReturnValues("None");
        doxygenDescription.AddElementParameter("None", CParameterType.INPUT);

        if (( "" == initRunnable.RunnableInfo.InterfaceName ) || ( null == initRunnable.RunnableInfo.InterfaceName ))
        {
          functionName = moduleInternalBehaviour.ModuleName + GenResources.GenRteGeneratorModuleInitFncPrefix;
        }
        else
        {
          functionName = moduleInternalBehaviour.ModuleName + GenResources.GenRteGeneratorModuleInitFncPrefix + "_" + initRunnable.RunnableInfo.InterfaceName;
        }
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
                     functionName, RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
                     groupIndex, doxygenDescription, parsedFncContent);

      }


      /*++++++++++++++++++++++++++++++++++++++++ Process all cyclic runnables ++++++++++++++++++++++++++++++++++++++++*/
      foreach (RteCyclicRunnable cyclicRunnable in moduleInternalBehaviour.CyclicRunnables)
      {
        doxygenDescription = new DoxygenElementDescription();
        parsedFncContent = "";

        DoxygenElementDescription parsedDoxygenDescription = new DoxygenElementDescription();
        CFileFunctionObjects parsedFunctionObject = new CFileFunctionObjects();
        List<objectsWithGroups> parsedGroupedFunctObjects;
        bool functionFound = false;

        if (null != rteParsedModuleFile)
        {
          /* ++++++++++ Source file available >> Check data ++++++++++*/
          parsedFncObj = rteParsedModuleFile.GetGlobalFunctionObject();

          //Check for existing UUID
          parsedGroupedFunctObjects = parsedFncObj.GetGroupedObjects();
          foreach (objectsWithGroups group in parsedGroupedFunctObjects)
          {
            foreach (singleObject fnc in group.objects)
            {
              /* Check for documentation object */
              if (true == parsedDoxygenDescription.IsParseStringStartObject(fnc.objectDesription))
              {
                parsedDoxygenDescription.ParseString(fnc.objectDesription);
                /* Check UUID */
                if (parsedDoxygenDescription.GetElementUUID() == cyclicRunnable.Runnable.RunnableInfo.UUID)
                {
                  /* Element found in parsed file */
                  functionFound = true;
                  parsedFncContent = fnc.objectString[4];
                  break;
                }
              }
            }

            if (true == functionFound)
            {
              break;
            }
          }
        }
        if (false == functionFound)
        {
          //Add dummy file content
          parsedFncContent = "";

          parsedFncContent += CFileResources.CFileFunctionInternalGeneralAreaString + "\n";
          parsedFncContent += CFileResources.CSourceFileStartOfUserImplementationArea + "\n";
          parsedFncContent += CFileResources.CFileFunctionInternalGeneralAreaString + "\n\n";
          parsedFncContent += CFileResources.CFileFunctionInternalGeneralAreaString + "\n";
          parsedFncContent += CFileResources.CSourceFileStopOfUserImplementationArea + "\n";
          parsedFncContent += CFileResources.CFileFunctionInternalGeneralAreaString;
        }

        // Setup Doxygen function description
        doxygenDescription.AddElementBrief("Rte generated module cyclic function.");
        //TODO Add accessible Port description

        newGeneral = new General(pathToConfiguratioFile);
        string doxString = newGeneral.hlpGenerateFunctionInterfaceInfoString(cyclicRunnable.Runnable, moduleInternalBehaviour);
        info = OsarGenHelper.MergeGenInfoType(info, newGeneral.GenInfo);

        doxygenDescription.AddElementDetails(doxString);
        doxygenDescription.AddElementNote("Trigger: Cyclic " + cyclicRunnable.CycleTime.ToString() + "ms");
        doxygenDescription.AddElementUUID(cyclicRunnable.Runnable.RunnableInfo.UUID);
        doxygenDescription.AddElementReturnValues("None");
        doxygenDescription.AddElementParameter("None", CParameterType.INPUT);
        functionName = moduleInternalBehaviour.ModuleName + "_" + cyclicRunnable.Runnable.RunnableInfo.InterfaceName;
        genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
                     functionName, RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
                     groupIndex, doxygenDescription, parsedFncContent);
      }


      /* ++++++++++++++++++++++++++++++++++++++++ Process all server runnables ++++++++++++++++++++++++++++++++++++++++ */
      foreach (RteAvailableServerPort serverPort in moduleInternalBehaviour.AvailableServerPorts)
      {
        foreach (RteRunnable serverRunnable in serverPort.ServerPortRunnables)
        {
          doxygenDescription = new DoxygenElementDescription();
          parsedFncContent = "";

          DoxygenElementDescription parsedDoxygenDescription = new DoxygenElementDescription();
          CFileFunctionObjects parsedFunctionObject = new CFileFunctionObjects();
          List<objectsWithGroups> parsedGroupedFunctObjects;
          bool functionFound = false;

          if (null != rteParsedModuleFile)
          {
            //bool functionFound = false;
            /* ++++++++++ Source file available >> Check data ++++++++++*/
            parsedFncObj = rteParsedModuleFile.GetGlobalFunctionObject();

            //Check for existing UUID
            parsedGroupedFunctObjects = parsedFncObj.GetGroupedObjects();
            foreach (objectsWithGroups group in parsedGroupedFunctObjects)
            {
              foreach (singleObject fnc in group.objects)
              {
                /* Check for documentation object */
                if (true == parsedDoxygenDescription.IsParseStringStartObject(fnc.objectDesription))
                {
                  parsedDoxygenDescription.ParseString(fnc.objectDesription);
                  /* Check UUID */
                  if (parsedDoxygenDescription.GetElementUUID() == serverRunnable.RunnableInfo.UUID)
                  {
                    /* Element found in parsed file */
                    functionFound = true;
                    parsedFncContent = fnc.objectString[4];
                    break;
                  }
                }
              }

              if (true == functionFound)
              {
                break;
              }
            }
          }
          if (false == functionFound)
          {
            //Add dummy file content
            parsedFncContent = "";

            parsedFncContent += CFileResources.CFileFunctionInternalGeneralAreaString + "\n";
            parsedFncContent += CFileResources.CSourceFileStartOfUserImplementationArea + "\n";
            parsedFncContent += CFileResources.CFileFunctionInternalGeneralAreaString + "\n\n";
            parsedFncContent += CFileResources.CFileFunctionInternalGeneralAreaString + "\n";
            parsedFncContent += CFileResources.CSourceFileStopOfUserImplementationArea + "\n";
            parsedFncContent += CFileResources.CFileFunctionInternalGeneralAreaString;
          }

          // Setup Doxygen function description
          doxygenDescription.AddElementBrief("Rte generated module Server Port Function.");

          newGeneral = new General(pathToConfiguratioFile);
          string doxString = newGeneral.hlpGenerateFunctionInterfaceInfoString(serverRunnable, moduleInternalBehaviour);
          info = OsarGenHelper.MergeGenInfoType(info, newGeneral.GenInfo);

          doxygenDescription.AddElementDetails(doxString);
          doxygenDescription.AddElementNote("Trigger: Operation Invocation");
          doxygenDescription.AddElementUUID(serverRunnable.RunnableInfo.UUID);

          try
          {
            newGeneral = new General(pathToConfiguratioFile);
            

            doxygenDescription.AddElementReturnValues(newGeneral.hlpGenerateFncRetVal(serverRunnable.RunnableInfo.InterfaceName) + " >> Available Error Types:\n" +
              newGeneral.hlpGenerateClientServerPortErrorTypeList(serverRunnable.RunnableInfo.InterfaceName, serverPort.ServerPort));

            newGeneral.hlpGenerateServerPortArgList(serverRunnable.RunnableInfo.InterfaceName, ref doxygenDescription);

            functionName = GenResources.GenRteServerFncInterfaceBaseString + serverPort.ServerPort.InterfacePrototype.InterfaceName + "_" + serverRunnable.RunnableInfo.InterfaceName;
            genFncObject.AddFunction(newGeneral.hlpGenerateFncRetVal(serverRunnable.RunnableInfo.InterfaceName),
                         functionName, newGeneral.hlpGenerateFncArgList(serverRunnable.RunnableInfo.InterfaceName), groupIndex, doxygenDescription, parsedFncContent);
          }
          finally
          {
            info = OsarGenHelper.MergeGenInfoType(info, newGeneral.GenInfo);
          }
        }
      }

      /*++++++++++++++++++++++++++++++++++++++++ Generate mem-map file ++++++++++++++++++++++++++++++++++++++++*/
      /* +++++ Generate Memory Mapping file +++++ */
      // TODO Check if this is needed 
      //OsarResources.Generic.OsarGenericHelper.generateModuleMemoryMappingFile(pathToModuleHeaderFile, moduleInternalBehaviour.ModuleName, ModuleResources.RteGeneratorName);

      /* ++++++++++++++++++++++++++++++++++++++++ Process include objects ++++++++++++++++++++++++++++++++++++++++ */
      CFileIncludeObjects genIncludes = new CFileIncludeObjects();
      genIncludes.AddIncludeFile(GenResources.GenRteTypesSourceFileName);
      genIncludes.AddIncludeFile(fileHeaderName);

      /*++++++++++++++++++++++++++++++++++++++++ Generate File ++++++++++++++++++++++++++++++++++++++++*/
      rteGenModuleFile.AddFileCommentHeader(doxygenFileHeader);
      rteGenModuleFile.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      rteGenModuleFile.AddFileName(fileName);
      rteGenModuleFile.AddFilePath(pathToModuleSourceFile);
      rteGenModuleFile.AddIncludeObject(genIncludes);
      rteGenModuleFile.AddGlobalFunctionObject(genFncObject);
      rteGenModuleFile.AddUserCodeSections();
      rteGenModuleFile.GenerateSourceFile();

      info.AddLogMsg("Module interface files generated for module: " + module.ModuleType + "\\" + module.ModuleName);
    }

    /**
      * @brief      Generate data from the Rte module internal behavior file >> Module Header file
      * @param[in]  RteConfig Module usage information
      * @param[out] RteModuleInternalBehavior parsed module internal behavior file
      * @details    Xml Config file based on RteModuleInternalBehavior class
      * @retval     none
      */
    private void generatringRteInterfaceHeaderFile(RteConfigUsedModuleCfg module, RteModuleInternalBehavior moduleInternalBehaviour)
    {
      info.AddLogMsg("Start rte module header file generation form MiB config for module: " + module.ModuleType + "\\" + module.ModuleName);

      string fileName = DefResources.ModuleName + "_" + moduleInternalBehaviour.ModuleName + ".h";

      CHeaderFile rteGenModuleFile = new CHeaderFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileFunctionObjects genFncObject = new CFileFunctionObjects();
      CFileDefinitionObjects headerDefines = new CFileDefinitionObjects();

      GeneralStartGroupObject groupMappingOpener = new GeneralStartGroupObject();
      GeneralEndGroupObject groupMappingCloser = new GeneralEndGroupObject();

      int groupIndexInitRunnable, groupIndexCyclicRunnable, groupIndexServerRunnable, groupIndexClientRunnable, groupIndexSRRunnable;
      int groupIndexDefineServerRunnableError, groupIndexDefineClientRunnableError;
      string functionName;

      General newGeneral = new General(pathToConfiguratioFile);

      try
      {
        /*++++++++++++++++++++++++++++++++++++++++ Create doxygen file header ++++++++++++++++++++++++++++++++++++++++*/
        doxygenFileHeader.AddFileBrief("Generated Rte Module Interface Header File: " + fileName);
        doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
        doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
        doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
        doxygenFileHeader.AddFileName(fileName);
        doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
        doxygenFileGroupOpener.AddToDoxygenMasterGroup(genericSource.DoxygenOsarMasterGroupBSW);
        doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

        /* ++++++++++++++++++++++++++++++++++++++++ Process include objects ++++++++++++++++++++++++++++++++++++++++ */
        CFileIncludeObjects includes = new CFileIncludeObjects();
        includes.AddIncludeFile(GenResources.GenRteTypesSourceFileName);
        includes.AddIncludeFile(GenResources.GenRteInteractionLayerFileName + ".h");

        /*++++++++++++++++++++++++++++++++++++++++ Process all init runnables  ++++++++++++++++++++++++++++++++++++++++*/
        groupMappingOpener.AddCustomStartGroupString(GenResources.GenRteModuleIfHeader_IncludeGroupStringInitRunnable);
        groupIndexInitRunnable = genFncObject.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);

        foreach (RteRunnable initRunnable in moduleInternalBehaviour.InitRunnables)
        {
          /* Create Init Runnable Function Prototypes */
          if (( "" == initRunnable.RunnableInfo.InterfaceName ) || ( null == initRunnable.RunnableInfo.InterfaceName ))
          {
            functionName = moduleInternalBehaviour.ModuleName + GenResources.GenRteGeneratorModuleInitFncPrefix;
          }
          else
          {
            functionName = moduleInternalBehaviour.ModuleName + GenResources.GenRteGeneratorModuleInitFncPrefix + "_" + initRunnable.RunnableInfo.InterfaceName;
          }

          genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
                       functionName, RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), groupIndexInitRunnable);
        }

        /*++++++++++++++++++++++++++++++++++++++++ Process all cyclic runnables  ++++++++++++++++++++++++++++++++++++++++*/
        groupMappingOpener.AddCustomStartGroupString(GenResources.GenRteModuleIfHeader_IncludeGroupStringCyclicRunnable);
        groupIndexCyclicRunnable = genFncObject.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);

        foreach (RteCyclicRunnable cyclicRunnable in moduleInternalBehaviour.CyclicRunnables)
        {
          /* Create Cyclic Runnable Function Prototypes */
          functionName = moduleInternalBehaviour.ModuleName + "_" + cyclicRunnable.Runnable.RunnableInfo.InterfaceName;
          genFncObject.AddFunction(RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD),
                       functionName, RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD), groupIndexCyclicRunnable);
        }

        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++ Process all server runnables ++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        groupMappingOpener.AddCustomStartGroupString(GenResources.GenRteModuleIfHeader_IncludeGroupStringServerPortRunnable);
        groupIndexServerRunnable = genFncObject.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);

        groupMappingOpener.AddCustomStartGroupString(GenResources.GenRteModuleIfHeader_DefinitionGroupStringServerPortErrors);
        groupIndexDefineServerRunnableError = headerDefines.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);

        foreach (RteAvailableServerPort serverPort in moduleInternalBehaviour.AvailableServerPorts)
        {
          foreach (RteRunnable serverRunnable in serverPort.ServerPortRunnables)
          {
            /* Create Server Port Runnable Function Prototypes */
            functionName = GenResources.GenRteServerFncInterfaceBaseString + serverPort.ServerPort.InterfacePrototype.InterfaceName + "_" + serverRunnable.RunnableInfo.InterfaceName;
            genFncObject.AddFunction(newGeneral.hlpGenerateFncRetVal(serverRunnable.RunnableInfo.InterfaceName),
                         functionName, newGeneral.hlpGenerateFncArgList(serverRunnable.RunnableInfo.InterfaceName), groupIndexServerRunnable);

            /* Create Server Port Error Information */
            hlpClientServerErrorTypeDefineGeneration(serverRunnable.RunnableInfo.InterfaceName, groupIndexDefineServerRunnableError, serverPort.ServerPort, ref headerDefines);
          }
        }

        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++ Process all client port +++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        groupMappingOpener.AddCustomStartGroupString(GenResources.GenRteModuleIfHeader_IncludeGroupStringClientPortRunnable);
        groupIndexClientRunnable = genFncObject.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);

        groupMappingOpener.AddCustomStartGroupString(GenResources.GenRteModuleIfHeader_DefinitionGroupStringServerPortErrors);
        groupIndexDefineClientRunnableError = headerDefines.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);

        foreach (RteAvailableClientPort clientPort in moduleInternalBehaviour.AvailableClientPorts)
        {
          foreach (RteGenericRunnableObject clientFunction in clientPort.ClientPortFunctions)
          {
            /* Create Client Port Function Prototypes */
            functionName = GenResources.GenRteClientFncInterfaceBaseString + clientPort.ClientPort.InterfacePrototype.InterfaceName + "_" +
                           clientFunction.Name;
            genFncObject.AddFunction("extern " + newGeneral.hlpGenerateFncRetVal(clientFunction.Name),
                         functionName, newGeneral.hlpGenerateFncArgList(clientFunction.Name), groupIndexClientRunnable);

            /* Create Client Port Error Information */
            hlpClientServerErrorTypeDefineGeneration(clientFunction.Name, groupIndexDefineClientRunnableError, clientPort.ClientPort, ref headerDefines);
          }
        }

        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++ Process all sender receiver port ++++++++++++++++++++++++++++++++++++++ */
        /* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
        groupMappingOpener.AddCustomStartGroupString(GenResources.GenRteModuleIfHeader_IncludeGroupStringSenderReceiverPortRunnable);
        groupIndexSRRunnable = genFncObject.AddCFileObjectGroup(groupMappingOpener, groupMappingCloser);

        /*++++++++++++++++++++++++++++++++++++++++ Generate File ++++++++++++++++++++++++++++++++++++++++*/
        rteGenModuleFile.AddFileCommentHeader(doxygenFileHeader);
        rteGenModuleFile.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
        rteGenModuleFile.AddFileName(fileName);
        rteGenModuleFile.AddFilePath(pathToModuleBaseFolder + GenResources.GenHeaderFilePath);
        rteGenModuleFile.AddGlobalFunctionPrototypeObject(genFncObject);
        rteGenModuleFile.AddIncludeObject(includes);
        rteGenModuleFile.AddDefinitionObject(headerDefines);
        rteGenModuleFile.GenerateSourceFile();

        info.AddLogMsg("Rte module header file generated for module: " + module.ModuleType + "\\" + module.ModuleName);
      }
      catch
      {
        // If error occurred >> Store error information
        info = OsarGenHelper.MergeGenInfoType(info, newGeneral.GenInfo);
      }
    }

    /// <summary>
    /// Helper function to generate the error information of the client server connection
    /// </summary>
    /// <param name="interfaceFncName"></param>
    /// <param name="groupIdx"></param>
    /// <param name="interfacePrototype"></param>
    /// <param name="definitonObj"></param>
    private void hlpClientServerErrorTypeDefineGeneration(string interfaceFncName, int groupIdx, RteCSInterfacePrototype interfacePrototype, ref CFileDefinitionObjects definitonObj)
    {
      string errorTypeName = "";
      bool elementFound = false;
      int idx = Convert.ToInt32(GenResources.GenRteErrorIdGenerationOffset);

      /* Check Client Server Interface */
      foreach (RteCSInterfaceBlueprint interfaceBlueprint in xmlInterface.ClientServerBlueprintList)
      {
        foreach (RteFunctionBlueprint interfaceFnc in interfaceBlueprint.Functions)
        {
          //if (interfaceFncUUID == interfaceFnc.UUID)
          // Do not check for UUID >> Prototype and Blueprint UUIDs are differed >> Check for FunctionName
          if (interfaceFncName == interfaceFnc.FunctionName)
          {
            foreach (string errorType in interfaceFnc.AvailableErrorTypes)
            {
              errorTypeName = GenResources.GenRteErrorTypeBaseString + interfacePrototype.InterfacePrototype.InterfaceName +
                "_" + interfaceFnc.FunctionName + "_" + errorType;

              definitonObj.AddDefinition(errorTypeName, idx.ToString(), groupIdx);
              idx++;

              elementFound = true;
            }
            break;
          }
        }

        if (true == elementFound)
        {
          break;
        }
      }

      if (false == elementFound)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_5001), GenErrorWarningCodes.Error_5001_Msg + interfaceFncName);
      }
    }
  }
}
/**
 * @}
 */
