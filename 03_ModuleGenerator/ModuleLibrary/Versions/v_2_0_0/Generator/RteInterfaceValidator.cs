﻿/*****************************************************************************************************************************
 * @file        RteInterfaceValidator.cs                                                                                     *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.01.2020                                                                                                   *
 * @brief       Implementation of the rte interface file validator                                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generator.Resources;
using OsarResources.Generic;
using OsarResources.XML;
using RteLib.RteConfig;
using RteLib.RteInterface;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class RteInterfaceValidator
  {
    RteConfig xmlCfg = new RteConfig();
    RteInterfaces rteXmlInterface = new RteInterfaces();
    string pathToConfiguratioFile;
    private GenInfoType info = new GenInfoType();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    public RteInterfaceValidator(string pathToCfgFile)
    {
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();

      xmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToCfgFile);
      string pathToRteInterfaceFile = Path.GetDirectoryName(pathToConfiguratioFile) + xmlCfg.PathToRteInterfaceFile;
      rteXmlInterface.ReadActiveRteInterfaceDataBaseFromXml(pathToRteInterfaceFile);
    }

    /// <summary>
    /// Interface to validate the rte interface configuration file
    /// </summary>
    /// <return> GenInfoType </return>
    public GenInfoType ValidateRteInterfaceConfiguration()
    {
      info.AddLogMsg(ValidateResources.LogMsg_StartOfRteTypesValidation);

      /* Validate configuration file version */
      XmlFileVersion defaultVersion = new XmlFileVersion();
      defaultVersion.MajorVersion = Convert.ToUInt16(DefResources.RteInterfaceFileMajorVersion);
      defaultVersion.MinorVersion = Convert.ToUInt16(DefResources.RteInterfaceFileMinorVersion);
      defaultVersion.PatchVersion = Convert.ToUInt16(DefResources.RteInterfaceFilePatchVersion);
      if (false == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(defaultVersion, rteXmlInterface.XmlFileVersion))
      {
        info.AddErrorMsg(Convert.ToUInt16(ErrorWarningCodes.Error_0000), ErrorWarningCodes.Error_0000_Msg);
      }


      // Add further validation !!!

      info.AddLogMsg(ValidateResources.LogMsg_RteTypesConfigValidationDone);

      return info;
    }
  }
}
/**
 * @}
 */
