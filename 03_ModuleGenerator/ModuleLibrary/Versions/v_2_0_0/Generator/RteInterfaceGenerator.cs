﻿/*****************************************************************************************************************************
 * @file        RteInterfaceGenerator.cs                                                                                     *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the rte interface file generator                                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generator.Resources;
using OsarResources.Generic;
using OsarResources.XML;
using RteLib.RteConfig;
using RteLib.RteInterface;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class RteInterfaceGenerator
  {
    RteConfig xmlCfg = new RteConfig();
    RteInterfaces rteXmlInterface = new RteInterfaces();
    string pathToConfiguratioFile;
    string pathToModuleBaseFolder;
    private GenInfoType info = new GenInfoType();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    /// <param name="absPathToBaseModuleFolder"></param>
    public RteInterfaceGenerator(string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();

      xmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToCfgFile);
      string pathToRteInterfaceFile = Path.GetDirectoryName(pathToConfiguratioFile) + xmlCfg.PathToRteInterfaceFile;
      rteXmlInterface.ReadActiveRteInterfaceDataBaseFromXml(pathToRteInterfaceFile);
    }

    /// <summary>
    /// Interface to generate the rte interface configuration file
    /// </summary>
    /// <return> GenInfoType </return>
    public GenInfoType GenerateRteInterfaceConfiguration()
    {
      info.AddLogMsg(GenResources.LogMsg_GenInterfaceHeaderFile);

      // Nothing to do
      info.AddInfoMsg("Noting to do here!");

      info.AddLogMsg(GenResources.LogMsg_InterfaceHeaderFileGenerated);

      return info;
    }
  }
}
/**
 * @}
 */
