﻿/*****************************************************************************************************************************
 * @file        General.cs                                                                                                   *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the rte general helper functions                                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using OsarResources.Generator;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using RteLib.RteConfig;
using RteLib.RteInterface;
using RteLib.RteModuleInternalBehavior;
using RteLib.RteTypes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class General
  {
    RteConfig xmlCfg = new RteConfig();
    RteTypes rteXmlTypes = new RteTypes();
    RteInterfaces rteXmlInterface = new RteInterfaces();
    string pathToConfiguratioFile;
    private GenInfoType info = new GenInfoType();

    public General(string pathToCfgFile)
    {
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();

      xmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToCfgFile);

      string pathToRteTypesFile = Path.GetDirectoryName(pathToConfiguratioFile) + xmlCfg.PathToRteTypeFile;
      rteXmlTypes.ReadActiveRteTypesDataBaseFromXml(pathToRteTypesFile);

      string pathToRteInterfaceFile = Path.GetDirectoryName(pathToConfiguratioFile) + xmlCfg.PathToRteInterfaceFile;
      rteXmlInterface.ReadActiveRteInterfaceDataBaseFromXml(pathToRteInterfaceFile);
    }

    /// <summary>
    /// Attribute interface for the generation property
    /// </summary>
    public GenInfoType GenInfo
    {
      get => info;
    }

    /**
      * @brief      Helper function to generate runnable interface comment string
      * @param[in]  RteRunnable active runnable 
      * @param[in]  RteModuleInternalBehavior active module internal behavior file
      * @retval     string with interface info data
      */
    public string hlpGenerateFunctionInterfaceInfoString(RteRunnable runnable, RteModuleInternalBehavior moduleInternalBehaviour)
    {
      string output = "";
      bool elementFound;

      /* Search for Client Interfaces */
      if (0 < runnable.AccessToClientPortFunctions.Count)
      {
        output += "Accessible Client Interfaces:";
        foreach (RteGenericRunnableObject clientFunction in runnable.AccessToClientPortFunctions)
        {
          elementFound = false;

          //Search in available Client List
          for (int idx = 0; idx < moduleInternalBehaviour.AvailableClientPorts.Count(); idx++)
          {
            for (int idx2 = 0; idx2 < moduleInternalBehaviour.AvailableClientPorts[idx].ClientPortFunctions.Count(); idx2++)
            {
              if (moduleInternalBehaviour.AvailableClientPorts[idx].ClientPortFunctions[idx2].UUID == clientFunction.UUID)
              {
                output += "\n" + hlpGenerateFncRetVal(moduleInternalBehaviour.AvailableClientPorts[idx].ClientPortFunctions[idx2].Name) + " " +
                          GenResources.GenRteClientFncInterfaceBaseString +
                          moduleInternalBehaviour.AvailableClientPorts[idx].ClientPort.InterfacePrototype.InterfaceName +
                          "_" + moduleInternalBehaviour.AvailableClientPorts[idx].ClientPortFunctions[idx2].Name + "( " +
                          hlpGenerateFncArgList(moduleInternalBehaviour.AvailableClientPorts[idx].ClientPortFunctions[idx2].Name) + " )";
                output += "\n" + "Available Error Types:\n" + hlpGenerateClientServerPortErrorTypeList(moduleInternalBehaviour.AvailableClientPorts[idx].ClientPortFunctions[idx2].Name,
                  moduleInternalBehaviour.AvailableClientPorts[idx].ClientPort);

                elementFound = true;
                break;
              }
            }

            if (true == elementFound)
            { break; }
          }
        }
      }

      /* Search for Receiver Interfaces */
      if (0 < runnable.AccessToSenderReceiverPortElements.Count)
      {
        output += "\nAccessible Receiver Interfaces:";
        foreach (RteGenericRunnableObject receiverElement in runnable.AccessToSenderReceiverPortElements)
        {
          elementFound = false;

          //Search in available Client List
          for (int idx = 0; idx < moduleInternalBehaviour.AvailableSRPorts.Count(); idx++)
          {
            if (RteSRInterfaceImplementationType.RECEIVER == moduleInternalBehaviour.AvailableSRPorts[idx].SenderReceiverPort.InterfaceType)
            {
              for (int idx2 = 0; idx2 < moduleInternalBehaviour.AvailableSRPorts[idx].SenderReceiverPortElements.Count(); idx2++)
              {
                if (moduleInternalBehaviour.AvailableSRPorts[idx].SenderReceiverPortElements[idx2].UUID == receiverElement.UUID)
                {
                  output += "\n" + hlpGenerateSRElementType(moduleInternalBehaviour.AvailableSRPorts[idx].SenderReceiverPortElements[idx2].Name) +
                    GenResources.GenRteReceiverElementInterfaceBaseString +
                    moduleInternalBehaviour.AvailableSRPorts[idx].SenderReceiverPort.InterfacePrototype.InterfaceName +
                    "_" + moduleInternalBehaviour.AvailableSRPorts[idx].SenderReceiverPortElements[idx2].Name + "();";

                  elementFound = true;
                  break;
                }
              }
            }

            if (true == elementFound)
            { break; }
          }
        }
      }

      /* Search for Sender Interfaces */
      if (0 < runnable.AccessToSenderReceiverPortElements.Count)
      {
        output += "\nAccessible Sender Interfaces:";
        foreach (RteGenericRunnableObject senderElement in runnable.AccessToSenderReceiverPortElements)
        {
          elementFound = false;

          //Search in available Client List
          for (int idx = 0; idx < moduleInternalBehaviour.AvailableSRPorts.Count(); idx++)
          {
            if (RteSRInterfaceImplementationType.SENDER == moduleInternalBehaviour.AvailableSRPorts[idx].SenderReceiverPort.InterfaceType)
            {
              for (int idx2 = 0; idx2 < moduleInternalBehaviour.AvailableSRPorts[idx].SenderReceiverPortElements.Count(); idx2++)
              {
                if (moduleInternalBehaviour.AvailableSRPorts[idx].SenderReceiverPortElements[idx2].UUID == senderElement.UUID)
                {
                  output += "\n" + RteDataTypeHelper.GetDataTypeWithAccessString("void", RteDataAccessType.STANDARD) + " " +
                    GenResources.GenRteSenderElementInterfaceBaseString +
                    moduleInternalBehaviour.AvailableSRPorts[idx].SenderReceiverPort.InterfacePrototype.InterfaceName +
                    "_" + moduleInternalBehaviour.AvailableSRPorts[idx].SenderReceiverPortElements[idx2].Name +
                    "( " + hlpGenerateSRElementType(moduleInternalBehaviour.AvailableSRPorts[idx].SenderReceiverPortElements[idx2].Name) + "x );";

                  elementFound = true;
                  break;
                }
              }
            }

            if (true == elementFound)
            { break; }
          }
        }
      }


      return output;
    }

    /**
      * @brief      Helper function to the function return string
      * @param[in]  interface function name
      * @retval     string with function return string
      * @note       To generate the string the interface database would be used
      *             Use argument exception!! with Error ID 5000
      */
    public string hlpGenerateFncRetVal(string interfaceFncName)
    {
      string retVal = "";
      bool elementFound = false;

      /* Check Client Server Interface */
      foreach (RteCSInterfaceBlueprint interfaceBlueprint in rteXmlInterface.ClientServerBlueprintList)
      {
        foreach (RteFunctionBlueprint interfaceFnc in interfaceBlueprint.Functions)
        {
          //if (interfaceFncUUID == interfaceFnc.UUID)
          // Do not check for UUID >> Prototype and Blueprint UUIDs are differed >> Check for FunctionName
          if (interfaceFncName == interfaceFnc.FunctionName)
          {
            retVal = RteDataTypeHelper.GetDataTypeWithAccessString(interfaceFnc.ReturnValue.DataTypeName, interfaceFnc.ReturnValue.DataAccessType);
            elementFound = true;
            break;
          }
        }

        if (true == elementFound)
        {
          break;
        }
      }

      if (false == elementFound)
      {
        string msg = GenErrorWarningCodes.Error_5000_Msg + interfaceFncName;
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_5000), msg);
        throw new ArgumentException(msg);
      }

      return retVal;
    }

    /**
      * @brief      Helper function to the function argument string
      * @param[in]  interface function UUID
      * @retval     string with function argument string
      * @note       To generate the string the interface database would be used
      *             Use argument exception!! with Error ID 5000
      */
    public string hlpGenerateFncArgList(string interfaceFncName)
    {
      string retVal = "";
      bool elementFound = false;

      /* Check Client Server Interface */
      foreach (RteCSInterfaceBlueprint interfaceBlueprint in rteXmlInterface.ClientServerBlueprintList)
      {
        foreach (RteFunctionBlueprint interfaceFnc in interfaceBlueprint.Functions)
        {
          //if (interfaceFncUUID == interfaceFnc.UUID)
          // Do not check for UUID >> Prototype and Blueprint UUIDs are differed >> Check for FunctionName
          if (interfaceFncName == interfaceFnc.FunctionName)
          {
            if (0 < interfaceFnc.ArgList.Count)
            {
              foreach (RteFunctionArgumentBlueprint arg in interfaceFnc.ArgList)
              {
                /* Detect first processing element */
                if (false == elementFound)
                {
                  retVal = RteDataTypeHelper.GetDataTypeWithAccessString(arg.ArgumentType.DataTypeName, arg.ArgumentType.DataAccessType) +
                    " " + arg.ArgumentName;
                }
                else
                {
                  retVal += ", " + RteDataTypeHelper.GetDataTypeWithAccessString(arg.ArgumentType.DataTypeName, arg.ArgumentType.DataAccessType) +
                    " " + arg.ArgumentName;
                }

                elementFound = true;
              }
            }
            else
            {
              // No argument configured >> Valid configuration
              elementFound = true;
            }

            break;
          }
        }

        if (true == elementFound)
        {
          break;
        }
      }

      if (false == elementFound)
      {
        string msg = GenErrorWarningCodes.Error_5000_Msg + interfaceFncName;
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_5000), msg);
        throw new ArgumentException(msg);
      }

      return retVal;
    }

    /**
      * @brief      Helper function to the element data string
      * @param[in]  interface element name
      * @retval     string with element data
      * @note       To generate the string the interface database would be used
      *             Use argument exception!! with Error ID 5000
      */
    private string hlpGenerateSRElementType(string interfaceElementName)
    {
      string retVal = "";
      bool elementFound = false;

      /* Check Client Server Interface */
      foreach (RteSRInterfaceBlueprint interfaceBlueprint in rteXmlInterface.SenderReceiverBlueprintList)
      {
        foreach (RteElementBlueprint interfaceElement in interfaceBlueprint.Elements)
        {
          //if (interfaceElementUUID == interfaceElement.UUID)
          /// Do not use UUID >> Use name instead >> Blueprint and Prototype UUIDs are not the Same
          if (interfaceElementName == interfaceElement.ElementName)
          {
            retVal = RteDataTypeHelper.GetDataTypeWithAccessString(interfaceElement.ElementType.DataTypeName, interfaceElement.ElementType.DataAccessType);
            retVal += " ";
            elementFound = true;
            break;
          }
        }

        if (true == elementFound)
        {
          break;
        }
      }

      if (false == elementFound)
      {
        string msg = GenErrorWarningCodes.Error_5000_Msg + interfaceElementName;
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_5000), msg);
        throw new ArgumentException(msg);
      }

      return retVal;
    }

    /**
      * @brief      Helper function to generate the client server port return type error List
      * @param[in]  interface function name
      * @retval     String with error data
      * @note       To generate the string the interface database would be used
      *             Use argument exception!! with Error ID 5000
      */
    public string hlpGenerateClientServerPortErrorTypeList(string interfaceFncName, RteCSInterfacePrototype interfacePrototype)
    {
      string retVal = "";
      bool elementFound = false;

      /* Check Client Server Interface */
      foreach (RteCSInterfaceBlueprint interfaceBlueprint in rteXmlInterface.ClientServerBlueprintList)
      {
        foreach (RteFunctionBlueprint interfaceFnc in interfaceBlueprint.Functions)
        {
          //if (interfaceFncUUID == interfaceFnc.UUID)
          /// Do not use UUID >> Use name instead >> Blueprint and Prototype UUIDs are not the Same
          if (interfaceFncName == interfaceFnc.FunctionName)
          {
            foreach (string errorType in interfaceFnc.AvailableErrorTypes)
            {
              if (false == elementFound)
              {
                retVal += "> " + GenResources.GenRteErrorTypeBaseString + interfacePrototype.InterfacePrototype.InterfaceName +
                  "_" + interfaceFnc.FunctionName + "_" + errorType;
              }
              else
              {
                retVal += "\n> " + GenResources.GenRteErrorTypeBaseString + interfacePrototype.InterfacePrototype.InterfaceName +
                  "_" + interfaceFnc.FunctionName + "_" + errorType;
              }



              elementFound = true;
            }
            break;
          }
        }

        if (true == elementFound)
        {
          break;
        }
      }

      if (false == elementFound)
      {
        string msg = GenErrorWarningCodes.Error_5000_Msg + interfaceFncName;
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_5000), msg);
        throw new ArgumentException(msg);
      }

      return retVal;
    }


    /**
    * @brief      Helper function to generate the server port argument List
    * @param[in]  interface function name
    * @param[out] Doxygen object with added parameter list
    * @retval     None
    * @note       To generate the string the interface database would be used
    *             Use argument exception!! with Error ID 5000
    */
    public void hlpGenerateServerPortArgList(string interfaceFncName, ref DoxygenElementDescription doxyGenDoc)
    {
      List<string> argList = new List<string>();
      bool elementFound = false;

      /* Check Client Server Interface */
      foreach (RteCSInterfaceBlueprint interfaceBlueprint in rteXmlInterface.ClientServerBlueprintList)
      {
        foreach (RteFunctionBlueprint interfaceFnc in interfaceBlueprint.Functions)
        {
          //if (interfaceFncUUID == interfaceFnc.UUID)
          /// Do not use UUID >> Use name instead >> Blueprint and Prototype UUIDs are not the Same
          if (interfaceFncName == interfaceFnc.FunctionName)
          {
            if (0 < interfaceFnc.ArgList.Count)
            {
              foreach (RteFunctionArgumentBlueprint arg in interfaceFnc.ArgList)
              {
                switch (arg.ArgumentType.DataAccessType)
                {
                  case RteDataAccessType.STANDARD:
                  doxyGenDoc.AddElementParameter(RteDataTypeHelper.GetDataTypeWithAccessString(arg.ArgumentType.DataTypeName, arg.ArgumentType.DataAccessType) + " " +
                    arg.ArgumentType.DataTypeName, CParameterType.INPUT);
                  break;

                  case RteDataAccessType.POINTER:
                  doxyGenDoc.AddElementParameter(RteDataTypeHelper.GetDataTypeWithAccessString(arg.ArgumentType.DataTypeName, arg.ArgumentType.DataAccessType) + " " +
                    arg.ArgumentType.DataTypeName, CParameterType.INPUT_OUTPUT);
                  break;

                  case RteDataAccessType.CONST_POINTER:
                  doxyGenDoc.AddElementParameter(RteDataTypeHelper.GetDataTypeWithAccessString(arg.ArgumentType.DataTypeName, arg.ArgumentType.DataAccessType) + " " +
                    arg.ArgumentType.DataTypeName, CParameterType.INPUT_OUTPUT);
                  break;

                  case RteDataAccessType.POINTER_TO_CONST:
                  doxyGenDoc.AddElementParameter(RteDataTypeHelper.GetDataTypeWithAccessString(arg.ArgumentType.DataTypeName, arg.ArgumentType.DataAccessType) + " " +
                    arg.ArgumentType.DataTypeName, CParameterType.INPUT_OUTPUT);
                  break;

                  case RteDataAccessType.CONST_POINTER_TO_CONST:
                  doxyGenDoc.AddElementParameter(RteDataTypeHelper.GetDataTypeWithAccessString(arg.ArgumentType.DataTypeName, arg.ArgumentType.DataAccessType) + " " +
                    arg.ArgumentType.DataTypeName, CParameterType.INPUT_OUTPUT);
                  break;
                }
                elementFound = true;
              }
            }
            else
            {
              // No arguments available >> Valid config
              elementFound = true;
            }
            break;
          }
        }

        if (true == elementFound)
        {
          break;
        }
      }

      if (false == elementFound)
      {
        string msg = GenErrorWarningCodes.Error_5000_Msg + interfaceFncName;
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_5000), msg);
        throw new ArgumentException(msg);
      }
    }

    /**
      * @brief      Helper function to search for the Client Server interface blueprint from an interface prototype
      * @param[in]  string of the interface prototype
      * @param[in]  List<RteModuleInternalBehavior> List with module internal behavior elements
      * @retval     RteCSInterfaceBlueprint interface blueprint element
      * @note       The interface data base and all module internal behaviors would be used so search the corresponding blueprint element
      *             Use argument exception!! with Error ID 5002 & 5004
      */
    public RteCSInterfaceBlueprint hlpGetFromCSIfPrototypeCSIfBlueprint(string ifPrototypeUUID, List<RteModuleInternalBehavior> rteMibList)
    {
      RteCSInterfaceBlueprint retVal = new RteCSInterfaceBlueprint();
      bool elementFound = false;
      string ifBlueprintUUID = "";

      /* Search in module internal behavior files the corresponding prototype element */
      foreach (RteModuleInternalBehavior module in rteMibList)
      {
        /* Search in each available server Port */
        foreach (RteAvailableServerPort serverPort in module.AvailableServerPorts)
        {
          if (serverPort.ServerPort.InterfacePrototype.UUID == ifPrototypeUUID)
          {
            ifBlueprintUUID = serverPort.ServerPort.InterfaceBlueprint.UUID;
            elementFound = true;
            break;
          }
        }

        /* Search in each available client Port */
        if (false == elementFound)
        {
          foreach (RteAvailableClientPort clientPort in module.AvailableClientPorts)
          {
            if (clientPort.ClientPort.InterfacePrototype.UUID == ifPrototypeUUID)
            {
              ifBlueprintUUID = clientPort.ClientPort.InterfaceBlueprint.UUID;
              elementFound = true;
              break;
            }
          }
        }

        /* Break if element has been found */
        if (true == elementFound)
        {
          break;
        }
      }

      /* Check if element has been found */
      if (false == elementFound)
      {
        string msg = GenErrorWarningCodes.Error_5004_Msg + ifPrototypeUUID;
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_5004), msg);
        throw new ArgumentException(msg);
      }

      /* Search for Interface blueprint */
      elementFound = false;
      foreach (RteCSInterfaceBlueprint ifBlueprint in rteXmlInterface.ClientServerBlueprintList)
      {
        if (ifBlueprint.InterfaceInfo.UUID == ifBlueprintUUID)
        {
          retVal = ifBlueprint;
          elementFound = true;
          break;
        }
      }

      /* Check if blueprint has been found */
      if (false == elementFound)
      {
        string msg = GenErrorWarningCodes.Error_5002_Msg + ifBlueprintUUID;
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_5002), msg);
        throw new ArgumentException(msg);
      }

      return retVal;
    }

    /**
      * @brief      Helper function to check if a port prototype is connected
      * @param[in]  string of the interface prototype
      * @retval     bool true == connected
      *                 false == unconnected
      * @note       The interface data base and all module internal behaviors would be used so search the corresponding blueprint element
      */
    public bool hlpCheckIfPortPrototypeIsConnected(string ifPrototypeUUID)
    {
      bool elementFound = false;

      /* Check Client Server Ports Connections */
      foreach (RteClientServerPortConnection csPortConnection in xmlCfg.RteClientServerConnections)
      {
        //Check Server Port
        if (ifPrototypeUUID == csPortConnection.RteServerPort.UUID)
        {
          elementFound = true;
          break;
        }

        //Check each connected Client Port
        foreach (RteGeneralInterfaceInfo clientPort in csPortConnection.RteClientPorts)
        {
          if (ifPrototypeUUID == clientPort.UUID)
          {
            elementFound = true;
            break;
          }
        }

        if (true == elementFound)
        {
          break;
        }
      }

      /* Check Sender Receiver  Ports Connections */
      foreach (RteSenderReceiverPortConnection srPortConnection in xmlCfg.RteSenderReceiverConnections)
      {
        //Check Sender Port
        if (ifPrototypeUUID == srPortConnection.RteSenderPort.UUID)
        {
          elementFound = true;
          break;
        }

        //Check each connected Receiver Port
        foreach (RteGeneralInterfaceInfo receiverPort in srPortConnection.RteReceiverPorts)
        {
          if (ifPrototypeUUID == receiverPort.UUID)
          {
            elementFound = true;
            break;
          }
        }

        if (true == elementFound)
        {
          break;
        }
      }

      return elementFound;
    }

    /**
      * @brief      Helper function to search for the Sender Receiver interface blueprint from an interface prototype
      * @param[in]  string of the interface prototype
      * @param[in]  List<RteModuleInternalBehavior> List with module internal behavior elements
      * @retval     RteSRInterfaceBlueprint interface blueprint element
      * @note       The interface data base and all module internal behaviors would be used so search the corresponding blueprint element
      *             Use argument exception!! with Error ID 5003
      */
    public RteSRInterfaceBlueprint hlpGetFromSRIfPrototypeSRIfBlueprint(string ifPrototypeUUID, List<RteModuleInternalBehavior> rteMibList)
    {
      RteSRInterfaceBlueprint retVal = new RteSRInterfaceBlueprint();
      bool elementFound = false;
      string ifBlueprintUUID = "";

      /* Search in module internal behavior files the corresponding prototype element */
      foreach (RteModuleInternalBehavior module in rteMibList)
      {
        /* Search in each available sender receiver Port */
        foreach (RteAvailableSRPort srPort in module.AvailableSRPorts)
        {
          if (srPort.SenderReceiverPort.InterfacePrototype.UUID == ifPrototypeUUID)
          {
            ifBlueprintUUID = srPort.SenderReceiverPort.InterfaceBlueprint.UUID;
            elementFound = true;
            break;
          }
        }

        /* Break if element has been found */
        if (true == elementFound)
        {
          break;
        }
      }

      /* Check if element has been found */
      if (false == elementFound)
      {
        string msg = GenErrorWarningCodes.Error_5003_Msg + ifPrototypeUUID;
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_5003), msg);
        throw new ArgumentException(msg);
      }

      /* Search for Interface blueprint */
      elementFound = false;
      foreach (RteSRInterfaceBlueprint ifBlueprint in rteXmlInterface.SenderReceiverBlueprintList)
      {
        if (ifBlueprint.InterfaceInfo.UUID == ifBlueprintUUID)
        {
          retVal = ifBlueprint;
          elementFound = true;
          break;
        }
      }

      /* Check if blueprint has been found */
      if (false == elementFound)
      {
        string msg = GenErrorWarningCodes.Error_5004_Msg + ifBlueprintUUID;
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_5005), msg);
        throw new ArgumentException(msg);
      }

      return retVal;
    }

  }
}
/**
 * @}
 */
