﻿/*****************************************************************************************************************************
 * @file        RteTypesValidator.cs                                                                                         *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.01.2020                                                                                                   *
 * @brief       Implementation of the rte types file validator                                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generator.Resources;
using OsarResources.Generic;
using OsarResources.XML;
using RteLib.RteConfig;
using RteLib.RteTypes;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class RteTypesValidator
  {
    RteConfig xmlCfg = new RteConfig();
    RteTypes rteXmlTypes = new RteTypes();
    string pathToConfiguratioFile;
    private GenInfoType info = new GenInfoType();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    public RteTypesValidator(string pathToCfgFile)
    {
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();

      xmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToCfgFile);
      string pathToRteTypesFile = Path.GetDirectoryName(pathToConfiguratioFile) + xmlCfg.PathToRteTypeFile;
      rteXmlTypes.ReadActiveRteTypesDataBaseFromXml(pathToRteTypesFile);
    }

    /// <summary>
    /// Interface to validate the rte types configuration file
    /// </summary>
    /// <return> GenInfoType </return>
    public GenInfoType ValidateRteTypesConfiguration()
    {
      info.AddLogMsg(ValidateResources.LogMsg_StartOfRteInterfaceValidation);

      /* Validate configuration file version */
      XmlFileVersion defaultVersion = new XmlFileVersion();
      defaultVersion.MajorVersion = Convert.ToUInt16(DefResources.RteTypesFileMajorVersion);
      defaultVersion.MinorVersion = Convert.ToUInt16(DefResources.RteTypesFileMinorVersion);
      defaultVersion.PatchVersion = Convert.ToUInt16(DefResources.RteTypesFilePatchVersion);
      if (false == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(defaultVersion, rteXmlTypes.XmlFileVersion))
      {
        info.AddErrorMsg(Convert.ToUInt16(ErrorWarningCodes.Error_0000), ErrorWarningCodes.Error_0000_Msg);
      }

      List<string> validationErrors = rteXmlTypes.ValidateRteTypesList();
      foreach (string error in validationErrors)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_2001), GenErrorWarningCodes.Error_2001_Msg + error);
      }

      info.AddLogMsg(ValidateResources.LogMsg_RteInterfaceConfigValidationDone);

      return info;
    }
  }
}
/**
 * @}
 */
