﻿/*****************************************************************************************************************************
 * @file        RteProjectFileHandler.cs                                                                                     *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.01.2020                                                                                                   *
 * @brief       Implementation of the rte project file handler                                                               *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RteLib.RteConfig;
using RteLib.RteTypes;
using RteLib.RteInterface;
using System.IO;
using OsarResources.Generator;
using ModuleLibrary.Versions.v_2_0_0.Models;
using System.Xml.Serialization;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class RteProjectFileHandler
  {
    RteConfig rteXmlCfg = new RteConfig();
    RteTypes rteXmlTypes = new RteTypes();
    RteInterfaces rteXmlInterface = new RteInterfaces();
    HyperspaceProjectConfig hsProjectCfg;
    
    string pathToModuleBaseFolder;
    string pathToConfigurationFile;
    string pathToRteTypesFile;
    string pathToRteInterfaceFile;


    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    /// <param name="absPathToBaseModuleFolder"></param>
    public RteProjectFileHandler(string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      /* Check if config file path is an rooted one */
      if (true == Path.IsPathRooted(pathToCfgFile))
      {
        pathToConfigurationFile = pathToCfgFile;
      }
      else
      {
        pathToConfigurationFile = absPathToBaseModuleFolder + pathToCfgFile;
      }

      /* Read database files */
      rteXmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToConfigurationFile);

      pathToRteTypesFile = Path.GetDirectoryName(pathToConfigurationFile) + rteXmlCfg.PathToRteTypeFile;
      rteXmlTypes.ReadActiveRteTypesDataBaseFromXml(pathToRteTypesFile);

      pathToRteInterfaceFile = Path.GetDirectoryName(pathToConfigurationFile) + rteXmlCfg.PathToRteInterfaceFile;
      rteXmlInterface.ReadActiveRteInterfaceDataBaseFromXml(pathToRteInterfaceFile);

      ReadHyperspaceProjectConfigFile();
    }


    #region Hyperspace project handling
    /**
      * @brief      Functionality to read the Hyperspace project configuration file
      * @param      None
      * @retval     None
      * @note       Read the Hyperspace internal configuration file
      */
    public void ReadHyperspaceProjectConfigFile()
    {
      /* Check if config file dose exist */
      string pathToHyperspaceCfg;
      if (true == Path.IsPathRooted(rteXmlCfg.PathToHyperspaceProjectFile))
      {
        pathToHyperspaceCfg = rteXmlCfg.PathToHyperspaceProjectFile;
      }
      else
      {
        pathToHyperspaceCfg = Path.GetDirectoryName(pathToConfigurationFile) + rteXmlCfg.PathToHyperspaceProjectFile;
      }

      if (File.Exists(pathToHyperspaceCfg))
      {
        HyperspaceProjectConfig tempConfig = new HyperspaceProjectConfig();

        XmlSerializer reader = new XmlSerializer(tempConfig.GetType());
        StreamReader file = new StreamReader(pathToHyperspaceCfg);
        tempConfig = (HyperspaceProjectConfig)reader.Deserialize(file);
        file.Close();

        hsProjectCfg = tempConfig;
      }
    }

    /// <summary>
    /// Interface to merge the hyperspace project into the rte config
    /// </summary>
    /// <returns>GenInfoType</returns>
    public GenInfoType MergeHsProjectFileIntoRteConfig()
    {
      GenInfoType info = new GenInfoType();

      if ("" != rteXmlCfg.PathToHyperspaceProjectFile && null != hsProjectCfg.CfgSpaceModuleList)
      {
        info.AddLogMsg("Start importing of Hyperspace modules / module MiB information into rte database.");

        /* Step 1: Merge Hyperspace modules into rte configuration */
        foreach (CfgSpace_ModuleCfg hsModule in hsProjectCfg.CfgSpaceModuleList)
        {
          // Check that configured module is not RTE
          if (DefResources.ModuleName == hsModule.ModuleName)
          {
            // RTE module detected >> Skipp further module processing
            info.AddLogMsg("Detect \"" + hsModule.ModuleName + "\"" + " Module >> Skipp import from Hyperspace project into rte module config.");
            continue;
          }

          // If module is not RTE go into import mechanism
          RteConfigUsedModuleCfg hsRteModule = new RteConfigUsedModuleCfg();
          try
          {
            hsRteModule = new RteConfigUsedModuleCfg();
            hsRteModule.ModuleName = hsModule.ModuleName;
            hsRteModule.ModuleType = hsModule.ModuleType;

            info.AddLogMsg(">> Start import of " + hsRteModule.ModuleName + " module");
            // Import MiB Information into RteConfig
            RteConfigImportHandler rteConfigImporter = new RteConfigImportHandler(ref rteXmlCfg, pathToConfigurationFile);
            rteConfigImporter.ImportMiBFileIntoRteConfig(hsRteModule);

            // Import MiB Information into RteInterface
            RteInterfaceImportHandler rteInterfaceImporter = new RteInterfaceImportHandler(rteXmlCfg,
              ref rteXmlInterface, pathToConfigurationFile);
            rteInterfaceImporter.ImportMiBFileIntoRteInterface(hsRteModule);

            // Import MiB Information into RteTypes
            RteTypesImportHandler retTypesImporter = new RteTypesImportHandler(rteXmlCfg,
              ref rteXmlTypes, pathToConfigurationFile);
            retTypesImporter.ImportMiBFileIntoRteTypes(hsRteModule);
          }
          catch (OsarResources.Generic.OsarConfigMismatchException ex)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_6000), ex.Message);
          }
          catch (OsarResources.Generic.OsarMergeException ex)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_6001), ex.Message);
          }
          catch (FileNotFoundException ex)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_6001), ex.Message);
          }
          catch (ArgumentException ex)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_6001), ex.Message);
          }
          catch (Exception ex)
          {
            info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_6001), ex.Message);
          }
          finally
          {
            info.AddLogMsg(">> Import of " + hsRteModule.ModuleName + " module finished");
          }
        }

        /* Store imported information to data base files */
        rteXmlCfg.SaveActiveRteConfigDataBaseToXml(pathToConfigurationFile);
        rteXmlInterface.SaveActiveRteInterfaceDataBaseToXml(pathToRteInterfaceFile);
        rteXmlTypes.SaveActiveRteTypesDataBaseToXml(pathToRteTypesFile);

      }
      else
      {
        info.AddWaringMsg(Convert.ToUInt16(GenErrorWarningCodes.Warning_4002), GenErrorWarningCodes.Warning_4002_Msg);
      }

      return info;
    }

    #endregion
  }
}
/**
 * @}
 */
