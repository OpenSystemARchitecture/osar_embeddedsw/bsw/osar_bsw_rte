﻿/*****************************************************************************************************************************
 * @file        ModuleGenerator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Generator Class                                                                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generator.Resources;
using System.Reflection;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.DocumentationObjects.General;
using RteLib;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  internal class ModuleGenerator
  {
    private string pathToConfiguratioFile;
    private string pathToModuleBaseFolder;
    private string moduleLibPath;

    GenInfoType mergeInformation = new GenInfoType();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    /// <param name="absPathToBaseModuleFolder"> Absolute path to module base folder</param>
    public ModuleGenerator(string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      moduleLibPath = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
    }

    /// <summary>
    /// Interface to generate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType GenerateConfiguration()
    {
      GenInfoType genInfo1 = new GenInfoType(), genInfoMerge = new GenInfoType();

      /* First Step: Merge Hyperspace Project file into config file !! */
      // NOTE: Merge just results >> Merging functionality out of generator context performed
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, mergeInformation);

      /* Second Step: Create module internal behavior list !! */
      RteMibGenerator mibGenerator = new RteMibGenerator(pathToConfiguratioFile, pathToModuleBaseFolder);
      genInfo1 = mibGenerator.SetupUsedMibModuleList();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      /* Second Step: Merge Types Data Base !! */
      // Done during startup phase >> No changes shall be available at this point of time

      /* Third Step: Merge Interface Data Base !! */
      // Done during startup phase >> No changes shall be available at this point of time

      /* 4. Step: Validate Types Data Base !! */
      // Done during validation phase

      /* 5. Step: Validate Overall elements !! */
      // Done during validation phase

      /* 6 Step: Process Rte Types xml file */
      RteTypesGenerator typesGenerator = new RteTypesGenerator(pathToConfiguratioFile, pathToModuleBaseFolder);
      genInfo1 = typesGenerator.GenerateRteTypesConfiguration();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      /* 7 Step: Process Rte Interface xml file */
      RteInterfaceGenerator interfaceGenerator = new RteInterfaceGenerator(pathToConfiguratioFile,
        pathToModuleBaseFolder);
      genInfo1 = interfaceGenerator.GenerateRteInterfaceConfiguration();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      /* 8. Step: Process Rte Internal Module Behavior files */
      genInfo1 = mibGenerator.GenerateRteMibModuleFiles();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      /* 9. Step: Generate Rte Interaction Layer */
      RteLayerGenerator layerGenerator = new RteLayerGenerator(pathToConfiguratioFile,
        pathToModuleBaseFolder, mibGenerator.RteModuleInternalBehaviorList);
      genInfo1 = layerGenerator.GenerateRteLayerFiles();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      /* 10. Step: Generate Rte Callout Stubs file */
      RteCalloutGenerator calloutGenerator = new RteCalloutGenerator(pathToConfiguratioFile,
        pathToModuleBaseFolder);
      genInfo1 = calloutGenerator.GenerateRteCalloutFiles();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      /* 11. Step: Generate Rte System Applications */
      RteSystemApplicationGenerator sysAppGenerator = new RteSystemApplicationGenerator(pathToConfiguratioFile,
        pathToModuleBaseFolder, mibGenerator.RteModuleInternalBehaviorList);
      genInfo1 = sysAppGenerator.GenerateSystemApplicationFiles();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      /* 12. Step: Generate Rte Makefile */
      RteMakefileGenerator makefileGenerator = new RteMakefileGenerator(pathToConfiguratioFile,
        pathToModuleBaseFolder);
      genInfo1 = makefileGenerator.GenerateMakefiles();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      // 13. Step: Generate Memory-Map-Header-File
      genInfo1 = GenerateMemoryMapHeaderFile();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      // 14. Step: Generate Pre-Build-Config-File
      RtePreBuildConfigGenerator rtePreBuildConfigGenerator = new RtePreBuildConfigGenerator(pathToConfiguratioFile,
        pathToModuleBaseFolder);
      genInfo1 = rtePreBuildConfigGenerator.GenerateRtePreBuildConfiguration();
      genInfoMerge = OsarGenHelper.MergeGenInfoType(genInfoMerge, genInfo1);

      return genInfoMerge;
    }

    /// <summary>
    /// Interface to report out of generator context merge results
    /// </summary>
    /// <param name="mergeInfo"></param>
    public void ReportMergeInfo(GenInfoType mergeInfo)
    {
      mergeInformation = mergeInfo;
    }


    /// <summary>
    /// Interface to generate the Memory-Map-Header-File
    /// </summary>
    /// <returns> Generation information </returns>
    private GenInfoType GenerateMemoryMapHeaderFile()
    {
      GenInfoType info = new GenInfoType();
      info.AddLogMsg(GenResources.LogMsg_StartGenMemMap);

      /* +++++ Generate Memory Mapping file +++++ */
      OsarResources.Generic.OsarGenericHelper.generateModuleMemoryMappingFile(pathToModuleBaseFolder + GenResources.GenHeaderFilePath, DefResources.ModuleName, DefResources.GeneratorName);

      info.AddLogMsg(GenResources.LogMsg_MemMapGenerated);

      return info;
    }

  }
}
/**
 * @}
 */
