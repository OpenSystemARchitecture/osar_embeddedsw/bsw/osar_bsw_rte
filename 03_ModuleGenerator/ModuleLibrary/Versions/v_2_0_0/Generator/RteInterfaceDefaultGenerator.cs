﻿/*****************************************************************************************************************************
 * @file        RteInterfaceDefaultGenerator.cs                                                                              *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        03.01.2020                                                                                                   *
 * @brief       Implementation of the Default Rte Interface Generator Class                                                  *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_2_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generator.Resources;
using RteLib.RteConfig;
using RteLib.RteInterface;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_2_0_0.Generator
{
  public class RteInterfaceDefaultGenerator
  {
    RteConfig xmlCfg = new RteConfig();
    string pathToConfiguratioFile;
    private GenInfoType info = new GenInfoType();

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile"></param>
    public RteInterfaceDefaultGenerator(string pathToCfgFile)
    {
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
      info.allSeq = new List<string>();

      xmlCfg.ReadActiveRteConfigDataBaseFromXml(pathToCfgFile);
    }

    /// <summary>
    /// Interface to generate a default rte interface configuration file
    /// </summary>
    /// <return> GenInfoType </return>
    public GenInfoType GenerateDefaultRteInterfaceConfiguration()
    {
      info.AddLogMsg(DefResources.LogMsg_StartDefaultInterfaceCreation);

      try
      {
        RteInterfaces rteXmlInterfaces = new RteInterfaces();
        rteXmlInterfaces.ClientServerBlueprintList = new List<RteCSInterfaceBlueprint>();
        rteXmlInterfaces.SenderReceiverBlueprintList = new List<RteSRInterfaceBlueprint>();

        string pathToRteInterfaceFile = Path.GetDirectoryName(pathToConfiguratioFile) + xmlCfg.PathToRteInterfaceFile;
        rteXmlInterfaces.SaveActiveRteInterfaceDataBaseToXml(pathToRteInterfaceFile);
      }
      catch (Exception ex)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_3000), GenErrorWarningCodes.Error_3000_Msg + ex);
      }

      info.AddLogMsg(DefResources.LogMsg_DefaultInterfaceCreationDone);

      return info;
    }
  }
}
/**
 * @}
 */
