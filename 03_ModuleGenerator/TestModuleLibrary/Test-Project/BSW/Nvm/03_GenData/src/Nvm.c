/*****************************************************************************************************************************
 * @file        Nvm.c                                                                                                        *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        20.12.2020 15:41:46                                                                                          *
 * @brief       Generated Rte Module Interface file: Nvm.c                                                                   *
 * @version     v.1.1.0                                                                                                      *
 * @generator   OSAR Rte Generator v.0.1.0.182                                                                               *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.9.1                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Nvm 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_Nvm.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of USER include and definition area           << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "Nvm.h"
#include "Nvm_Int.h"
#include "Nvm_RequestList.h"

/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Nvm_START_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"
uint16 nvmReadAll_LastBlkIdx, nvmWriteAll_LastBlkIdx;
#define Nvm_STOP_SEC_NOINIT_VAR
#include "Nvm_MemMap.h"

/*-------------------------------------------------- External defined variables --------------------------------------------*/
extern const Nvm_BlockCfgType nvmBlkCfgList[];
extern Nvm_RequestListEntryType nvmRequestList[];

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           End of USER include and definition area            << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map functions into code memory ---------------------------------------------*/
#define Nvm_START_SEC_CODE
#include "Nvm_MemMap.h"
/**
 * @brief           Rte generated module initialization function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: System Init
 * @uuid            97da873590ae423e9ef657cbaee83198
 */
VAR(void) Nvm_Init( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  Nvm_Int_Init();
  Nvm_RequestList_Init();
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module cyclic function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: Cyclic 10ms
 * @uuid            34d99ac803f948d6822825d937bf571a
 */
VAR(void) Nvm_Mainfunction( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  Nvm_Int_ProcessActiveRequests();
  Nvm_Int_ProcessNewRequests();
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module Server Port Function.
 * @param[in]       VAR(uint16) uint16
 * @param[in,out]   P2VAR(Nvm_BlockStatusType) Nvm_BlockStatusType
 * @retval          VAR(Rte_ErrorType) >> Available Error Types:
 *                  > Rte_ErrorType_PpGeneralNvm_Nvm_GetErrorStatus_NotOk
 * @note            Trigger: Operation Invocation
 * @uuid            9248831213ba4e4d974b7459402d0723
 */
VAR(Rte_ErrorType) Rte_Server_PpGeneralNvm_Nvm_GetErrorStatus( VAR(uint16) nvmBlockId, P2VAR(Nvm_BlockStatusType) nvmBlockStatus )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  Rte_ErrorType retVal = RTE_E_OK;

  if(E_NOT_OK == Nvm_GetErrorStatus(nvmBlockId, nvmBlockStatus))
  {
    retVal = Rte_ErrorType_PpGeneralNvm_Nvm_GetErrorStatus_NotOk;
  }

  return retVal;
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module Server Port Function.
 * @param[in]       VAR(uint16) uint16
 * @param[in,out]   P2VAR(uint8) uint8
 * @retval          VAR(Rte_ErrorType) >> Available Error Types:
 *                  > Rte_ErrorType_PpGeneralNvm_Nvm_WriteBlock_NotOk
 *                  > Rte_ErrorType_PpGeneralNvm_Nvm_WriteBlock_Skipped
 * @note            Trigger: Operation Invocation
 * @uuid            269c82f573aa41da83e6acf7f4df2e4e
 */
VAR(Rte_ErrorType) Rte_Server_PpGeneralNvm_Nvm_WriteBlock( VAR(uint16) nvmBlockId, P2VAR(uint8) pNvmWriteData )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  Rte_ErrorType retVal = RTE_E_OK;
  Std_ReturnType fncRetVal;

  fncRetVal = Nvm_WriteBlock(nvmBlockId, pNvmWriteData);

  if(E_SKIPPED == fncRetVal)
  {
    retVal = Rte_ErrorType_PpGeneralNvm_Nvm_WriteBlock_Skipped;
  } else if(E_NOT_OK == fncRetVal)
  {
    retVal = Rte_ErrorType_PpGeneralNvm_Nvm_WriteBlock_NotOk;
  }

  return retVal;
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module Server Port Function.
 * @param[in]       VAR(uint16) uint16
 * @param[in,out]   P2VAR(uint8) uint8
 * @retval          VAR(Rte_ErrorType) >> Available Error Types:
 *                  > Rte_ErrorType_PpGeneralNvm_Nvm_ReadBlock_NotOk
 *                  > Rte_ErrorType_PpGeneralNvm_Nvm_ReadBlock_Skipped
 * @note            Trigger: Operation Invocation
 * @uuid            352cb060069648b9b74209b6d6a55838
 */
VAR(Rte_ErrorType) Rte_Server_PpGeneralNvm_Nvm_ReadBlock( VAR(uint16) nvmBlockId, P2VAR(uint8) pNvmReadData )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  Rte_ErrorType retVal = RTE_E_OK;
  Std_ReturnType fncRetVal;

  fncRetVal = Nvm_ReadBlock(nvmBlockId, pNvmReadData);

  if(E_SKIPPED == fncRetVal)
  {
    retVal = Rte_ErrorType_PpGeneralNvm_Nvm_ReadBlock_Skipped;
  } else if(E_NOT_OK == fncRetVal)
  {
    retVal = Rte_ErrorType_PpGeneralNvm_Nvm_ReadBlock_NotOk;
  }

  return retVal;
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module Server Port Function.
 * @param[in]       VAR(uint16) uint16
 * @retval          VAR(Rte_ErrorType) >> Available Error Types:
 *                  > Rte_ErrorType_PpGeneralNvm_Nvm_EraseNvBlock_NotOk
 *                  > Rte_ErrorType_PpGeneralNvm_Nvm_EraseNvBlock_Skipped
 * @note            Trigger: Operation Invocation
 * @uuid            72b85b0342e648b4967863177f7640ef
 */
VAR(Rte_ErrorType) Rte_Server_PpGeneralNvm_Nvm_EraseNvBlock( VAR(uint16) nvmBlockId )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  Rte_ErrorType retVal = RTE_E_OK;
  Std_ReturnType fncRetVal;

  fncRetVal = Nvm_EraseNvBlock(nvmBlockId);

  if(E_SKIPPED == fncRetVal)
  {
    retVal = Rte_ErrorType_PpGeneralNvm_Nvm_EraseNvBlock_Skipped;
  } else if(E_NOT_OK == fncRetVal)
  {
    retVal = Rte_ErrorType_PpGeneralNvm_Nvm_EraseNvBlock_NotOk;
  }

  return retVal;
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

#define Nvm_STOP_SEC_CODE
#include "Nvm_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of USER function area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define Nvm_START_SEC_CODE
#include "Nvm_MemMap.h"
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfuntion.
 */
void Nvm_InitMemory( void )
{
  nvmReadAll_LastBlkIdx = 0;
  nvmWriteAll_LastBlkIdx = 0;
}


/**
 * @brief           API to request the error / current status of an specific Nvm block
 * @param[in]       uint16 nvm block id
 * @param[out]      Nvm_BlockStatusType* current nvm block status
 *                  > NVM_BLK_OK                  >> Nvm block is valid
 *                  > NVM_BLK_PENDING             >> Nvm block is ia current in progress
 *                  > NVM_BLK_SKIPPED             >> Nvm block processing has been skipped
 *                  > NVM_BLK_INTEGRITY_FAILED    >> Nvm block data is invalid
 *                  > NVM_BLK_INVALIDATE          >> Nvm block not found in memory
 * @retval          Std_ReturnType
 *                  > E_OK                        >> Request accepted
 *                  > E_NOT_OK                    >> Request not accepted
 */
Std_ReturnType Nvm_GetErrorStatus( uint16 nvmBlockId, Nvm_BlockStatusType* nvmBlockStatus )
{
  Std_ReturnType retVal;
  Nvm_ReturnType fncRetVal;
  fncRetVal = Nvm_Int_GetNvmBlockStatus(nvmBlockId, nvmBlockStatus);
  if (NVM_E_OK == fncRetVal)
  {
    retVal = E_OK;
  }
  else
  {
    retVal = E_NOT_OK;
  }
  return retVal;
}

/**
 * @brief           API to request an nvm block write
 * @param[in]       uint16 nvm block id
 * @param[in]       uint8* pointer to data which shall be written
 * @retval          Std_ReturnType
 *                  > E_OK                        >> Request accepted
 *                  > E_NOT_OK                    >> Request failed
 *                  > E_SKIPPED                   >> Request not accepted
 * @notes           The status of the active block must be additional requested.
 */
Std_ReturnType Nvm_WriteBlock( uint16 nvmBlockId, uint8* pNvmWriteData  )
{
  Std_ReturnType retVal = E_NOT_OK;
  Nvm_ReturnType fncRetVal;

  fncRetVal = Nvm_RequestList_InsertANewWriteRequest(nvmBlockId, pNvmWriteData);
  if (NVM_E_OK == fncRetVal)
  {
    Nvm_Int_SetNvmBlockStatus(nvmBlockId, NVM_BLK_PENDING);
    retVal = E_OK;
  }
  else if (NVM_E_SKIPPED == fncRetVal)
  {
    Nvm_Int_SetNvmBlockStatus(nvmBlockId, NVM_BLK_SKIPPED);
    retVal = E_SKIPPED;
  }
  else
  {
    Nvm_Int_SetNvmBlockStatus(nvmBlockId, NVM_BLK_SKIPPED);
    retVal = E_NOT_OK;
  }

  return retVal;
}

/**
 * @brief           API to request an nvm block read
 * @param[in]       uint16 nvm block id
 * @param[in]       uint8* pointer to data where the read data could be stored
 * @retval          Std_ReturnType
 *                  > E_OK                        >> Request accepted
 *                  > E_NOT_OK                    >> Request failed
 *                  > E_SKIPPED                   >> Request not accepted
 * @notes           The status of the active block must be additional requested.
 */
Std_ReturnType Nvm_ReadBlock( uint16 nvmBlockId, uint8* pNvmReadData  )
{
  Std_ReturnType retVal = E_NOT_OK;
  Nvm_ReturnType fncRetVal;

  fncRetVal = Nvm_RequestList_InsertANewReadRequest(nvmBlockId, pNvmReadData);
  if (NVM_E_OK == fncRetVal)
  {
    Nvm_Int_SetNvmBlockStatus(nvmBlockId, NVM_BLK_PENDING);
    retVal = E_OK;
  }
  else if (NVM_E_SKIPPED == fncRetVal)
  {
    Nvm_Int_SetNvmBlockStatus(nvmBlockId, NVM_BLK_SKIPPED);
    retVal = E_SKIPPED;
  }
  else
  {
    Nvm_Int_SetNvmBlockStatus(nvmBlockId, NVM_BLK_SKIPPED);
    retVal = E_NOT_OK;
  }

  return retVal;
}

/**
 * @brief           API to request an nvm block erase
 * @param[in]       uint16 nvm block id
 * @retval          Std_ReturnType
 *                  > E_OK                        >> Request accepted
 *                  > E_NOT_OK                    >> Request failed
 *                  > E_SKIPPED                   >> Request not accepted
 * @note            Nvm block data would be filled with 0x00. The status of the active block must be additional requested.
 */
Std_ReturnType Nvm_EraseNvBlock( uint16 nvmBlockId )
{
  Std_ReturnType retVal = E_NOT_OK;
  Nvm_ReturnType fncRetVal;

  fncRetVal = Nvm_RequestList_InsertANewEraseRequest(nvmBlockId);
  if (NVM_E_OK == fncRetVal)
  {
    Nvm_Int_SetNvmBlockStatus(nvmBlockId, NVM_BLK_PENDING);
    retVal = E_OK;
  }
  else if (NVM_E_SKIPPED == fncRetVal)
  {
    Nvm_Int_SetNvmBlockStatus(nvmBlockId, NVM_BLK_SKIPPED);
    retVal = E_SKIPPED;
  }
  else
  {
    Nvm_Int_SetNvmBlockStatus(nvmBlockId, NVM_BLK_SKIPPED);
    retVal = E_NOT_OK;
  }

  return retVal;
}

/**
 * @brief           API to read all nvm blocks from nvm memory into ram
 * @param[in]       None
 * @retval          Std_ReturnType
 *                  > E_OK                        >> Request accepted
 *                  > E_NOT_OK                    >> Request not accepted
 *                  > E_PENDING                   >> Request pending
 * @note            Only selected nvm blocks. This function must be called as long the function reports "E_PENDING".
 *                  Also the equal base module must be called to process the selected Nvm blocks.
 */
Std_ReturnType Nvm_ReadAll( void )
{
  Std_ReturnType retVal;
  Nvm_ReturnType fncRetVal;
  uint8 reqBufIdx;
  boolean reqPending = FALSE;
  /* Set as mutch as possible request to Nvm Request buffer if this is requested */
  for (nvmReadAll_LastBlkIdx; nvmReadAll_LastBlkIdx < NVM_CNT_CONFIGURED_BLOCKS; nvmReadAll_LastBlkIdx++)
  {
    /* Check if read all is requested for current element */
    if (TRUE == nvmBlkCfgList[nvmReadAll_LastBlkIdx].nvmBlockReadAll)
    {
      fncRetVal = Nvm_RequestList_InsertANewReadRequest(nvmBlkCfgList[nvmReadAll_LastBlkIdx].nvmBlockId, nvmBlkCfgList[nvmReadAll_LastBlkIdx].nvmBlockUserRamVariable);

      /* Check retrun value */
      if (NVM_E_SKIPPED == fncRetVal)
      {
        /* Request Buffer full >> break active calculation */
        break;
      }
    }
    else
    {
      /* Set unprocessed elements to skipped */
      Nvm_Int_SetNvmBlockStatus(nvmReadAll_LastBlkIdx, NVM_BLK_SKIPPED);
    }
  }

  /* Trigger Nvm Mainfunction */
  Nvm_Mainfunction();

  if (nvmReadAll_LastBlkIdx == NVM_CNT_CONFIGURED_BLOCKS)
  {
    /* Check if request buffer is empty */
    for(reqBufIdx = 0; reqBufIdx < NVM_REQUEST_LIST_SIZE; reqBufIdx++)
    {
      if (NVM_BLK_EMPTY != nvmRequestList[reqBufIdx].nvmRequestStatus)
      {
        reqPending = TRUE;
        break;
      }
    }

    if (TRUE == reqPending)
    {
      retVal = E_PENDING;
    }
    else
    {
      nvmReadAll_LastBlkIdx = 0;
      retVal = E_OK;
    }
  }
  else
  {
    retVal = E_PENDING;
  }

  return retVal;
}

/**
 * @brief           API to write all nvm blocks to nvm memory from ram
 * @param[in]       None
 * @retval          Std_ReturnType
 *                  > E_OK                        >> Request accepted
 *                  > E_NOT_OK                    >> Request not accepted
 *                  > E_PENDING                   >> Request pending
 * @note            Only selected nvm blocks. This function must be called as long the function reports "E_PENDING".
 *                  Also the equal base module must be called to process the selected Nvm blocks.
 */
Std_ReturnType Nvm_WriteAll( void )
{
  Std_ReturnType retVal;
  Nvm_ReturnType fncRetVal;
  uint8 reqBufIdx;
  boolean reqPending = FALSE;
  /* Set as mutch as possible request to Nvm Request buffer if this is requested */
  for (nvmWriteAll_LastBlkIdx; nvmWriteAll_LastBlkIdx < NVM_CNT_CONFIGURED_BLOCKS; nvmWriteAll_LastBlkIdx++)
  {
    /* Check if read all is requested for current element */
    if (TRUE == nvmBlkCfgList[nvmWriteAll_LastBlkIdx].nvmBlockWriteAll)
    {
      fncRetVal = Nvm_RequestList_InsertANewWriteRequest(nvmBlkCfgList[nvmWriteAll_LastBlkIdx].nvmBlockId, nvmBlkCfgList[nvmWriteAll_LastBlkIdx].nvmBlockUserRamVariable);

      /* Check retrun value */
      if (NVM_E_SKIPPED == fncRetVal)
      {
        /* Request Buffer full >> break active calculation */
        break;
      }
    }
    else
    {
      /* Set unprocessed elements to skipped */
      Nvm_Int_SetNvmBlockStatus(nvmWriteAll_LastBlkIdx, NVM_BLK_SKIPPED);
    }
  }

  /* Trigger Nvm Mainfunction */
  Nvm_Mainfunction();

  if (nvmWriteAll_LastBlkIdx == NVM_CNT_CONFIGURED_BLOCKS)
  {
    /* Check if request buffer is empty */
    for (reqBufIdx = 0; reqBufIdx < NVM_REQUEST_LIST_SIZE; reqBufIdx++)
    {
      if (NVM_BLK_EMPTY != nvmRequestList[reqBufIdx].nvmRequestStatus)
      {
        reqPending = TRUE;
        break;
      }
    }

    if (TRUE == reqPending)
    {
      retVal = E_PENDING;
    }
    else
    {
      nvmWriteAll_LastBlkIdx = 0;
      retVal = E_OK;
    }
  }
  else
  {
    retVal = E_PENDING;
  }

  return retVal;
}
#define Nvm_STOP_SEC_CODE
#include "Nvm_MemMap.h"

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of USER function area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

