/*****************************************************************************************************************************
 * @file        Nvm_PBCfg.h                                                                                                  *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        13.12.2020 14:41:26                                                                                          *
 * @brief       Generated header file data of the Nvm module.                                                                *
 * @version     v.1.2.1                                                                                                      *
 * @generator   OSAR Nvm Generator v.1.0.0.5                                                                                 *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.7.1                                                                           *
*****************************************************************************************************************************/

#ifndef __NVM_PBCFG_H
#define __NVM_PBCFG_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Nvm 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Nvm available base module definitions.
 */
typedef enum{
  NVM_FLSEEP = 0,
}Nvm_BaseModules;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*----------------------------------------------- Nvm Module Det Information -----------------------------------------------*/
#define NVM_DET_MODULE_ID                          9
#define NVM_MODULE_USE_DET                         STD_ON
/*-------------------------------------------- General Nvm module configuration --------------------------------------------*/
#define NVM_CNT_CONFIGURED_BLOCKS                  1
#define NVM_CNT_CONFIGURED_BASEMODULES             1
#define NVM_REQUEST_LIST_SIZE                      10
#define NVM_MAX_REQUEST_DATA_SIZE                  16
/*------------------------------- Nvm User Nvm block configuration >> Block Name: "DemoBlk" -------------------------------*/
#define NVM_BLK_ID_DEMOBLK                         0
#define Nvm_WriteBlock_DemoBlk(pNvmWriteData)      Nvm_WriteBlock(0, pNvmWriteData)
#define Nvm_ReadBlock_DemoBlk(pNvmReadData)        Nvm_ReadBlock(0, pNvmReadData)
#define Nvm_EraseBlock_DemoBlk()                   Nvm_EraseNvBlock(0)
#define Nvm_GetErrorStatus_DemoBlk(nvmBlockStatus) Nvm_GetErrorStatus(0, nvmBlockStatus)
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __NVM_PBCFG_H*/
