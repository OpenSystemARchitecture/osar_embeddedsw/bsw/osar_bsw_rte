/*****************************************************************************************************************************
 * @file        FlsEep.h                                                                                                     *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.08.2018 08:29:23                                                                                          *
 * @brief       Implementation of definitions / interface function prototypes / datatypes and generic module interface       *
 *              informations of the "FlsEep" module.                                                                         *
 *                                                                                                                           *
 * @details     The FlsEep module implements the flash eeprom emulation driver and the basic interfaces for higher layer     *
 *              modules like an Non volatile memory management unit.                                                         *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __FLSEEP_H
#define __FLSEEP_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup FlsEep
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
#include "FlsEep_Types.h"
#include "FlsEep_PBCfg.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfuntion.
 */
void FlsEep_InitMemory( void );

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void FlsEep_Init( void );

/**
 * @brief           Module global mainfunction.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called form the actual runtime environment within an fixed cycle.
 */
void FlsEep_Mainfunction( void );

/**
 * @brief           Trigger a write action into the Flash
 * @param[in]       uint32 FlsEep configured block id
 * @param[in]       uin8* pointer to data which shall be written
 * @retval          Std_ReturnType
 *                  > E_OK        >> Request accepted
 *                  > E_NOT_OK    >> Request failed.
 *                  > E_SKIPPED   >> Request has not been accepted.
 * @details         Interface function to trigger a write action. This Function dose not perform the write action. The status
 *                  of the active block has to be additional requested.
 */
Std_ReturnType FlsEep_RequestWriteData( uint32 flsBlockId, uint8* pWriteData );

/**
 * @brief           Trigger a read action into the Flash
 * @param[in]       uint32 FlsEep configured block id
 * @param[in]       uint8* pointer to data where the read data shall be stored
 * @retval          Std_ReturnType
 *                  > E_OK        >> Request accepted
 *                  > E_NOT_OK    >> Request failed.
 *                  > E_SKIPPED   >> Request has not been accepted.
 * @details         Interface function to trigger a read action. This Function dose not perform the write action. The status
 *                  of the active block has to be additional requested. The FlsEep module would write the read data into the given pointer location.
 */
Std_ReturnType FlsEep_RequestReadData( uint32 flsBlockId, uint8* pReadData );

/**
 * @brief           Request the actual block status
 * @param[in]       uint32 FlsEep configured block id
 * @retval          Data_StatusType
 *                  > DATA_E_OK            >> Block is valid
 *                  > DATA_E_PENDING       >> Block processing is still in progress
 *                  > DATA_E_INVALIDATE    >> Block data is not available
 *                  > DATA_E_SKIPPED       >> Request failed / not accepted
 * @details         Interface function to request the actual block status.
 */
Data_StatusType FlsEep_RequestBlockStatus( uint32 flsBlockId );

/**
 * @brief           Request the actual module status
 * @param[in]       None
 * @retval          FlsEep_ReturnType
 *                  > E_OK            >> Module is in idle mode
 *                  > E_PENDING       >> Module is still processing data
 *                  > E_NOT_OK        >> Module is in safe state
 * @details         Interface function to request the actual module status.
 */
Std_ReturnType FlsEep_RequestModuleStatus( void );

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __FLSEEP_H*/
