/*****************************************************************************************************************************
 * @file        UartIf.c                                                                                                     *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        20.03.2019 11:55:42                                                                                          *
 * @brief       Generated Rte Module Interface file: UartIf.c                                                                *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup UartIf 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_UartIf.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of USER include and definition area           << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "UartIf.h"
#include "UartIf_Buffer.h"

#define UartIf_START_SEC_NOINIT_VAR
#include "UartIf_MemMap.h"
/*
 * @brief   State machine state variable of the uart module
 */
UartIf_StateMachineStateType uartIfActualStatemachineState;

/*
 * @brief Notification variables for Tx and Rx
 */
boolean txModuleNotificationPollingMode[UARTIF_CNT_OF_USED_MODULES];
boolean rxModuleNotificationPollingMode[UARTIF_CNT_OF_USED_MODULES];
#define UartIf_STOP_SEC_NOINIT_VAR
#include "UartIf_MemMap.h"

#define UartIf_START_SEC_NOINIT_VAR
#include "UartIf_MemMap.h"
/* Create work pointer variables of the buffer frame data */
UartIf_Buffer_Frame *pToWorkBufferTxFrames[UARTIF_CNT_OF_USED_MODULES];
#define UartIf_STOP_SEC_NOINIT_VAR
#include "UartIf_MemMap.h"

/**
 * @brief           Statemachine processing function to process the tx buffer data
 * @param[in]       None
 * @retval          None
 * @details         This function would be call during the run state of the Uart Statemachine. It would check if there is any
 *                  Data to be transmitted
 */
void UartIf_ProcessTxBufferData(void);

/**
 * @brief           Statemachine processing function to process the rx buffer data
 * @param[in]       None
 * @retval          None
 * @details         This function would be call during the run state of the Uart Statemachine. It would check if there is any
 *                  Data received;
 */
void UartIf_ProcessRxBufferData(void);

/**
* @brief           Helper function to store an rx frame in buffer
* @param[in]       UartIf_RxFrameType frameData
* @retval          Std_ReturnType
*/
Std_ReturnType UartIf_StoreRxElementInUartIfBuffer(UartIf_RxFrameType frameData);

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           End of USER include and definition area            << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map functions into code memory ---------------------------------------------*/
#define UartIf_START_SEC_CODE
#include "UartIf_MemMap.h"
/**
 * @brief           Rte generated module initialization function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: System Init
 * @uuid            f14fc1dfc70a4bbdb1bc4c57bcd6ce80
 */
VAR(void) UartIf_Init( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  /* Change Statemachine from uninit to run state */
  uartIfActualStatemachineState = UART_STATEMACHINESTATE_RUN;
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module cyclic function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: Cyclic 10ms
 * @uuid            c73ea0a13c894e638728f2f106f9699d
 */
VAR(void) UartIf_Mainfunction( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  /* Main State machine */
  switch (uartIfActualStatemachineState)
  {
  case UART_STATEMACHINESTATE_UNINIT:
    break;

  case UART_STATEMACHINESTATE_RUN:
    /* Check if buffer overlow has been detected */
    if (UARTIF_E_BUFFER_OVERFLOW == UartIf_Buffer_GetGeneralBufferModuleStatus())
    {
      uartIfActualStatemachineState = UART_STATEMACHINESTATE_SAFE;
    }
    else
    {
      UartIf_ProcessTxBufferData();
      UartIf_ProcessRxBufferData();
    }
    break;

  case UART_STATEMACHINESTATE_SAFE:
    break;

  default:
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_GENERIC_PROGRAMMING_FAILURE);
#endif
    break;
  }
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

#define UartIf_STOP_SEC_CODE
#include "UartIf_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of USER function area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define UartIf_START_SEC_CODE
#include "UartIf_MemMap.h"
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfunction.
 */
void UartIf_InitMemory( void )
{
  uint8 idx;
  /* Initializes uart buffers */
  UartIf_Buffer_InitMemory();

  /* Init module static work elements */
  for (idx = 0; idx < UARTIF_CNT_OF_USED_MODULES; idx++)
  {
    pToWorkBufferTxFrames[idx] = NULL_PTR;
  }

  /* Set valid Statemachine state */
  uartIfActualStatemachineState = UART_STATEMACHINESTATE_UNINIT;
}




/* ######################################################################################################################## */
/* ######################################## UART PROVIDED HIGH LEVEL INTERFACES ########################################### */
/* ######################################################################################################################## */
/**
 * @brief           API function to read a received data frame
 * @param[in/out]   UartIf_RxFrameType   Higher layer frame data
 * @retval          UartReturnType
 *                  >> UARTIF_E_NULL_POINTER
 *                  >> UARTIF_E_BUFFER_EMPTY
 *                  >> UARTIF_E_BUFFER_OVERFLOW
 *                  >> UARTIF_E_OK
 *                  >> UARTIF_E_NOT_OK
 * @details         This function could be used to read a receive data frame from an uart module
 */
UartIf_ReturnType UartIf_GetReceivedData(UartIf_RxFrameType *dataFrame)
{
  uint8 dummyCnt;
  UartIf_ReturnType retVal;
  UartIf_Buffer_Frame *pToUartBufferFrame;
  uint8 idx;

  /* Check input data */
  if ((NULL_PTR == dataFrame) || (NULL_PTR == dataFrame->pData))
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_NULL_POINTER);
#endif
    return UARTIF_E_NULL_POINTER;
  }

  /* Check if data is available */
  switch (UartIf_Buffer_GetActualRxBufferLoad(dataFrame->moduleID, &dummyCnt))
  {
  case UARTIF_E_BUFFER_EMPTY:
    return UARTIF_E_BUFFER_EMPTY;
    break;

  case UARTIF_E_BUFFER_FULL:
  case UARTIF_E_OK:
    retVal = UartIf_Buffer_GetRxFrameDataSize(dataFrame->moduleID, &dummyCnt);
    if (UARTIF_E_OK != retVal)
    {
      return retVal;  /* Break processing of error occurred */
    }
    else
    {
      /* Request actual read element from buffer */
      retVal = UartIf_Buffer_GetRxReadFramePointer(dataFrame->moduleID, &pToUartBufferFrame);
      if ( (UARTIF_E_NOT_OK == retVal) || (UARTIF_E_BUFFER_OVERFLOW == retVal) || (UARTIF_E_PENDING == retVal))
      {
        return retVal;  /* Break processing of error occurred */
      }
      /* Copy now received data */
      dataFrame->frameState = pToUartBufferFrame->frameStatus;
      dataFrame->cntOfData = pToUartBufferFrame->frameDataLength;
      for (idx = 0; idx < dummyCnt; idx++)
      {
        dataFrame->pData[idx] = pToUartBufferFrame->pToActualFrameData[idx];
      }
      pToUartBufferFrame->frameStatus = UART_FRAME_INVALID;   /* Set frame status back to invalid */
    }
    return UARTIF_E_OK;
    break;

  case UARTIF_E_BUFFER_OVERFLOW:
    return UARTIF_E_BUFFER_OVERFLOW;
    break;

  default:
    return UARTIF_E_NOT_OK;
    break;

  }
}

/**
* @brief           API function to set a transmitt data frame
* @param[in/out]   UartIf_TxFrameType   Higher layer frame data
* @retval          UartReturnType
*                  >> UARTIF_E_NULL_POINTER
*                  >> UARTIF_E_BUFFER_EMPTY
*                  >> UARTIF_E_BUFFER_OVERFLOW
*                  >> UARTIF_E_OK
*                  >> UARTIF_E_NOT_OK
* @details         This function could be used to set a new transmit data frame for the uart module
*/
UartIf_ReturnType UartIf_SetTransmitData(UartIf_TxFrameType *dataFrame)
{
  uint8 dummyCnt;
  UartIf_ReturnType retVal;
  UartIf_Buffer_Frame *pToUartBufferFrame;
  uint8 idx;

  /* Check input data */
  if ( (NULL_PTR == dataFrame) || (NULL_PTR == dataFrame->pData) )
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_NULL_POINTER);
#endif
    return UARTIF_E_NULL_POINTER;
  }

  /* Check if requested length is grater than tansmitt length*/
  retVal = UartIf_Buffer_GetTxFrameDataSize(dataFrame->moduleID, &dummyCnt);
  if (UARTIF_E_OK != retVal)
  {
    return retVal;  /* Break processing of error occurred */
  }
  if (dataFrame->cntOfData > dummyCnt)
  {
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_INVALID_PARAMETER);
#endif
    return UARTIF_E_INVALID_PARAMETER; /* Frame data length is grater than buffer length >> Invalid Size configuration */
  }

  /* Check if data is available */
  switch (UartIf_Buffer_GetActualTxBufferLoad(dataFrame->moduleID, &dummyCnt))
  {
  case UARTIF_E_BUFFER_FULL:
    return UARTIF_E_BUFFER_FULL;
    break;

  case UARTIF_E_BUFFER_EMPTY:
  case UARTIF_E_OK:
    /* Request actual write element from buffer */
    retVal = UartIf_Buffer_GetTxWriteFramePointer(dataFrame->moduleID, &pToUartBufferFrame);
    if ((UARTIF_E_NOT_OK == retVal) || (UARTIF_E_PENDING == retVal))
    {
      return retVal;  /* Break processing of error occurred */
    }
    /* Copy now transmit data */
    pToUartBufferFrame->frameDataLength = dataFrame->cntOfData;
    for (idx = 0; idx < dataFrame->cntOfData; idx++)
    {
      pToUartBufferFrame->pToActualFrameData[idx] = dataFrame->pData[idx];
    }
    pToUartBufferFrame->frameStatus = UART_FRAME_READY;   /* Set frame status to ready for transmission */
    return UARTIF_E_OK;
    break;

  default:
    return UARTIF_E_NOT_OK;
    break;

  }
}

/* ######################################################################################################################## */
/* #################################### MCAL UART MANDATORY LOW LEVEL INTERFACES ########################################## */
/* ######################################################################################################################## */
/**
* @brief           API function to notify the uart module about an tx interrupt from a specific module
* @param[in]       uint8 module id
* @retval          Std_ReturnType
* @details         This function could be used to notify the uart about an tx interrupt. It would inform the necessary high
*                  level module about the interrupt if this is configured.
* @note            This function has to be called in the Interrupt service routine of the corresponding module.
*/
Std_ReturnType UartIf_TxIsrNotification(uint8 moduleId)
{
  /* Update Frame status */
  pToWorkBufferTxFrames[moduleId]->frameStatus = UART_FRAME_OK;      /* Update Frame Status to pending */

  /* Throw Tx Isr notification */
  if (NULL_PTR != uartIfModuleConfiguration[moduleId].txIsrNotificationFnc)
  {
    uartIfModuleConfiguration[moduleId].txIsrNotificationFnc(moduleId);
  }

  return E_OK;
}

/**
* @brief           API function to notify the uart module about an rx interrupt from a specific module
* @param[in]       UartIf_RxFrameType received frame data
* @retval          Std_ReturnType
* @details         This function could be used to notify the uart about an rx interrupt. It would inform the necessary high
*                  level module about the interrupt if this is configured.
* @note            This function has to be called in the Interrupt service routine of the corresponding module.
*/
Std_ReturnType UartIf_RxIsrNotification(UartIf_RxFrameType frameData)
{
  Std_ReturnType stdRetVal;
  stdRetVal = UartIf_StoreRxElementInUartIfBuffer(frameData);

  /* Throw Tx Isr notification */
  if (NULL_PTR != uartIfModuleConfiguration[frameData.moduleID].rxIsrNotificationFnc)
  {
    uartIfModuleConfiguration[frameData.moduleID].rxIsrNotificationFnc(frameData.moduleID);
  }

  return stdRetVal;
}

/**
* @brief           API function to notify the uart module about an tx event from a specific module
* @param[in]       uint8 module id
* @retval          Std_ReturnType
* @details         This function could be used to notify the uart about an tx event. It would inform the necessary high
*                  level module about the event if this is configured.
*/
Std_ReturnType UartIf_TxNotification(uint8 moduleId)
{
  /* Update Frame status */
  pToWorkBufferTxFrames[moduleId]->frameStatus = UART_FRAME_OK;      /* Update Frame Status to pending */
  txModuleNotificationPollingMode[moduleId] = TRUE;
  return E_OK;
}

/**
* @brief           API function to notify the uart module about an rx event from a specific module
* @param[in]       UartIf_RxFrameType received frame data
* @retval          Std_ReturnType
* @details         This function could be used to notify the uart about an rx event. It would inform the necessary high
*                  level module about the event if this is configured.
*/
Std_ReturnType UartIf_RxNotification(UartIf_RxFrameType frameData)
{
  Std_ReturnType stdRetVal;
  stdRetVal = UartIf_StoreRxElementInUartIfBuffer(frameData);
  rxModuleNotificationPollingMode[frameData.moduleID] = TRUE;

  return stdRetVal;
}

/* ######################################################################################################################## */
/* ############################################ PRIVATE UARTIF FUNCTION ################################################### */
/* ######################################################################################################################## */
/**
 * @brief           Statemachine processing function to process the tx buffer data
 * @param[in]       None
 * @retval          None
 * @details         This function would be call during the run state of the Uart Statemachine. It would check if there is any
 *                  Data to be transmitted
 */
void UartIf_ProcessTxBufferData(void)
{
  uint8 actualModuleId, bufferLoad;
  UartIf_ReturnType retVal;
  Std_ReturnType stdRetVal;
  UartIf_TxFrameType txFrame;
  UartIf_Buffer_Frame dummxBufferFrame;
  boolean breakGeneralProcessing = FALSE;
  boolean breakModuleDataProcessing = FALSE;

  /* Process Tx data for each module ID */
  for (actualModuleId = 0; actualModuleId < UARTIF_CNT_OF_USED_MODULES; actualModuleId++)
  {
    breakModuleDataProcessing = FALSE;
    /* Check system startup */
    if (NULL_PTR == pToWorkBufferTxFrames[actualModuleId])                            /* System startup >> No transition in progress */
    {
      /* Check actual load of the corresponding transmittion buffer */
      retVal = UartIf_Buffer_GetActualTxBufferLoad(actualModuleId, &bufferLoad);
      if ((UARTIF_E_OK == retVal) || (UARTIF_E_BUFFER_FULL == retVal))
      {
        /* Set dummy element */
        dummxBufferFrame.frameStatus = UART_FRAME_OK;
        pToWorkBufferTxFrames[actualModuleId] = &dummxBufferFrame;
      }
      else
      {
        breakGeneralProcessing = TRUE;
      }
    }

    /* Check System startup */
    if (FALSE == breakGeneralProcessing)
    {
      while (FALSE == breakModuleDataProcessing)
      {
        /* Check transmission of last element has been finisched */
        if ((UART_FRAME_OK == pToWorkBufferTxFrames[actualModuleId]->frameStatus) ||      /* Last transmission finished */
          (UART_FRAME_ERROR == pToWorkBufferTxFrames[actualModuleId]->frameStatus))
        {
          /* Check actual load of the corresponding transmittion buffer */
          retVal = UartIf_Buffer_GetActualTxBufferLoad(actualModuleId, &bufferLoad);
          if ((UARTIF_E_OK == retVal) || (UARTIF_E_BUFFER_FULL == retVal))                    /* Contine processing if buffer is full and data is available */
          {
            /* Get next Tx element */
            retVal = UartIf_Buffer_GetTxReadFramePointer(actualModuleId, &pToWorkBufferTxFrames[actualModuleId]);
            if (UARTIF_E_OK == retVal)
            {
              /* Request OK >> continue with processing */
              txFrame.cntOfData = pToWorkBufferTxFrames[actualModuleId]->frameDataLength;
              txFrame.moduleID = actualModuleId;
              txFrame.pData = pToWorkBufferTxFrames[actualModuleId]->pToActualFrameData;
              stdRetVal = Uart_Transmit(txFrame);
              /* Check return value of Mcal module API */
              if (E_NOT_OK == stdRetVal)
              {
                /* Uart has reported a error to the request >> Mark Element as error and continue with next one */
                pToWorkBufferTxFrames[actualModuleId]->frameStatus = UART_FRAME_ERROR;
                /* Increment buffer Pointer to process next request */
                UartIf_Buffer_IncrementTxReadFramePointer(actualModuleId);
                //TODO: Implement error handling?
              }
              else if(E_SKIPPED == stdRetVal)
              {
                /* Uart has skipped the request >> Try next cycle again >> Update Frame Status to UART_FRAME_READY */
                pToWorkBufferTxFrames[actualModuleId]->frameStatus = UART_FRAME_READY;
              }
              else if (E_OK == stdRetVal)
              {
                /* Uart has accepted the request and complete processed >> Update Frame Status to UART_FRAME_OK */
                pToWorkBufferTxFrames[actualModuleId]->frameStatus = UART_FRAME_OK;

                /* Increment buffer Pointer to process next request */
                UartIf_Buffer_IncrementTxReadFramePointer(actualModuleId);
              }
              else
              {
                /* Uart has accepted the request >> Update Frame Status to UART_FRAME_PENDING */
                pToWorkBufferTxFrames[actualModuleId]->frameStatus = UART_FRAME_PENDING;

                /* Increment buffer Pointer to process next request */
                UartIf_Buffer_IncrementTxReadFramePointer(actualModuleId);
              }
            }
            else
            {
              /* Request Failed >> Break processing */
              pToWorkBufferTxFrames[actualModuleId] = NULL_PTR;
            }
          }
          else
          {
            /* Break current module data processing */
            breakModuleDataProcessing = TRUE; /* The module buffer has not data to be transmitted */
          }
        }
        else
        {
          /* Check if frame status is pending */
          if(UART_FRAME_PENDING == pToWorkBufferTxFrames[actualModuleId]->frameStatus)
          {
            /* Break current module data processing */
            breakModuleDataProcessing = TRUE; /* Actual transmission request is still pending >> Contine with nex module transmission */
          }
        }

      } /* END >> while (FALSE == breakModuleDataProcessing) */
    }

    /* Check if Notification have to be thrown */

    if ((NULL_PTR != uartIfModuleConfiguration[actualModuleId].txNotificationFnc) && (TRUE == txModuleNotificationPollingMode[actualModuleId]))
    {
      uartIfModuleConfiguration[actualModuleId].txNotificationFnc(actualModuleId);
      txModuleNotificationPollingMode[actualModuleId] = FALSE;
    }

  }
}

/**
 * @brief           Statemachine processing function to process the rx buffer data
 * @param[in]       None
 * @retval          None
 * @details         This function would be call during the run state of the Uart Statemachine. It would check if there is any
 *                  Data received;
 */
void UartIf_ProcessRxBufferData(void)
{
  uint8 actualModuleId;
  /* Check if Notification have to be thrown */
  for (actualModuleId = 0; actualModuleId < UARTIF_CNT_OF_USED_MODULES; actualModuleId++)
  {
    if ( (NULL_PTR != uartIfModuleConfiguration[actualModuleId].rxNotificationFnc) && (TRUE == rxModuleNotificationPollingMode[actualModuleId]) )
    {
      uartIfModuleConfiguration[actualModuleId].rxNotificationFnc(actualModuleId);
      rxModuleNotificationPollingMode[actualModuleId] = FALSE;
    }
  }
}

/**
* @brief           Helper function to store an rx frame in buffer
* @param[in]       UartIf_RxFrameType frameData
* @retval          Std_ReturnType
*/
Std_ReturnType UartIf_StoreRxElementInUartIfBuffer(UartIf_RxFrameType frameData)
{
  UartIf_Buffer_Frame *pToWorkRxFrames;
  UartIf_ReturnType retVal;
  uint8 maxFrameDataLength;
  uint8 idx;

  /* Check data length */
  retVal = UartIf_Buffer_GetRxFrameDataSize(frameData.moduleID, &maxFrameDataLength);
  if ((UARTIF_E_OK != retVal) || (frameData.cntOfData >= maxFrameDataLength) || (frameData.pData == NULL_PTR))
  {
    /* Break processing of error occurred */
    return E_NOT_OK;              /* Break Processing and skip element */
  }

  /* Request Rx Write frame pointer */
  retVal = UartIf_Buffer_GetRxWriteFramePointer(frameData.moduleID, &pToWorkRxFrames);
  switch (retVal)
  {
  case UARTIF_E_OK:
    /* Continue processing >> Copy Data into buffer element */
    pToWorkRxFrames->frameDataLength = frameData.cntOfData;
    /* Copy Data */
    for (idx = 0; idx < frameData.cntOfData; idx++)
    {
      pToWorkRxFrames->pToActualFrameData[idx] = frameData.pData[idx];
    }
    pToWorkRxFrames->frameStatus = UART_FRAME_OK;
    break;

  case UARTIF_E_NOT_OK:
  case UARTIF_E_BUFFER_FULL:
  case UARTIF_E_PENDING:
    return E_OK;              /* Break Processing and skip element */
    break;

  default:
#if(STD_ON == UARTIF_MODULE_USE_DET)
    Det_ReportError(UARTIF_DET_MODULE_ID, UARTIF_E_GENERIC_PROGRAMMING_FAILURE);
#endif
    return E_OK;              /* Break Processing and skip element */
    break;
  }

  return E_NOT_OK;
}
#define UartIf_STOP_SEC_CODE
#include "UartIf_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of USER function area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

