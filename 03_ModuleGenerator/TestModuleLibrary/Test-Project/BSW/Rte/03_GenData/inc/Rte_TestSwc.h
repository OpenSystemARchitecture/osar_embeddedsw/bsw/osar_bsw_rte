/*****************************************************************************************************************************
 * @file        Rte_TestSwc.h                                                                                                *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        01.01.2021 11:09:54                                                                                          *
 * @brief       Generated Rte Module Interface Header File: Rte_TestSwc.h                                                    *
 * @version     v.1.1.1                                                                                                      *
 * @generator   OSAR Rte Generator v.1.0.0.3                                                                                 *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.9.1                                                                           *
*****************************************************************************************************************************/

#ifndef __RTE_TESTSWC_H
#define __RTE_TESTSWC_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_Interfaces.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of available Server Port Error Information ++++++++++++++++++++++++++++++ */
#define Rte_ErrorType_PpTestServerPort_fnc1_E_OK   100
#define Rte_ErrorType_PpTestServerPort_fnc2_E_OK   100
#define Rte_ErrorType_PpTestServerPort_fnc2_error2 101
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of available Server Port Error Information ++++++++++++++++++++++++++++++ */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++++++++++ Generation of Init Runnable Prototypes ++++++++++++++++++++++++++++++++++++++++ */
VAR(void) TestSwc_Init_Init( VAR(void) );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++++++++++++++ Generation of Cyclic Runnable Prototypes +++++++++++++++++++++++++++++++++++++++ */
VAR(void) TestSwc_Cyclic( VAR(void) );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++++++++++++ Generation of Server Port Runnable Prototypes ++++++++++++++++++++++++++++++++++++ */
VAR(Rte_ErrorType) Rte_Server_PpTestServerPort_fnc1( VAR(uint8) arg1, VAR(sint8) arg2 );

VAR(Rte_ErrorType) Rte_Server_PpTestServerPort_fnc2( VAR(uint16) arg1, VAR(uint32) arg2 );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* ++++++++++++++++++++++++++++++++ Generation of Client Port Runnable Prototype Information ++++++++++++++++++++++++++++++ */
extern VAR(Rte_ErrorType) Rte_Client_PpTestClientPort_fnc1( VAR(uint8) arg1, VAR(sint8) arg2 );

extern VAR(Rte_ErrorType) Rte_Client_PpTestClientPort_fnc2( VAR(uint16) arg1, VAR(uint32) arg2 );

/*------------------------------------------------------------  ------------------------------------------------------------*/
/* +++++++++++++++++++++++++++ Generation of Sender Receiver Port Runnable Prototype Information ++++++++++++++++++++++++++ */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __RTE_TESTSWC_H*/
