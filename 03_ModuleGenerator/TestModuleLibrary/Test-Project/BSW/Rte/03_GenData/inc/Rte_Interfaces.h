/*****************************************************************************************************************************
 * @file        Rte_Interfaces.h                                                                                             *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        01.01.2021 11:09:54                                                                                          *
 * @brief       Generated Rte Interaction Layer Header File: Rte_Interfaces.h                                                *
 * @version     v.1.1.1                                                                                                      *
 * @generator   OSAR Rte Generator v.1.0.0.3                                                                                 *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.9.1                                                                           *
*****************************************************************************************************************************/

#ifndef __RTE_INTERFACES_H
#define __RTE_INTERFACES_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup Rte 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Cfg.h"
#include "Rte_Types.h"
#include "Rte_TestSwc.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Dummy implementation for unconnected Sender Receiver port elements
 */
#define Rte_Receiver_PpTestreceiverPort_element1() Rte_Receiver_PpTestreceiverPort_element1_Var
/**
 * @brief           Dummy implementation for unconnected Sender Receiver port elements
 */
#define Rte_Receiver_PpTestreceiverPort_element2() Rte_Receiver_PpTestreceiverPort_element2_Var
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* Connect Server Port : "Rte_Server_PpTestServerPort" */
/*------------------------------------------------------------  ------------------------------------------------------------*/
/* Connect Sender Port : "Rte_Sender_PpTestSenderPort" */
#define Rte_Sender_PpTestSenderPort_element1( x )  ( Rte_Sender_PpTestSenderPort_element1_Var = x )
#define Rte_Sender_PpTestSenderPort_element2( x )  ( Rte_Sender_PpTestSenderPort_element2_Var = x )
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
extern VAR(uint8) Rte_Sender_PpTestSenderPort_element1_Var;

extern VAR(uint8) Rte_Sender_PpTestSenderPort_element2_Var;

/**
 * @brief           Dummy implementation for unconnected Sender Receiver port elements
 */
extern VAR(uint8) Rte_Receiver_PpTestreceiverPort_element1_Var;

/**
 * @brief           Dummy implementation for unconnected Sender Receiver port elements
 */
extern VAR(uint8) Rte_Receiver_PpTestreceiverPort_element2_Var;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Dummy implementation for unconnected client port function
 */
VAR(Rte_ErrorType) Rte_Client_PpTestClientPort_fnc1( VAR(uint8) arg1, VAR(sint8) arg2 );

/**
 * @brief           Dummy implementation for unconnected client port function
 */
VAR(Rte_ErrorType) Rte_Client_PpTestClientPort_fnc2( VAR(uint16) arg1, VAR(uint32) arg2 );

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __RTE_INTERFACES_H*/
