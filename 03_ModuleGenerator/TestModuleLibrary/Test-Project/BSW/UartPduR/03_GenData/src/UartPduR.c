/*****************************************************************************************************************************
 * @file        UartPduR.c                                                                                                   *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        21.03.2019 10:57:03                                                                                          *
 * @brief       Generated Rte Module Interface file: UartPduR.c                                                              *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup UartPduR 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_UartPduR.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of USER include and definition area           << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "UartPduR.h"

/* Check if E2E Communication is selected and include CRC Algorithm */
#if(UARTPDUR_USE_CRC32ETHERNET_E2E_COMMUNICATION == STD_ON)
#include "Crc.h"
#endif

/**
* @brief           Helper function to seach for the correct PDU configuration
* @param[in]       uint16 pduId
* @param[out]      uint16 index of the requested pdu id
* @retval          UartPduR_ReturnType
*                  > UARTPDUR_E_OK
*                  > UARTPDUR_E_ID_UNKNOWN
*/
UartPduR_ReturnType hlpUartPduRSearchForCorrectPduCfgIndex(uint16 pduId, uint16* pToConfigIndex);

/**
* @brief           Helper function to check of the receifed Frame CRC is valid
* @param[in]       uint8 pData
* @param[out]      uint16 lengthOfData
* @retval          UartPduR_ReturnType
*                  > UARTPDUR_E_OK
*                  > UARTPDUR_E_CRC_FAILURE
* @details         ---------------------------------------------------------
*                  | PduId | Data / lengthOfData | Crc32 | FrameEndPattern |
*                  ---------------------------------------------------------
*                                                    |
*                  -------------------------------   |
*                  |     CRC Protected Data      |<---
*                  -------------------------------
*/
UartPduR_ReturnType hlpUartPduRCheckReceivedCrcData(uint8 *pData, uint16 lengthOfData);
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           End of USER include and definition area            << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map functions into code memory ---------------------------------------------*/
#define UartPduR_START_SEC_CODE
#include "UartPduR_MemMap.h"
/**
 * @brief           Rte generated module initialization function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: System Init
 * @uuid            5456f532f1c4438396edb5c21e165467
 */
VAR(void) UartPduR_Init( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/

  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module cyclic function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: Cyclic 10ms
 * @uuid            dda818bdf76c40649e25e233248fc106
 */
VAR(void) UartPduR_Mainfunction( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  uint8 idx = 0;
  uint16 pduCfgIdx;
  UartIf_ReturnType uartIfReturnValue;
  UartIf_RxFrameType tempRxFrame;
  UartPduR_ReturnType retVal;
  boolean continueProcessing = TRUE;
  uint16 tempPduId;
  uint8 dataBuffer[UARTPDUR_CNT_OF_GLOBAL_MAX_DATA_BYTES + UARTPDUR_CNT_PDU_END_PATTERN + 2];   //Length = dataLeng + pduIdLength + cntOfEndPattern
  tempRxFrame.pData = &dataBuffer[0];
  for (idx = 0; idx < UARTPDUR_CNT_OF_USED_UART_MODULES; idx++)
  {
    /* Request received data frame */
    continueProcessing = TRUE;
    while (continueProcessing == TRUE)
    {
      tempRxFrame.moduleID = listOfUsedUartModules[idx];
      uartIfReturnValue = UartIf_GetReceivedData(&tempRxFrame);
      if ((UARTIF_E_BUFFER_EMPTY == uartIfReturnValue))
      {
        continueProcessing = FALSE;
      }
      else if (uartIfReturnValue != UARTIF_E_OK)
      {
        #if (UARTPDUR_MODULE_USE_DET == STD_ON)
        Det_ReportError(UARTPDUR_DET_MODULE_ID, UARTPDUR_E_GENERIC_PROGRAMMING_FAILURE);
        #endif
      }
      else
      {
        /* Check for uart frame id */
        tempPduId = 0;
        tempPduId |= (((uint16)tempRxFrame.pData[0]) << 8);
        tempPduId |= (tempRxFrame.pData[1]);
        retVal = hlpUartPduRSearchForCorrectPduCfgIndex(tempPduId, &pduCfgIdx);

        /* Do only continue if pdu has been found in config buffer */
        if(UARTPDUR_E_OK == retVal)
        {
          /* Check if function notification pointer is available */
          if (NULL_PTR != uartPduR_PduConfigList[pduCfgIdx].rxNotificationFnc)
          {
            #if(UARTPDUR_USE_CRC32ETHERNET_E2E_COMMUNICATION == STD_ON)
            /* Check Crc if Requested */
            retVal = hlpUartPduRCheckReceivedCrcData(tempRxFrame.pData, tempRxFrame.cntOfData);
            if (retVal == UARTPDUR_E_OK)
            {
              /* CRC is valid so continue Processing */
              /* Call Notification function */
              uartPduR_PduConfigList[pduCfgIdx].rxNotificationFnc(&tempRxFrame.pData[2], (tempRxFrame.cntOfData - UARTPDUR_CNT_PDU_END_PATTERN - 4 - 2));
            }
            #else
            /* Call Notification function */
            uartPduR_PduConfigList[pduCfgIdx].rxNotificationFnc(&tempRxFrame.pData[2], (tempRxFrame.cntOfData - UARTPDUR_CNT_PDU_END_PATTERN - 2));
            #endif
          }
        }
      }
    }
  }
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module Server Port Function.
 * @param[in]       VAR(uint16) uint16
 * @param[in,out]   P2VAR(uint8) uint8
 * @param[in]       VAR(uint16) uint16
 * @retval          VAR(Rte_ErrorType) >> Available Error Types:
 *                  > Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_NullPointer
 *                  > Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_InvalidPduLength
 *                  > Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_UnknownId
 *                  > Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_TransmitBufferOverflow
 *                  > Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_GenericProgrammingFailure
 * @note            Trigger: Operation Invocation
 * @uuid            fe6d9cdddb9142329221c4b5812bff8d
 */
VAR(Rte_ErrorType) Rte_Server_PpGeneralUartPduR_UartPduR_SendUartPdu( VAR(uint16) pduId, P2VAR(uint8) pData, VAR(uint16) dataLength )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  Rte_ErrorType retVal = RTE_E_OK;
  UartPduR_ReturnType fncRetVal;

  fncRetVal = UartPduR_SendUartPdu(pduId, pData, dataLength);

  switch(fncRetVal)
  {
    case UARTPDUR_E_NULL_POINTER:
    retVal = Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_NullPointer;
    break;

    case UARTPDUR_E_INVALID_PDU_LENGTH:
    retVal = Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_InvalidPduLength;
    break;

    case UARTPDUR_E_ID_UNKNOWN:
    retVal = Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_UnknownId;
    break;

    case UARTPDUR_E_TRANSMITBUFFER_OVERFLOW:
    retVal = Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_TransmitBufferOverflow;
    break;

    case UARTPDUR_E_GENERIC_PROGRAMMING_FAILURE:
    retVal = Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_GenericProgrammingFailure;
    break;

    case UARTPDUR_E_OK:
    retVal = RTE_E_OK;
    break;

    default:
    retVal = Rte_ErrorType_PpGeneralUartPduR_UartPduR_SendUartPdu_GenericProgrammingFailure;
    break;
  }

  return retVal;

  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

#define UartPduR_STOP_SEC_CODE
#include "UartPduR_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of USER function area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define UartPduR_START_SEC_CODE
#include "UartPduR_MemMap.h"

/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfuntion.
 */
void UartPduR_InitMemory( void )
{

}


/* ######################################################################################################################## */
/* ################################## UARTPDUR PROVIDED GENERIC HIGH LEVEL INTERFACES ##################################### */
/* ######################################################################################################################## */
/**
* @brief           Module generic transmit function for UART Pdus
* @param[in]       uint16 unique UART PDU ID
* @param[in]       uint8 input pointer to transmitted Data
* @param[in]       uint16 length of data to be transmitted
* @retval          UartIf_ReturnType
*                  > UARTPDUR_E_NULL_POINTER
*                  > UARTPDUR_E_INVALID_PDU_LENGTH
*                  > UARTPDUR_E_ID_UNKNOWN
*                  > UARTPDUR_E_TRANSMITBUFFER_OVERFLOW
*                  > UARTPDUR_E_GENERIC_PROGRAMMING_FAILURE
*                  > UARTPDUR_E_OK
* @details         This function shall note be called from any other application. The equal interface functions would be generated
*/
UartPduR_ReturnType UartPduR_SendUartPdu(uint16 pduId, uint8 *pData, uint16 dataLength)
{
  UartIf_TxFrameType tempTxFrame;
  uint16 cfgIdx, idx, idx2 = 0;
  UartPduR_ReturnType retVal;
  UartIf_ReturnType retValUartIf;
  uint8 dataBuffer[UARTPDUR_CNT_OF_GLOBAL_MAX_DATA_BYTES + UARTPDUR_CNT_PDU_END_PATTERN + 2];   //Length = dataLeng + pduIdLength + cntOfEndPattern

  /* Input Data validation */
  if (NULL_PTR == pData)
  {
#if (UARTPDUR_MODULE_USE_DET == STD_ON)
    Det_ReportError(UARTPDUR_DET_MODULE_ID, UARTPDUR_E_NULL_POINTER);
#endif
    return UARTPDUR_E_NULL_POINTER;
  }
  /* Check Data lenght */
  if ( (0 == dataLength) || (dataLength >= (65535 -2 - UARTPDUR_CNT_PDU_END_PATTERN) ))
  {
#if (UARTPDUR_MODULE_USE_DET == STD_ON)
    Det_ReportError(UARTPDUR_DET_MODULE_ID, UARTPDUR_E_INVALID_PDU_LENGTH);
#endif
    return UARTPDUR_E_INVALID_PDU_LENGTH;
  }
  /* Search for config index */
  retVal = hlpUartPduRSearchForCorrectPduCfgIndex(pduId, &cfgIdx);
  if (retVal == UARTPDUR_E_ID_UNKNOWN)
  {
#if (UARTPDUR_MODULE_USE_DET == STD_ON)
    Det_ReportError(UARTPDUR_DET_MODULE_ID, UARTPDUR_E_ID_UNKNOWN);
#endif
    return UARTPDUR_E_ID_UNKNOWN;
  }

  /* Select Data length with and without CRC */
#if(UARTPDUR_USE_CRC32ETHERNET_E2E_COMMUNICATION == STD_ON)
  tempTxFrame.cntOfData = dataLength + UARTPDUR_CNT_PDU_END_PATTERN + 2 + 4;      //Length = dataLeng + cntOfEndPattern + pduIdLength + CRC32Length
#else
  tempTxFrame.cntOfData = dataLength + UARTPDUR_CNT_PDU_END_PATTERN + 2;      //Length = dataLeng + cntOfEndPattern + pduIdLength + CRC32Length
#endif

  /* Frame Data */
  tempTxFrame.moduleID = uartPduR_PduConfigList[cfgIdx].moduleID;
  tempTxFrame.pData = &dataBuffer[0];

  /* Copy Data */
  dataBuffer[0] = (uint8)(pduId >> 8);
  dataBuffer[1] = (uint8)pduId;
  for (idx = 2; idx < (dataLength + 2); idx++)
  {
    dataBuffer[idx] = pData[idx2++];
  }

  /* Calculate and perform CRC if configured */
#if(UARTPDUR_USE_CRC32ETHERNET_E2E_COMMUNICATION == STD_ON)
  Crc_ReturnType crcRetVal;
  uint32 crcResultData;
  crcRetVal = Crc32_0x04C11DB7_Compute(&dataBuffer[0], (dataLength + 2), &crcResultData);
  if (crcRetVal != CRC_E_OK)
  {
#if (UARTPDUR_MODULE_USE_DET == STD_ON)
    Det_ReportError(UARTPDUR_DET_MODULE_ID, UARTPDUR_E_CRC_FAILURE);
#endif
    return UARTPDUR_E_CRC_FAILURE;
  }

  /* Insert CRC Data */
  dataBuffer[idx++] = (uint8)(crcResultData >> 24);
  dataBuffer[idx++] = (uint8)(crcResultData >> 16);
  dataBuffer[idx++] = (uint8)(crcResultData >> 8);
  dataBuffer[idx++] = (uint8)(crcResultData);
#endif

  for (idx2 = 0; idx2 < UARTPDUR_CNT_PDU_END_PATTERN; idx2++)
  {
    dataBuffer[idx++] = pduEndPattern[idx2];
  }

  retValUartIf = UartIf_SetTransmitData(&tempTxFrame);
  if (UARTIF_E_BUFFER_OVERFLOW == retValUartIf)
  {
#if (UARTPDUR_MODULE_USE_DET == STD_ON)
    Det_ReportError(UARTPDUR_DET_MODULE_ID, UARTPDUR_E_TRANSMITBUFFER_OVERFLOW);
#endif
    return UARTPDUR_E_TRANSMITBUFFER_OVERFLOW;
  }
  if (UARTIF_E_OK != retValUartIf)
  {
#if (UARTPDUR_MODULE_USE_DET == STD_ON)
    Det_ReportError(UARTPDUR_DET_MODULE_ID, UARTPDUR_E_GENERIC_PROGRAMMING_FAILURE);
#endif
    return UARTPDUR_E_GENERIC_PROGRAMMING_FAILURE;
  }

  return UARTPDUR_E_OK;
}

/* ######################################################################################################################## */
/* ############################################## UARTPDUR INTERNAL FUNCTIONS ############################################# */
/* ######################################################################################################################## */

/**
* @brief           Helper function to seach for the correct PDU configuration
* @param[in]       uint16 pduId
* @param[out]      uint16 index of the requested pdu id
* @retval          UartPduR_ReturnType
*                  > UARTPDUR_E_OK
*                  > UARTPDUR_E_ID_UNKNOWN
*/
UartPduR_ReturnType hlpUartPduRSearchForCorrectPduCfgIndex(uint16 pduId, uint16* pToConfigIndex)
{
  uint16 idx = 0;
  for (idx = 0; idx < UARTPDUR_CNT_OF_CONFIGURED_PDUS; idx++)
  {
    if (pduId == uartPduR_PduConfigList[idx].pduId)
    {
      /* Pdu found >> Report Pdu Config Index */
      *pToConfigIndex = idx;
      return UARTPDUR_E_OK;
    }
  }

  return UARTPDUR_E_ID_UNKNOWN;
}

/**
* @brief           Helper function to check of the received Frame CRC is valid
* @param[in]       uint8 pData
* @param[out]      uint16 lengthOfData
* @retval          UartPduR_ReturnType
*                  > UARTPDUR_E_OK
*                  > UARTPDUR_E_CRC_FAILURE
* @details         ---------------------------------------------------------
*                  | PduId | Data / lengthOfData | Crc32 | FrameEndPattern |
*                  ---------------------------------------------------------
*                                                    |
*                  -------------------------------   |
*                  |     CRC Protected Data      |<---
*                  -------------------------------
*/
UartPduR_ReturnType hlpUartPduRCheckReceivedCrcData(uint8 *pData, uint16 lengthOfData)
{
  /* Check if E2E Communication is selected */
#if(UARTPDUR_USE_CRC32ETHERNET_E2E_COMMUNICATION == STD_ON)
  Crc_ReturnType crcRetVal;
  uint32 crcResultData;
  uint32 crcFrameData = 0;
  uint16 lengthOfCrcProtectedData = lengthOfData - 4 - 2; /* Frame Length - Crc Length - Frame End Pattern Length */
  
  /* Calculate CRC of received Frame */
  crcRetVal = Crc32_0x04C11DB7_Compute(pData, lengthOfCrcProtectedData, &crcResultData);
  if (crcRetVal != CRC_E_OK)
  {
#if (UARTPDUR_MODULE_USE_DET == STD_ON)
    Det_ReportError(UARTPDUR_DET_MODULE_ID, UARTPDUR_E_CRC_FAILURE);
#endif
    return UARTPDUR_E_CRC_FAILURE;
  }

  /* Check both CRCs */
  crcFrameData |= (uint32)(pData[lengthOfCrcProtectedData++] << 24);
  crcFrameData |= (uint32)(pData[lengthOfCrcProtectedData++] << 16);
  crcFrameData |= (uint32)(pData[lengthOfCrcProtectedData++] << 8);
  crcFrameData |= (uint32)(pData[lengthOfCrcProtectedData++]);

  if (crcFrameData != crcResultData)
  {
    /* CRC missmatch detected >> Report CRC failure */
    return UARTPDUR_E_CRC_FAILURE;
  }
  else
#endif
  {
    /* CRC correct or not neeted */
    return UARTPDUR_E_OK;
  }
}

#define UartPduR_STOP_SEC_CODE
#include "UartPduR_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of USER function area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

