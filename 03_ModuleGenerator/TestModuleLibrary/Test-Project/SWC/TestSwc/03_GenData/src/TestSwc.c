/*****************************************************************************************************************************
 * @file        TestSwc.c                                                                                                    *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        01.01.2021 11:10:45                                                                                          *
 * @brief       Generated Rte Module Interface file: TestSwc.c                                                               *
 * @version     v.1.1.1                                                                                                      *
 * @generator   OSAR Rte Generator v.1.0.0.4                                                                                 *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.9.1                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_SWC 
 * @{
 */
/**
 * @addtogroup TestSwc 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_TestSwc.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of USER include and definition area           << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           End of USER include and definition area            << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map functions into code memory ---------------------------------------------*/
#define TestSwc_START_SEC_CODE
#include "TestSwc_MemMap.h"
/**
 * @brief           Rte generated module initialization function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: System Init
 * @uuid            629bf17dcc7740c59ea69eab2470ce2e
 */
VAR(void) TestSwc_Init_Init( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/

  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module cyclic function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: Cyclic 100ms
 * @uuid            d8c21d1ab4e0424686710fb850579f9b
 */
VAR(void) TestSwc_Cyclic( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/

  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module Server Port Function.
 * @param[in]       VAR(uint8) uint8
 * @param[in]       VAR(sint8) sint8
 * @retval          VAR(Rte_ErrorType) >> Available Error Types:
 *                  > Rte_ErrorType_PpTestServerPort_fnc1_E_OK
 * @note            Trigger: Operation Invocation
 * @uuid            00e616ed41a24bd48ef81b10bcd0e16c
 */
VAR(Rte_ErrorType) Rte_Server_PpTestServerPort_fnc1( VAR(uint8) arg1, VAR(sint8) arg2 )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/

  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

/**
 * @brief           Rte generated module Server Port Function.
 * @param[in]       VAR(uint16) uint16
 * @param[in]       VAR(uint32) uint32
 * @retval          VAR(Rte_ErrorType) >> Available Error Types:
 *                  > Rte_ErrorType_PpTestServerPort_fnc2_E_OK
 *                  > Rte_ErrorType_PpTestServerPort_fnc2_error2
 * @note            Trigger: Operation Invocation
 * @uuid            ac101bc3e7c54c09ab64212c30333bbb
 */
VAR(Rte_ErrorType) Rte_Server_PpTestServerPort_fnc2( VAR(uint16) arg1, VAR(uint32) arg2 )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/

  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

#define TestSwc_STOP_SEC_CODE
#include "TestSwc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of USER function area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of USER function area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

