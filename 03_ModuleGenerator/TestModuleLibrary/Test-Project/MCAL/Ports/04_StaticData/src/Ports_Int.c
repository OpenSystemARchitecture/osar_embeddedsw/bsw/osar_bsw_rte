/*****************************************************************************************************************************
 * @file        Ports.c                                                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.08.2018 14:30:44                                                                                          *
 * @brief       Implementation of functionalities from the "Ports" module.                                                   *
 *                                                                                                                           *
 * @details     OSAR MCAL implementation of the Ports module. Implements functionalies to configure the MCU Ports ad the     *
 *              abstraction of digital inputs and outputs.                                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Ports
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "Ports.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

extern Ports_PinConfigType port_ActivePinConfiguration[PORT_CNT_OF_CONFIGURED_PINS];

/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Ports_START_SEC_CONST
#include "Ports_MemMap.h"
#define Ports_STOP_SEC_CONST
#include "Ports_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Ports_START_SEC_NOINIT_VAR
#include "Ports_MemMap.h"
#define Ports_STOP_SEC_NOINIT_VAR
#include "Ports_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Ports_START_SEC_INIT_VAR
#include "Ports_MemMap.h"
#define Ports_STOP_SEC_INIT_VAR
#include "Ports_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Ports_START_SEC_ZERO_INIT_VAR
#include "Ports_MemMap.h"
#define Ports_STOP_SEC_ZERO_INIT_VAR
#include "Ports_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define Ports_START_SEC_CONST
#include "Ports_MemMap.h"
#define Ports_STOP_SEC_CONST
#include "Ports_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define Ports_START_SEC_NOINIT_VAR
#include "Ports_MemMap.h"
#define Ports_STOP_SEC_NOINIT_VAR
#include "Ports_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define Ports_START_SEC_INIT_VAR
#include "Ports_MemMap.h"
#define Ports_STOP_SEC_INIT_VAR
#include "Ports_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define Ports_START_SEC_ZERO_INIT_VAR
#include "Ports_MemMap.h"
#define Ports_STOP_SEC_ZERO_INIT_VAR
#include "Ports_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define Ports_START_SEC_CODE
#include "Ports_MemMap.h"
#define Ports_STOP_SEC_CODE
#include "Ports_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------------------------  ------------------------------------------------------------*/
#define Ports_START_SEC_CODE
#include "Ports_MemMap.h"
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfunction.
 */
void Ports_InitMemory( void )
{

}

/**
 * @brief           Set an specific dio state to an given channel id
 * @param[in]       uint16 channel id >> available channel ids would be generated
 * @param[in]       Dio_PinState >> pin state to be set on dio
 * @retval          Ports_ReturnType
 *                  > PORTS_E_OK
 *                  > PORTS_E_NOT_OK
 */
Ports_ReturnType Dio_SetPinState(uint16 channelId, Dio_PinState pinState)
{
  Ports_ReturnType retVal = PORTS_E_OK;

  /* Check input parameter */
  if(PORT_CNT_OF_CONFIGURED_PINS <= channelId)
  {
#if (STD_ON == PORTS_MODULE_USE_DET)
    Det_ReportError(PORTS_DET_MODULE_ID, PORTS_E_INVALID_PARAMETER);
    retVal = PORTS_E_NOT_OK;
#endif
  }
  else
  {

    if( (port_ActivePinConfiguration[channelId].pinOutputMode == GPIO_MODE_ANALOG) ||
        (port_ActivePinConfiguration[channelId].pinOutputMode == GPIO_MODE_AF_PP) ||
        (port_ActivePinConfiguration[channelId].pinOutputMode == GPIO_MODE_AF_OD) )
    {
#if (STD_ON == PORTS_MODULE_USE_DET)
      Det_ReportError(PORTS_DET_MODULE_ID, PORTS_E_INVALID_PARAMETER);
      retVal = PORTS_E_NOT_OK;
#endif
    }
  }

  /* Perform action */
  if(PORTS_E_OK == retVal)
  {
    HAL_GPIO_WritePin(port_ActivePinConfiguration[channelId].portId, port_ActivePinConfiguration[channelId].pinId, pinState);
  }

  return retVal;
}

/**
 * @brief           Get an specific dio state from an given channel id
 * @param[in]       uint16 channel id >> available channel ids would be generated
 * @param[out]      Dio_PinState >> pin state from dio
 * @retval          Ports_ReturnType
 *                  > PORTS_E_OK
 *                  > PORTS_E_NOT_OK
 */
Ports_ReturnType Dio_GetPinState(uint16 channelId, Dio_PinState* pinState)
{
  Ports_ReturnType retVal = PORTS_E_OK;

  /* Check input parameter */
  if( (PORT_CNT_OF_CONFIGURED_PINS <= channelId) || (NULL_PTR == pinState))
  {
#if (STD_ON == PORTS_MODULE_USE_DET)
    Det_ReportError(PORTS_DET_MODULE_ID, PORTS_E_INVALID_PARAMETER);
    retVal = PORTS_E_NOT_OK;
#endif
  }
  else
  {

    if( !(port_ActivePinConfiguration[channelId].pinOutputMode != GPIO_MODE_ANALOG))
    {
#if (STD_ON == PORTS_MODULE_USE_DET)
      Det_ReportError(PORTS_DET_MODULE_ID, PORTS_E_INVALID_PARAMETER);
      retVal = PORTS_E_NOT_OK;
#endif
    }
  }

  /* Perform action */
  if(PORTS_E_OK == retVal)
  {
    *pinState = HAL_GPIO_ReadPin(port_ActivePinConfiguration[channelId].portId, port_ActivePinConfiguration[channelId].pinId);
  }

  return retVal;
}

/**
 * @brief           Toggle the dio state to an given channel id
 * @param[in]       uint16 channel id >> available channel ids would be generated
 * @retval          Ports_ReturnType
 *                  > PORTS_E_OK
 *                  > PORTS_E_NOT_OK
 */
Ports_ReturnType Dio_TogglePinState(uint16 channelId)
{
  Ports_ReturnType retVal = PORTS_E_OK;

  /* Check input parameter */
  if(PORT_CNT_OF_CONFIGURED_PINS <= channelId)
  {
#if (STD_ON == PORTS_MODULE_USE_DET)
    Det_ReportError(PORTS_DET_MODULE_ID, PORTS_E_INVALID_PARAMETER);
    retVal = PORTS_E_NOT_OK;
#endif
  }
  else
  {

    if( (port_ActivePinConfiguration[channelId].pinOutputMode == GPIO_MODE_ANALOG) ||
        (port_ActivePinConfiguration[channelId].pinOutputMode == GPIO_MODE_AF_PP) ||
        (port_ActivePinConfiguration[channelId].pinOutputMode == GPIO_MODE_AF_OD) )
    {
#if (STD_ON == PORTS_MODULE_USE_DET)
      Det_ReportError(PORTS_DET_MODULE_ID, PORTS_E_INVALID_PARAMETER);
      retVal = PORTS_E_NOT_OK;
#endif
    }
  }

  /* Perform action */
  if(PORTS_E_OK == retVal)
  {
    HAL_GPIO_TogglePin(port_ActivePinConfiguration[channelId].portId, port_ActivePinConfiguration[channelId].pinId);
  }

  return retVal;
}

#define Ports_STOP_SEC_CODE
#include "Ports_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */
