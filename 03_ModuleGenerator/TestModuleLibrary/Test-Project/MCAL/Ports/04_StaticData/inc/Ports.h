/*****************************************************************************************************************************
 * @file        Ports.h                                                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        04.08.2018 14:30:44                                                                                          *
 * @brief       Implementation of definitions / interface function prototypes / datatypes and generic module interface       *
 *              informations of the "Ports" module.                                                                          *
 *                                                                                                                           *
 * @details     OSAR MCAL implementation of the Ports module. Implements functionalies to configure the MCU Ports ad the     *
 *              abstraction of digital inputs and outputs.                                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __PORTS_H
#define __PORTS_H
/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Ports
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
#include "Ports_Types.h"
#include "Ports_PBCfg.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfunction.
 */
void Ports_InitMemory( void );

/**
 * @brief           Set an specific dio state to an given channel id
 * @param[in]       uint16 channel id >> available channel ids would be generated
 * @param[in]       Dio_PinState >> pin state to be set on dio
 * @retval          Ports_ReturnType
 *                  > PORTS_E_OK
 *                  > PORTS_E_NOT_OK
 */
Ports_ReturnType Dio_SetPinState(uint16 channelId, Dio_PinState pinState);

/**
 * @brief           Get an specific dio state from an given channel id
 * @param[in]       uint16 channel id >> available channel ids would be generated
 * @param[out]      Dio_PinState >> pin state from dio
 * @retval          Ports_ReturnType
 *                  > PORTS_E_OK
 *                  > PORTS_E_NOT_OK
 */
Ports_ReturnType Dio_GetPinState(uint16 channelId, Dio_PinState* pinState);

/**
 * @brief           Toggle the dio state to an given channel id
 * @param[in]       uint16 channel id >> available channel ids would be generated
 * @retval          Ports_ReturnType
 *                  > PORTS_E_OK
 *                  > PORTS_E_NOT_OK
 */
Ports_ReturnType Dio_TogglePinState(uint16 channelId);

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __PORTS_H*/
