/*****************************************************************************************************************************
 * @file        Adc_Types.h                                                                                                  *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        29.04.2018 08:59:08                                                                                          *
 * @brief       Implementation of module global datatypes from the "Adc" module.                                             *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#ifndef __ADC_TYPES_H
#define __ADC_TYPES_H

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Adc
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
#include "stm32f4xx_hal.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Available application errors of the Adc
 * @details         This enumeration implements all available application return and error values within this specific 
 *                  module. The Application error could be used as DET information or as return type of internal functions. 
 */
typedef enum{
  ADC_E_OK = 0,
  ADC_E_NOT_OK = 1,
  ADC_E_PENDING = 2,
  ADC_E_NOT_IMPLEMENTED = 3,
  ADC_E_GENERIC_PROGRAMMING_FAILURE = 4,
  ADC_E_BUSY = 5,
  ADC_E_NULL_POINTER = 100,
  ADC_E_INVALID_CONFIGURATION = 101,
  ADC_E_HAL_ERROR = 102,
}Adc_ReturnType;

/**
 * @brief           Available application return values of the Adc
 * @details         Redefinition of the Adc_ReturnType as error type.
 */
typedef Adc_ReturnType Adc_ErrorType;

/**
 * @brief           ADC Channel Configuration structure
 */
typedef struct
{
  ADC_HandleTypeDef* moduleHdl;         /* Reference to the ADC module handle object */
  ADC_ChannelConfTypeDef channelCfg;    /* Specific channel configuration parameters */
} Adc_ChannelCfgType;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __ADC_TYPES_H*/
