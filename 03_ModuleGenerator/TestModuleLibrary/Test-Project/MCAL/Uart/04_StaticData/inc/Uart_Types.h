/*****************************************************************************************************************************
 * @file        Uart_Types.h                                                                                                 *
 * @author      OSAR Team Sebastian Reinemuth                                                                                *
 * @date        11.03.2018 10:25:43                                                                                          *
 * @brief       Implementation of module global datatypes from the "Uart" module.                                            *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.0.1.2.15                                                                          *
*****************************************************************************************************************************/
#ifndef __UART_TYPES_H
#define __UART_TYPES_H

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Uart
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Available application errors of the Uart
 * @details         This enumeration implements all available application return and error values within this specific 
 *                  module. The Application error could be used as DET information or as return type of internal functions. 
 */
typedef enum{
  UART_E_OK = 0,
  UART_E_NOT_OK = 1,
  UART_E_PENDING = 2,
  UART_E_NOT_IMPLEMENTED = 3,
  UART_E_GENERIC_PROGRAMMING_FAILURE = 4,
  UART_E_TIMEOUT = 100,
  UART_E_UNEXPECTED_HAL_MODULE_STATE = 101,
  UART_E_NULL_POINTER = 102,
  UART_E_INVALID_PARAMETER = 103
}Uart_ReturnType;

/**
 * @brief           Available application return values of the Uart
 * @details         Redefinition of the Uart_ReturnType as error type.
 */
typedef Uart_ReturnType Uart_ErrorType;

/* ######################################################################################################################## */
/* ################################################## Uart HW data types ################################################## */
/* ######################################################################################################################## */
/**
 * @brief           Possible Uart Parity types
 */
typedef enum {
  MCAL_UART_PARITY_NONE = 0,
  MCAL_UART_PARITY_EVEN,
  MCAL_UART_PARITY_ODD
}Uart_ParityType;

/**
 * @brief           Possible Uart modes
 */
typedef enum {
  MCAL_UART_MODE_TX = 0,
  MCAL_UART_MODE_RX,
  MCAL_UART_MODE_TXRX
}Uart_ModeType;

/**
* @brief           Uart transmission type
*/
typedef enum {
  MCAL_UART_TRANSMISSION_POLLING = 0,             /*!< Used transmission type == polling */
  MCAL_UART_TRANSMISSION_INTERRUPT                /*!< Used transmission type == interrupt */
}Uart_TransmissionType;

/**
* @brief           Uart Stop Bits
*/
typedef enum {
  MCAL_UART_STOPBIT_1 = 0,                        /*!< Use 1 stop bit  */
  MCAL_UART_STOPBIT_1_5,                          /*!< Use 1.5 stop bits  */
  MCAL_UART_STOPBIT_2                             /*!< Use 2 stop bits  */
}Uart_StopBitsType;

/**
* @brief           Uart Hardware flow control type
*/
typedef enum {
  MCAL_UART_HWCONTROL_NONE = 0,                   /*!< No hardware flow control*/
  MCAL_UART_HWCONTROL_RTS,                        /*!< Hardware flow control using RTS */
  MCAL_UART_HWCONTROL_CTS,                        /*!< Hardware flow control using CTS */
  MCAL_UART_HWCONTROL_RTS_CTS                     /*!< Hardware flow control using RTS and CTS */
}Uart_HwFlowControlType;

/**
 * @brief           Uart hardware configuration type
 */
typedef struct {
  uint8  uartModuleId;                            /*!< Id of the UART HW Module which shall be used */
  uint8  uartDataWidth;                           /*!< Width an Uart Word >> Could be 8 or 9 */
  uint32 uartBaudRate;                            /*!< Baudrate which shall be used during the uart transmission */
  uint32 uartTxTimeout;                           /*!< Used timeout of a polling tx transmission in ms */
  uint32 uartRxTimeout;                           /*!< Used timeout of a polling rx transmission in ms */
  uint16 uartMaxRxBytes;                          /*!< Maximum rx bytes which could be received in one rx transmission */
  Uart_StopBitsType  uartStopBits;                /*!< Cnt of start bits for the uart transmission >> Could be 1 or 2 */
  Uart_HwFlowControlType uartHwFlowControl;       /*!< Uart hardware flow control */
  Uart_ParityType uartParity;                     /*!< Parity which shall be used during the uart transmission */
  Uart_ModeType uartMode;                         /*!< Transmission mode which shall be used during the uart transmission */
  Uart_TransmissionType uartTransmissionType;     /*!< Which transmission type shall be used. */
}Uart_HwConfigType;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __UART_TYPES_H*/
